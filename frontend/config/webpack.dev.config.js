const config = require('./webpack.config.js');

config.output.filename = config.output.filename.replace('.min', '');
config.devtool = 'source-map';
config.devServer = {
  proxy: {
    '/api/*': {
      target: 'http://p4welo.usermd.net/',
      changeOrigin: true,
      secure: false
    }
  },
  port: 3002
};

module.exports = config;
