const config = require('./webpack.config.js');

config.output.filename = config.output.filename.replace('.min', '');

module.exports = config;