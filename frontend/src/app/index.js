import angular from 'angular';
import ngTouch from 'angular-touch';
import ngAnimate from 'angular-animate';
import ngTranslate from 'angular-translate';
import ngRecaptcha from 'angular-recaptcha';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import ngSocketIo from 'angular-socket-io';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import uiMask from 'angular-ui-mask';

import authentication from './authentication';
import common from './common';
import config from './config';
import main from './main';
import store from './store';
import utils from './utils';

const app = angular.module('app', [
  ngTouch,
  ngAnimate,
  ngTranslate,
  ngResource,
  ngSanitize,
  ngRecaptcha,
  uiBootstrap,
  uiMask,
  uiRouter,
  common,
  config,
  authentication,
  main,
  utils,
  store,
  'btford.socket-io'
])
    .run(($ngRedux, $log, $rootScope) => {
      'ngInject';
      $ngRedux.subscribe(() => {
        $rootScope.$evalAsync(() => {
          $log.log('[DEVTOOLS]: ', $ngRedux.getState());
        });
      });
    })
    .name;

angular.bootstrap(document, [app]);
