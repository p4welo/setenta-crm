import angular from 'angular';
import actions from './actions';
import actionTypes from './action-types';
import middleware from './middleware';
import reducers from './reducers';

const reduxStore = angular.module('reduxStore', [
  actions,
  actionTypes,
  middleware,
  reducers
])
    .name;

export default reduxStore;
