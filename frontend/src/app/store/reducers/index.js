import angular from 'angular';
import authenticationReducer from './authenticationReducer';

const reduxReducers = angular.module('reduxReducers', [])

    .factory('authenticationReducer', authenticationReducer)
    .name;

export default reduxReducers;
