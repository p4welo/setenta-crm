import {pipe, filter, append, map} from 'ramda';

export default (AUTHENTICATION_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    clientId: ''
  };

  return (state = DEFAULT_STATE, {type, payload}) => {
    switch (type) {
      default:
        return state;
    }
  };
}