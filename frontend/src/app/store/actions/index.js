import angular from 'angular';
import authenticationActions from './authenticationActions';

const reduxActions = angular.module('reduxActions', [])
    .service('authenticationActions', authenticationActions)
    .name;

export default reduxActions;
