export default (
    notificationService,
    COURSE_ACTION_TYPES,
    ADVERTISEMENT_ACTION_TYPES,
    USER_TYPE_ACTION_TYPES,
    USER_ACTION_TYPES,
    COMPETITION_ACTION_TYPES
) => {
  'ngInject';

  return store => next => action => {
    switch (action.type) {
      case COURSE_ACTION_TYPES.GET_COURSES_ERROR:
      case COURSE_ACTION_TYPES.DELETE_COURSE_ERROR:
      case COURSE_ACTION_TYPES.UPDATE_COURSE_ERROR:
      case COURSE_ACTION_TYPES.CREATE_COURSE_ERROR:
      case USER_TYPE_ACTION_TYPES.GET_USER_TYPES_ERROR:
      case USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_ERROR:
      case USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_ERROR:
      case USER_ACTION_TYPES.GET_USERS_ERROR:
      case USER_ACTION_TYPES.CREATE_USER_ERROR:
      case USER_ACTION_TYPES.DELETE_USER_ERROR: {
        notificationService.error('Wystąpił błąd');
        break;
      }
      case COURSE_ACTION_TYPES.DELETE_COURSE_SUCCESS:
      case USER_ACTION_TYPES.DELETE_USER_SUCCESS:
      case USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_SUCCESS: {
        notificationService.success('Pomyślnie usunięto');
        break;
      }
      case COURSE_ACTION_TYPES.CREATE_COURSE_SUCCESS:
      case USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_SUCCESS:
      case USER_ACTION_TYPES.CREATE_USER_SUCCESS:
      case COMPETITION_ACTION_TYPES.CREATE_COMPETITION_SUCCESS:
      case ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_SUCCESS: {
        notificationService.success('Pomyślnie dodano');
        break;
      }
      case COURSE_ACTION_TYPES.UPDATE_COURSE_SUCCESS:
      case ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_SUCCESS:
      case USER_ACTION_TYPES.UPDATE_USER_SUCCESS: {
        notificationService.success('Pomyślnie zmieniono');
        break;
      }
    }

    next(action);
  }
};