import angular from 'angular';
import notificationMiddleware from './notificationMiddleware';

const middleware = angular.module('middleware', [])
    .factory('notificationMiddleware', notificationMiddleware)
    .name;

export default middleware;
