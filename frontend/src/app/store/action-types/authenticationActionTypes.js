const moduleName = 'Authentication';
export default {
  LOGIN_START:    `[${moduleName}] Login started.`,
  LOGIN_SUCCESS:  `[${moduleName}] Login succeeded.`,
  LOGIN_ERROR:    `[${moduleName}] Login error.`,
  SET_CLIENT_ID:  `[${moduleName}] ClientId set.`,
}