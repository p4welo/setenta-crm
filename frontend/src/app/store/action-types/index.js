import angular from 'angular';
import authenticationActionTypes from './authenticationActionTypes';
import courseActionTypes from '../../main/course/redux/courseActionTypes';

const reduxActionTypes = angular.module('reduxActionTypes', [])
    .constant('COURSE_ACTION_TYPES', courseActionTypes)
    .constant('AUTHENTICATION_ACTION_TYPES', authenticationActionTypes)
    .name;

export default reduxActionTypes;
