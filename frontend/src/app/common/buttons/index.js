import angular from 'angular';
import ladda from './ladda';

const buttons = angular.module('buttons', [ladda])
    .name;

export default buttons;
