export default laddaProvider => {
  'ngInject';
  laddaProvider.setOption({ /* optional */
    style: 'slide-left'
  });
}
