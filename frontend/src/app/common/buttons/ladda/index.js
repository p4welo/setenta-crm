import angular from 'angular';
import 'ladda/dist/spin.min';
import 'ladda';
import ngLadda from 'angular-ladda';
import laddaConfig from './laddaConfig';

const ladda = angular.module('ladda', [ngLadda])
    .config(laddaConfig)
    .name;

export default ladda;
