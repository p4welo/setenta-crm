import angular from 'angular';
import 'chart.js';
import chartJs from 'angular-chart.js';
import chartConfig from './chartConfig';

const common = angular.module('chart', [
  chartJs
])
    .config(chartConfig)
    .name;

export default common;
