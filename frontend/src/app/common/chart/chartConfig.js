export default ChartJsProvider => {
  'ngInject';

  ChartJsProvider.setOptions({
    colors: [
      '#ff0000',
      '#DCDCDC',
      '#00ADF9',
      '#803690',
      '#46BFBD',
      '#FDB45C',
      '#949FB1',
      '#4D5360'
    ]
  });
}
