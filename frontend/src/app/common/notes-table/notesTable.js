import template from './notesTable.html';

export default {
  template: template,
  bindings: {
    notes: '<',
    onAddClick: '&'
  },
  controller() {

    this.$onInit = () => {
    };
  }
};
