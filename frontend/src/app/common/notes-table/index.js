import angular from 'angular';
import notesTable from './notesTable';

const buttons = angular.module('notesTable', [])
    .component('notesTable', notesTable)
    .name;

export default buttons;
