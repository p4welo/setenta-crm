import angular from 'angular';
import viewHeader from './viewHeader';

export default angular.module('viewHeader', [])
    .component('viewHeader', viewHeader)
    .name;
