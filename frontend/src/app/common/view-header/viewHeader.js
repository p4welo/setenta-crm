import template from './viewHeader.html';

export default {
  template: template,
  bindings: {
    icon: '@',
    titleKey: '@',
    subtitleKey: '@',
    backTo: '@'
  },
  controller() {
    'ngInject';
  }
};