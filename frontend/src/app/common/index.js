import angular from 'angular';
import navbar from './navbar';
import sidebar from './sidebar';
import buttons from './buttons';
import sockets from './sockets';
import addNote from './add-note';
import notesTable from './notes-table';
import notification from './notification';
import viewHeader from './view-header';
import chart from './chart';

const common = angular.module('common', [
  addNote,
  buttons,
  chart,
  navbar,
  notesTable,
  notification,
  sockets,
  sidebar,
  viewHeader
])
    .name;

export default common;
