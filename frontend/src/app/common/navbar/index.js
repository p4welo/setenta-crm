import angular from 'angular';
import navbarComponent from './navbar';

const navbar = angular.module('navbar', [])
    .component('navbar', navbarComponent)
    .name;

export default navbar;
