import './navbar.less';
import template from './navbar.html';

export default {
  template: template,
  bindings: {
    onMobileTogglerClick: '&',
    onTogglerClick: '&',
    onAddEntranceClick: '&'
  },
  controller(authenticationService, $location) {
    'ngInject';

    this.logout = () => authenticationService.logout()
        .then(() => $location.path('/'));

    this.$onInit = () => {
      this.currentUser = authenticationService.getCurrentUser();
    }
  }
};
