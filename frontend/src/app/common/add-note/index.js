import angular from 'angular';
import addNote from './addNote';

const buttons = angular.module('addNote', [])
    .component('addNoteWindow', addNote)
    .name;

export default buttons;
