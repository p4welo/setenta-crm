import template from './addNote.html';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller() {

    this.$onInit = () => {
      this.note = {
        value: ''
      }
    };

    this.$onDestroy = () => {
    };

    this.ok = () => {
      this.close({$value: this.note});
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};
