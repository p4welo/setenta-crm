import angular from 'angular';
import notificationService from './notificationService';

const sidebar = angular.module('notification', [])
    .service('notificationService', notificationService)
    .name;

export default sidebar;
