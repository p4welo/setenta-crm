import io from 'socket.io-client';

class SocketService {
  constructor(authenticationService) {
    'ngInject';
    this.authenticationService = authenticationService;
  }

  open() {
    this.socket = io('http://localhost:8080/?uid=crm');
  }

  close() {
    this.socket.disconnect();
  }

  on($event, callback) {
    this.socket.on($event, callback);
  }
}
export default SocketService;
