import angular from 'angular';
import socketService from './socketService';

const sockets = angular.module('sockets', [])
    .service('socketService', socketService)
    .name;

export default sockets;
