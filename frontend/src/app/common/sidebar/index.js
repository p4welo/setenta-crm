import angular from 'angular';
import sidebarComponent from './sidebar';

const sidebar = angular.module('sidebar', [])
    .component('sidebar', sidebarComponent)
    .name;

export default sidebar;
