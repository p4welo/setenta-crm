import './sidebar.less';
import template from './sidebar.html';

export default {
  template: template,
  controller($state, authenticationService) {
    'ngInject';
    Object.assign(this, {
      $state
    });

    this.$onInit = () => {
      this.isAdmin = authenticationService.getCurrentUser() === 'p4welo@gmail.com';
    }

    this.user = authenticationService.getCurrentUser();
  }
};
