import angular from 'angular';
import activation from './activation/activation';
import registration from './registration/registration';
import login from './login/login';
import authenticationService from './authenticationService';

export default angular.module('authentication', [])
    .component('activation', activation)
    .component('registration', registration)
    .component('login', login)
    .service('authenticationService', authenticationService)
    .name;
