import './login.less';
import template from './login.html';

export default {
  template: template,
  controller($location, $log, authenticationService) {
    'ngInject';

    this.login = () => {
      this.logging = true;
      this.error = false;
      authenticationService.login(this.credentials)
          .then(
              (response) => {
                console.log(response);
                $location.path('/')
              },
              (response) => {
                if (response.status === 401) {
                  this.error = true;
                }
                else {
                  //TODO: handle error notification
                  $log.error(response);
                }
              }
          )
          .finally(() => {
            this.logging = false;
          });
    }
  }
};
