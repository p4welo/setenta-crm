class RegistrationService {
  constructor($http, API_ROUTES) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES
    });
  }

  register({email, password}) {
    return this.$http.post(this.API_ROUTES.AUTH.REGISTER(), {email, password})
  }
}
export default RegistrationService;
