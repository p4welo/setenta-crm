import './registration.less';
import template from './registration.html';

export default {
  template: template,
  controller(authenticationService) {
    'ngInject';

    this.register = () => {
      this.registering = true;
      authenticationService.register(this.credentials)
          .then((result) => {
            delete this.registering;
          });
    }
  }
};
