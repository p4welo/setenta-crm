class AuthenticationService {
  constructor($http, $rootScope, $q, $cookieStore, $window, base64Service, API_ROUTES) {
    'ngInject';
    Object.assign(this, {
      $http,
      $rootScope,
      $cookieStore,
      $window,
      $q,
      base64Service,
      API_ROUTES
    });
  }

  login(credentials) {
    const deferred = this.$q.defer();
    this.clearCredentials();
    credentials.type = 'CRM';

    this.$http.post(this.API_ROUTES.AUTH.LOGIN(), credentials).then(
        response => {
          this.setToken(response.data);
          this.getUser();
          deferred.resolve(response);
        },
        response => deferred.reject(response)
    );
    return deferred.promise;
  }

  getCurrentUser() {
    const globals = this.$rootScope.globals;
    if (globals) {
      return globals.email;
    }
  }

  getClientId() {
    const globals = this.$rootScope.globals;
    if (globals) {
      return globals.client;
    }
  }

  logout() {
    const deferred = this.$q.defer();
    this.clearCredentials();
    deferred.resolve();
    return deferred.promise;
  }

  setToken({token, user}) {
    const {email, client} = user;
    this.$rootScope.globals = {token, email, client};
    this.$window.sessionStorage._ap_token = token;
    this.$window.sessionStorage._ap_email = email;
    this.$window.sessionStorage._ap_client = client;
    this.$http.defaults.headers.common.Authorization = 'JWT ' + token;
  }

  getUser() {
    return this.$http.get(this.API_ROUTES.AUTH.USER());
  }

  clearCredentials() {
    this.$cookieStore.remove('globals');
    delete this.$window.sessionStorage._ap_token;
    delete this.$window.sessionStorage._ap_email;
    delete this.$window.sessionStorage._ap_client;
    this.$rootScope.globals = {};
    delete this.$http.defaults.headers.common.Authorization;
  }

  register({email, password}) {
    return this.$http.post(this.API_ROUTES.AUTH.REGISTER(), {email, password})
  }

  activate(activationId) {
    return this.$http.get(this.API_ROUTES.AUTH.ACTIVATE(activationId));
  }
}
export default AuthenticationService;
