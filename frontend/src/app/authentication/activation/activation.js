import template from './activation.html';

export default {
  template: template,
  bindings: {
    activationId: '<'
  },
  controller(authenticationService) {
    'ngInject';

    this.$onInit = () => {
      this.result = 0;
      authenticationService.activate(this.activationId)
          .then(() => {
            this.result = 2;
          })
          .catch(() => {
            this.result = 1;
          })
    }
  }
};
