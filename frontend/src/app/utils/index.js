import angular from 'angular';
import base64 from './base64';
import ngReally from './ng-really';
import eventEmitter from './event-emitter';
import reduxUtils from './redux-utils';

const common = angular.module('utils', [base64, ngReally, eventEmitter, reduxUtils])
    .name;

export default common;
