import angular from 'angular';
import base64Service from './base64Service';

const common = angular.module('base64', [])
    .service('base64Service', base64Service)
    .name;

export default common;