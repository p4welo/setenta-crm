class NgReally {
  constructor() {
    this.restrict = 'A';
  }

  link(scope, element, attrs) {
    element.bind('click', function() {
      var message = attrs.ngReallyMessage;
      if (message && confirm(message)) {
        scope.$apply(attrs.ngReallyClick);
      }
    });
  }
}

export default NgReally;
