import angular from 'angular';
import ngReallyDirective from './ngReallyDirective';

const common = angular.module('ngReally', [])
    .directive('ngReallyClick', () => new ngReallyDirective())
    .name;

export default common;
