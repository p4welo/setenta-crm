import angular from 'angular';
import modelQuery from './modelQuery';

const reduxUtils = angular.module('reduxUtils', [])
    .service('modelQuery', modelQuery)
    .name;

export default reduxUtils;
