import R from 'ramda';

class ModelQuery {


  byIdentity(identity) {
    return function (collection) {
      return R.pathOr([], [identity], collection);
    };
  }

  /**
   * returns booking model as long array for the purpose of price calculation
   *
   * @param {Object} booking model slice of a state
   * @returns {Array} jouned list of all objects
   */
  joinAll(state) {
    return Object
        .keys(state)
        .reduce((list, key) => list.concat(state[key]), [])
        .filter(R.pipe(
            R.path([]),
            R.has('model')
        ));
  }

  join(part) {
    return function (collection) {
      return collection.concat(part);
    };
  }

  /**
   * returns the complement collection
   *
   * subtract the source from collection if
   * the collection key is the superset of source key
   * and
   * the collection model is superset of source model
   *
   * @param {Array} accepts array
   * @returns {Function(Object[]):Object[]}
   */
  complement(collection) {
    return function (source) {
      return collection.filter(c => {
        const predModel = R.whereEq(R.__, c.model);
        const predKey = R.whereEq(R.__, c.key);
        return !source.some(s => predModel(s.model) && predKey(s.key));
      });
    };
  }

  /**
   * returns complement collection
   *
   * subtract the collection from source if
   * the collection key is the subset of source key
   * and
   * the collection model is subset of source model
   *
   * if the properties of key/model in source element match the collection element
   *
   * @param {Object[]}
   * @returns {Fucntion(Object[]):Object[]}
   */
  subtractWeak(collection) {
    return function (source) {
      return source.filter(s => {
        const predModel = R.whereEq(s.model);
        const predKey = R.whereEq(s.key);
        return !collection.some(c => predModel(c.model) && predKey(c.key));
      });
    };
  }

  /**
   * returns complement collection
   *
   * subtract the collection from source if
   * the collection key is the superset of source key
   * and
   * the collection model is superset of source model
   *
   * if all the properties of key/model in collection el match source element
   *
   * @param {Object[]}
   * @returns {Fucntion(Object[]):Object[]}
   */
  subtract(collection) {
    return function (source) {
      return source.filter(s => {
        const predModel = R.whereEq(R.__, s.model);
        const predKey = R.whereEq(R.__, s.key);
        return !collection.some(c => predModel(c.model) && predKey(c.key));
      });
    };
  }

  /**
   * returns new boxed array of objects filtered by containing path and value
   *
   * @param {String[]|String}
   * @param {Any}
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byKeyValue(path, value, inv) {
    return function (source) {
      return source.filter(el => (R.path(path, el) === value) ^ inv);
    };
  }

  /**
   * returns new boxed array of objects filtered by containing path and property
   *
   * @param {String[]|String}
   * @param {Any}
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byHasProperty(path, prop) {
    return R.filter(el => R.has(prop, R.path(path, el)));
  }

  /**
   * returns function to filter the collection by comparing the search object
   * to el.model.
   *
   * @param {Object}
   * @param {Boolean} if true it will use return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byModel(searchObj, inv) {
    return R.filter(function (el) {
      return R.whereEq(searchObj)(el.model) ^ inv;
    });
  }

  /**
   * checks if collection has any of the matching properties on the model
   *
   * @sig Predicate -> collection -> Boolean
   *
   * @param {Object}
   * @param {Boolean} if true it will use return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  hasAnyModel(searchObj, inv) {
    return R.any(function (el) {
      return R.whereEq(searchObj)(el.model) ^ inv;
    });
  }

  /**
   * returns function to filter the collection by applying predicate
   * to el.model.
   *
   * @param {Object:Ramda.whereEq}
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byPredicate(pred, inv) {
    return function (source) {
      return source.filter(el => pred(el.model) ^ inv);
    };
  }

  /**
   * returns new array of objects filtered by given key
   * match is found when key is a subset of collection entry key
   *
   * @sig Object:SearchCriteria -> collection -> filteredCollection
   *
   * @param {Object}
   * @returns {Function(Object[]):Object[]}
   */
  byKey(searchObj) {
    return function (source) {
      return source.filter(el => R.whereEq(searchObj)(el.key));
    };
  }

  /**
   * returns function to filter collection by hasAny relationship with source collection
   *
   * @param {Object[]} collection to be matched against
   * @param {Fucntion:Function[]?} optional array of filters applied on set FIXME might not need collection
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byHasAny(collection, inv) {
    return function (source) {
      return source.filter(s => inv ^ collection.some(c => c.key[s.identity] === s.key.id));
    };
  }

  /**
   * returns new boxed array of objects filtered by relationship
   * belongs to any of given set - has surrogate key referencing any of the given set
   * e.g. get bags for checked in passengers... (or return journey)
   *
   * @param {Object[]} collection to be matched against
   * @param {Fucntion:Function[]?} optional array of filters applied on set FIXME might not need collection
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byBelongsToAny(collection, inv) {
    return function (source) {
      return source.filter(s => inv ^ collection.some(c => c.key.id === s.key[c.identity]));
    };
  }

  /**
   * FIXME suggest better naming?
   * it is composition of belongsToAny if the given set is filtered by byHasAny of some other set
   * basically looking for the matching surrogate key in two sets
   *
   * returns the new collection of the object associates with the given collection through the given keys
   * collection targets the objects with surrogate key
   *
   * @param {Object} slice of an object to match,
   * if key is {a: 1, b: 2, c: 3}
   * then {a: 1, c: 3} will match
   * @param {Object[]} associated keys
   * @param {Boolean?} true will return complementary collection
   * @returns {Function(Object[]):Object[]}
   */
  byAssociation(association, set, inv) {
    return R.filter(c => inv ^ set.some(s => association.every(a => c.key[a] === s.key[a])));
  }

  /**
   * returns new boxed array of objects filtered by local flag indicating wether the
   * object was added, removed or in any way diverted from remote counterpart
   *
   * @param {Boolean}
   * @returns {Function(Object[]):Object[]}
   */
  byLocal(flag) {
    return function (source) {
      return source.filter(el => !!el.local === flag);
    };
  }

  /**
   * returns new boxed array of objects filtered by removed flag indicating wether the
   * object was locally removed
   *
   * @param {Boolean}
   * @returns {Function(Object[]):Object[]}
   */
  byRemoved(flag) {
    return function (collection) {
      return collection.filter(el => (flag ? flag === el.removed : !el.removed));
    };
  }

  /**
   * maps objects found in given collection
   * e.g. replaces references with the original objects
   *
   * @param {Array}
   * @param {String[]} path to compared value in mapped collection
   * @param {String[]} path to compared value in mapping collection
   * @returns {Function(Object[]):Object[]}
   */
  mapByUuid(col) {
    return function (collection) {
      return collection.map(m => col
          .find(c => R.path(['key', 'uuid'], m) === R.path(['uuid'], c)));
    };
  }

  /**
   * filters the source collection by comparator collection by key
   * in a way that source collection key is a superset to comparator collection key
   *
   * @param {Object[]} comparator collection, need to contain key as object
   * @returns {Function(Object[]):Object[]}
   */
  bySameKey(set) {
    return function (collection) {
      return collection.filter(c => {
        const pred = R.whereEq(c.key);
        return set.some(s => pred(s.key));
      });
    };
  }

  /**
   * filters the source collection by comparator collection by model
   * in a way that source collection model is a superset to comparator collection model
   *
   * @param {Object[]} comparator collection
   * @returns {Function(Object[]):Object[]}
   */
  bySameModel(set) {
    return function (collection) {
      return collection.filter(c => {
        const pred = R.whereEq(c.model);
        return set.some(s => pred(s.model));
      });
    };
  }

  /**
   * checks if collection is not empty
   * @returns {Boolean}
   */
  hasAny(collection) {
    return !!collection.length;
  }

  /**
   * checks if an element is a member of a collection
   * check is done by deep check for key and model
   *
   * @param {Object[]}
   *
   * returns {Function(Object):Boolean}
   */
  isMemberOf(collection) {
    return function (element) {
      const predModel = R.whereEq(element.model);
      const predKey = R.whereEq(element.key);
      return collection.some(c => predModel(c.model) && predKey(c.key));
    };
  }

  headSlice(collection) {
    return R.pathOr([], [0], collection);
  }

  byKeyValueAny(path, values) {
    return R.filter(
        R.pipe(
            R.path(path),
            R.contains(R.__, values)
        ));
  }

  /**
   * Split the store items if the quantity is bigger than 1
   * @param {array} acc - accumulator
   * @param {object} el - Store object
   * @return {array} Array of splitted elements by quantity
   */
  inflateQuantities(acc, el) {
    let mappedElement;
    const qty = R.pathOr(1, ['model', 'qty'], el);
    if (qty > 1) {
      mappedElement = R.reduce(result => result.concat({
        ...el,
        model: {
          ...el.model,
          total: el.model.total / qty,
          amt: el.model.amt / qty,
          qty: 1
        }
      }), [], Array(qty));
    } else {
      mappedElement = el;
    }
    return acc.concat(mappedElement);
  }

  /**
   * @name findByModel
   * @description find first item in the collection which satisfies the query given through modelQuery parameter
   * Use it when you want to search for only one specific element in the collection of models
   * @param {Object} modelQuery - object with set of poperties which should be satisfied on the returned item
   * @returns {Object} single item from the collection which satisfies the query
   */
  findByModel(modelQuery) {
    return R.find((item) => R.whereEq(modelQuery, item.model));
  }

  /**
   @name findByKey
   * @description find first item in the collection which satisfies the query given through keyQuery parameter
   * Use it when you want to search for only one specific element in the collection of models
   * @param {Object} keyQuery - object with set of poperties which should be satisfied on the returned item
   * @returns {Object} single item from the collection which satisfies the query
   */
  findByKey(keyQurey) {
    return R.find((item) => R.whereEq(keyQurey, item.key));
  }

  /**
   * FIXME - remove or rename to byBelongToAnyPassenger
   *
   * @name byBelongToAny
   * @description filter passed collection by ownership. Returns array of items which belongs to passengers
   * passed as function argument.
   * @param {Array} passengers - array of passengers which are used to filter passed list
   * @returns {function} filter function which returns elements which belongs to any of passengers
   */
  byBelongToAny(passengers) {
    const passengersNumbers = passengers.map(R.path(['key', 'id']));
    return R.filter((item) => R.contains(item.key.passengers, passengersNumbers));
  }
}

export default ModelQuery;