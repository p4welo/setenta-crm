import './main.less';
import template from './main.html';

export default {
  template: template,
  controller($uibModal, $rootScope) {
    'ngInject';

    this.toggleSidebar = () => (this.sidebarXs = !this.sidebarXs);
    this.toggleSidebarOnMobile = () => (this.sidebarVisibleOnMobile = !this.sidebarVisibleOnMobile);

    this.createEntrance = () => {
      $uibModal.open({
        component: 'createEntrance'
      }).result.then(() => {
      });
    };

    this.$onInit = () => {
      this.sidebarXs = false;
      this.sidebarVisibleOnMobile = false;
      $rootScope.$on('$stateChangeStart', () => {
        this.sidebarVisibleOnMobile = false;
      });
    }
  }
};
