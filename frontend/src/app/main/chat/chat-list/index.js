import angular from 'angular';
import chatList from './chatList';

export default angular.module('chatList', [])
    .component('chatList', chatList)
    .name;
