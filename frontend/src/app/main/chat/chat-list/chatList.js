import template from './chatList.html';

export default {
  template: template,
  bindings: {
  },
  controller(chatService) {
    'ngInject';


    this.$onInit = () => {
      chatService.connect();
    };

    this.$onDestroy = () => {
      chatService.disconnect();
    }
  }
};