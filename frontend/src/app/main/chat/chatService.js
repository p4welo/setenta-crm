class ChatService {
  constructor($http, API_ROUTES, socketService) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES,
      socketService
    });
  }

  connect() {
    this.socketService.open();
  }

  disconnect() {
    this.socketService.close();
  }
}
export default ChatService;
