import angular from 'angular';
import chatList from './chat-list';
import chatService from './chatService';

export default angular.module('chat', [
      chatList
    ])
    .service('chatService', chatService)
    .name;
