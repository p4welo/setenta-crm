import template from './advertisementDetails.html';

export default {
  template: template,
  bindings: {
    advertisementId: '<'
  },
  controller($ngRedux, $uibModal, advertisementActions, advertisementService) {
    'ngInject';

    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, advertisementActions)(this);
      $ngRedux.dispatch(advertisementActions.list());
      this.states = advertisementService.getStates();
    };

    this.$onDestroy = () => this.unsubscribe();

    this.addAdvertisementNote = () => {
      $uibModal.open({
        component: 'addNoteWindow',
        resolve: {
        }
      }).result.then((note) => {
        $ngRedux.dispatch(advertisementActions.addNote(this.advertisement._id, note));
      });
    };

    this.setState = (state) => {
      $ngRedux.dispatch(advertisementActions.update(this.advertisement._id, {state}));
    };

    const mapStateToThis = ({advertisementModel}) => {
      if (advertisementModel) {
        return {
          advertisement: advertisementModel.advertisements.filter((advertisement) => advertisement._id === this.advertisementId)[0],
          isUpdateLoading: advertisementModel.isUpdateLoading
        };
      }
      return {};
    };
  }
};
