import angular from 'angular';
import components from './components';
import advertisementDetails from './advertisementDetails';

export default angular.module('advertisementDetails', [
  components
])
    .component('advertisementDetails', advertisementDetails)
    .name;
