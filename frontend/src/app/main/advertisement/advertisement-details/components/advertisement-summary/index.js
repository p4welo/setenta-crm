import angular from 'angular';
import advertisementSummary from './advertisementSummary';

export default angular.module('advertisementSummary', [])
    .component('advertisementSummary', advertisementSummary)
    .name;
