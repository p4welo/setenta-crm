import template from './advertisementSummary.html';

export default {
  template: template,
  bindings: {
    advertisement: '<',
    states: '<',
    isUpdateInProgress: '<',
    onSetStateClick: '&'
  },
  controller(eventEmitter) {
    'ngInject';

    this.setState = (state) => {
      this.onSetStateClick(eventEmitter(state.name));
    }
  }
};
