import angular from 'angular';
import advertisementSummary from './advertisement-summary';

export default angular.module('advertisementDetailsComponents', [
  advertisementSummary
])
    .name;
