class AdvertisementService {
  constructor($http, API_ROUTES, authenticationService) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES,
      authenticationService
    });
  }

  getStates() {
    return [
      {name: 'STILL_LOOKING'},
      {name: 'OUTDATED'},
      {name: 'SUSPENDED'},
      {name: 'FOUND'}
    ];
  }
}

export default AdvertisementService;
