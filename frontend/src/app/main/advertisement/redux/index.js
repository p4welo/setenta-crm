import angular from 'angular';

import advertisementActionTypes from './advertisementActionTypes';
import advertisementReducer from './advertisementReducer';
import advertisementActions from './advertisementActions';

export default angular.module('advertisementRedux', [
])
    .constant('ADVERTISEMENT_ACTION_TYPES', advertisementActionTypes)
    .factory('advertisementReducer', advertisementReducer)
    .service('advertisementActions', advertisementActions)

    .run(($ngRedux, advertisementReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          advertisementModel: advertisementReducer
        });
      }
    })
    .name;
