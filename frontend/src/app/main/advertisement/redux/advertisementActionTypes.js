const moduleName = 'Advertisements';
export default {
  GET_ADVERTISEMENTS_START:    `[${moduleName}] Get advertisements started.`,
  GET_ADVERTISEMENTS_SUCCESS:  `[${moduleName}] Get advertisements succeeded.`,
  GET_ADVERTISEMENTS_ERROR:    `[${moduleName}] Get advertisements error.`,

  GET_ADVERTISEMENT_START:    `[${moduleName}] Get advertisement started.`,
  GET_ADVERTISEMENT_SUCCESS:  `[${moduleName}] Get advertisement succeeded.`,
  GET_ADVERTISEMENT_ERROR:    `[${moduleName}] Get advertisement error.`,

  DELETE_ADVERTISEMENT_START:     `[${moduleName}] Delete advertisement started.`,
  DELETE_ADVERTISEMENT_SUCCESS:   `[${moduleName}] Delete advertisement succeeded.`,
  DELETE_ADVERTISEMENT_ERROR:     `[${moduleName}] Delete advertisement error.`,

  UPDATE_ADVERTISEMENT_START:     `[${moduleName}] Update advertisement started.`,
  UPDATE_ADVERTISEMENT_SUCCESS:   `[${moduleName}] Update advertisement succeeded.`,
  UPDATE_ADVERTISEMENT_ERROR:     `[${moduleName}] Update advertisement error.`,

  ADD_NOTE_START:     `[${moduleName}] Add note started.`,
  ADD_NOTE_SUCCESS:   `[${moduleName}] Add note succeeded.`,
  ADD_NOTE_ERROR:     `[${moduleName}] Add note error.`,
}