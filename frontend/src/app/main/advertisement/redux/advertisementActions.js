export default ($http, API_ROUTES, authenticationService, ADVERTISEMENT_ACTION_TYPES, notificationService) => {
  'ngInject';

  return {
    list() {
      return (dispatch, getState) => {
        dispatch({
          type: ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_START
        });

        const clientId = authenticationService.getClientId();
        return $http.get(API_ROUTES.ADVERTISEMENT.LIST(clientId))
            .then(({ data }) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_ERROR,
              payload
            }));
      }
    },

    addNote(advertisementId, note) {
      return (dispatch, getState) => {
        dispatch({
          type: ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_START
        });

        const clientId = authenticationService.getClientId();
        return $http.put(API_ROUTES.ADVERTISEMENT.ADD_NOTE(clientId, advertisementId), note)
            .then(({ data }) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_ERROR,
              payload
            }));
      }
    },

    update(advertisementId, properties) {
      return (dispatch, getState) => {
        dispatch({
          type: ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_START
        });

        const clientId = authenticationService.getClientId();
        return $http.put(API_ROUTES.ADVERTISEMENT.UPDATE(clientId, advertisementId), properties)
            .then(({ data }) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_ERROR,
              payload
            }));
      }
    }

    // delete(course) {
    //   return (dispatch, getState) => {
    //     dispatch({
    //       type: COURSE_ACTION_TYPES.DELETE_ADVERTISEMENT_START
    //     });
    //
    //     const clientId = authenticationService.getClientId();
    //     return $http.delete(API_ROUTES.ADVERTISEMENT.DELETE(clientId, course._id))
    //         .then(() => dispatch({
    //           type: COURSE_ACTION_TYPES.DELETE_ADVERTISEMENT_SUCCESS,
    //           payload: course
    //         }))
    //         .catch((payload) => dispatch({
    //           type: COURSE_ACTION_TYPES.DELETE_ADVERTISEMENT_ERROR,
    //           payload
    //         }));
    //   }
    // },
    //
    // update(courseId, properties) {
    //   return (dispatch, getState) => {
    //     dispatch({
    //       type: COURSE_ACTION_TYPES.UPDATE_ADVERTISEMENT_START
    //     });
    //
    //     const clientId = authenticationService.getClientId();
    //     return $http.put(API_ROUTES.COURSE.UPDATE(clientId, courseId), properties)
    //         .then(({data}) => dispatch({
    //           type: COURSE_ACTION_TYPES.UPDATE_ADVERTISEMENT_SUCCESS,
    //           payload: data
    //         }))
    //         .catch((payload) => dispatch({
    //           type: COURSE_ACTION_TYPES.UPDATE_ADVERTISEMENT_ERROR,
    //           payload
    //         }));
    //   }
    // },
    //
    // get(courseId) {
    //   return (dispatch, getState) => {
    //     dispatch({
    //       type: COURSE_ACTION_TYPES.GET_ADVERTISEMENT_START
    //     });
    //
    //     const clientId = authenticationService.getClientId();
    //     return $http.get(API_ROUTES.COURSE.GET(clientId, courseId))
    //         .then(({data}) => dispatch({
    //           type: COURSE_ACTION_TYPES.GET_ADVERTISEMENT_SUCCESS,
    //           payload: data
    //         }))
    //         .catch((payload) => dispatch({
    //           type: COURSE_ACTION_TYPES.GET_ADVERTISEMENT_ERROR,
    //           payload
    //         }));
    //   }
    // }
  }
};
