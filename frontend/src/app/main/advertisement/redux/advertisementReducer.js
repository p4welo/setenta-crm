import {pipe, filter, append, map} from 'ramda';

export default (ADVERTISEMENT_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    advertisements: []
  };

  return (state = DEFAULT_STATE, {type, payload}) => {
    switch (type) {
      case ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_START:
      case ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_START: {
        return {
          ...state,
          isUpdateLoading: true
        };
      }

      case ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_ERROR:
      case ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_ERROR:
      case ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case ADVERTISEMENT_ACTION_TYPES.GET_ADVERTISEMENTS_SUCCESS: {
        return {
          ...state,
          advertisements: payload,
          isLoading: false
        };
      }

      case ADVERTISEMENT_ACTION_TYPES.UPDATE_ADVERTISEMENT_SUCCESS:
      case ADVERTISEMENT_ACTION_TYPES.ADD_NOTE_SUCCESS: {
        const advertisements = pipe(
            modelQuery.byIdentity('advertisements'),
            map((advertisement) => {
              if (advertisement._id === payload._id) {
                return {
                  ...advertisement,
                  ...payload
                };
              }
              return advertisement;
            })
        )(state);

        return {
          ...state,
          advertisements,
          isLoading: false,
          isUpdateLoading: false
        }
      }
      default:
        return state;
    }
  };
}