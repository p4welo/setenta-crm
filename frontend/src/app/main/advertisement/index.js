import angular from 'angular';
import advertisementList from './advertisement-list';
import advertisementDetails from './advertisement-details';
import advertisementService from './advertisementService';
import redux from './redux';

export default angular.module('advertisements', [
  advertisementList,
  advertisementDetails,
  redux
])
    .service('advertisementService', advertisementService)
    .name;
