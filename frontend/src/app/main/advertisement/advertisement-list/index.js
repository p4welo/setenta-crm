import angular from 'angular';
import components from './components';
import advertisementList from './advertisementList';

export default angular.module('advertisementList', [components])
    .component('advertisementList', advertisementList)
    .name;
