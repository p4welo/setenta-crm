import angular from 'angular';
import advertisementTable from './advertisementTable';

export default angular.module('advertisementTable', [])
    .component('advertisementTable', advertisementTable)
    .name;
