import template from './advertisementTable.html';

export default {
  template: template,
  bindings: {
    advertisements: '<',
    onAddNoteClick: '&',
    onDetailsClick: '&'
  },
  controller(eventEmitter) {
    'ngInject';

    this.$onInit = () => {
    };

    this.goToDetails = (advertisement) => this.onDetailsClick(eventEmitter(advertisement));

    this.addNote = (advertisement) => this.onAddNoteClick(eventEmitter(advertisement));
  }
};
