import angular from 'angular';
import advertisementTable from './advertisement-table';

export default angular.module('advertisementListComponents', [
  advertisementTable
])
    .name;
