import './advertisementList.less';
import template from './advertisementList.html';

export default {
  template: template,
  controller($ngRedux, $uibModal, $state, advertisementActions) {
    'ngInject';

    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, advertisementActions)(this);
      $ngRedux.dispatch(advertisementActions.list());
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.addAdvertisementNote = (advertisement) => {
      $uibModal.open({
        component: 'addNoteWindow',
        resolve: {
        }
      }).result.then((note) => {
        $ngRedux.dispatch(advertisementActions.addNote(advertisement._id, note));
      });
    };

    this.goToDetails = (advertisement) => $state.go('main.advertisement.details', {id: advertisement._id});

    const mapStateToThis = ({advertisementModel}) => {
      if (advertisementModel) {
        return {
          advertisements: advertisementModel.advertisements,
          isLoading: advertisementModel.isLoading
        };
      }
      return {};
    };
  }
};
