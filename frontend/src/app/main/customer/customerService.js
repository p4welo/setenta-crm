class CustomerService {
  constructor($http, API_ROUTES) {
    'ngInject';
    this.$http = $http;
    this.API_ROUTES = API_ROUTES;
  }

  findByCode(code) {
    return this.$http.get(this.API_ROUTES.CUSTOMER.FIND_BY_CODE + code);
  }
}

export default CustomerService;
