import template from './newCustomer.html';

export default {
  template: template,
  controller($stateParams) {
    'ngInject';

    this.$onInit = () => {
      this.params = $stateParams;
    }
  }
};
