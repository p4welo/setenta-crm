import angular from 'angular';
import createEntrance from './create-entrance/createEntrance';
import newCustomer from './new-customer/newCustomer';
import customerService from './customerService';

export default angular.module('customer', [])
    .component('createEntrance', createEntrance)
    .component('newCustomer', newCustomer)
    .service('customerService', customerService)
    .name;
