import template from './createEntrance.html';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller($state, customerService) {
    'ngInject';

    const resetWarning = () => this.showNotFoundWarning = false;

    this.$onInit = () => {
      resetWarning();
    };

    this.searchByCode = () => {
      resetWarning();
      this.searchingByCode = true;
      customerService.findByCode(this.cardCode).then((response) => {
        this.searchingByCode = false;
        const customer = response.data.customer;
        if (!customer) {
          this.showNotFoundWarning = true;
        }
      })
    };

    this.reset = () => {
      resetWarning();
    };

    this.createCustomer = () => {
      $state.go('main.newCustomer', {code: this.cardCode});
      this.close();
    };

    this.ok = () => {
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};
