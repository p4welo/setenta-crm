class CourseService {
  constructor($http, API_ROUTES, authenticationService) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES,
      authenticationService
    });
  }

  // create(course) {
  //   const clientId = this.authenticationService.getClientId();
  //   return this.$http.post(this.API_ROUTES.COURSE.CREATE(clientId), course);
  // }
  //
  // update(courseId, properties) {
  //   const clientId = this.authenticationService.getClientId();
  //   return this.$http.put(this.API_ROUTES.COURSE.UPDATE(clientId, courseId), properties);
  // }

  updateList(courseIds, properties) {
    const clientId = this.authenticationService.getClientId();
    properties.ids = courseIds;
    return this.$http.put(this.API_ROUTES.COURSE.UPDATE_LIST(clientId), properties);
  }

  // get(courseId) {
  //   const clientId = this.authenticationService.getClientId();
  //   return this.$http.get(this.API_ROUTES.COURSE.GET(clientId, courseId));
  // }
  //
  // list() {
  //   const clientId = this.authenticationService.getClientId();
  //   return this.$http.get(this.API_ROUTES.COURSE.LIST(clientId));
  // }

  registrationList(courseId) {
    const clientId = this.authenticationService.getClientId();
    return this.$http.get(this.API_ROUTES.COURSE.REGISTRATION_LIST(clientId, courseId));
  }

  remove(course) {
    const clientId = this.authenticationService.getClientId();
    return this.$http.delete(this.API_ROUTES.COURSE.DELETE(clientId, course._id));
  }

  register(courseId, customer) {
    const clientId = this.authenticationService.getClientId();
    return this.$http.post(this.API_ROUTES.COURSE.REGISTER(clientId, courseId), customer);
  }

  getDays() {
    return [
      {id: 0},
      {id: 1},
      {id: 2},
      {id: 3},
      {id: 4},
      {id: 5},
      {id: 6}
    ]
  }

  getStates() {
    return [
      {name: 'REGISTRATION', icon: 'icon-pencil7'},
      {name: 'IN_PROGRESS', icon: 'icon-play3'},
      {name: 'ON_HOLD', icon: 'icon-pause'},
      {name: 'CLOSED', icon: 'icon-stop'}
    ];
  }

  getLevels() {
    return [
      {name: 'BEG'},
      {name: 'INT'},
      {name: 'ADV'}
    ]
  }

  getInitialCourse(day = 0) {
    function getTime(hours, minutes) {
      const result = new Date();
      result.setHours(hours);
      result.setMinutes(minutes);
      return result;
    }

    const startTime = getTime(17, 0);
    const endTime = getTime(18, 0);
    const clientId = this.authenticationService.getClientId();
    return {
      day,
      startTime,
      endTime,
      clientId
    };
  }
}
export default CourseService;
