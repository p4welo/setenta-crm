import angular from 'angular';

import courseDetails from './course-details';
import courseList from './course-list';
import redux from './redux';

import courseService from './courseService';

export default angular.module('course', [
  courseList,
  courseDetails,
  redux
])
    .service('courseService', courseService)
    .name;
