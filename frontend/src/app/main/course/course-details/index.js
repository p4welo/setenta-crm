import angular from 'angular';
import components from './components';
import courseDetails from './courseDetails';

export default angular.module('courseDetails', [
    components
])
    .component('courseDetails', courseDetails)
    .name;
