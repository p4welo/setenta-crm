import template from './newCourseDetails.html';

export default {
  template: template,
  bindings: {
    courseId: '<'
  },
  controller($ngRedux, courseActions, courseService) {
    'ngInject';

    this.$onInit = () => {
      this.levels = courseService.getLevels();
      this.states = courseService.getStates();
      this.days = courseService.getDays();
      this.unsubscribe = $ngRedux.connect(mapStateToThis, courseActions)(this);
      $ngRedux.dispatch(courseActions.list());
      $ngRedux.dispatch(courseActions.registrations(this.courseId));
    };

    this.$onDestroy = () => this.unsubscribe();

    this.updateCourse = (course) => {
      $ngRedux.dispatch(courseActions.update(this.courseId, course));
    };

    const mapStateToThis = ({courseModel}) => {
      if (courseModel) {
        return {
          course: courseModel.courses.filter((course) => course._id === this.courseId)[0],
          registrationList: courseModel.registrations,
          isLoading: courseModel.isLoading,
          isUpdateLoading: courseModel.isUpdateLoading,
          isRegistrationsLoading: courseModel.isRegistrationsLoading
        };
      }
      return {};
    };


  }
};