import angular from 'angular';
import courseRegistrations from './courseRegistrations';

export default angular.module('courseRegistrations', [])
    .component('courseRegistrations', courseRegistrations)
    .name;
