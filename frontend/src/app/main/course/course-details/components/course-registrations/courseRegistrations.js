import template from './courseRegistrations.html';
import './courseRegistrations.less';

export default {
  template: template,
  bindings: {
    registrations: '<',
    isLoading: '<'
  },
  controller(eventEmitter) {
    'ngInject';

  }
};