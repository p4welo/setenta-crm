import template from './courseProperties.html';

export default {
  template: template,
  bindings: {
    course: '<',
    levels: '<',
    states: '<',
    days: '<',
    isUpdateInProgress: '<',
    onUpdateClick: '&'
  },
  controller(eventEmitter) {
    'ngInject';

    this.$onChanges = ({isUpdateInProgress}) => {
      if (isUpdateInProgress && isUpdateInProgress.previousValue && !isUpdateInProgress.currentValue) {
        this.loading = false;
        this.isEditMode = false;
      }
    };

    this.editCourse = () => {
      this.courseCopy = Object.assign({}, this.course);
      this.isEditMode = true;
    };

    this.cancel = () => {
      this.isEditMode = false;
    };

    this.update = () => {
      this.loading = true;
      this.onUpdateClick(eventEmitter(this.courseCopy));
    }
  }
};