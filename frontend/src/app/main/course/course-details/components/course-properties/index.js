import angular from 'angular';
import courseProperties from './courseProperties';

export default angular.module('courseProperties', [])
    .component('courseProperties', courseProperties)
    .name;
