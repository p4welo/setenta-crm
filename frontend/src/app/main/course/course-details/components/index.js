import angular from 'angular';
import courseProperties from './course-properties';
import courseRegistrations from './course-registrations';

export default angular.module('courseDetailsComponents', [
  courseProperties,
  courseRegistrations
])
    .name;
