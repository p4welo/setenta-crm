import angular from 'angular';
import components from './components';
import courseList from './courseList';

export default angular.module('courseList', [components])
    .component('courseList', courseList)
    .name;
