import template from './courseList.html';

export default {
  template: template,
  controller($ngRedux, $uibModal, $state, courseService, courseActions) {
    'ngInject';

    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, courseActions)(this);
      $ngRedux.dispatch(courseActions.list());
      this.days = courseService.getDays();
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.filterByDay = (day) => $ngRedux.dispatch(courseActions.setDayFilter(day));

    this.goToDetails = (course) => $state.go('main.course.details', {id: course._id});

    this.delete = (course) => $ngRedux.dispatch(courseActions.delete(course));

    this.toggleIsPublic = (course) => $ngRedux.dispatch(courseActions.update(course._id, {isPublic: !course.isPublic}));

    this.toggleIsRegistrationOpen = (course) => $ngRedux.dispatch(courseActions.update(course._id, {isRegistrationOpen: !course.isRegistrationOpen}));


    this.add = (day) => {
      $uibModal.open({
        component: 'addCourse',
        resolve: {
          day: () => day
        }
      }).result.then((course) => {
        $ngRedux.dispatch(courseActions.create(course));
      });
    };

    const mapStateToThis = ({courseModel}) => {
      if (courseModel) {
        return {
          courses: courseModel.courses,
          day: courseModel.day,
          isLoading: courseModel.isLoading
        };
      }
      return {};
    };
  }
};