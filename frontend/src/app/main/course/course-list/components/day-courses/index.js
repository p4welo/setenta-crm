import angular from 'angular';
import dayCourses from './dayCourses';

export default angular.module('dayCourses', [])
    .component('dayCourses', dayCourses)
    .name;
