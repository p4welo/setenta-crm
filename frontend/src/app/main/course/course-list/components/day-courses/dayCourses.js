import template from './dayCourses.html';

export default {
  template: template,
  bindings: {
    day: '<',
    courses: '<',
    onDetailsClick: '&',
    onAddClick: '&',
    onDeleteClick: '&',
    onToggleRegistrationClick: '&',
    onTogglePublicClick: '&'
  },
  controller(eventEmitter) {
    'ngInject';

    this.goToDetails = (course) => this.onDetailsClick(eventEmitter(course));

    this.addNew = () => this.onAddClick(eventEmitter(this.day));

    this.delete = (course) => this.onDeleteClick(eventEmitter(course));

    this.toggleIsPublic = (course) => this.onTogglePublicClick(eventEmitter(course));

    this.toggleIsRegistrationOpen = (course) => this.onToggleRegistrationClick(eventEmitter(course));
  }
};