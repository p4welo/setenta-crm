import angular from 'angular';
import addCourse from './add-course';
import dayFilter from './day-filter';
import dayCourses from './day-courses';

export default angular.module('courseListComponents', [
  addCourse,
  dayFilter,
  dayCourses
])
    .name;
