import template from './dayFilter.html';

export default {
  template: template,
  bindings: {
    currentDay: '<',
    days: '<',
    onFilterChange: '&'
  },
  controller(eventEmitter) {
    'ngInject';

    this.setFilter = (dayId) => {
      this.onFilterChange(eventEmitter(dayId));
    };
  }
};