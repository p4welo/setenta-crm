import angular from 'angular';
import dayFilter from './dayFilter';

export default angular.module('dayFilter', [])
    .component('dayFilter', dayFilter)
    .name;
