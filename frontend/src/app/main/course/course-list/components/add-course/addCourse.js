import './addCourse.less';
import template from './addCourse.html';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller($filter, courseService) {
    const dateFilter = $filter('date');

    this.$onInit = () => {
      this.course = courseService.getInitialCourse(this.resolve.day.id);
      this.days = courseService.getDays();
      this.states = courseService.getStates();
      this.levels = courseService.getLevels();
    };

    this.$onDestroy = () => {
      delete this.course;
      delete this.days;
    };

    this.ok = () => {
      this.course.timeFrom = dateFilter(this.course.startTime, 'HH:mm');
      this.course.timeTo = dateFilter(this.course.endTime, 'HH:mm');
      delete this.course.startTime;
      delete this.course.endTime;
      this.close({$value: this.course});
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};
