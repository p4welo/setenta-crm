import angular from 'angular';
import addCourse from './addCourse';

export default angular.module('addCourse', [])
    .component('addCourse', addCourse)
    .name;
