import angular from 'angular';

import courseActionTypes from './courseActionTypes';
import courseReducer from './courseReducer';
import courseActions from './courseActions';

export default angular.module('courseRedux', [
])
    .constant('COURSE_ACTION_TYPES', courseActionTypes)
    .factory('courseReducer', courseReducer)
    .service('courseActions', courseActions)

    .run(($ngRedux, courseReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          courseModel: courseReducer
        });
      }
    })
    .name;
