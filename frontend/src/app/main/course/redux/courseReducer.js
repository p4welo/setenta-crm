import {pipe, filter, append, map} from 'ramda';

export default (COURSE_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    courses: []
  };

  return (state = DEFAULT_STATE, {type, payload}) => {
    switch (type) {
      case COURSE_ACTION_TYPES.GET_COURSES_START:
      case COURSE_ACTION_TYPES.CREATE_COURSE_START:
      case COURSE_ACTION_TYPES.DELETE_COURSE_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case COURSE_ACTION_TYPES.UPDATE_COURSE_START: {
        return {
          ...state,
          isUpdateLoading: true
        };
      }

      case COURSE_ACTION_TYPES.GET_REGISTRATIONS_START: {
        return {
          ...state,
          isRegistrationsLoading: true
        };
      }

      case COURSE_ACTION_TYPES.GET_COURSES_ERROR:
      case COURSE_ACTION_TYPES.DELETE_COURSE_ERROR:
      case COURSE_ACTION_TYPES.UPDATE_COURSE_ERROR:
      case COURSE_ACTION_TYPES.GET_REGISTRATIONS_ERROR:
      case COURSE_ACTION_TYPES.CREATE_COURSE_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case COURSE_ACTION_TYPES.GET_COURSES_SUCCESS: {
        return {
          ...state,
          courses: payload,
          isLoading: false
        };
      }

      case COURSE_ACTION_TYPES.GET_REGISTRATIONS_SUCCESS: {
        console.log(payload);
        return {
          ...state,
          registrations: payload,
          isRegistrationsLoading: false,
          isUpdateLoading: false
        };
      }

      case COURSE_ACTION_TYPES.DELETE_COURSE_SUCCESS: {
        const courses = pipe(
            modelQuery.byIdentity('courses'),
            filter((course) => course._id !== payload._id)
        )(state);

        return {
          ...state,
          courses,
          isLoading: false
        }
      }

      case COURSE_ACTION_TYPES.CREATE_COURSE_SUCCESS: {
        const courses = pipe(
            modelQuery.byIdentity('courses'),
            append(payload)
        )(state);

        return {
          ...state,
          courses,
          isLoading: false
        }
      }

      case COURSE_ACTION_TYPES.UPDATE_COURSE_SUCCESS: {
        const courses = pipe(
            modelQuery.byIdentity('courses'),
            map((course) => {
              if (course._id === payload._id) {
                return {
                  ...course,
                  ...payload
                };
              }
              return course;
            })
        )(state);

        return {
          ...state,
          courses,
          isLoading: false,
          isUpdateLoading: false
        }
      }

      case COURSE_ACTION_TYPES.DAY_FILTER_CHANGED: {
        return {
          ...state,
          day: payload,
          isLoading: false
        }
      }
      default:
        return state;
    }
  };
}