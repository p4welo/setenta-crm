const moduleName = 'Courses';
export default {
  GET_COURSES_START:    `[${moduleName}] Get courses started.`,
  GET_COURSES_SUCCESS:  `[${moduleName}] Get courses succeeded.`,
  GET_COURSES_ERROR:    `[${moduleName}] Get courses error.`,

  GET_REGISTRATIONS_START:    `[${moduleName}] Get registrations started.`,
  GET_REGISTRATIONS_SUCCESS:  `[${moduleName}] Get registrations succeeded.`,
  GET_REGISTRATIONS_ERROR:    `[${moduleName}] Get registrations error.`,

  DAY_FILTER_CHANGED:   `[${moduleName}] Day filter changed.`,

  CREATE_COURSE_START:     `[${moduleName}] Create course started.`,
  CREATE_COURSE_SUCCESS:   `[${moduleName}] Create course succeeded.`,
  CREATE_COURSE_ERROR:     `[${moduleName}] Create course error.`,

  DELETE_COURSE_START:     `[${moduleName}] Delete course started.`,
  DELETE_COURSE_SUCCESS:   `[${moduleName}] Delete course succeeded.`,
  DELETE_COURSE_ERROR:     `[${moduleName}] Delete course error.`,

  UPDATE_COURSE_START:     `[${moduleName}] Update course started.`,
  UPDATE_COURSE_SUCCESS:   `[${moduleName}] Update course succeeded.`,
  UPDATE_COURSE_ERROR:     `[${moduleName}] Update course error.`,
}