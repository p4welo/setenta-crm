export default ($http, API_ROUTES, authenticationService, COURSE_ACTION_TYPES, notificationService) => {
  'ngInject';

  return {
    list() {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.GET_COURSES_START
        });

        const clientId = authenticationService.getClientId();
        return $http.get(API_ROUTES.COURSE.LIST(clientId))
            .then(({data}) => dispatch({
              type: COURSE_ACTION_TYPES.GET_COURSES_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COURSE_ACTION_TYPES.GET_COURSES_ERROR,
              payload
            }));
      }
    },

    delete(course) {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.DELETE_COURSE_START
        });

        const clientId = authenticationService.getClientId();
        return $http.delete(API_ROUTES.COURSE.DELETE(clientId, course._id))
            .then(() => dispatch({
              type: COURSE_ACTION_TYPES.DELETE_COURSE_SUCCESS,
              payload: course
            }))
            .catch((payload) => dispatch({
              type: COURSE_ACTION_TYPES.DELETE_COURSE_ERROR,
              payload
            }));
      }
    },

    setDayFilter(day) {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.DAY_FILTER_CHANGED,
          payload: day
        });
      }
    },

    create(course) {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.CREATE_COURSE_START
        });

        const clientId = authenticationService.getClientId();
        return $http.post(API_ROUTES.COURSE.CREATE(clientId), course)
            .then(({data}) => dispatch({
              type: COURSE_ACTION_TYPES.CREATE_COURSE_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COURSE_ACTION_TYPES.CREATE_COURSE_ERROR,
              payload
            }));
      }
    },

    update(courseId, properties) {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.UPDATE_COURSE_START
        });

        const clientId = authenticationService.getClientId();
        return $http.put(API_ROUTES.COURSE.UPDATE(clientId, courseId), properties)
            .then(({data}) => dispatch({
              type: COURSE_ACTION_TYPES.UPDATE_COURSE_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COURSE_ACTION_TYPES.UPDATE_COURSE_ERROR,
              payload
            }));
      }
    },

    registrations(courseId) {
      return (dispatch, getState) => {
        dispatch({
          type: COURSE_ACTION_TYPES.GET_REGISTRATIONS_START
        });

        const clientId = authenticationService.getClientId();
        return $http.get(API_ROUTES.COURSE.REGISTRATION_LIST(clientId, courseId))
            .then(({data}) => dispatch({
              type: COURSE_ACTION_TYPES.GET_REGISTRATIONS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COURSE_ACTION_TYPES.GET_REGISTRATIONS_ERROR,
              payload
            }));
      }
    }

    // get(courseId) {
    //   return (dispatch, getState) => {
    //     dispatch({
    //       type: COURSE_ACTION_TYPES.GET_COURSE_START
    //     });
    //
    //     const clientId = authenticationService.getClientId();
    //     return $http.get(API_ROUTES.COURSE.GET(clientId, courseId))
    //         .then(({data}) => dispatch({
    //           type: COURSE_ACTION_TYPES.GET_COURSE_SUCCESS,
    //           payload: data
    //         }))
    //         .catch((payload) => dispatch({
    //           type: COURSE_ACTION_TYPES.GET_COURSE_ERROR,
    //           payload
    //         }));
    //   }
    // }
  }
};
