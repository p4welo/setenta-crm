import './courseList.less';
import template from './courseList.html';
import R from 'ramda';

export default {
  template: template,
  bindings: {
    days: '<',
    courses: '<',
    removeCourse: '&',
    createCourse: '&',
    updateCourseList: '&'
  },
  controller(scheduleService, notificationService, eventEmitter, $uibModal) {
    'ngInject';

    this.$onInit = () => {
      this.states = [
        {name: 'REGISTRATION', icon: 'icon-pencil7'},
        {name: 'IN_PROGRESS', icon: 'icon-play3'},
        {name: 'ON_HOLD', icon: 'icon-pause'},
        {name: 'CLOSED', icon: 'icon-stop'}
      ];
      this.dayFilter = '';
      this.selection = {};

      this.order = {};
      scheduleService.getDays().forEach(({id}) => {
        this.order[id] = {field: 'timeFrom', reverse: false};
        this.selection[id] = [];
      });
    };

    this.toggleOrder = (day, field) => {
      if (this.order[day].field === field) {
        this.order[day].reverse = !this.order[day].reverse;
        return;
      }
      this.order[day].field = field;
      this.order[day].reverse = false;
    };

    const select = (course, value) => {
      const id = course._id;
      if (value) {
        this.selection[course.day].push(id);
        return;
      }
      this.selection[course.day] = R.without(id, this.selection[course.day]);
    };

    this.addCourse = (day) => $uibModal.open({
      component: 'addCourse',
      resolve: {
        day: () => day
      }
    }).result.then((course) => this.createCourse(eventEmitter(course)));

    this.selectAll = (day) => {
      //var fromGivenDay = course => course.day === day;
      //R.filter(fromGivenDay, this.courses)
      //    .map((course) => select(course, true));
    };

    this.clearSelection = (day) => {
      this.selection[day] = [];
    };

    this.setPublicOfSelected = (day) => this.updateList(day, {isPublic: true});
    this.setNotPublicOfSelected = (day) => this.updateList(day, {isPublic: false});
    this.setRegistrationOpenOfSelected = (day) => this.updateList(day, {isRegistrationOpen: true});
    this.setRegistrationClosedOfSelected = (day) => this.updateList(day, {isRegistrationOpen: false});

    this.updateList = (day, properties) => {
      this.updateCourseList(eventEmitter({
        ids: this.selection[day],
        properties: properties
      }));
      this.clearSelection(day);
    };

    this.toggleSelect = (course) => {
      course.selected = !course.selected;
      select(course, course.selected);
    };

    this.sortingColumnClass = (day, field) => ({
      'sorting_asc': this.order[day].field === field && !this.order[day].reverse,
      'sorting_desc': this.order[day].field === field && this.order[day].reverse,
      'sorting': this.order[day].field !== field
    });

    this.addToDay = (day) => {
      day.adding = true;
    };

    this.isSomeSelected = (day) => this.selection[day].length > 0;

    this.toggleIsPublic = (course) => {
      course.updatingIsPublic = true;
      scheduleService.update(course._id, {isPublic: !course.isPublic})
          .then((result) => {
            Object.assign(course, {isPublic: result.data.isPublic});
            delete course.updatingIsPublic;
            notificationService.success("Pomyślnie zmieniono widoczność kursu");
          })
    };
    this.toggleIsRegistrationOpen = (course) => {
      course.updatingIsRegistrationOpen = true;
      scheduleService.update(course._id, {isRegistrationOpen: !course.isRegistrationOpen})
          .then((result) => {
            Object.assign(course, {isRegistrationOpen: result.data.isRegistrationOpen});
            delete course.updatingIsRegistrationOpen;
            notificationService.success("Pomyślnie zmieniono ustawienia grupy");
          })
    };

    this.setState = (course, state) => {
      if (course.state === state) return;
      course['updating' + state] = true;
      scheduleService.update(course._id, {state})
          .then((result) => {
            Object.assign(course, {state: result.data.state});
            delete course['updating' + state];
            notificationService.success("Pomyślnie zmieniono status");
          })
    };

    this.removeCourse = (course) => this.removeCourse(eventEmitter(course));
  }
};
