import angular from 'angular';
import dashboard from './dashboard';
import course from './course';
import dancePartner from './dance-partner';
import customer from './customer';
import mainComponent from './main';
import advertisement from './advertisement';
import registration from './registration';
import visitor from './visitor';
import competition from './competition';
import school from './school';
import user from './user';
import userType from './user-type';
import competitionModule from './competition-module';
import competitionRegistration from './competition-registration';

export default angular.module('main', [
  dashboard,
  course,
  dancePartner,
  customer,
  advertisement,
  registration,
  visitor,
  competition,
  school,
  user,
  userType,
  competitionModule,
  competitionRegistration
])
    .component('main', mainComponent)
    .name;
