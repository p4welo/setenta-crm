import template from './visitorList.html';

export default {
  template: template,
  bindings: {
  },
  controller(visitorService) {
    'ngInject';


    this.$onInit = () => {
      visitorService.list()
          .then(({data}) => {
            this.visitors = data;
          });
    }
  }
};