import angular from 'angular';
import visitorList from './visitorList';

export default angular.module('visitorList', [])
    .component('visitorList', visitorList)
    .name;
