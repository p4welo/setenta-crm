class VisitorService {
  constructor($http, API_ROUTES, authenticationService) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES,
      authenticationService
    });
  }

  list () {
    const clientId = this.authenticationService.getClientId();
    return this.$http.get(this.API_ROUTES.VISITOR.LIST(clientId));
  }
}
export default VisitorService;
