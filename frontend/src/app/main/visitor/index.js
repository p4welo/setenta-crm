import angular from 'angular';
import visitorList from './visitor-list';
import visitorService from './visitorService';

export default angular.module('visitor', [
      visitorList
    ])
    .service('visitorService', visitorService)
    .name;
