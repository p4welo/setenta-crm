import { pipe, filter, append, map } from 'ramda';

export default (SCHOOL_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    schools: []
  };

  return (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
      case SCHOOL_ACTION_TYPES.GET_SCHOOLS_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case SCHOOL_ACTION_TYPES.GET_SCHOOLS_ERROR:
      case SCHOOL_ACTION_TYPES.DELETE_SCHOOL_ERROR:
      case SCHOOL_ACTION_TYPES.CREATE_SCHOOL_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case SCHOOL_ACTION_TYPES.GET_SCHOOLS_SUCCESS: {
        return {
          ...state,
          schools: payload,
          isLoading: false
        };
      }

      case SCHOOL_ACTION_TYPES.CREATE_SCHOOL_SUCCESS: {
        const schools = pipe(
            modelQuery.byIdentity('schools'),
            append(payload)
        )(state);

        return {
          ...state,
          schools,
          isLoading: false
        }
      }

      case SCHOOL_ACTION_TYPES.DELETE_SCHOOL_SUCCESS: {
        const schools = pipe(
            modelQuery.byIdentity('schools'),
            filter((userType) => userType.sid !== payload.sid)
        )(state);

        return {
          ...state,
          schools,
          isLoading: false
        }
      }

      default:
        return state;
    }
  };
}