import angular from 'angular';

import schoolActionTypes from './schoolActionTypes';
import schoolReducer from './schoolReducer';
import schoolActions from './schoolActions';

export default angular.module('schoolRedux', [
])
    .constant('SCHOOL_ACTION_TYPES', schoolActionTypes)
    .factory('schoolReducer', schoolReducer)
    .service('schoolActions', schoolActions)

    .run(($ngRedux, schoolReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          schoolModel: schoolReducer
        });
      }
    })
    .name;
