export default ($http, API_ROUTES, SCHOOL_ACTION_TYPES) => {
  'ngInject';

  return {
    list() {
      return (dispatch) => {
        dispatch({
          type: SCHOOL_ACTION_TYPES.GET_SCHOOLS_START
        });

        return $http.get(API_ROUTES.SCHOOL.LIST())
            .then(({ data }) => dispatch({
              type: SCHOOL_ACTION_TYPES.GET_SCHOOLS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: SCHOOL_ACTION_TYPES.GET_SCHOOLS_ERROR,
              payload
            }));
      }
    },

    create(competition) {
      return (dispatch) => {
        dispatch({
          type: SCHOOL_ACTION_TYPES.CREATE_SCHOOL_START
        });

        return $http.post(API_ROUTES.SCHOOL.LIST(), competition)
            .then(({ data }) => dispatch({
              type: SCHOOL_ACTION_TYPES.CREATE_SCHOOL_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: SCHOOL_ACTION_TYPES.CREATE_SCHOOL_ERROR,
              payload
            }));
      }
    },

    delete(competition) {
      return (dispatch) => {
        dispatch({
          type: SCHOOL_ACTION_TYPES.DELETE_SCHOOL_START
        });

        return $http.delete(API_ROUTES.SCHOOL.GET(competition.sid))
            .then(() => dispatch({
              type: SCHOOL_ACTION_TYPES.DELETE_SCHOOL_SUCCESS,
              payload: competition
            }))
            .catch((payload) => dispatch({
              type: SCHOOL_ACTION_TYPES.DELETE_SCHOOL_ERROR,
              payload
            }));
      }
    }
  }
};
