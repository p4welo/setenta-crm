const moduleName = 'Competitions';
export default {
  GET_SCHOOLS_START:    `[${moduleName}] Get schools started.`,
  GET_SCHOOLS_SUCCESS:  `[${moduleName}] Get schools succeeded.`,
  GET_SCHOOLS_ERROR:    `[${moduleName}] Get schools error.`,

  CREATE_SCHOOL_START:     `[${moduleName}] Create school started.`,
  CREATE_SCHOOL_SUCCESS:   `[${moduleName}] Create school succeeded.`,
  CREATE_SCHOOL_ERROR:     `[${moduleName}] Create school error.`,

  DELETE_SCHOOL_START:     `[${moduleName}] Delete school started.`,
  DELETE_SCHOOL_SUCCESS:   `[${moduleName}] Delete school succeeded.`,
  DELETE_SCHOOL_ERROR:     `[${moduleName}] Delete school error.`,
}