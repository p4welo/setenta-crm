import angular from 'angular';
import schoolList from './list';
import redux from './redux';

export default angular.module('school', [ schoolList, redux])
    .name;
