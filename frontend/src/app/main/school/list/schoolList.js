import template from './school.html';

export default {
  template: template,
  bindings: {
    list: '<'
  },
  controller($uibModal, notificationService, $ngRedux, schoolActions) {
    this.isLoading = true;
    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, schoolActions)(this);
      $ngRedux.dispatch(schoolActions.list());
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.add = () => {
      // $uibModal.open({
      //   component: 'addUserType',
      //   resolve: {
      //   }
      // }).result.then((school) => {
      //   $ngRedux.dispatch(schoolActions.create(school));
      // });
    };

    this.delete = (school) => {
      $ngRedux.dispatch(schoolActions.delete(school));
    };

    const mapStateToThis = ({schoolModel}) => {
      if (schoolModel) {
        return {
          schools: schoolModel.schools,
          isLoading: schoolModel.isLoading
        };
      }
      return {};
    };
  }
};