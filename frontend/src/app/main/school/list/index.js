import angular from 'angular';
import schoolList from './schoolList';
// import components from './components';

export default angular.module('schoolList', [])
    .component('schoolList', schoolList)
    .name;
