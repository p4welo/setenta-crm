import angular from 'angular';
import userList from './list';
import redux from './redux';

export default angular.module('user', [ userList, redux])
    .name;
