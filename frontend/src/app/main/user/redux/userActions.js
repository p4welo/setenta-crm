export default ($http, API_ROUTES, USER_ACTION_TYPES) => {
  'ngInject';

  return {
    list() {
      return (dispatch) => {
        dispatch({
          type: USER_ACTION_TYPES.GET_USERS_START
        });

        return $http.get(API_ROUTES.USER.LIST())
            .then(({ data }) => dispatch({
              type: USER_ACTION_TYPES.GET_USERS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: USER_ACTION_TYPES.GET_USERS_ERROR,
              payload
            }));
      }
    },

    create(user) {
      return (dispatch) => {
        dispatch({
          type: USER_ACTION_TYPES.CREATE_USER_START
        });

        return $http.post(API_ROUTES.USER.LIST(), user)
            .then(({ data }) => dispatch({
              type: USER_ACTION_TYPES.CREATE_USER_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: USER_ACTION_TYPES.CREATE_USER_ERROR,
              payload
            }));
      }
    },

    update(user, properties) {
      return (dispatch) => {
        dispatch({
          type: USER_ACTION_TYPES.UPDATE_USER_START
        });

        return $http.put(API_ROUTES.USER.GET(user.sid), properties)
            .then(({ data }) => dispatch({
              type: USER_ACTION_TYPES.UPDATE_USER_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: USER_ACTION_TYPES.UPDATE_USER_ERROR,
              payload
            }));
      }
    },

    delete(user) {
      return (dispatch) => {
        dispatch({
          type: USER_ACTION_TYPES.DELETE_USER_START
        });

        return $http.delete(API_ROUTES.USER.GET(user.sid))
            .then(() => dispatch({
              type: USER_ACTION_TYPES.DELETE_USER_SUCCESS,
              payload: user
            }))
            .catch((payload) => dispatch({
              type: USER_ACTION_TYPES.DELETE_USER_ERROR,
              payload
            }));
      }
    }
  }
};
