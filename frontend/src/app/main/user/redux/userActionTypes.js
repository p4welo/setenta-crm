const moduleName = 'Users';
export default {
  GET_USERS_START:    `[${moduleName}] Get users started.`,
  GET_USERS_SUCCESS:  `[${moduleName}] Get users succeeded.`,
  GET_USERS_ERROR:    `[${moduleName}] Get users error.`,

  CREATE_USER_START:     `[${moduleName}] Create user started.`,
  CREATE_USER_SUCCESS:   `[${moduleName}] Create user succeeded.`,
  CREATE_USER_ERROR:     `[${moduleName}] Create user error.`,

  UPDATE_USER_START:     `[${moduleName}] Update user started.`,
  UPDATE_USER_SUCCESS:   `[${moduleName}] Update user succeeded.`,
  UPDATE_USER_ERROR:     `[${moduleName}] Update user error.`,

  DELETE_USER_START:     `[${moduleName}] Delete user started.`,
  DELETE_USER_SUCCESS:   `[${moduleName}] Delete user succeeded.`,
  DELETE_USER_ERROR:     `[${moduleName}] Delete user error.`,
}