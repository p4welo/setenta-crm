import angular from 'angular';

import userActionTypes from './userActionTypes';
import userReducer from './userReducer';
import userActions from './userActions';

export default angular.module('userRedux', [
])
    .constant('USER_ACTION_TYPES', userActionTypes)
    .factory('userReducer', userReducer)
    .service('userActions', userActions)

    .run(($ngRedux, userReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          userModel: userReducer
        });
      }
    })
    .name;
