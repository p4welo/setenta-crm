import {pipe, filter, append, map} from 'ramda';

export default (USER_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    users: []
  };

  return (state = DEFAULT_STATE, {type, payload}) => {
    switch (type) {
      case USER_ACTION_TYPES.GET_USERS_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case USER_ACTION_TYPES.GET_USERS_ERROR:
      case USER_ACTION_TYPES.DELETE_USER_ERROR:
      case USER_ACTION_TYPES.UPDATE_USER_ERROR:
      case USER_ACTION_TYPES.CREATE_USER_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case USER_ACTION_TYPES.GET_USERS_SUCCESS: {
        return {
          ...state,
          users: payload,
          isLoading: false
        };
      }

      case USER_ACTION_TYPES.CREATE_USER_SUCCESS: {
        const users = pipe(
            modelQuery.byIdentity('users'),
            append(payload)
        )(state);

        return {
          ...state,
          users,
          isLoading: false
        }
      }

      case USER_ACTION_TYPES.DELETE_USER_SUCCESS: {
        const users = pipe(
            modelQuery.byIdentity('users'),
            filter((user) => user.sid !== payload.sid)
        )(state);

        return {
          ...state,
          users,
          isLoading: false
        }
      }

      case USER_ACTION_TYPES.UPDATE_USER_SUCCESS: {
        const users = pipe(
            modelQuery.byIdentity('users'),
            map((user) => {
              if (user.sid === payload.sid) {
                return {
                  ...user,
                  ...payload
                };
              }
              return user;
            })
        )(state);

        return {
          ...state,
          users,
          isLoading: false
        }
      }

      default:
        return state;
    }
  };
}