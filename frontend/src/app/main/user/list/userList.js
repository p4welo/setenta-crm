import template from './userList.html';

export default {
  template: template,
  bindings: {
    list: '<'
  },
  controller($uibModal, $ngRedux, userActions) {
    this.isLoading = true;
    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, userActions)(this);
      $ngRedux.dispatch(userActions.list());
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.add = () => {
      $uibModal.open({
        component: 'addUser',
        resolve: {
        }
      }).result.then((user) => {
        $ngRedux.dispatch(userActions.create(user));
      });
    };

    this.toggleState = (user) => {
      const objectState = user.objectState === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
      $ngRedux.dispatch(userActions.update(user, { objectState }));
    };

    this.delete = (user) => {
      $ngRedux.dispatch(userActions.delete(user));
    };

    const mapStateToThis = ({userModel}) => {
      if (userModel) {
        return {
          users: userModel.users,
          isLoading: userModel.isLoading
        };
      }
      return {};
    };
  }
};