import angular from 'angular';
import userList from './userList';
import components from './components';

export default angular.module('userList', [components])
    .component('userList', userList)
    .name;
