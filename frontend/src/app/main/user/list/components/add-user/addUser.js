import template from './addUser.html';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller() {
    this.$onInit = () => {
      this.user = {};
    };

    this.$onDestroy = () => {
    };

    this.ok = () => {
      this.close({$value: this.user});
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};