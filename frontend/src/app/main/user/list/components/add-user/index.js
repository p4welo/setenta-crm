import angular from 'angular';
import addUser from './addUser';

export default angular.module('addUser', [])
    .component('addUser', addUser)
    .name;
