import angular from 'angular';
import addUser from './add-user';

export default angular.module('usersComponents', [
  addUser
])
    .name;
