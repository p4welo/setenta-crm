import angular from 'angular';
import competitionList from './list';
import competitionDetails from './details';
import competitionDetailsInfo from './details-info';
import competitionDetailsRules from './details-rules';
import competitionDetailsSchools from './details-schools';
import competitionService from './competitionService';
import redux from './redux';
import routes from './routes';

export default angular.module('competition', [
  competitionList,
  competitionDetails,
  competitionDetailsInfo,
  competitionDetailsRules,
  competitionDetailsSchools,
  redux
])
    .config(routes)
    .service('competitionService', competitionService)
    .name;
