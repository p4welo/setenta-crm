import angular from 'angular';
import competitionList from './competitionList';
import components from './components';

export default angular.module('competitionList', [ components ])
    .component('competitionList', competitionList)
    .name;
