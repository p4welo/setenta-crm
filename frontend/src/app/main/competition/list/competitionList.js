import template from './competitionList.html';

export default {
  template: template,
  bindings: {
    list: '<'
  },
  controller($uibModal, $state, notificationService, competitionService) {

    this.$onInit = () => {
      this.isLoading = true;
      this.unsubscribe = competitionService.mapState(mapStateToThis, this);
      competitionService.dispatchCompetitionListFetch();
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.add = () => {
      $uibModal.open({
        component: 'addCompetition',
        resolve: {
        }
      }).result.then((competition) => {
        competitionService.dispatchCompetitionCreate(competition);
      });
    };

    this.delete = (competition) => {
      competitionService.dispatchCompetitionDelete(competition);
    };

    this.goToDetails = (competition) => $state.go('main.competition.details.info', {id: competition.sid});

    const mapStateToThis = ({competitionModel}) => {
      if (competitionModel) {
        return {
          competitions: competitionModel.competitions,
          isLoading: competitionModel.isLoading
        };
      }
      return {};
    };
  }
};