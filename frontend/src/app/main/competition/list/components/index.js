import angular from 'angular';
import addCompetition from './add-competition';

export default angular.module('competitionComponents', [
  addCompetition
])
    .name;
