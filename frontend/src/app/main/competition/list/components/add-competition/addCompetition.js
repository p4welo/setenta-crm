import template from './addCompetition.html';
import moment from 'moment';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller() {

    this.$onInit = () => {
      this.competition = {};
    };

    this.$onDestroy = () => {
    };

    this.ok = () => {
      console.log(this.competition.start);
      console.log(moment(this.competition.start).startOf('day').toDate());
      console.log(moment(this.competition.end).endOf('day').toDate());
      this.close({$value: {
            ...this.competition,
          start: moment(this.competition.start).startOf('day').toDate(),
          end: moment(this.competition.end).endOf('day').toDate(),
          registrationEnd: moment(this.competition.registrationEnd).endOf('day').toDate()
        }});
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};
