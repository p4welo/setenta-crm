import angular from 'angular';
import addCompetition from './addCompetition';

export default angular.module('addCompetition', [])
    .component('addCompetition', addCompetition)
    .name;
