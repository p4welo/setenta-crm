import template from './competitionDetails.html';

export default {
  template: template,
  bindings: {
    competitionId: '<'
  },
  controller($state, $scope, competitionService) {
    'ngInject';

    this.$onInit = () => {
      this.unsubscribe = competitionService.mapState(mapStateToThis, $scope);
      competitionService.dispatchCompetitionListFetch();
    };

    const mapStateToThis = ({competitionModel}) => {
      if (competitionModel) {
        return {
          competition: competitionModel.competitions.filter((competition) => competition.sid === this.competitionId)[0]
        };
      }
      return {};
    };

    this.isView = (name) => {
      return $state.includes(name);
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };
  }
};