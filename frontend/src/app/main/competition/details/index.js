import angular from 'angular';
import competitionDetails from './competitionDetails';
// import components from './components';

export default angular.module('competitionDetails', [  ])
    .component('competitionDetails', competitionDetails)
    .name;
