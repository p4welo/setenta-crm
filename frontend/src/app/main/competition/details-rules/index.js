import angular from 'angular';
import competitionDetailsRules from './competitionDetailsRules';

export default angular.module('competitionDetailsRules', [])
    .component('competitionDetailsRules', competitionDetailsRules)
    .name;
