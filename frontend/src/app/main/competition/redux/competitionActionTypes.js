const moduleName = 'Competitions';
export default {
  GET_COMPETITIONS_START:    `[${moduleName}] Get competitions started.`,
  GET_COMPETITIONS_SUCCESS:  `[${moduleName}] Get competitions succeeded.`,
  GET_COMPETITIONS_ERROR:    `[${moduleName}] Get competitions error.`,

  CREATE_COMPETITION_START:     `[${moduleName}] Create competition started.`,
  CREATE_COMPETITION_SUCCESS:   `[${moduleName}] Create competition succeeded.`,
  CREATE_COMPETITION_ERROR:     `[${moduleName}] Create competition error.`,

  DELETE_COMPETITION_START:     `[${moduleName}] Delete competition started.`,
  DELETE_COMPETITION_SUCCESS:   `[${moduleName}] Delete competition succeeded.`,
  DELETE_COMPETITION_ERROR:     `[${moduleName}] Delete competition error.`,

  GET_COMPETITION_SCHOOLS_START:     `[${moduleName}] Get competition school list started.`,
  GET_COMPETITION_SCHOOLS_SUCCESS:   `[${moduleName}] Get competition school list succeeded.`,
  GET_COMPETITION_SCHOOLS_ERROR:     `[${moduleName}] Get competition school list error.`,
}