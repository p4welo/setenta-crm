import { pipe, filter, append, map } from 'ramda';

export default (COMPETITION_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    competitions: []
  };

  return (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
      case COMPETITION_ACTION_TYPES.GET_COMPETITIONS_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case COMPETITION_ACTION_TYPES.GET_COMPETITIONS_ERROR:
      case COMPETITION_ACTION_TYPES.DELETE_COMPETITION_ERROR:
      case COMPETITION_ACTION_TYPES.CREATE_COMPETITION_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case COMPETITION_ACTION_TYPES.GET_COMPETITIONS_SUCCESS: {
        return {
          ...state,
          competitions: payload,
          isLoading: false
        };
      }

      case COMPETITION_ACTION_TYPES.CREATE_COMPETITION_SUCCESS: {
        const competitions = pipe(
            modelQuery.byIdentity('competitions'),
            append(payload)
        )(state);

        return {
          ...state,
          competitions,
          isLoading: false
        }
      }

      case COMPETITION_ACTION_TYPES.DELETE_COMPETITION_SUCCESS: {
        const competitions = pipe(
            modelQuery.byIdentity('competitions'),
            filter((userType) => userType.sid !== payload.sid)
        )(state);

        return {
          ...state,
          competitions,
          isLoading: false
        }
      }

      default:
        return state;
    }
  };
}