import angular from 'angular';

import competitionActionTypes from './competitionActionTypes';
import competitionReducer from './competitionReducer';
import competitionActions from './competitionActions';

export default angular.module('competitionRedux', [
])
    .constant('COMPETITION_ACTION_TYPES', competitionActionTypes)
    .factory('competitionReducer', competitionReducer)
    .service('competitionActions', competitionActions)

    .run(($ngRedux, competitionReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          competitionModel: competitionReducer
        });
      }
    })
    .name;
