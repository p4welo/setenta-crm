export default ($http, API_ROUTES, COMPETITION_ACTION_TYPES) => {
  'ngInject';

  return {
    list() {
      return (dispatch) => {
        dispatch({
          type: COMPETITION_ACTION_TYPES.GET_COMPETITIONS_START
        });

        return $http.get(API_ROUTES.COMPETITION.LIST())
            .then(({ data }) => dispatch({
              type: COMPETITION_ACTION_TYPES.GET_COMPETITIONS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COMPETITION_ACTION_TYPES.GET_COMPETITIONS_ERROR,
              payload
            }));
      }
    },

    schoolList(competition) {
      return (dispatch) => {
        dispatch({
          type: COMPETITION_ACTION_TYPES.GET_COMPETITION_SCHOOLS_START
        });

        return $http.get(API_ROUTES.COMPETITION.SCHOOL_LIST(competition.sid))
            .then(({ data }) => dispatch({
              type: COMPETITION_ACTION_TYPES.GET_COMPETITION_SCHOOLS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COMPETITION_ACTION_TYPES.GET_COMPETITION_SCHOOLS_ERROR,
              payload
            }));
      }
    },

    create(competition) {
      return (dispatch) => {
        dispatch({
          type: COMPETITION_ACTION_TYPES.CREATE_COMPETITION_START
        });

        return $http.post(API_ROUTES.COMPETITION.LIST(), competition)
            .then(({ data }) => dispatch({
              type: COMPETITION_ACTION_TYPES.CREATE_COMPETITION_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: COMPETITION_ACTION_TYPES.CREATE_COMPETITION_ERROR,
              payload
            }));
      }
    },

    delete(competition) {
      return (dispatch) => {
        dispatch({
          type: COMPETITION_ACTION_TYPES.DELETE_COMPETITION_START
        });

        return $http.delete(API_ROUTES.COMPETITION.GET(competition.sid))
            .then(() => dispatch({
              type: COMPETITION_ACTION_TYPES.DELETE_COMPETITION_SUCCESS,
              payload: competition
            }))
            .catch((payload) => dispatch({
              type: COMPETITION_ACTION_TYPES.DELETE_COMPETITION_ERROR,
              payload
            }));
      }
    }
  }
};
