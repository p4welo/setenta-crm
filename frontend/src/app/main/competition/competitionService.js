class CompetitionService {
  constructor($ngRedux, competitionActions) {
    'ngInject';
    Object.assign(this, {
      $ngRedux,
      competitionActions
    });
  }

  mapState(mappingFn, destination) {
    return this.$ngRedux.connect(mappingFn, this.competitionActions)(destination);
  }

  dispatchCompetitionListFetch() {
    this.$ngRedux.dispatch(this.competitionActions.list());
  }

  dispatchCompetitionCreate(competition) {
    this.$ngRedux.dispatch(this.competitionActions.create(competition));
  }

  dispatchCompetitionSchoolListFetch(competition) {
    this.$ngRedux.dispatch(this.competitionActions.schoolList(competition));
  }

  dispatchCompetitionDelete(competition) {
    this.$ngRedux.dispatch(this.competitionActions.delete(competition));;
  }
}

export default CompetitionService;