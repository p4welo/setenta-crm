export default function ($stateProvider) {
  'ngInject';

  $stateProvider
      .state('main.competition', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.competition.list', {
        url: '/competition',
        template: '<competition-list></competition-list>'
      })
      .state('main.competition.details', {
        url: '/competition/:id',
        abstract: true,
        controllerAs: '$ctrl',
        template: `<competition-details competition-id="$ctrl.competitionId"></competition-details>`,
        controller($state, $stateParams) {
          'ngInject';
          if ($stateParams.id) {
            $state.go('main.competition.details.info', { id: this.competitionId = $stateParams.id });
          }
          else {
            $state.go('main.competition.list');
          }
        }
      })
      .state('main.competition.details.info', {
        url: '/info',
        template: '<competition-details-info competition="competition"></competition-details-info>',
      })
      .state('main.competition.details.rules', {
        url: '/rules',
        template: '<competition-details-rules competition="competition"></competition-details-rules>'
      })
      .state('main.competition.details.schools', {
        url: '/schools',
        template: '<competition-details-schools competition="competition"></competition-details-schools>'
      })
}