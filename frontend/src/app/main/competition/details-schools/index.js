import angular from 'angular';
import competitionDetailsSchools from './competitionDetailsSchools';

export default angular.module('competitionDetailsSchools', [])
    .component('competitionDetailsSchools', competitionDetailsSchools)
    .name;
