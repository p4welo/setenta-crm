import template from './competitionDetailsSchools.html';

export default {
  template: template,
  bindings: {
    competition: '<'
  },
  controller(competitionActions, competitionService) {
    this.$onInit = () => {
      this.isLoading = true;
      this.unsubscribe = competitionService.mapState(mapStateToThis, this);
      competitionService.dispatchCompetitionSchoolListFetch(this.competition);
    };

    const mapStateToThis = ({competitionModel}) => {
      if (competitionModel) {
        return {
          schools: competitionModel.schools,
          isLoading: competitionModel.isLoading
        };
      }
      return {};
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };
  }
};