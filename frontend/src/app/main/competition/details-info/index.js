import angular from 'angular';
import competitionDetailsInfo from './competitionDetailsInfo';

export default angular.module('competitionDetailsInfo', [])
    .component('competitionDetailsInfo', competitionDetailsInfo)
    .name;
