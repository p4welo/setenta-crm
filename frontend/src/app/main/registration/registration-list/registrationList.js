import './registrationList.less';
import template from './registrationList.html';

export default {
  template: template,
  bindings: {},
  controller(registrationService, courseService) {
    'ngInject';

    this.select = (course) => {
      if (this.selected && this.selected._id === course._id) {
        delete this.selected;
        return;
      }
      this.selected = course;
    };

    this.$onInit = () => {
      this.days = courseService.getDays();
      registrationService.list()
          .then(({data})=> {
            this.courseList = data;
          });
    }
  }
};