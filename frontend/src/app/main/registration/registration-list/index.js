import angular from 'angular';
import registrationList from './registrationList';

export default angular.module('registrationList', [])
    .component('registrationList', registrationList)
    .name;
