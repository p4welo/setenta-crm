import angular from 'angular';
import registrationList from './registration-list';
import registrationService from './registrationService';

export default angular.module('registration', [
      registrationList
    ])
    .service('registrationService', registrationService)
    .name;
