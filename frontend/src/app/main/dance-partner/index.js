import angular from 'angular';
import dancePartner from './dancePartner';
import dancePartnerList from './list/dancePartnerList';
import dancePartnerDetails from './details/dancePartnerDetails';
import dancePartnerService from './dancePartnerService';
import dancePartnerRoutes from './dancePartnerRoutes';

export default angular.module('dancePartner', [])
    .config(dancePartnerRoutes)

    .component('dancePartner', dancePartner)
    .component('dancePartnerList', dancePartnerList)
    .component('dancePartnerDetails', dancePartnerDetails)

    .service('dancePartnerService', dancePartnerService)
    .name;
