import './dancePartnerList.less';
import template from './dancePartnerList.html';

export default {
  template: template,
  bindings: {
    list: '<'
  },
  controller(dancePartnerService) {
    'ngInject';

    this.$onInit = () => {
      dancePartnerService.list().then(result => {
        this.partnerList = result.data;
      });
    }
  }
};
