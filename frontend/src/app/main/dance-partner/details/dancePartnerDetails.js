import './dancePartnerDetails.less';
import template from './dancePartnerDetails.html';

export default {
  template: template,
  bindings: {
    partner: '<'
  },
  controller() {
    'ngInject';

    this.$onInit = () => {
    }
  }
};
