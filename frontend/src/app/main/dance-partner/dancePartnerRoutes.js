export default function ($stateProvider) {
  'ngInject';

  $stateProvider
      .state('main.dancePartner', {
        url: '/dancePartner',
        template: '<dance-partner></dance-partner><ui-view></ui-view>'
      })
      .state('main.dancePartner.list', {
        url: '',
        template: '<dance-partner-list></dance-partner-list>'
      })
      ;
}
