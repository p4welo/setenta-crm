class DancePartnerService {
  constructor($http, API_ROUTES) {
    'ngInject';
    this.$http = $http;
    this.API_ROUTES = API_ROUTES;
  }

  list() {
    return this.$http.get(this.API_ROUTES.PARTNER.LIST);
  }
}
export default DancePartnerService;
