import angular from 'angular';
import competitionRegistrationList from './competition-registration-list';
import competitionRegistrationService from './competitionRegistrationService';

export default angular.module('competitionRegistration', [competitionRegistrationList])
    .service('competitionRegistrationService', competitionRegistrationService)
    .name;
