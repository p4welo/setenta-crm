import template from './competitionRegistrationList.html';
import { reduce, countBy, toLower, trim } from 'ramda';

export default {
  template: template,
  controller(competitionRegistrationService, $state, competitionService) {
    'ngInject';

    this.$onInit = () => {
      this.isLoading = true;
      competitionRegistrationService.list()
          .then(({ data }) => {
            this.list = data;
            this.chargeSum = this.getChargeSum(this.list);
            this.performanceSum = this.getPerformanceSum(this.list);
            this.isLoading = false;
          });
    };

    this.getChargeSum = (registrations) => {
      const addCharges = (sum, registration) => {
        return sum + registration.charge;
      };

      return reduce(addCharges, 0, registrations);
    };

    this.getPerformanceSum = (registrations) => {
      const addPerformances = (sum, registration) => {
        return sum + registration.performances.length;
      };

      return reduce(addPerformances, 0, registrations);
    };

    this.goToStartingNumbers = (registration) => {
      const url = $state.href('main.competition.school-starting-list', {
        id: registration.school._id
      });
      window.open(url, '_blank');
    };

    this.toggle = (registration) => {
      registration.opened = !registration.opened;
    };

    this.deletePerformance = (performance) => {
      competitionRegistrationService.deletePerformance(performance._id);
    };
  }
};