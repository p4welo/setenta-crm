import angular from 'angular';
// import components from './components';
import courseList from './competitionRegistrationList';

export default angular.module('competitionRegistrationList', [])
    .component('competitionRegistrationList', courseList)
    .name;
