class CompetitionRegistrationService {
  constructor($http, API_ROUTES) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES
    });
  }

  list() {
    return this.$http.get(this.API_ROUTES.COMPETITION.FINANCE.SUMMARY());
  }

  deletePerformance(performanceId) {
    return this.$http.delete(this.API_ROUTES.COMPETITION.PERFORMANCE.DELETE(performanceId));
  }
}

export default CompetitionRegistrationService;
