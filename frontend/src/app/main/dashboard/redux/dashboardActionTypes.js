const moduleName = 'Dashboard';
export default {
  GET_RECENT_REGISTRATIONS_START:    `[${moduleName}] Get recent registrations started.`,
  GET_RECENT_REGISTRATIONS_SUCCESS:  `[${moduleName}] Get recent registrations succeeded.`,
  GET_RECENT_REGISTRATIONS_ERROR:    `[${moduleName}] Get recent registrations error.`,
}