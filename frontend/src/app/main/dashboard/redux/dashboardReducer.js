import {pipe, filter, append, map} from 'ramda';

export default (DASHBOARD_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    users: []
  };

  return (state = DEFAULT_STATE, {type, payload}) => {
    switch (type) {
      case DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_SUCCESS: {
        return {
          ...state,
          registrations: payload,
          isLoading: false
        };
      }

      default:
        return state;
    }
  };
}