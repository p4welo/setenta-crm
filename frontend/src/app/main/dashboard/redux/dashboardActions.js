export default ($http, API_ROUTES, DASHBOARD_ACTION_TYPES, authenticationService) => {
  'ngInject';

  return {
    list() {
      return (dispatch) => {
        dispatch({
          type: DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_START
        });

        const clientId = authenticationService.getClientId();
        return $http.get(API_ROUTES.REGISTRATION.LIST(clientId))
            .then(({ data }) => dispatch({
              type: DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: DASHBOARD_ACTION_TYPES.GET_RECENT_REGISTRATIONS_ERROR,
              payload
            }));
      }
    },
  }
};
