import angular from 'angular';

import dashboardActionTypes from './dashboardActionTypes';
import dashboardReducer from './dashboardReducer';
import dashboardActions from './dashboardActions';

export default angular.module('dashboardRedux', [
])
    .constant('DASHBOARD_ACTION_TYPES', dashboardActionTypes)
    .factory('dashboardReducer', dashboardReducer)
    .service('dashboardActions', dashboardActions)

    .run(($ngRedux, dashboardReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          dashboardModel: dashboardReducer
        });
      }
    })
    .name;
