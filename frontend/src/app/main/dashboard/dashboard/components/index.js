import angular from 'angular';
import visitChart from './visit-chart';

export default angular.module('dashboardComponents', [
  visitChart
])
    .name;
