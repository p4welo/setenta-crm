import template from './visitChart.html';

export default {
  template: template,
  bindings: {
    visits: '<'
  },
  controller(eventEmitter, visitorService) {
    'ngInject';

    this.$onInit = () => {
      this.loading = true;
      visitorService.list()
          .then(({data}) => {
            this.loading = false;
            this.visits = data;
          });
    };
  }
};
