import angular from 'angular';
import dashboard from './dashboard';

export default angular.module('dashboardView', [])
    .component('dashboard', dashboard)
    .name;
