import './dashboard.less';
import template from './dashboard.html';

export default {
  template: template,
  controller($ngRedux, dashboardActions) {
    'ngInject';

    this.isLoading = true;
    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, dashboardActions)(this);
      $ngRedux.dispatch(dashboardActions.list());
    };

    this.registrationRowClass = (registration) => {
      return {
        'bg-success': registration.daysBefore === 0,
        'success': registration.daysBefore === 1
      }
    };

    const mapStateToThis = ({ dashboardModel }) => {
      if (dashboardModel) {
        return {
          registrations: dashboardModel.registrations,
          isLoading: dashboardModel.isLoading
        };
      }
      return {};
    };
  }
};
