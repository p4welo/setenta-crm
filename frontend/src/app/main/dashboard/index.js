import angular from 'angular';
import dashboard from './dashboard';
import redux from './redux';

export default angular.module('dashboardModule', [dashboard, redux])
    .name;
