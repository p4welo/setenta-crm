import './userTypeList.less';
import template from './userTypeList.html';

export default {
  template: template,
  bindings: {
    list: '<'
  },
  controller($uibModal, notificationService, $ngRedux, userTypeActions) {
    this.isLoading = true;
    this.$onInit = () => {
      this.unsubscribe = $ngRedux.connect(mapStateToThis, userTypeActions)(this);
      $ngRedux.dispatch(userTypeActions.list());
    };

    this.$onDestroy = () => {
      this.unsubscribe();
    };

    this.add = () => {
      $uibModal.open({
        component: 'addUserType',
        resolve: {
        }
      }).result.then((userType) => {
        $ngRedux.dispatch(userTypeActions.create(userType));
      });
    };

    this.delete = (userType) => {
      $ngRedux.dispatch(userTypeActions.delete(userType));
    };

    const mapStateToThis = ({userTypeModel}) => {
      if (userTypeModel) {
        return {
          userTypes: userTypeModel.userTypes,
          isLoading: userTypeModel.isLoading
        };
      }
      return {};
    };
  }
};