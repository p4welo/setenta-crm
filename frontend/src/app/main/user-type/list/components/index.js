import angular from 'angular';
import addUserType from './add-user-type';

export default angular.module('userTypeComponents', [
  addUserType
])
    .name;
