import template from './addUserType.html';

export default {
  template: template,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller() {

    this.$onInit = () => {
      this.userType = {};
    };

    this.$onDestroy = () => {
    };

    this.ok = () => {
      this.close({$value: this.userType});
    };

    this.cancel = () => {
      this.dismiss({$value: 'cancel'});
    };
  }
};
