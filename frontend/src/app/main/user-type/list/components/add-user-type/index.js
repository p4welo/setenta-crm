import angular from 'angular';
import addUserType from './addUserType';

export default angular.module('addUserType', [])
    .component('addUserType', addUserType)
    .name;
