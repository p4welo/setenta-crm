import angular from 'angular';
import userTypeList from './userTypeList';
import components from './components';

export default angular.module('userTypeList', [components])
    .component('userTypeList', userTypeList)
    .name;
