import angular from 'angular';
import userTypeList from './list';
import redux from './redux';

export default angular.module('userType', [ userTypeList, redux])
    .name;
