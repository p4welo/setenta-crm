const moduleName = 'User Types';
export default {
  GET_USER_TYPES_START:    `[${moduleName}] Get user types started.`,
  GET_USER_TYPES_SUCCESS:  `[${moduleName}] Get user types succeeded.`,
  GET_USER_TYPES_ERROR:    `[${moduleName}] Get user types error.`,

  CREATE_USER_TYPE_START:     `[${moduleName}] Create user type started.`,
  CREATE_USER_TYPE_SUCCESS:   `[${moduleName}] Create user type succeeded.`,
  CREATE_USER_TYPE_ERROR:     `[${moduleName}] Create user type error.`,

  DELETE_USER_TYPE_START:     `[${moduleName}] Delete user type started.`,
  DELETE_USER_TYPE_SUCCESS:   `[${moduleName}] Delete user type succeeded.`,
  DELETE_USER_TYPE_ERROR:     `[${moduleName}] Delete user type error.`,
}