export default ($http, API_ROUTES, USER_TYPE_ACTION_TYPES) => {
  'ngInject';

  return {
    list() {
      return (dispatch) => {
        dispatch({
          type: USER_TYPE_ACTION_TYPES.GET_USER_TYPES_START
        });

        return $http.get(API_ROUTES.USER_TYPE.LIST())
            .then(({ data }) => dispatch({
              type: USER_TYPE_ACTION_TYPES.GET_USER_TYPES_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: USER_TYPE_ACTION_TYPES.GET_USER_TYPES_ERROR,
              payload
            }));
      }
    },

    create(userType) {
      return (dispatch) => {
        dispatch({
          type: USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_START
        });

        return $http.post(API_ROUTES.USER_TYPE.LIST(), userType)
            .then(({ data }) => dispatch({
              type: USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_SUCCESS,
              payload: data
            }))
            .catch((payload) => dispatch({
              type: USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_ERROR,
              payload
            }));
      }
    },

    delete(userType) {
      return (dispatch) => {
        dispatch({
          type: USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_START
        });

        return $http.delete(API_ROUTES.USER_TYPE.GET(userType.sid))
            .then(() => dispatch({
              type: USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_SUCCESS,
              payload: userType
            }))
            .catch((payload) => dispatch({
              type: USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_ERROR,
              payload
            }));
      }
    }
  }
};
