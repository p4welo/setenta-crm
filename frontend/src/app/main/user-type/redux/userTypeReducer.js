import { pipe, filter, append, map } from 'ramda';

export default (USER_TYPE_ACTION_TYPES, modelQuery) => {
  'ngInject';

  const DEFAULT_STATE = {
    isLoading: false,
    userTypes: []
  };

  return (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
      case USER_TYPE_ACTION_TYPES.GET_USER_TYPES_START: {
        return {
          ...state,
          isLoading: true
        };
      }

      case USER_TYPE_ACTION_TYPES.GET_USER_TYPES_ERROR:
      case USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_ERROR:
      case USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_ERROR: {
        return {
          ...state,
          errors: payload,
          isLoading: false
        };
      }

      case USER_TYPE_ACTION_TYPES.GET_USER_TYPES_SUCCESS: {
        return {
          ...state,
          userTypes: payload,
          isLoading: false
        };
      }

      case USER_TYPE_ACTION_TYPES.CREATE_USER_TYPE_SUCCESS: {
        const userTypes = pipe(
            modelQuery.byIdentity('userTypes'),
            append(payload)
        )(state);

        return {
          ...state,
          userTypes,
          isLoading: false
        }
      }

      case USER_TYPE_ACTION_TYPES.DELETE_USER_TYPE_SUCCESS: {
        const userTypes = pipe(
            modelQuery.byIdentity('userTypes'),
            filter((userType) => userType.sid !== payload.sid)
        )(state);

        return {
          ...state,
          userTypes,
          isLoading: false
        }
      }

      default:
        return state;
    }
  };
}