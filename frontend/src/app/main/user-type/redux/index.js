import angular from 'angular';

import userTypeActionTypes from './userTypeActionTypes';
import userTypeReducer from './userTypeReducer';
import userTypeActions from './userTypeActions';

export default angular.module('userTypeRedux', [
])
    .constant('USER_TYPE_ACTION_TYPES', userTypeActionTypes)
    .factory('userTypeReducer', userTypeReducer)
    .service('userTypeActions', userTypeActions)

    .run(($ngRedux, userTypeReducer) => {
      'ngInject';

      if ($ngRedux.addReducers) {
        $ngRedux.addReducers({
          userTypeModel: userTypeReducer
        });
      }
    })
    .name;
