import angular from 'angular';
import startingList from './startingList';

export default angular.module('startingList', [])
    .component('startingList', startingList)
.name;