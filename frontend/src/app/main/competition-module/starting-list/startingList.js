import template from './startingList.html';
import { groupBy, toLower, trim } from 'ramda';

export default {
  template: template,
  bindings: {
    schoolId: '<'
  },
  controller(competitionServiceOld) {
    'ngInject';

    this.days = [
      {
        key: 'SATURDAY', blocks: [
          { id: 0 },
          { id: 1 },
          { id: 2 },
          { id: 3 }
        ]
      },
      {
        key: 'SUNDAY', blocks: [
          { id: 0 },
          { id: 1 },
          { id: 2 },
          { id: 3 },
          { id: 4 }
        ]
      }
    ];
    this.day = this.days[0];

    this.$onInit = () => {
      this.isLoading = true;
      competitionServiceOld.getStartingList(this.schoolId)
          .then(({ data }) => {
            this.list = data;
            this.isLoading = false;

            this.generateReport();
          })
    };

    this.print = () => {
      window.print();
    };

    this.generateReport = () => {
      this.saturdayPerformances = [];
      this.sundayPerformances = [];
      this.list.forEach((block) => {
        if (block.day === 'SATURDAY') {
          this.saturdayPerformances.push(...block.performances);
        }
        else {
          this.sundayPerformances.push(...block.performances);
        }
      });

      console.log('================SOBOTA================');
      console.log('Występów:', this.saturdayPerformances.length);
      console.log('Solo:', this.saturdayPerformances.filter(performance => performance.type === 'SOLO').length);
      console.log('Duety:', this.saturdayPerformances.filter(performance => performance.type === 'DUO').length);
      console.log('Mini formacje:', this.saturdayPerformances.filter(performance => performance.type === 'MINI_FORMATION').length);
      console.log('Formacje:', this.saturdayPerformances.filter(performance => performance.type === 'FORMATION').length);

      console.log('================NIEDZIELA================');
      console.log('Występów:', this.sundayPerformances.length);
      console.log('Solo:', this.sundayPerformances.filter(performance => performance.type === 'SOLO').length);
      console.log('Duety:', this.sundayPerformances.filter(performance => performance.type === 'DUO').length);
      console.log('Mini formacje:', this.sundayPerformances.filter(performance => performance.type === 'MINI_FORMATION').length);
      console.log('Formacje:', this.sundayPerformances.filter(performance => performance.type === 'FORMATION').length);

      this.saturdaySchools = this.groupSchools(this.saturdayPerformances);
      console.log(this.saturdaySchools);
      this.sundaySchools = this.groupSchools(this.sundayPerformances);
      console.log(this.sundaySchools);
    };

    this.groupSchools = (performances) => {
      const performancesGrouped = groupBy((performance) => {
        return performance.school.name;
      }, performances);

      const schools = Object.keys(performancesGrouped).map((key) => {
        const performances = performancesGrouped[key];
        const dancers = [];
        performances.forEach((performance) => {
          dancers.push(...performance.dancers);
        });
        const dancersGrouped = groupBy((dancer) => {
          return toLower(trim(dancer.firstName) + trim(dancer.lastName));
        }, dancers);
        return {
          name: key,
          amount: Object.keys(dancersGrouped).length
        }
      });
      return schools;
    }
  }
}