import angular from 'angular';
import startingList from './starting-list';
import schoolStartingList from './school-starting-list';
import judgeCards from './judge-cards';
import competitionService from './competitionService';

export default angular.module('competitionModule', [startingList, schoolStartingList, judgeCards])
    .service('competitionServiceOld', competitionService)
    .name;