import template from './schoolStartingList.html';
import { countBy, toLower, trim } from 'ramda';

export default {
  template: template,
  bindings: {
    schoolId: '<'
  },
  controller(competitionServiceOld) {
    'ngInject';

    this.days = [
      { key: 'SATURDAY', blocks: [
          {id: 0},
          {id: 1},
          {id: 2},
          {id: 3}
        ] },
      { key: 'SUNDAY', blocks: [
          {id: 0},
          {id: 1},
          {id: 2},
          {id: 3},
          {id: 4}
        ] }
    ];
    this.day = this.days[0];

    this.$onInit = async () => {
      this.isLoading = true;
      competitionServiceOld.getStartingList(this.schoolId)
          .then(({ data }) => {
            this.list = data;
            this.isLoading = false;
            return competitionServiceOld.getSchool(this.schoolId);
          })
          .then(({data}) => {
            this.school = data;
            document.title = `${data.email} - ${data.name}`;
            return competitionServiceOld.getSchoolDancers(this.school);
          })
          .then(({data}) => {
            this.dancersAmount = data ? data.length : 0;
            this.report();
          })
    };

    this.print = () => {
      window.print();
    };

    this.report = () => {
      const saturdayDancers = [];
      const sundayDancers = [];
      this.list.forEach(block => {
        if (block.day === 'SATURDAY') {
          const dancers = [];
          block.performances.forEach((performance) => dancers.push(...performance.dancers));
          saturdayDancers.push(...dancers);
        }
        else {
          const dancers = [];
          block.performances.forEach((performance) => dancers.push(...performance.dancers));
          sundayDancers.push(...dancers);
        }
      });
      const saturday = countBy((dancer) => {
        return toLower(trim(dancer.firstName) + trim(dancer.lastName))
      }, saturdayDancers);
      const sunday = countBy((dancer) => {
        return toLower(trim(dancer.firstName) + trim(dancer.lastName))
      }, sundayDancers);
      console.log('SATURDAY: ', saturday.length);
      console.log('SUNDAY: ', sunday.length);
    };
  }
}