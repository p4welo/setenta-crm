import angular from 'angular';
import schoolStartingList from './schoolStartingList';

export default angular.module('schoolStartingList', [])
    .component('schoolStartingList', schoolStartingList)
    .name;