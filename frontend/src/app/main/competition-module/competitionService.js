class CompetitionService {
  constructor($http, API_ROUTES) {
    'ngInject';
    Object.assign(this, {
      $http,
      API_ROUTES
    });
  }

  getStartingList(schoolId) {
    if (schoolId) {
      return this.$http.get(this.API_ROUTES.COMPETITION.SCHOOL.STARTING_NUMBERS(schoolId));
    }
    return this.$http.get(this.API_ROUTES.COMPETITION.REPORT.STARTING_LIST());
  }

  getSchool(schoolId) {
    return this.$http.get(this.API_ROUTES.COMPETITION.SCHOOL.GET(schoolId));
  }

  getSchoolDancers(school) {
    return this.$http.get(this.API_ROUTES.COMPETITION.SCHOOL.DANCERS(school._id));
  }

  generateStages(amount = 1) {
    const result = [];
    for (let i = 0; i < amount; i++) {
      if (i === 0) {
        result.push('F');
      }
      else if (i === 1) {
        result.push('1/2F');
      }
      else if (i === amount - 1) {
        result.push('EL');
      }
      else {
        const value = Math.pow(2, i);
        result.push(`1/${value}F`)
      }
    }
    return result;
  }
}

export default CompetitionService;