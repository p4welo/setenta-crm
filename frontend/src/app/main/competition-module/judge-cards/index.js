import angular from 'angular';
import judgeCards from './judgeCards';

export default angular.module('judgeCards', [])
    .component('judgeCards', judgeCards)
    .name;