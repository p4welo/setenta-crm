import template from './judgeCards.html';

export default {
  template: template,
  bindings: {},
  controller(competitionServiceOld) {
    'ngInject';

    this.$onInit = () => {
      this.isLoading = true;
      competitionServiceOld.getStartingList(this.schoolId)
          .then(({ data }) => {
            this.list = [];
            data.forEach((block) => {
              competitionServiceOld.generateStages(block.stages).forEach((stage) => {
                this.list.push({
                  ...block,
                  stage
                })
              })

            });
            // this.list = data;
            this.isLoading = false;

          })
    };

    this.print = () => {
      window.print();
    };
  }
}