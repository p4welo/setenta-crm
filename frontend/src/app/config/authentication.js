export default function ($rootScope, $location, $http, $cookieStore, $window) {
  'ngInject';

  const token = $window.sessionStorage._ap_token;
  const email = $window.sessionStorage._ap_email;
  const client = $window.sessionStorage._ap_client;
  $rootScope.globals = {token, email, client};
  if (token) {
    $http.defaults.headers.common['Authorization'] = 'JWT ' + token;
  }

  $rootScope.$on('$locationChangeStart', () => {
    const pointsToSecuredLocation = (path) => !path.startsWith('/login') && !path.startsWith('/registration') && !path.startsWith('/activation');
    if (pointsToSecuredLocation($location.path()) && !$rootScope.globals.token) {
      $location.path('/login');
    }
  });
}
