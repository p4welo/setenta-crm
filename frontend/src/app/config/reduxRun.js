export default function ($ngRedux, authenticationReducer) {
  'ngInject';

  if ($ngRedux.addReducers) {
    $ngRedux.addReducers({
      authenticationModel: authenticationReducer
    });
  }
}
