export default {
  AUTH: {
    USER: () => `/api/auth`,
    LOGIN: () => `/api/v2/auth/login`,
    ACTIVATE: (id) => `/api/auth/activate/${id}`,
    REGISTER: () => `/api/auth/register`
  },
  USER: {
    GET: (userSid) => `/api/v2/users/${userSid}`,
    LIST: () => `/api/v2/users`
  },
  USER_TYPE: {
    GET: (userTypeSid) => `/api/v2/userTypes/${userTypeSid}`,
    LIST: () => `/api/v2/userTypes`
  },
  SCHOOL: {
    GET: (schoolSid) => `/api/v2/schools/${schoolSid}`,
    LIST: () => `/api/v2/schools`
  },
  PARTNER: {
    LIST: '/api/partner',
    RECENT: '/api/partner/recent',
  },
  COURSE: {
    LIST: (clientId) => `/api/clients/${clientId}/courses`,
    CREATE: (clientId) => `/api/clients/${clientId}/courses`,
    GET: (clientId, id) => `/api/clients/${clientId}/courses/${id}`,
    UPDATE: (clientId, id) => `/api/clients/${clientId}/courses/${id}`,
    UPDATE_LIST: (clientId) => `/api/clients/${clientId}/courses`,
    DELETE: (clientId, id) => `/api/clients/${clientId}/courses/${id}`,
    REGISTER: (clientId, id) => `/api/clients/${clientId}/courses/${id}/registrations`,
    REGISTRATION_LIST: (clientId, id) => `/api/clients/${clientId}/courses/${id}/registrations`
  },
  CUSTOMER: {
    FIND_BY_CODE: '/api/customer/byCode/'
  },
  REGISTRATION: {
    LIST: (clientId) => `/api/clients/${clientId}/registrations`
  },
  VISITOR: {
    LIST: (clientId) => `/api/clients/${clientId}/visitors`
  },
  ADVERTISEMENT: {
    LIST: (clientId) => `/api/clients/${clientId}/advertisements`,
    UPDATE: (clientId, id) => `/api/clients/${clientId}/advertisements/${id}`,
    ADD_NOTE: (clientId, advertisementId) => `/api/clients/${clientId}/advertisements/${advertisementId}/notes`
  },
  COMPETITION: {
    LIST: () => `/api/v2/competitions`,
    GET: (competitionSid) => `/api/v2/competitions/${competitionSid}`,
    SCHOOL_LIST: (competitionSid) => `/api/v2/competitions/${competitionSid}/schools`,

    FINANCE: {
      SUMMARY: () => `/api/competition/finances/summary`
    },
    SCHOOL: {
      GET: (id) => `/api/competition/schools/${id}`,
      DANCERS: (schoolId) => `/api/competition/schools/${schoolId}/dancers`,
      STARTING_NUMBERS: (schoolId) => `/api/competition/schools/${schoolId}/starting`
    },
    PERFORMANCE: {
      GET: (id) => `/api/competition/performances/${id}`,
      DELETE: (id) => `/api/competition/performances/${id}`
    },
    REPORT: {
      STARTING_LIST: () => `/api/competition/reports/starting`
    }
  }
}
