export default {
  "TEXTS.ARE_YOU_SURE": "Czy na pewno?",
  "DAYS": {
    "LONG": {
      "DAY0": "Poniedziałek",
      "DAY1": "Wtorek",
      "DAY2": "Środa",
      "DAY3": "Czwartek",
      "DAY4": "Piątek",
      "DAY5": "Sobota",
      "DAY6": "Niedziela"
    },
    "SHORT": {
      "DAY0": "PN",
      "DAY1": "WT",
      "DAY2": "ŚR",
      "DAY3": "CZ",
      "DAY4": "PT",
      "DAY5": "SB",
      "DAY6": "ND"
    },
    "ALL": "Wszystkie"
  },
  "DAYS.FULL.SATURDAY": "Sobota",
  "DAYS.FULL.SUNDAY": "Niedziela",
  "ADVERTISEMENTS.STILL_LOOKING": "W toku",
  "ADVERTISEMENTS.OUTDATED": "Przedawniony",
  "ADVERTISEMENTS.SUSPENDED": "Zawieszony",
  "ADVERTISEMENTS.FOUND": "Znaleziony",

  "LEVELS": {
    "BEG": "Początkujący",
    "INT": "Średniozaawansowany",
    "ADV": "Zaawansowany",
    "SHORT": {
      "BEG": "Pocz",
      "INT": "Śred",
      "ADV": "Zaaw"
    }
  },
  "COMPETITION.STYLES.HIP_HOP":"Hip-hop",
  "COMPETITION.STYLES.JAZZ":"Jazz",
  "COMPETITION.STYLES.CONTEMPORARY":"Taniec współczesny",
  "COMPETITION.STYLES.SHOW_DANCE":"Show dance",
  "COMPETITION.STYLES.BALLET":"Balet",
  "COMPETITION.STYLES.DISCO_DANCE":"Disco dance",
  "COMPETITION.STYLES.ART":"Art",
  "COMPETITION.STYLES.MODERN":"Nowoczesny",
  "COMPETITION.TYPES.SOLO":"Solo",
  "COMPETITION.TYPES.DUO":"Duet",
  "COMPETITION.TYPES.MINI_FORMATION":"Mini formacja",
  "COMPETITION.TYPES.FORMATION":"Formacja",
  "COMPETITION.AGE_LEVELS.TO_8": "do 8 lat",
  "COMPETITION.AGE_LEVELS.FROM_9_TO_11": "9-11 lat",
  "COMPETITION.AGE_LEVELS.FROM_12_TO_15": "12-15 lat",
  "COMPETITION.AGE_LEVELS.FROM_16": "powyżej 15 lat",
  "COMPETITION.EXP_LEVELS.DEBUT":"Debiuty",
  "COMPETITION.EXP_LEVELS.MASTER": "Mistrzowie",
  "TEXTS": {
    "COMMON": {
      "true": "Tak",
      "false": "Nie"
    },
    "NAVBAR": {
      "ENTRANCE": "Rejestruj wejście",
      "LOGOUT": "Wyloguj"
    },
    "LOGIN": {
      "TITLE": "Zaloguj się",
      "SUBTITLE": "Wprowadź dane logowania poniżej",
      "USERNAME": "Login",
      "PASSWORD": "Hasło",
      "SIGN_IN": "Zaloguj",
      "PASSWORD_RECOVERY": "Zapomniałeś hasła?"
    },
    "SCHEDULE": {
      "TITLE": "Grafik zajęć",
      "ADD_CLASS": "Dodaj",
      "COURSE": {
        "DAY": "Dzień",
        "TIME": "Godzina",
        "NAME": "Nazwa",
        "LEVEL": "Poziom",
        "INSTRUCTOR": "Instruktor",
        "ACTIONS": "Akcje",
        "DELETE_CONFIRM": "Czy na pewno chcesz usunąć?",
        "DELETE": "Usuń",
        "NO_ENTRIES": "Nie znaleziono żadnych wpisów",
        "PUBLIC": "Widoczność",
        "REGISTRATION": "Zapisy",
        "IS_PUBLIC": {
          "true": "Publiczny",
          "false": "Niepubliczny"
        },
        "IS_REGISTRATION_OPEN": {
          "true": "Otwarte",
          "false": "Zamknięte"
        },
        "STATE": {
          "LABEL": "Status",
          "REGISTRATION": {
            "LABEL": "Na zapisy",
            "DESCRIPTION": "Grupa oczekująca, zbierająca chętnych"
          },
          "IN_PROGRESS": {
            "LABEL": "Trwająca",
            "DESCRIPTION": "Grupa istniejąca"
          },
          "ON_HOLD": {
            "LABEL": "Czasowo wstrzymana",
            "DESCRIPTION": "Grupa czasowo wstrzymana, np. przez okres wakacyjny"
          },
          "CLOSED": {
            "LABEL": "Zamknięta",
            "DESCRIPTION": "Grupa definitywnie zamknięta"
          },
          "DELETED": {
            "LABEL": "Usunięta",
            "DESCRIPTION": "Grupa usunięta"
          }
        }
      }

    },
    "DASHBOARD": {
      "TITLE": "Dashboard"
    },
    "COURSE_LIST": {
      "TITLE": "Grafik zajęć",
      "SUBTITLE": "Zarządzanie grafikiem na stronie internetowej"
    },
    "COURSE_DETAILS": {
      "TITLE": "Szczegóły kursu",
      "SUBTITLE": "Zarządzanie grafikiem na stronie internetowej"
    },
    "VISITOR_LIST": {
      "TITLE": "Odwiedziny"
    },
    "REGISTRATION_LIST": {
      "TITLE": "Zapisy"
    },
    "ADVERTISEMENT_LIST": {
      "TITLE": "Partnerzy do tańca",
      "SUBTITLE": "Ogłoszenia klientów poszukujących partnera do tańca"
    },
    "ADVERTISEMENT_DETAILS": {
      "TITLE": "Szczegóły partnera",
      "SUBTITLE": "Ogłoszenia klientów poszukujących partnera do tańca"
    },
    "LIVE_CHAT": {
      "TITLE": "Live chat"
    },
    "CREATE_ENTRANCE": {
      "LABEL": "Zbliż kartę do skanera",
      "CARD_CODE": "Kod z karty",
      "SEARCH_BY_CODE": "Szukaj"
    },
    "HELP": {
      "CREATE_ENTRANCE": "Zeskanuj kod kreskowy znajdujący się na karcie posiadacza lub przepisz go w pole poniżej"
    }
  },
  "TEXTS.COMPETITION_REGISTRATION_LIST.TITLE": "Zgłoszenia na turniej",
  "TEXTS.COMPETITION_REGISTRATION_LIST.SUBTITLE": "Zestawienie zgłoszeń",
  "TEXTS.COMPETITION_STARTING_LIST.TITLE": "Listy startowe",
  "TEXTS.COMPETITION_STARTING_LIST.SUBTITLE": "",
  "TEXTS.COMPETITION_JUDGE_CARDS.TITLE": "Karty sędziów",
  "TEXTS.COMPETITION_JUDGE_CARDS.SUBTITLE": "",

  "TEXTS.USER_TYPE_LIST.TITLE": "Typy użytkowników",
  "TEXTS.USER_TYPE_LIST.SUBTITLE": "",
  "TEXTS.USER_TYPE_LIST.ADD_USER_TYPE": "Dodaj",
  "TEXTS.USER_TYPE_LIST.NAME": "Nazwa",
  "TEXTS.USER_TYPE_LIST.ACTIONS": "Akcje",

  "TEXTS.SCHOOL_LIST.TITLE": "Szkoły",
  "TEXTS.SCHOOL_LIST.SUBTITLE": "",
  "TEXTS.SCHOOL_LIST.ADD_SCHOOL": "Dodaj",
  "TEXTS.SCHOOL_LIST.NAME": "Nazwa",
  "TEXTS.SCHOOL_LIST.ADDRESS": "Adres",
  "TEXTS.SCHOOL_LIST.USER": "Użytkownik",
  "TEXTS.SCHOOL_LIST.ACTIONS": "Akcje",

  "TEXTS.USER_LIST.TITLE": "Użytkownicy",
  "TEXTS.USER_LIST.SUBTITLE": "",
  "TEXTS.USER_LIST.ADD_USER": "Dodaj",
  "TEXTS.USER_LIST.LOGIN": "Login",
  "TEXTS.USER_LIST.TYPE": "Typ",
  "TEXTS.USER_LIST.OBJECT_STATE": "Status",
  "TEXTS.USER_LIST.ACTIONS": "Akcje",

  "TEXTS.COMPETITION_LIST.TITLE": "Turnieje",
  "TEXTS.COMPETITION_LIST.SUBTITLE": "",
  "TEXTS.COMPETITION_LIST.ADD": "Dodaj",
  "TEXTS.COMPETITION_LIST.NAME": "Nazwa",
  "TEXTS.COMPETITION_LIST.ACTIONS": "Akcje",

  "TEXTS.COMPETITION_DETAILS.TITLE": "Szczegóły turnieju",
  "TEXTS.COMPETITION_DETAILS.SUBTITLE": "",


  "TEXTS.DASHBOARD.RECENT_REGISTRATIONS": "Zapisy w tym tygodniu"
};
