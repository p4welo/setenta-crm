import thunk from 'redux-thunk';

export default ($provide, $ngReduxProvider) => {
  'ngInject';

  const enhancer = [];
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    enhancer.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }
  $ngReduxProvider.createStoreWith(state => state, [thunk, 'notificationMiddleware'], enhancer);

  $provide.decorator('$ngRedux', ($delegate) => {
    const reducers = {};

    const rootReducer = (state, action) => {
      const keys = Object.keys(reducers);
      if (keys.length === 0) {
        return Object.assign({}, state);
      }
      const nextState = Object.assign({}, state);

      keys.forEach(key => {
        nextState[key] = reducers[key](state[key], action);
      });
      return nextState;
    };

    $delegate.addReducers = (reducersObject) => {
      Object.assign(reducers, reducersObject);
    };

    $delegate.replaceReducer(rootReducer);

    return $delegate;
  });
};