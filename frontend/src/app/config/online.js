export default function ($rootScope, $window) {
  'ngInject';

  $rootScope.online = navigator.onLine;
  $window.addEventListener("offline", () => {
    $rootScope.$apply(() => $rootScope.online = false);
  }, false);

  $window.addEventListener("online", () => {
    $rootScope.$apply(() => $rootScope.online = true);
  }, false);
}
