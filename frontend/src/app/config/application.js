export default function ($compileProvider, $urlRouterProvider, $translateProvider) {
  'ngInject';
  $compileProvider.debugInfoEnabled(false);
  $urlRouterProvider.otherwise('/dashboard');
  $translateProvider.preferredLanguage('pl');
}
