export default function ($stateProvider) {
  'ngInject';

  $stateProvider
      .state('login', {
        url: '/login',
        template: '<login></login>'
      })
      .state('register', {
        url: '/register',
        template: '<registration></registration>'
      })
      .state('activate', {
        url: '/activate/:id',
        controllerAs: '$ctrl',
        template: '<activation activation-id="$ctrl.activationId"></activation>',
        controller($stateParams) {
          'ngInject';
          this.activationId = $stateParams.id;
        }
      })
      .state('main', {
        abstract: true,
        template: '<main></main>'
      })

      //DASHBOARD
      .state('main.dashboard', {
        url: '/dashboard',
        template: '<dashboard></dashboard>',
        secured: true
      })

      //ADVERTISEMENT
      .state('main.advertisement', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.advertisement.list', {
        url: '/advertisement',
        template: '<advertisement-list></advertisement-list>'
      })
      .state('main.advertisement.details', {
        url: '/advertisement/:id',
        controllerAs: '$ctrl',
        template: '<advertisement-details advertisement-id="$ctrl.advertisementId"></advertisement-details>',
        controller($stateParams, $state) {
          'ngInject';
          if ($stateParams.id) {
            this.advertisementId = $stateParams.id;
          }
          else {
            $state.go('main.advertisement.list');
          }
        }
      })

      //REGISTRATIONS
      .state('main.registration', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.registration.list', {
        url: '/registration',
        template: '<registration-list></registration-list>'
      })

      //USERS
      .state('main.user', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.user.list', {
        url: '/user',
        template: '<user-list></user-list>'
      })

      //USER TYPES
      .state('main.userType', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.userType.list', {
        url: '/user-type',
        template: '<user-type-list></user-type-list>'
      })

      //SCHOOLS
      .state('main.school', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.school.list', {
        url: '/school',
        template: '<school-list></school-list>'
      })

      //COMPETITION
      // .state('main.competition', {
      //   template: '<ui-view></ui-view>',
      //   abstract: true
      // })
      // .state('main.competition.list', {
      //   url: '/competition',
      //   template: '<competition-list></competition-list>'
      // })
      // .state('main.competition.details', {
      //   url: '/competition/:id',
      //   abstract: true,
      //   controllerAs: '$ctrl',
      //   template: `<competition-details competition-id="$ctrl.competitionId"></competition-details>`,
      //   controller($state, $stateParams) {
      //     'ngInject';
      //
      //     if ($stateParams.id) {
      //       $state.go('main.competition.details.info', { id: this.competitionId = $stateParams.id });
      //     }
      //     else {
      //       $state.go('main.competition.list');
      //     }
      //   }
      // })
      // .state('main.competition.details.info', {
      //   url: '/info',
      //   template: '<pre>{{competition | json }}</pre>',
      //   controller() {
      //     'ngInject';
      //   }
      // })

      //VISITORS
      .state('main.visitor', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.visitor.list', {
        url: '/visitor',
        template: '<visitor-list></visitor-list>'
      })

      //VISITORS
      .state('main.chat', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.chat.list', {
        url: '/chat',
        template: '<chat-list></chat-list>'
      })

      //COMPETITION - REGISTRATIONS
      .state('main.competition-registration', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.competition-registration.list', {
        url: '/competition-registration',
        template: '<competition-registration-list></competition-registration-list>'
      })
      .state('main.competition.starting-list', {
        url: '/competition/starting-list',
        template: '<starting-list></starting-list>'
      })
      .state('main.competition.judge-cards', {
        url: '/competition/judge-cards',
        template: '<judge-cards></judge-cards>'
      })
      .state('main.competition.school-starting-list', {
        url: '/competition/school/:id/starting-list',
        controllerAs: '$ctrl',
        template: '<school-starting-list school-id="$ctrl.schoolId"></school-starting-list>',
        controller($stateParams, $state) {
          'ngInject';
          if ($stateParams.id) {
            this.schoolId = $stateParams.id;
          }
          else {
            $state.go('main.competition.starting-list');
          }
        }
      })

      //COURSE
      .state('main.course', {
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('main.course.list', {
        url: '/course',
        template: '<course-list></course-list>'
      })
      .state('main.course.details', {
        url: '/course/:id',
        controllerAs: '$ctrl',
        template: '<course-details course-id="$ctrl.courseId"></course-details>',
        controller($stateParams, $state) {
          'ngInject';
          if ($stateParams.id) {
            this.courseId = $stateParams.id;
          }
          else {
            $state.go('main.course.list');
          }
        }
      })

      .state('main.newCustomer', {
        url: '/card/:code/new-customer',
        template: '<new-customer></new-customer>',
        secured: true
      });
}
