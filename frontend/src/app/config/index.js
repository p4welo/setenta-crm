import angular from 'angular';
import ngCookies from 'angular-cookies';
import ngRedux from 'ng-redux';
import application from './application';
import appRoutes from './appRoutes';
import translations from './translations';
import authentication from './authentication';
import uiBootstrap from './uiBootstrap';
import apiRoutes from './apiRoutes';
import reduxRun from './reduxRun';
import reduxConfig from './reduxConfig';

export default angular.module('config', [
  ngCookies,
  ngRedux,
])
    .config(application)
    .config(appRoutes)
    .config(translations)
    .config(reduxConfig)
    .run(authentication)
    .run(uiBootstrap)
    .run(reduxRun)
    .constant('API_ROUTES', apiRoutes)
    .name;