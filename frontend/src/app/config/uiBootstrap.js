export default function (uibButtonConfig) {
  'ngInject';

  uibButtonConfig.activeClass = 'btn-primary';
}
