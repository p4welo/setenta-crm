import React from 'react';
import logo from '../images/logo.png';
import { Link } from '@reach/router';

const Component = React.Component;

class Navbar extends Component {

  scrollToFeatures() {
    const el = document.getElementById('learnMore');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  scrollToPriceList() {
    const el = document.getElementById('price-list');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  scrollToReferences() {
    const el = document.getElementById('references');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  scrollToContact() {
    const el = document.getElementById('contact-form');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  render() {
    return (
        <div className="navbar navbar-default navbar-fixed-top" style={{ position: 'fixed' }}>
          <div className='container'>
            <div className="navbar-header" style={{ minWidth: '160px', float: 'left' }}>
              <Link to='' className="navbar-brand" style={{ paddingTop: '11px' }}>
                <img src={logo} alt="" style={{
                  marginTop: 0,
                  height: `22px`
                }}/>
              </Link>
            </div>

            <ul className="nav navbar-nav navbar-right" style={{ float: 'right' }}>
              <li style={{ float: 'left' }} className='hidden-xs'>
                <a onClick={this.scrollToFeatures}>Funkcje</a>
              </li>
              <li style={{ float: 'left' }} className='hidden-xs'>
                <a onClick={this.scrollToPriceList}>Cennik</a>
              </li>
              <li style={{ float: 'left' }} className='hidden-xs'>
                <a onClick={this.scrollToReferences}>Referencje</a>
              </li>
              <li style={{ float: 'left' }} className='hidden-xs'>
                <Link to='/privacy/'>Polityka prywatności</Link>
              </li>
              <li style={{ float: 'left' }} className='hidden-xs'>
                <a onClick={this.scrollToContact}>Kontakt</a>
              </li>
              <li style={{ float: 'left' }}>
                <a href="https://moja.kartazgloszen.pl"
                    target="_blank"
                    rel="noopener noreferrer nofollow"
                    style={{backgroundColor: `#00BCD4`, marginRight: '0'}}
                    className="btn btn-info text-white navbar-btn pt-10 pb-10 mt-5 mb-5 ">
                  Przejdź do aplikacji
                </a>
              </li>
            </ul>
          </div>
        </div>
    )
  }
}

export default Navbar;