import React from 'react';
import top from '../images/top-logo.jpg';
import trendy from '../images/trendy-logo.jpg';
import pc from '../images/pc.png';
import managerPhone from '../images/manager-phone.jpg';
import managerWoman from '../images/manager-woman.jpg';
import magiatanca from '../images/magiatanca.png';
import pasjatanca from '../images/pasja.png';
import latinfitness from '../images/latinfitness.png';
import goldcontest from '../images/gold-contest.jpg';
import setenta from '../images/setenta.jpg';
import pceik from '../images/pceik.jpg';
import poezja from '../images/poezja.png';
import cooltura from '../images/cooltura.png';
import payment from '../images/payment.png';
import time from '../images/time.jpg';
import Navbar from '../components/navbar';
import SEO from '../components/seo';

const Component = React.Component;

class IndexPage extends Component {
  scrollToDetails() {
    const el = document.getElementById('learnMore');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  scrollToContactForm() {
    const el = document.getElementById('contact-form');
    if (el) {
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: el.offsetTop
      });
    }
  }

  render() {
    return (
        <>
          <SEO title='Strona główna'/>
          <Navbar/>

          <section className="hero">
            <div className='hero-inner'>
              <div className="pl-10 pr-10" style={{ marginTop: '100px' }}>
                <h1 className="no-margin">Organizatorze!</h1>
                <h1 className="no-margin" style={{fontSize: '3em'}}>My wiemy, co spędza Ci sen z powiek</h1>
                <h2 className="mb-20"></h2>
                <div className='text-center mt-50 main-buttons'>
                  <button onClick={this.scrollToContactForm}
                      className="btn btn-info btn-xlg">
                    <i className="far fa-envelope position-left"></i>
                    <span>wypróbuj&nbsp;</span>
                    ZA DARMO
                  </button>
                  <button onClick={this.scrollToDetails}
                      className="btn btn-flat border-white text-white btn-xlg">
                    dowiedz się więcej
                  </button>
                </div>
              </div>
            </div>
            <div className='text-center main-image'>
              <img src={pc} alt='screenshot'/>
            </div>
          </section>

          <section id='references' style={{
            width: '100%',
            background: 'white',
            paddingBottom: '50px',
            paddingTop: '30px'
          }}>
            <div className='container'>
              <h1 className='text-bold mb-30'>Nasi użytkownicy</h1>
              <div className='row'>
                <div className='col-sm-6'>
                  <blockquote className="mb-20">
                    <img className="img-circle"
                        src='https://avatars.dicebear.com/v2/jdenticon/magda-mroz-murawska.svg'
                        alt="magda mroz murawska"/>
                    Bardzo szybki sposób rejestracji (standardowe karty to jakiś koszmar), po
                    turnieju w każdej chwili można sprawdzić wyniki całych kategorii i mega
                    sprawna
                    obsługa.
                    <footer>
                      Magda Mróz-Murawska
                      <br/>
                      <cite>Instruktor Studio tańca BellArt, Wrocław</cite>
                    </footer>
                  </blockquote>
                </div>
                <div className='col-sm-6'>
                  <blockquote className="mb-20">
                    <img className="img-circle"
                        src='https://avatars.dicebear.com/v2/jdenticon/agata-judkowiak.svg'
                        alt="magda mroz murawska"/>
                    Świetna aplikacja, która: usprawnia pracę organizatora, ułatwia proces
                    przyjmowania zgłoszeń, daje możliwość szybkiej obsługi zgłaszających się
                    zespołów.
                    Karta zgłoszeń jest łatwa w obsłudze i niezawodna, a to w szczególności
                    dzięki administratorowi, który wraz z możliwością zarządzania konkursem
                    poprzez aplikację, oferuje swoją pomoc kiedy tylko jest potrzebna. POLECAM!!!
                    <footer>
                      Agata Judkowiak
                      <br/>
                      <cite>Kierownik XVII Ogólnopolskiego Konkursu Tanecznego "Magia Tańca" im.
                        Aliny Janikowskiej, Ostrów Wielkopolski</cite>
                    </footer>
                  </blockquote>
                </div>
              </div>
            </div>
          </section>

          <div className='content'>
            <div className="container">
              <h1 className='text-bold mb-30'>Zaufali nam</h1>

              <div className='competitions'>
                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ top } alt='turniej top'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ trendy } alt='festiwal trendy'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ pasjatanca } alt='cheerlederki pasja'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ goldcontest } alt='gold contest'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ pceik } alt='pceik'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ magiatanca } alt='magia tanca'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ setenta } alt='setenta'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ poezja } alt='poezja'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ cooltura } alt='cooltura'/>
                  </a>
                </div>

                <div className='competitions__item mb-20'>
                  <a href='https://moja.kartazgloszen.pl'>
                    <img src={ latinfitness } alt='latinfitness'/>
                  </a>
                </div>
              </div>

              <div className='row feature-row' id='learnMore'>
                <div className='col-sm-6'>
                  <img src={ managerWoman } alt='manaqger'/>
                </div>

                <div className='col-sm-6 pr-20 pl-20'>
                  <h2 className='text-bold mb-30'>Maksymalna kontrola nad
                    turniejem</h2>
                  <ul className="media-list">
                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-tablet-alt text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Wygodny dostęp z poziomu telefonu, tabletu i komputera
                          </label>
                        </h6>
                        <p>w każdym miejscu i o każdej porze</p>
                      </div>
                    </li>

                    <li className="media">
                      <div className="media-left">
                        <i className="far fa-list-alt text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Przejrzyste zestawienie zgłoszonych szkół, tancerzy
                          </label>
                        </h6>
                        <p>z podziałem na dni i kategorie</p>
                      </div>
                    </li>

                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-music text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Obsługa muzyki
                          </label>
                        </h6>
                        <p>pobieraj pliki audio w kolejności występów na turnieju</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div className='row feature-row'>
                <div className='col-sm-6 switched'>
                  <img src={payment} alt='listy startowe'/>
                </div>

                <div className='col-sm-6 pl-20 pr-20 '>
                  <h2 className='text-bold mb-30'>Obsługa płatności</h2>
                  <ul className="media-list">
                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-search-dollar text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Nadzór nad płatnościami
                          </label>
                        </h6>
                        <p>przejrzyste podsumowanie opłaty startowej dla każdej szkoły</p>
                      </div>
                    </li>

                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-file-invoice-dollar text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Integracja z popularnymi systemami księgowymi
                          </label>
                        </h6>
                        <p>jednym kliknięciem wystawiaj i wysyłaj faktury swoim klientom</p>
                      </div>
                    </li>

                    <li className="media">
                      <div className="media-left">
                        <i className="far fa-credit-card text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Integracja z systemem płatności online
                          </label>
                        </h6>
                        <p>automatyczne księgowanie płatności - opcja</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div className='row feature-row'>
                <div className='col-sm-6'>
                  <img src={managerPhone} alt='listy startowe'/>
                </div>

                <div className='col-sm-6 pl-20 pr-20 '>
                  <h2 className='text-bold mb-30'>Trzymaj rękę na pulsie</h2>
                  <ul className="media-list">
                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-sms text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Powiadomienia SMS/email na żywo
                          </label>
                        </h6>
                        <p>otrzymuj powiadomienia o nowych zgłoszeniach i przepełnionych
                          kategoriach</p>
                      </div>
                    </li>

                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-chart-line text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Zestawienia i raporty przydatne podczas przygotowań
                          </label>
                        </h6>
                        <p>zestawienia zapisanych szkół, przewidywane wpływy, zapotrzebowanie na
                          szatnie itp.</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div className='row feature-row'>
                <div className='col-sm-6 switched'>
                  <img src={time} alt='czas wolny'/>
                </div>

                <div className='col-sm-6 pr-20 pl-20'>
                  <h2 className='text-bold mb-30'>Oszczędność czasu</h2>

                  <ul className="media-list">
                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-sync-alt text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            Automatyczne generowanie list i dokumentów
                          </label>
                        </h6>
                        <p>numerów startowych, list startowych, arkuszy jury, dyplomów, teczki
                          uczestnika, itp.</p>
                      </div>
                    </li>
                    <li className="media">
                      <div className="media-left">
                        <i className="fas fa-gavel text-blue position-left"></i>
                      </div>
                      <div className="media-body">
                        <h6 className="media-heading">
                          <label className="no-margin text-bold">
                            RODO
                          </label>
                        </h6>
                        <p>gwarantujemy bezpieczeństwo i prywatność Twoich klientów zgodnie z
                          obowiązującymi przepisami</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div className='text-center main-buttons'>
                <button onClick={this.scrollToContactForm}
                    className="btn btn-info btn-xlg">
                  <i className="far fa-envelope position-left"></i>
                  <span>wypróbuj&nbsp;</span>
                  ZA DARMO
                </button>
              </div>
            </div>
          </div>

          <section id='price-list'>
            <div className='container'>
              <h2 className="text-bold mb-30">Cennik</h2>

              <div className="panel panel-price">
                <div className="table-responsive">
                  <table className="table table-hover table-striped table-bordered text-center">
                    <thead>
                    <tr>
                      <th>Wybierz pakiet</th>
                      <th className="text-center">
                        <h6 className="text-semibold mt-5 mb-5">Podstawowy</h6>
                        <h4 className="text-semibold no-margin">
                          399 zł&nbsp;
                          <span className="text-size-base text-muted text-regular">/ turniej</span>
                        </h4>
                      </th>
                      <th className="text-center">
                        <h6 className="text-semibold mt-5 mb-5">Rozszerzony</h6>
                        <h4 className="text-semibold no-margin">
                          549 zł&nbsp;
                          <span className="text-size-base text-muted text-regular">/ turniej</span>
                        </h4>
                      </th>
                      <th className="text-center">
                        <h6 className="text-semibold mt-5 mb-5">Pełny</h6>
                        <h4 className="text-semibold no-margin">
                          <span className="text-size-base text-muted text-regular">wkrótce!</span>
                        </h4>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td className="text-left">
                        Zapisy internetowe
                      </td>
                      <td>
                        <p className='text-muted no-margin-bottom'>bez limitu uczestników</p>
                      </td>
                      <td>
                        <p className='text-muted no-margin-bottom'>bez limitu uczestników</p>
                      </td>
                      <td>
                        <p className='text-muted no-margin-bottom'>bez limitu uczestników</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Kontrola płatności</td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">
                        <p className='no-margin-bottom'>Listy startowe</p>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">
                        <p className='no-margin-bottom'>Numery startowe</p>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">
                        <p className='no-margin-bottom'>Arkusze jury</p>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">
                        <p className='no-margin-bottom'>Powiadomienia email</p>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Wyniki</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Obsługa muzyki</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Księgowość</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Powiadomienia SMS</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Płatności online</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left">Wydruk dyplomów</td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-danger'>
                            <i className="fas fa-times"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left text-semibold">Wsparcie 24/7</td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-left text-semibold">Gwarancja zwrotu pieniędzy</td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                      <td>
                        <span className='badge badge-success'>
                            <i className="fas fa-check"></i>
                        </span>
                      </td>
                    </tr>

                    {/*<tr className="border-solid">*/}
                    {/*  <td></td>*/}
                    {/*  <td><a className="btn bg-blue legitRipple" href="#"><i*/}
                    {/*      className="icon-cart position-left"></i> Order</a></td>*/}
                    {/*  <td><a className="btn bg-blue legitRipple" href="#"><i*/}
                    {/*      className="icon-cart position-left"></i> Order</a></td>*/}
                    {/*</tr>*/}
                    </tbody>
                  </table>
                </div>

                <div className='panel-footer panel-footer-condensed'>
                  <h4 className='text-center'>Możliwa wycena wg indywidualnych potrzeb</h4>
                </div>
              </div>
            </div>


          </section>

          <section id='contact-form' style={{ minHeight: '150px', paddingTop: '50px' }}>
            <div className='container'>
              <div className='col-sm-6 col-sm-offset-3'>
                <h2 className='no-margin-bottom'>Dołącz do nas</h2>
                <h4>
                  lub zadzwoń po więcej informacji:
                  <a href='tel:793432042' className='position-right btn btn-default'>793 432 042</a>
                </h4>
                <div className='panel panel-flat'>
                  <div className='panel-body'>
                    <form action="https://formspree.io/p4welo@gmail.com" method="POST">
                      <div className="form-group">
                        <input type="text"
                            className="form-control"
                            placeholder="Nazwa turnieju"
                            name='turniej'/>
                      </div>
                      <div className="form-group">
                        <input type="text"
                            className="form-control"
                            placeholder="Imię i nazwisko"
                            name='organizator'/>
                      </div>
                      <div className="form-group">
                        <input type="text"
                            className="form-control"
                            placeholder="Telefon"
                            name='telefon'/>
                      </div>
                      <div className="form-group">
                        <input type="email"
                            className="form-control"
                            placeholder="Email"
                            name='email'/>
                      </div>
                      <div className="form-group">
                            <textarea className="form-control"
                                placeholder="Komentarz"
                                rows='5'
                                name='komentarz'/>
                      </div>
                      <button className='btn btn-info' type='submit'>Wyślij</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
    );
  }
}

export default IndexPage;