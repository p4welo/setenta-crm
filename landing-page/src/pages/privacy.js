import React from 'react'
import Navbar from '../components/navbar';
import SEO from '../components/seo';

const PrivacyPage = () => (
    <>
      <SEO title='Polityka prywatności'/>
      <Navbar/>

      <div className='container'>
        <h3 className='text-bold'>Polityka Prywatności oraz Regulamin stron www należących do Pawła
          Radomskiego</h3>

        <ol type='I'>
          <li className='mt-20'>
            <p className='text-bold'>Niniejszy Regulamin został przygotowany w oparciu o przepisy
              prawa obowiązujące na
              terytorium Rzeczpospolitej Polskiej i określa zasady funkcjonowania stron należących
              do,
              nazwanych dalej witrynami.</p>
            <p>Regulamin określa obowiązki i zakres odpowiedzialności witryn jako podmiotu
              zarządzającego
              i prowadzącego strony.</p>
            <ol>
              <li>Administratorem witryn jest Paweł Radomski.</li>
              <li>Witryny wraz z wszystkimi jej składowymi jest chroniona przepisami prawa,
                w szczególności Ustawy z dnia 04.02.1994 r. o prawie autorskim i prawach pokrewnych
                (Dz. U. z
                2000r. Nr 80 Poz. 904 z późn. zm.) i Ustawy z dnia 16.04.1993 r. o zwalczaniu
                nieuczciwej
                konkurencji (Dz. U. z 1993 Nr 47 Poz. 211 z późn. zm.
              </li>
              <li>Autorzy zamieszczanych utworów graficznych oraz treści nie wyrażają zgody na ich
                jakąkolwiek publikację lub modyfikację w jakikolwiek sposób (wydawnictwach,
                publikacjach,
                prezentacjach, stronach internetowych i w inny sposób ujęty w ustawie o ochronie
                praw
                autorskich) bez ich pisemnej zgody.
              </li>
              <li>W przypadku bezprawnego ich wykorzystywania (bez względu na to, czy działania
                związane z wykorzystaniem grafik i treści miały charakter komercyjny, czy też
                niekomercyjny),
                zarówno Właściciel strony jak i jej autorzy grafik będą występowali na drogę prawną
                w celu
                wyegzekwowania przysługujących im praw określonych w Ustawie o Ochronie Praw
                Autorskich.
              </li>
              <li>Użytkownik ma prawo do korzystania z całości treści opublikowanych na witrynach
                internetowych, pod warunkiem nienaruszania praw autorskich. Żadna część strony nie
                może być
                wykorzystywana w celach komercyjnych bez uprzedniej zgody właściciela strony.
              </li>
              <li>Właściciel strony jak i jej autorzy nie ponoszą odpowiedzialności za straty
                moralne oraz
                finansowe poniesione w wyniku wykorzystania treści znajdujących się na witrynach.
              </li>
              <li>Witryny internetowe wykorzystują ciasteczka "Cookies", które służą identyfikacji
                przeglądarki podczas korzystania ze strony. Ciasteczka nie zbierają żadnych danych
                osobowych.
                Zbierane dane jak np. rodzaj przeglądarki użytkownika, czas spędzony na stronie itd.
                są
                wykorzystywane przy analizie statystyk poszczególnych witryn.
              </li>
              <li>Oferta handlowa przedstawiona na witrynach nie stanowi oferty w rozumieniu
                przepisów
                Kodeksy Cywilnego art. 66 §1 kodeksu cywilnego oraz innych właściwych przepisów
                prawnych.
                Każda strona ma charakter wyłącznie informacyjny.
              </li>
              <li>Do wszelkich kwestii nieuregulowanych w niniejszym polityce prywatności mają
                zastosowanie odpowiednie przepisy prawa.
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Ochrona Danych Osobowych</p>
            <ol>
              <li>Dane osobowe są przetwarzane przez Pawła Radomskiego zgodnie z przepisami
                Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016
                r. w
                sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w
                sprawie
                swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (zwane dalej:
                RODO), a także na podstawie innych przepisów regulujących ochronę danych osobowych
                osób
                fizycznych.
              </li>
              <li>Dane osobowe zbierane poprzez wszelkiego rodzaju formularze zamieszczone na
                witrynach internetowych Administratora, obejmujące m.in.: takie informacje takie
                jak: imię i
                nazwisko, telefon kontaktowy, adres e -mail, adres www oraz nazwę firmy, służą tylko
                do
                identyfikacji Klienta, nawiązania z nimi kontaktu handlowego, przekazania Klientowi
                darmowych
                materiałów przygotowanych przez Administratora, przekazania wyceny usług
                realizowanych
                przez Administratora, prowadzenia negocjacji handlowych z Klientem i ewentualnie
                podpisania i
                realizacji umowy. Jeżeli Klient wyrazi na to zgodę, to przekazane przez niego dane
                osobowe, będą
                wykorzystywane przez Administratora w celach marketingowych własnych produktów i
                usług lub
                marketingu produktów i usług podmiotów trzecich oraz dla celów monitorowania ruchu
                na stronie
                internetowej Administratora (profilowanie).
              </li>
              <li>Dane osobowe zbierane poprzez wszelkiego rodzaju formularze zamieszczone na
                witrynach internetowych, będą przetwarzane przez Administratora przez czas niezbędny
                do
                przekazania Klientom darmowych materiałów czy wyceny usług Administratora, przez
                czas
                trwania negocjacji handlowych zmierzających do podpisania umowy (nie dłużej jednak
                niż przez
                rok od dnia otrzymania przez Klienta oferty handlowej), a dalej przez czas wykonania
                umowy czy
                realizacji usługi, a także przez czas, w którym Administrator jest obowiązany do
                przechowywania
                dokumentów sprzedaży.
              </li>
              <li>Dane osobowe zbierane poprzez wszelkiego rodzaju formularze zamieszczone na
                witrynach internetowych Administratora są przetwarzane na podstawie zgody osoby
                (art. 6 ust. 1
                lit. a RODO), a także na podstawie art. 6 ust. 1 lit. b) RODO, tj. ich przetwarzanie
                jest niezbędne
                do wykonania umowy, której stroną jest osoba, której dane dotyczą, lub do podjęcia
                działań na
                żądanie osoby, której dane dotyczą, przed zawarciem umowy.
              </li>
              <li>W każdym czasie zgoda na przetwarzanie danych osobowych może być przez Klientów
                Administratora cofnięta. Cofnięcie zgody na przetwarzanie danych nie ma wpływu na
                zgodność z
                prawem przetwarzania danych dokonanych przez Administratora na podstawie zgody przed
                jej
                cofnięciem.
              </li>
              <li>Cofnięcie zgody może nastąpić poprzez wysłanie przez Państwa na adres e-mail:
                kontakt@kartazgloszen.pl stosownego oświadczenia, którego przykład przedstawiamy
                poniżej:
                "Działając w imieniu (nazwa podmiotu) z siedzibą w (adres siedziby) jako jej
                (funkcja osoby
                składające oświadczenie wraz z wykazaniem jej umocowania do reprezentacji podmiotu)/
                albo Ja -
                imię i nazwisko, adres zamieszkania - niniejszym cofam zgodę na przetwarzanie przez
                Pawła
                Radomskiego moich danych osobowych".
              </li>
              <li>Kategoriami odbiorców, którym dane osobowe Klientów Administratora mogą zostać
                ujawnione są: pracownicy, współpracownicy Administratora w szczególności podmioty
                zajmujące
                się fizyczną administracją serwerów, na których dane są przechowywane jak też
                podmioty stale
                współpracujące z Pawłem Radomskim w ramach organizowania kursów tańca, imprez
                masowych,
                w tym turniejów tanecznych, biuro rachunkowe, kancelarie prawne, krajowe rejestry
                długów.
                Wszystkie te osoby zostały przeszkolone przez Administratora w zakresie należytego
                przetwarzania danych osobowych i zobowiązały się do ich przetwarzania w sposób
                gwarantujący
                najwyższy poziom bezpieczeństwa tych danych.
              </li>
              <li>Paweł Radomski nie przekazuje i nie będzie przekazywał Państwa danych do odbiorców
                w państwach trzecich lub do organizacji międzynarodowych.
              </li>
              <li>Paweł Radomski przetwarza dane osobowe Klientów za pomocą systemów
                komputerowych i oprogramowania zapewniającego bezpieczeństwo przetwarzania tych
                danych
                osobowych na najwyższym poziomie (takie jak m.in. szyfrowanie i anonimizacja
                przesyłanych
                informacji, cykliczne zmiany haseł dostępu do systemów). Administrator przetwarza
                dane
                osobowe swoich Klientów poza systemem informatycznym przy użyciu środków
                technicznych i
                organizacyjnych zapewniających najwyższy poziom bezpieczeństwa przetwarzania danych
                osobowych.
              </li>
              <li>Osoba, której dane dotyczą, ma prawo dostępu do treści swoich danych i ich
                sprostowania, usunięcia (prawo do bycia zapomnianym), ograniczenia ich
                przetwarzania, prawo
                do przenoszenia danych, prawo do cofnięcia zgody na ich przetwarzanie w dowolnym
                momencie
                bez wpływu na zgodność z prawem przetwarzania dokonanego na podstawie zgody przed
                jej
                cofnięciem.
              </li>
              <li>Osoba, której dane dotyczą, ma prawo dno wniesienia sprzeciwu od przetwarzania
                swoich
                danych przez Administratora. Sprzeciw powinien być wniesiony na adres:
                kontakt@kartazgloszen.pl.
              </li>
              <li>Gdy osoba, której dane dotyczą uzna, że przetwarzanie przez Administratora jej
                danych
                osobowych narusza przepisy Rozporządzenia o ochronie danych osobowych, ma prawo
                wnieść
                skargę do Generalnego Inspektora Ochrony Danych Osobowych, który po wejściu w życie
                Rozporządzenia o ochronie danych osobowych zostanie zastąpiony przez Prezesa Urzędu
                Ochrony Danych Osobowych.
              </li>
              <li>W przypadku naruszenia ochrony danych osobowych, Administrator bez zbędnej zwłoki
                w miarę możliwości, nie później niż w terminie 72 godzin po stwierdzeniu naruszenia
                - zgłasza je
                organowi nadzorczemu (Generalnemu Inspektorowi Ochrony Danych Osobowych, a po 25
                maja
                2018 r. Prezesowi Urzędu Ochrony Danych Osobowych), chyba że jest mało
                prawdopodobne, by
                naruszenie to skutkowało ryzykiem naruszenia praw lub wolności osób fizycznych. Do
                zgłoszenia
                przekazanego organowi nadzorczemu po upływie 72 godzin, Administrator dołącza
                wyjaśnienie
                przyczyn opóźnienia. Jeżeli naruszenie ochrony danych osobowych może powodować
                wysokie
                ryzyko naruszenia praw lub wolności osób fizycznych, Administrator bez zbędnej
                zwłoki
                zawiadamia osobę, której dane dotyczą, o takim naruszeniu.
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Zasady przetwarzania danych osobowych przez Administratora
              Danych Osobowych
              (Pawła Radomskiego)</p>

            <p className='text-bold'>Paweł Radomski przy przetwarzaniu danych osobowych przestrzega
              następujących
              zasad:</p>
            <ol>
              <li>
                <p className='text-bold'>Zasada zgodności z prawem, rzetelności i przejrzystości przetwarzania danych</p>
                <p>Wedle
                  której dane są przetwarzane przez Administratora zgodnie z prawem, rzetelnie i w
                  sposób
                  przejrzysty dla osoby, której dane dotyczą.</p>
              </li>
              <li>
                <p className='text-bold'>Ograniczenie celu przetwarzania danych</p>
                <p>Dane są
                  zbierane przez Administratora
                  w sposób zgodny z celami ich przetwarzania. Dalsze przetwarzanie do celów
                  archiwalnych w
                  interesie publicznym, do celów badań naukowych lub historycznych lub do celów
                  statystycznych
                  nie jest uznawane przez Administratora za niezgodne z pierwotnymi celami.</p>
              </li>
              <li>
                <p className='text-bold'>Minimalizacja ilości przetwarzania danych</p>
                <p>Administrator przetwarza taką ilość
                  danych, która jest adekwatna, stosowna oraz ograniczona do tego, co niezbędne do
                  celów, w
                  których są przetwarzane.</p>
              </li>
              <li>
                <p className='text-bold'>Prawidłowość przetwarzanych danych</p>
                <p>Administrator przetwarza tylko dane prawidłowe
                  i w razie potrzeby uaktualniane. Administrator podejmuje wszelkie rozsądne
                  działania, aby dane
                  osobowe, które są nieprawidłowe w świetle celów ich przetwarzania, zostały
                  niezwłocznie
                  usunięte lub sprostowane.</p>
              </li>
              <li>
                <p className='text-bold'>Ograniczenie czasookresu i celów, dla których dane mogą być przechowywane</p>
                <p>Administrator przechowuje dane w formie umożliwiającej identyfikację osoby, której
                  dane
                  dotyczą, przez okres nie dłuższy, niż jest to niezbędne do celów, w których dane te
                  są
                  przetwarzane. Administrator może przechowywać dane osobowe przez okres dłuższy, o
                  ile będą
                  one przechowywane do celów statystycznych. Administrator wdroży jednocześnie
                  odpowiednie
                  środki techniczne i organizacyjne w celu ochrony praw i wolności osób, których dane
                  te dotyczą.</p>
              </li>
              <li>
                <p className='text-bold'>Zapewnienie przetwarzania danych w sposób integralny i poufny</p>
                <p>Przetwarza dane w
                  sposób zapewniający odpowiednie bezpieczeństwo danych osobowych, w tym ochronę przed
                  niedozwolonym lub niezgodnym z prawem przetwarzaniem oraz przypadkową utratą,
                  zniszczeniem lub uszkodzeniem, za pomocą odpowiednich środków technicznych lub
                  organizacyjnych.</p>
              </li>
              <li>
                <p className='text-bold'>Zasada rozliczalności</p>
                <p>Paweł Radomski jest
                  odpowiedzialny za przestrzeganie
                  wszystkich wymienionych w punktach 1 - 6 zasad przetwarzania danych osobowych i jest
                  w
                  stanie wykazać ich przestrzeganie. Administrator wdraża odpowiednie środki
                  techniczne i
                  organizacyjne, które zapewniają najwyższy poziom bezpieczeństwa przetwarzanych przez
                  niego
                  danych osobowych, a to wedle zasad wymienionych powyżej.</p>
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Obowiązki informacyjne Administratora danych osobowych</p>
            <p>W zależności czy Paweł Radomski zbiera dane od osoby, której dane dotyczą, czy w
              sposób inny niż
              od osoby, której dane dotyczą, Rozporządzenie unijne o ochronie danych osobowych
              nakłada na
              Administratora osobne wymogi informacyjne.</p>
            <p className='text-bold'>Obowiązki informacyjne Administratora danych osobowych wspólne
              zarówno dla sytuacji, gdy
              dane zostały zebrane od osoby, której dane dotyczą, jak i w sposób pośredni:</p>

            <ol>
              <li>Obowiązek podania tożsamości administratora, danych kontaktowych oraz tożsamości i
                danych kontaktowych przedstawiciela administratora.
              </li>
              <li>Obowiązek podania danych kontaktowych inspektora ochrony danych (jeżeli takowy
                został
                ustanowiony).
              </li>
              <li>Obowiązek wskazania celów przetwarzania danych osobowych oraz podstawy prawnej
                przetwarzania.
              </li>
              <li>Obowiązek podania informacji o odbiorcach danych osobowych lub o kategoriach
                odbiorców, którym dane będą przekazywane.
              </li>
              <li>Obowiązek informowania o zamiarze przekazania danych osobowych do państwa
                trzeciego
                lub organizacji międzynarodowej, gdy takowa sytuacja zachodzi.
              </li>
              <li>Obowiązek wskazania okresu, przez który dane osobowe będą przechowywane, a gdy nie
                jest to możliwe, wskazanie kryteriów ustalania tego okresu.
              </li>
              <li>Obowiązek podania informacji o uprawnieniach osób, których dane dotyczą.</li>
              <li>Obowiązek podania informacji o prawie do wniesienia skargi do organu nadzorczego
                (podaje się pełną nazwę tego organu oraz jego adres).
              </li>
              <li>Obowiązek informowania osoby, której dane dotyczą, czy podanie danych osobowych
                jest
                wymogiem ustawowym lub umownym lub warunkiem zawarcia umowy oraz czy osoba, której
                dane dotyczą, jest zobowiązana do ich podania i jakie są ewentualne konsekwencje
                niepodania
                danych.
              </li>
              <li>Obowiązek poinformowania o zautomatyzowanym podejmowaniu decyzji, w tym
                o profilowaniu
              </li>
              <li>
                <p>Obowiązek informowania o nowym celu przetwarzania danych, niż ten, który został
                  wskazany w pierwotnej informacji:</p>
                <p className='text-bold'>Obowiązki informacyjne administratora, gdy dane nie zostały
                  zebrane od osoby, której dotyczą
                  (tj. w sposób pośredni)</p>
                <p>Paweł Radomski w takim przypadku musi dodatkowo poinformować osobę, której dane
                  dotyczą, o:</p>

                <ol type='a'>
                  <li>kategoriach odnośnych danych osobowych, które są przetwarzane - a więc rodzaju
                    przetwarzanych danych, np. imię, nazwisko, adres, data urodzenia itp.
                  </li>
                  <li>źródle pochodzenia danych, a gdy ma to zastosowanie - czy pochodzą one ze
                    źródeł
                    publicznie dostępnych.
                  </li>
                </ol>

                <p className='text-bold mt-20'>Zwolnienie administratora, który pozyskał dane w sposób
                  pośredni, z obowiązku
                  informacyjnego</p>
                <p>Administrator, który pozyskał dane w sposób pośredni, nie jest obowiązany do
                  wypełnienia
                  obowiązku informacyjnego, gdy - i w zakresie, w jakim:</p>
                <ol type='a'>
                  <li>osoba, której dane dotyczą, dysponuje już tymi informacjami;</li>
                  <li>udzielenie takich informacji okazuje się niemożliwe lub wymagałoby
                    niewspółmiernie
                    dużego wysiłku; w szczególności w przypadku przetwarzania danych do celów
                    archiwalnych w
                    interesie publicznym, do celów badań naukowych lub historycznych lub do celów
                    statystycznych;
                  </li>
                  <li>pozyskiwanie lub ujawnianie jest wyraźnie uregulowane prawem Unii lub prawem
                    państwa członkowskiego, któremu podlega administrator, przewidującym odpowiednie
                    środki
                    chroniące prawnie uzasadnione interesy osoby, której dane dotyczą; lub
                  </li>
                  <li>dane osobowe muszą pozostać poufne zgodnie z obowiązkiem zachowania tajemnicy
                    zawodowej przewidzianym w prawie Unii lub w prawie państwa członkowskiego, w tym
                    ustawowym obowiązkiem zachowania tajemnicy.
                  </li>
                </ol>
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Obowiązek przejrzystej komunikacji Administratora z osobą,
              której dane dotyczą (art.
              12 RODO):</p>
            <ol>
              <li>Administrator w zwięzłej, przejrzystej, zrozumiałej i łatwo dostępnej formie,
                jasnym
                i prostym językiem - w szczególności gdy informacje są kierowane do dziecka -
                udziela osobie,
                której dane dotyczą, wszelkich informacji, o których mowa w art. 13 i 14 RODO, oraz
                prowadzi z
                nią wszelką komunikację na mocy art. 15-22 i 34 RODO. Informacji udziela się na
                piśmie lub w
                inny sposób, w tym w stosownych przypadkach - elektronicznie. Jeżeli osoba, której
                dane
                dotyczą, tego zażąda, informacji można udzielić ustnie, o ile innymi sposobami
                potwierdzi się
                tożsamość osoby, której dane dotyczą.
              </li>
              <li>Jeżeli Administrator ma uzasadnione wątpliwości co do tożsamości osoby fizycznej
                składającej żądanie, o którym mowa w art. 15-21 RODO, może zażądać dodatkowych
                informacji
                niezbędnych do potwierdzenia tożsamości osoby, której dane dotyczą.
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Terminy spełnienia obowiązków informacyjnych przez Administratora</p>
            <ol>
              <li>W przypadku pozyskania danych osobowych od osoby, której dane dotyczą, wszelkie
                wskazane wyżej informacje powinny być tej osobie przekazane podczas pozyskiwania danych.</li>
              <li>W przypadku, gdy administrator pozyskuje dane w sposób pośredni, spełnia swój
                obowiązek informacyjny w następujących terminach:
                <ol type='a'>
                  <li>w rozsądnym terminie po pozyskaniu danych osobowych - najpóźniej w ciągu miesiąca
                    - mając na uwadze konkretne okoliczności przetwarzania danych osobowych;</li>
                  <li>jeżeli dane osobowe mają być stosowane do komunikacji z osobą, której dane dotyczą -
                    najpóźniej przy pierwszej takiej komunikacji z osobą, której dane dotyczą; lub</li>
                  <li>jeżeli planuje się ujawnić dane osobowe innemu odbiorcy - najpóźniej przy ich
                    pierwszym ujawnieniu.</li>
                </ol>
              </li>
            </ol>
          </li>
          <li className='mt-20'>
              <p className='text-bold'>Obowiązki Administratora Danych Osobowych przy przetwarzaniu danych
                osobowych</p>
            <p>Uwzględniając charakter, zakres, kontekst i cele przetwarzania oraz ryzyko naruszenia praw lub
              wolności osób fizycznych o różnym prawdopodobieństwie i wadze zagrożenia, Administrator
              wdrożył odpowiednie środki techniczne i organizacyjne, aby przetwarzanie odbywało się zgodnie z

              RODO i aby móc to wykazać. Środki te są raz na pół roku poddawane przeglądom i uaktualniane.</p>
            <p>Obowiązkami Pawła Radomskiego jako administratora danych osobowych wynikającymi wprost z
              RODO są w szczególności:</p>
            <ol>
              <li>
                <p className='text-bold'>Uwzględnianie ochrony danych w fazie projektowania oraz domyślna ochrona danych</p>
                <p>Administrator, w celu realizacji tego obowiązku, wdrożył odpowiednie środki techniczne i
                  organizacyjne, takie jak pseudonimizacja, zaprojektowane w celu skutecznej realizacji zasad
                  ochrony danych, takich jak minimalizacja danych, oraz w celu nadania przetwarzaniu
                  niezbędnych zabezpieczeń, tak by w jak najwyższy sposób chronić prawa osób, których dane
                  dotyczą.</p>
              </li>
              <li>
                <p className='text-bold'>Powierzenie danych do przetwarzania na podstawie pisemnej umowy</p>
                <p>Administrator
                  korzysta wyłącznie z usług takich podmiotów przetwarzających, które zapewniają wystarczające
                  gwarancje wdrożenia odpowiednich środków technicznych i organizacyjnych, by chroniło prawa
                  osób, których dane dotyczą.</p>
                <p>Przetwarzanie przez podmiot przetwarzający odbywa się na podstawie umowy lub innego
                  instrumentu prawnego, które podlegają prawu Unii lub prawu państwa członkowskiego i wiążą
                  podmiot przetwarzający i Administratora, określają przedmiot i czas trwania przetwarzania,
                  charakter i cel przetwarzania, rodzaj danych osobowych oraz kategorie osób, których dane
                  dotyczą, obowiązki i prawa administratora.</p>
              </li>
              <li>
                <p className='text-bold'>Rejestrowanie czynności przetwarzania.</p>
                <p>Administrator obowiązany jest do prowadzenie Rejestru czynności przetwarzania danych
                  osobowych.</p>
                <p>Rejestr czynności przetwarzania prowadzony jest przez Administratora w formie elektronicznej i
                  jest na bieżąco aktualizowany.</p>
                <p>Administrator udostępnia rejestr na żądanie organu nadzorczego.</p>
              </li>
              <li>
                <p className='text-bold'>Bezpieczeństwo przetwarzania.</p>
                <p>Paweł Radomski wdrożył i stosuje następujące środki techniczne i organizacyjne, aby
                  zminimalizować ryzyko naruszenia ochrony danych osobowych:</p>
                <ol type='a'>
                  <li>pseudonimizację i szyfrowanie danych osobowych;</li>
                  <li>zdolność do ciągłego zapewnienia poufności, integralności, dostępności i odporności
                    systemów i usług przetwarzania;</li>
                  <li>zdolność do szybkiego przywrócenia dostępności danych osobowych i dostępu do nich
                    w razie incydentu fizycznego lub technicznego;</li>
                  <li>regularne testowanie, mierzenie i ocenianie skuteczności środków technicznych
                    i organizacyjnych mających zapewnić bezpieczeństwo przetwarzania.</li>
                </ol>
                <p>Oceniając, czy stopień bezpieczeństwa jest odpowiedni, Administrator uwzględnia
                  w szczególności ryzyko wiążące się z przetwarzaniem, w szczególności wynikające
                  z przypadkowego lub niezgodnego z prawem zniszczenia, utraty, modyfikacji, nieuprawnionego
                  ujawnienia lub nieuprawnionego dostępu do danych osobowych przesyłanych, przechowywanych
                  lub w inny sposób przetwarzanych.</p>
              </li>
              <li>
                <p className='text-bold'>Zgłaszanie naruszenia ochrony danych osobowych organowi nadzorczemu.</p>
                <p>W przypadku naruszenia ochrony danych osobowych, Administrator bez zbędnej zwłoki w miarę
                  możliwości, nie później niż w terminie 72 godzin po stwierdzeniu naruszenia - zgłasza je
                  organowi nadzorczemu, chyba że jest mało prawdopodobne, by naruszenie to skutkowało
                  ryzykiem naruszenia praw lub wolności osób fizycznych. Do zgłoszenia przekazanego organowi
                  nadzorczemu po upływie 72 godzin, Administrator dołącza wyjaśnienie przyczyn opóźnienia.</p>
                <p>Jeżeli naruszenie ochrony danych osobowych może powodować wysokie ryzyko naruszenia praw
                  lub wolności osób fizycznych, administrator bez zbędnej zwłoki zawiadamia osobę, której dane
                  dotyczą, o takim naruszeniu.</p>
              </li>
              <li>
                <p className='text-bold'>Przeprowadzanie oceny skutków dla ochrony danych.</p>
                <p>Jeżeli dany rodzaj przetwarzania - w szczególności z użyciem nowych technologii - ze względu na
                  swój charakter, zakres, kontekst i cele z dużym prawdopodobieństwem może powodować wysokie
                  ryzyko naruszenia praw lub wolności osób fizycznych, Administrator przed rozpoczęciem
                  przetwarzania obowiązany jest dokonać oceny skutków planowanych operacji przetwarzania dla
                  ochrony danych osobowych.</p>
              </li>
              <li>
                <p className='text-bold'>Przeprowadzanie uprzednich konsultacji.</p>
                <p>Jeżeli ocena skutków dla ochrony danych, wskaże, że przetwarzanie powodowałoby wysokie
                  ryzyko, gdyby Administrator nie zastosował środków w celu zminimalizowania tego ryzyka, to
                  przed rozpoczęciem przetwarzania Administrator będzie konsultował się w sprawie możliwości i
                  sposobów przetwarzania z organem nadzorczym.</p>
              </li>
            </ol>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Przetwarzanie danych osób niepełnoletnich</p>
            <p>Z założenia wszelkie działania Pawła Radomskiego kierowane są do osób pełnoletnich, mogących
              podejmować decyzje, bądź wpływać na ich podejmowanie. Jeśli opiekunowie prawni osoby
              niepełnoletniej posiądą informację o tym, że wypełniła ona formularz dostępny na stronach
              serwisów należących do Administratora prosimy o skontaktowanie się z Administratorem w celu
              usunięcia tych danych z bazy, bądź wycofanie zgody poprzez wysłanie stosownego e -maila na
              adres: kontakt@kartazgloszen.pl .</p>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Prawa użytkowników stron internetowych należących do Pawła Radomskiego</p>
            <p>Osoba, której dane dotyczą:</p>
            <ol type='a'>
              <li>jest uprawniona do uzyskania od Administratora potwierdzenia, czy przetwarzane są dane
              osobowe jej dotyczące, a jeżeli ma to miejsce, jest uprawniona do uzyskania dostępu do nich
                oraz szeregu informacji (Artykuł 15 RODO),</li>
              <li>ma prawo żądania od Administratora niezwłocznego sprostowania dotyczących jej
                danych osobowych, które są nieprawidłowe (Artykuł 16 RODO),</li>
              <li>ma prawo żądania od Administratora niezwłocznego usunięcia dotyczących jej danych
                osobowych w wyszczególnionych okolicznościach (Artykuł 17 RODO),</li>
              <li>ma prawo żądania od Administratora ograniczenia przetwarzania danych
                w wyszczególnionych przypadkach (Artykuł 18 RODO),</li>
              <li>ma prawo otrzymać w ustrukturyzowanym, powszechnie używanym formacie nadającym
                się do odczytu maszynowego dane osobowe jej dotyczące, które dostarczył Administrator, oraz</li>
              <li>ma prawo, w wymienionych przypadkach, przesłać te dane osobowe innemu
              administratorowi bez przeszkód ze strony administratora, któremu dostarczono te dane (Artykuł
                20 RODO),</li>
              <li>ma prawo wnieść sprzeciw wobec przetwarzania dotyczących jej danych osobowych
                (Artykuł 21 RODO ),</li>
              <li>ma prawo nie podlegać decyzji, która opiera się wyłącznie na zautomatyzowanym
                przetwarzaniu jej danych osobowych, w tym profilowaniu (Artykuł 22 RODO).</li>
            </ol>
            <p>Administrator danych osobowych umożliwia osobie, której dane dotyczą wymienionych wyżej
              uprawnień na zasadach określonych w przepisach Rozporządzenia unijnego o ochronie danych
              osobowych.</p>
          </li>
          <li className='mt-20'>
            <p className='text-bold'>Zmiana polityki prywatności</p>
            <p>Paweł Radomski zastrzega sobie prawo zmiany powyższej polityki prywatności i regulaminu poprzez
              opublikowanie nowej polityki prywatności na tej stronie. W razie dodatkowych pytań dotyczących
              polityki prywatności i regulaminu strony, prosimy o kontakt z Administratorem pod adresem e-mail:
              kontakt@kartazgloszen.pl lub pod numerem telefonu: (+48) 793432042.</p>
          </li>
        </ol>
      </div>
    </>
);

export default PrivacyPage
