let activeEnv = process.env.ACTIVE_ENV || process.env.NODE_ENV || 'development';

console.log(`Using environment config: '${activeEnv}'`);

require('dotenv').config({
  path: `.env.${activeEnv}`,
});

module.exports = {
  siteMetadata: {
    siteUrl: `https://www.kartazgloszen.pl`,
    title: `Karta zgłoszeń`,
    description: `Karta zgłoszeń to skuteczna automatyzacja Twojego turnieju! Zarządzaj wygodnie z poziomu komputera, tabletu i telefonu. Elektroniczne zapisy i nie tylko! Kompleksowy system do organizacji turnieju tańca.`,
    author: `Paweł Radomski`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Karta zgloszen`,
        short_name: `kartazgloszen.pl`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fecd57`,
        display: `minimal-ui`,
        icon: `src/images/android-chrome-512x512.png`
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-remark`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-purgecss`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-fullstory`,
      options: {
        fs_org: process.env.FULLSTORY_ORG_ID,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_TRACKING_ID,
      },
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: process.env.GOOGLE_TAGMANAGER_ID,
        includeInDevelopment: false,
      },
    },
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-robots-txt`
  ],
};