const config = require('../config/app');
const passport = require('passport');
const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;
const { UserDao } = require('../dao');

module.exports = {
  initialize: () => {
    const jwtOptions = {
      secretOrKey: config.auth.secretKey,
      jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
    };
    const strategy = new Strategy(jwtOptions, async (payload, done) => {
      try {
        const { sid, email } = await UserDao.getBy({ sid: payload.id });
        done(null, {
          id: sid,
          username: email
        })
      }
      catch (error) {
        return done(new Error('User not found'), null);
      }
    });
    passport.use(strategy);
    return passport.initialize();
  },
  authenticate: function () {
    return passport.authenticate('jwt', {
      session: false
    });
  }
};