const morgan = require('morgan');
const addRequestId = require('express-request-id');
const logger = rootRequire('logger');

module.exports.init = (app) => {
  const errorStream = { write: (message) => logger.error(message.replace(/(\r\n|\n|\r)/gm, '')) };
  const infoStream = { write: (message) => logger.info(message.replace(/(\r\n|\n|\r)/gm, '')) };

// https://medium.com/@tobydigz/logging-in-a-node-express-app-with-morgan-and-bunyan-30d9bf2c07a
  app.use(addRequestId());
  morgan.token('id', (req) => req.id);
  morgan.token('user', (req) => req.user ? req.user.username : 'unknown');

  const loggerFormat = ':id [:user][:date[web]] ":method :url" :status :response-time';

  app.use(morgan(loggerFormat, {
    skip: (req, res) => res.statusCode < 400,
    stream: errorStream
  }));

  app.use(morgan(loggerFormat, {
    skip: (req, res) => res.statusCode >= 400 || req.method === 'OPTIONS',
    stream: infoStream
  }));
};