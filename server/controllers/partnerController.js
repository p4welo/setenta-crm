'use strict';

const express = require('express');
const passport = require('passport');
const PartnerAdvertisement = require('../models/PartnerAdvertisement');
const EmailService = require('../services/EmailService');

const router = express.Router()
    .post('/', save);

function save(req, res) {
  if (!req.body) return res.sendStatus(400);
  PartnerAdvertisement.create(req.body)
      .then((advertisement) => EmailService.customerConfirmationAfterAdvertisementPublish(advertisement))
      .then((advertisement) => EmailService.clientNotificationAfterAdvertisementPublish(advertisement))
      .then((advertisement) => res.json(advertisement));
}

module.exports = router;
