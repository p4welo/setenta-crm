'use strict';

const express = require('express');
const Customer = require('../models/Customer');
// import auth from '../middleware/auth';

const router = express.Router();

router.get('/', listCustomers);
router.get('/byCode/:code', findByCode);

function listCustomers(req, res) {
    res.json({test: true});
    // Customer.find({})
    //     .exec((err, customers) => {
    //         if (err) {
    //             res.send('error');
    //         }
    //         else {
    //             res.json(customers);
    //         }
    //     });
}

function findByCode(req, res) {
    const cardCode = req.params.code;

    Customer.findOne({
        cardCodes: {"$in": [cardCode]}
    }).exec((err, customer) => {
        res.json({
            code: cardCode,
            customer
        });
    });
}

module.exports = router;
