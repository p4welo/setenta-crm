'use strict';

const express = require('express');
const passport = require('passport');
const { map } = require('ramda');
const Course = require('../models/Course');
const SmsService = require('../services/smsService');
const ObjectId = mongoose.Schema.Types.ObjectId;

const list = (request, response) => {
    const parseLowValues = value => value < 10 ? '0' + value : value;

    Course
        .find({})
        .exec((err, courses) => {
            if (err) {
                console.log(err);
                response.status(500).send('error');
            }
            else {
                response.json(map(
                    course => ({
                        _id: course._id,
                        day: course.day,
                        name: course.name,
                        level: course.level,
                        instructor: course.instructor,
                        isPublic: course.isPublic,
                        timeFrom: parseLowValues(course.hourFrom) + ':' + parseLowValues(course.minuteFrom),
                        timeTo: parseLowValues(course.hourTo) + ':' + parseLowValues(course.minuteTo)
                    }),
                    courses
                ));
            }
        });
};

const register = (request, response) => {
    const findCourse = (id) => Course.find({_id: id}).exec();
    const createCustomer = (course) => {
        const data = request.body;
        const customer = {
            firstName,
            lastName,
            email,
            phone
        };
        return Course.create(customer).exec();
        //SmsService.sendTest();
    };

    findCourse(request.params.id)
        .then(createCustomer)
        .then(sendSms);
};

const router = express.Router()
    .get('/', list)
    .get('/list', list)
    .post('/', passport.authenticate('basic', {session: false}), save)
    .get('/:id', get)
    .post('/:id/register', register)
    .delete('/:id', passport.authenticate('basic', {session: false}), remove)
    .put('/:id', passport.authenticate('basic', {session: false}), update);

function get(req, res) {
    Course
        .findById(req.id)
        .exec((err, found) => {
            if (err) {
                console.log(err);
                res.status(500).send('error');
            }
            else {
                res.json(found);
            }
        })
}

function remove(req, res) {
    console.log(req.params.id);
    Course
        .findById(req.params.id)
        .remove()
        .exec((err, found) => {
            if (err) {
                console.log(err);
                res.status(500).send('Wystąpił ');
            }
            else {
                res.send('ok');
            }
        })
}

function update(req, res) {
    Course
        .findById(req.id)
        .exec((err, found) => {
            if (err) {
                console.log(err);
                res.status(500).send('error');
            }
            else {
                res.json(found);
            }
        })
}

function save(req, res) {
    if (!req.body) return res.sendStatus(400);
    Course
        .create(req.body, (err, created) => {

            if (err) {
                console.log(err);
                res.status(500).send('error');
            }
            else {
                res.json(created);
            }
        });
}

module.exports = router;
