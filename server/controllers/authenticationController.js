const express = require('express');
const passport = require('passport');

const router = express.Router();

router.get('/loggedin', (req, res) => {
    res.send(req.isAuthenticated() ? req.user : '0');
});

router.get(
    '/login',
    passport.authenticate('basic', {session: false}),
    (req, res) => {
        res.send(JSON.stringify(req.baseUrl));
    }
);

module.exports = router;
