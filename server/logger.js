const winston = require('winston');
require('winston-daily-rotate-file');

const infoFileLogger = new (winston.transports.DailyRotateFile)({
  dirname: '../logs',
  filename: 'application-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
  level: 'info'
});

const transports = [
    new winston.transports.Console()
];

if (process.env.NODE_ENV === 'production') {
  transports.push(
      infoFileLogger,
      new winston.transports.File({
        dirname: '../logs',
        filename: 'logs/errors.log',
        level: 'error'
      })
  )
}

const logger = new winston.createLogger({
  format: winston.format.simple(),
  transports
});


module.exports = logger;