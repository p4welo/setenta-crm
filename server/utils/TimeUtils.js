const parseLowValues = (value) => value < 10 ? '0' + value : value;

module.exports = {
  parseLowValues
};
