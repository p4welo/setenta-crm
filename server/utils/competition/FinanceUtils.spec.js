const {
  getDancersCharge,
  getPerformancesCharge,
  getSchoolCharges
} = require('./FinanceUtils');

const dancersAMock = [
  { _id: 0, firstName: 'Paweł', lastName: 'Radomski', yearOfBirth: 1988 },
  { _id: 1, firstName: 'Paweł', lastName: 'Radomski', yearOfBirth: null },
  { _id: 2, firstName: 'Błażej', lastName: 'Bukowski', yearOfBirth: 2000 }
];

const dancersBMock = [
  { _id: 3, firstName: 'Marcin', lastName: 'Wieczorek', yearOfBirth: 1988 },
  { _id: 4, firstName: 'Paweł', lastName: 'Radomski', yearOfBirth: null },
  { _id: 5, firstName: 'Łukasz', lastName: 'Znajder', yearOfBirth: 2000 }
];

const dancersCMock = [
  { _id: 6, firstName: 'Marcin', lastName: 'Wieczorek', yearOfBirth: 1988 },
  { _id: 7, firstName: 'Paweł', lastName: 'Radomski', yearOfBirth: null },
  { _id: 8, firstName: 'Łukasz', lastName: 'Znajder', yearOfBirth: 2000 }
];

const performancesAMock = [
  { dancers: dancersAMock },
  { dancers: dancersBMock }
];

const performancesBMock = [
  { dancers: dancersCMock }
];

const schoolPerformancesMock = [
  {school: {_id: 0}, performances: performancesAMock},
  {school: {_id: 1}, performances: performancesBMock},
];
describe('FinanceUtils', () => {
  describe('getDancersCharge', () => {
    it('should return correct charge for dancers A', () => {
      expect(getDancersCharge(dancersAMock)).toBe(60);
    });

    it('should return correct charge for dancers B', () => {
      expect(getDancersCharge(dancersBMock)).toBe(75);
    });

    it('should return correct charge for dancers C', () => {
      expect(getDancersCharge(dancersCMock)).toBe(75);
    });
  });

  describe('getPerformancesCharge', () => {
    it('should return correct value for performance A', () => {
      expect(getPerformancesCharge(performancesAMock)).toBe(120);
    });

    it('should return correct value for performance B', () => {
      expect(getPerformancesCharge(performancesBMock)).toBe(75);
    });
  });

  describe('getSchoolCharges', () => {
    it('should return correct value', () => {
      expect(getSchoolCharges(schoolPerformancesMock)[0].charge).toBe(120);
      expect(getSchoolCharges(schoolPerformancesMock)[1].charge).toBe(75);
    });
  });
});