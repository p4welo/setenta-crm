const dayjs = require('dayjs');

const isOutdated = (competition) => {
  return dayjs().isAfter(dayjs(competition.end));
};

const registrationInProgress = (competition) => {
  return dayjs().isBefore(dayjs(competition.registrationEnd)) || competition.is_registration_started;
};

const registrationEnded = (competition) => {
  return dayjs().isAfter(dayjs(competition.registrationEnd));
};

module.exports = {
  isOutdated,
  registrationInProgress,
  registrationEnded
};