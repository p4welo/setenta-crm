const { countBy, reduce, toLower, trim } = require('ramda');

function getDancersCharge(dancers) {
  const grouppedDancers = countBy((dancer) => {
    return toLower(trim(dancer.firstName) + trim(dancer.lastName))
  }, dancers);

  const charges = Object.keys(grouppedDancers).map((key) => {
    const value = grouppedDancers[key];
    return {
      name: key,
      amount: value,
      value: 25 + (value - 1) * 10
    }
  });

  const addCharge = (sum, charge) => {
    return sum + charge.value;
  };
  return reduce(addCharge, 0, charges);
}

function getPerformancesCharge(performances) {
  let dancers = [];
  performances.forEach((performance) => {
    dancers = [
      ...dancers,
      ...performance.dancers
    ];
  });
  return getDancersCharge(dancers);
}

function getSchoolCharges(schoolPerformances) {
  return schoolPerformances.map((schoolPerformance) => ({
    school: schoolPerformance.school,
    performances: schoolPerformance.performances,
    charge: getPerformancesCharge(schoolPerformance.performances)
  }));
}

module.exports = {
  getDancersCharge,
  getPerformancesCharge,
  getSchoolCharges
};