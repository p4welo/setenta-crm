const { sort, prop, ascend } = require('ramda');

const getDancersAgeLevel = (dancers = [], ageLevels) => {
  const age = getPerformanceLeadingAge(dancers);
  if (age) {
    const foundLevels = ageLevels.filter((level) => level.min <= age && level.max >= age);
    if (foundLevels.length > 0) {
      return foundLevels[0].name;
    }
  }
  return undefined;
};

const getPerformanceLeadingAge = (dancers) => {
  const length = dancers.length;
  if (length > 0) {
    const sorted = sort(ascend(prop('yearOfBirth')), dancers);
    const numberToIgnore = length > 2 ? Math.ceil(length * 0.2) : 0;
    return new Date().getFullYear() - +sorted[numberToIgnore].yearOfBirth;
  }
  return undefined;
};

module.exports = {
  getDancersAgeLevel
};