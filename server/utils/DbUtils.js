const DataObjectStates = require('../models/constants/DataObjectStates');
const SidUtils = require('./SidUtils');

const fillSidIfNeeded = (obj) => {
  if (!obj.sid) {
    obj.sid = SidUtils.generate();
  }
};

const fillObjectStateIfNeeded = (obj) => {
  if (!obj.objectState) {
    obj.objectState = DataObjectStates.ACTIVE;
  }
};

module.exports = {
  fillSidIfNeeded,
  fillObjectStateIfNeeded
};