const crypto = require('crypto');

const generate = () => crypto.randomBytes(16).toString("hex");

module.exports = {
  generate
};
