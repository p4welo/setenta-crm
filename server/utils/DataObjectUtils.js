const DataObjectStates = require('../models/constants/DataObjectStates');

const isNotDeleted = {objectState: { $ne: DataObjectStates.DELETED }};

module.exports = {
  isNotDeleted
};