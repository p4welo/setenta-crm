'use strict';
global.rootRequire = name => require(`${__dirname}/${name}`);

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const auth = rootRequire('middleware/auth');
const cors = rootRequire('middleware/cors');
const logger = rootRequire('middleware/httpLogger');
const routes = rootRequire('routes');

const initApp = () => {
  const app = express();
  logger.init(app);
  app.disable('etag');

  app.use(auth.initialize());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cors);
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/api/', routes);
  return app;
};

module.exports = initApp();