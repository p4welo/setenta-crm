const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const TemplateSchema = Object.assign({}, DataObject, {
  name: String,
  content: String,
  clientId: String
});

module.exports = mongoose.model('Template', new Schema(TemplateSchema));
