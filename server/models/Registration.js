const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('./abstract/DataObject');

const RegistrationSchema = Object.assign({}, DataObject, {
  firstName: String,
  lastName: String,
  email: String,
  phone: String,
  course: {
    type: ObjectId,
    ref: 'Course'
  },
  zipCode: String
});

module.exports = mongoose.model('Registration', new Schema(RegistrationSchema));
