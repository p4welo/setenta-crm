'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('./abstract/DataObject');

const CustomerSchema = Object.assign({}, DataObject, {
  user: {
    type: ObjectId,
    ref: 'User'
  },
  firstName: String,
  lastName: String,
  contactData: [],
  customerCards: [],
  courses: [{
    type: ObjectId,
    ref: 'Course'
  }],
  tickets: [{
    type: {
      type: ObjectId,
      ref: 'TicketType'
    },
    entrances: [{
      dateTime: Date,
      course: {
        type: ObjectId,
        ref: 'Course'
      }
    }]
  }],
  notes: [{
    createdAt: {
      type: Date,
      default: Date.now
    },
    content: String
  }]
});

module.exports = mongoose.model('Customer', CustomerSchema);
