'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');
const AdvertisementStates = require('./constants/AdvertisementStates');

const PartnerAdvertisementSchema = Object.assign({}, DataObject, {
  advertiser: {
    firstName: String,
    lastName: String,
    age: {
      type: Number,
      min: 1,
      max: 99
    },
    height: {
      type: Number,
      min: 1,
      max: 300
    },
    experience: String
  },
  preferences: {
    gender: String,
    style: String,
    comment: String
  },
  contact: {
    mobile: String,
    email: String
  },
  agreement: {
    store: Boolean,
    offers: Boolean
  },
  clientId: String,
  state: {
    type: String,
    default: AdvertisementStates.STILL_LOOKING
  },
  notes: [{
    createdAt: {
      type: Date,
      default: Date.now
    },
    createdBy: String,
    value: String
  }],
});

module.exports = mongoose.model('PartnerAdvertisement', new Schema(PartnerAdvertisementSchema));
