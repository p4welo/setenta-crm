'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('../abstract/DataObject');

const DancerSchema = Object.assign({}, DataObject, {
  firstName: String,
  lastName: String,
  yearOfBirth: Number,
  school: {
    type: ObjectId,
    ref: 'CompetitionSchool'
  }
});
const schema = new Schema(DancerSchema);
module.exports = mongoose.model('CompetitionDancer', schema);