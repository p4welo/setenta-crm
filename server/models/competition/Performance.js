'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('../abstract/DataObject');

const PerformanceSchema = Object.assign({}, DataObject, {
  number: Number,
  name: String,
  type: String,
  style: String,
  ageLevel: String,
  expLevel: String,
  dancers: [{
    type: ObjectId,
    ref: 'CompetitionDancer'
  }],
  school: {
    type: ObjectId,
    ref: 'CompetitionSchool'
  }
});
const schema = new Schema(PerformanceSchema);
module.exports = mongoose.model('CompetitionPerformance', schema);