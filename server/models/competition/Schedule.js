'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleSchema = Object.assign({}, {
  order: Number,
  block: Number,
  day: String,
  style: String,
  type: String,
  stages: Number,
  expLevel: String,
  ageLevel: String,
  performances: []
});
const schema = new Schema(ScheduleSchema);
module.exports = mongoose.model('CompetitionSchedule', schema);