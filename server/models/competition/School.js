'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('../abstract/DataObject');

const SchoolSchema = Object.assign({}, DataObject, {
  name: String,
  city: String,
  zip: String,
  street: String,
  phone: String,
  email: String,
  nip: String,
  invoiceName: String,
  invoiceStreet: String,
  invoiceZip: String,
  invoiceCity: String,
  agreement: {
    type: Boolean,
    default: Boolean.false
  },
  user: {
    type: ObjectId,
    ref: 'User'
  }
});
const schema = new Schema(SchoolSchema);
module.exports = mongoose.model('CompetitionSchool', schema);