'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const CourseSchema = Object.assign({}, DataObject, {
  clientId: {
    type: String,
    default: '0'
  },
  name: String,
  shortDescription: String,
  description: String,
  hourFrom: {
    type: Number
  },
  minuteFrom: {
    type: Number
  },
  hourTo: {
    type: Number,
    min: 0,
    max: 23
  },
  minuteTo: {
    type: Number,
    min: 0,
    max: 59
  },
  state: String,
  timeFrom: String,
  timeTo: String,
  day: {
    type: Number,
    min: 0,
    max: 6
  },
  level: String,
  instructor: String,
  firstClassDate: Date,
  color: String,
  isPublic: {
    type: Boolean,
    default: Boolean.false
  },
  isRegistrationOpen: {
    type: Boolean,
    default: Boolean.false
  }
});
const schema = new Schema(CourseSchema);
module.exports = mongoose.model('Course', schema);
