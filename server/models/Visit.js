const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const VisitSchema = Object.assign({}, DataObject, {
  email: String,
  userKey: String,
  clientId: String,
  ip: String,
  url: String,
  resolution: String
});

module.exports = mongoose.model('Visit', new Schema(VisitSchema));
