'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const TicketTypeSchema = Object.assign({}, DataObject, {
  name: String,
  entranceLimit: Number,
  timeLimit: Number
});

module.exports = mongoose.model('TicketType', new Schema(TicketTypeSchema));
