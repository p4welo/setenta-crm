const DataObjectStates = require('../constants/DataObjectStates');

module.exports = {
  objectState: {
    type: String,
    default: DataObjectStates.ACTIVE
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  deletedAt: Date
};