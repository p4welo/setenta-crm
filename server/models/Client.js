const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const ClientSchema = Object.assign({}, DataObject, {
  name: String
});

module.exports = mongoose.model('Client', new Schema(ClientSchema));
