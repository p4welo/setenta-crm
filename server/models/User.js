const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const DataObject = require('./abstract/DataObject');
const UserStates = require('./constants/UserStates');

const UserSchema = Object.assign({}, DataObject, {
  email: String,
  password: String,
  type: String,
  client: {
    type: ObjectId,
    ref: 'Client'
  },
  state: {
    type: String,
    default: UserStates.INACTIVE
  }
});

module.exports = mongoose.model('User', new Schema(UserSchema));
