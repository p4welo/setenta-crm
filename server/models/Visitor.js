const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DataObject = require('./abstract/DataObject');

const TemplateSchema = Object.assign({}, DataObject, {
  email: String,
  clientId: String,
  visits: [{
    createdAt: {
      type: Date,
      default: Date.now
    },
    ip: String,
    url: String,
    resolution: String
  }]
});

module.exports = mongoose.model('Visitor', new Schema(TemplateSchema));
