const { attributes } = require('structure');

const Email = attributes({
  id: Number,
  sid: String,
  objectState: String,
  from: String,
  to: String,
  subject: String,
  content: String,
  readCount: Number
}, {
})(class Email {
});

module.exports = Email;
