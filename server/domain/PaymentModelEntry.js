const { attributes } = require('structure');

const PaymentModelEntry = attributes({
  id: Number,
  sid: String,
  objectState: String,
  date: Date,
  amount: Number,
  value: Number,
  competition: {
    type: 'Competition'
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class PaymentModelEntry {
});

module.exports = PaymentModelEntry;