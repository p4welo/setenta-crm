const { attributes } = require('structure');

const InvoiceData = attributes({
  id: Number,
  sid: String,
  name: String,
  nip: String,
  street: String,
  zip: String,
  city: String,
  user: {
    type: 'User',
    required: true
  }
}, {
  dynamics: {
    User: () => require('./User')
  }
})(class InvoiceData {
});

module.exports = InvoiceData;
