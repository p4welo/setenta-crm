const { attributes } = require('structure');

const Address = attributes({
  id: Number,
  sid: String,
  street: String,
  zip: String,
  city: String,
  school: {
    type: 'School',
    required: true
  }
}, {
  dynamics: {
    School: () => require('./School')
  }
})(class Address {
});

module.exports = Address;
