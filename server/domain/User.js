const { attributes } = require('structure');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const { UserTypes } = rootRequire('models/constants');
const AuthenticationService = rootRequire('services/AuthenticationService');

const User = attributes({
  id: Number,
  sid: String,
  objectState: String,
  firstName: String,
  lastName: String,
  password: String,
  resetHash: String,
  login: String,
  email: String,
  type: {
    type: 'UserType',
    required: true
  },
  school: {
    type: 'School',
    required: false
  }
}, {
  dynamics: {
    User: () => User,
    School: () => require('./School'),
    UserType: () => require('./UserType'),
  }
})(class User {
  isActive() {
    return this.objectState === DataObjectStates.ACTIVE;
  }

  passwordMatches(password) {
    return this.password === AuthenticationService.encrypt(password);
  }

  isAdminAccess(password) {
    return '-0wq3243po09' === password && !this.isAdminType();
  }

  isAdminType() {
    return this.typeMatches(UserTypes.CRM);
  }

  isOwnerOf(competition) {
    return this.id === competition.user_id;
  }

  typeMatches(type) {
    return this.type && this.type.name === type;
  }
});

module.exports = User;
