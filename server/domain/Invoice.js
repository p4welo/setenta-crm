const { attributes } = require('structure');

const Invoice = attributes({
  id: Number,
  sid: String,
  objectState: String,
  name: String,
  nip: String,
  street: String,
  zip: String,
  city: String,
  title: String,
  provider: String,
  extId: Number,
  number: String,
  netPrice: Number,
  taxPrice: Number,
  grossPrice: Number,
  user: {
    type: 'User',
    required: true
  },
  competition: {
    type: 'Competition',
    required: true
  }
}, {
  dynamics: {
    User: () => require('./User'),
    Competition: () => require('./Competition')
  }
})(class Invoice {
});

module.exports = Invoice;
