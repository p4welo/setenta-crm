const { attributes } = require('structure');

const PerformanceAge = attributes({
  id: Number,
  sid: String,
  name: String,
  min: Number,
  max: Number,
  competition: {
    type: 'Competition',
    required: true
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class PerformanceAge {
});

module.exports = PerformanceAge;
