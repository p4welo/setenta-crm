const { attributes } = require('structure');

const PerformanceType = attributes({
  id: Number,
  sid: String,
  name: String,
  min: Number,
  max: Number,
  showName: Boolean,
  competition: {
    type: 'Competition',
    required: true
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class PerformanceType {
});

module.exports = PerformanceType;
