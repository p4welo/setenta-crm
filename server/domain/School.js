const { attributes } = require('structure');

const School = attributes({
  id: Number,
  sid: String,
  objectState: String,
  name: String,
  city: String,
  phone: String,
  user: {
    type: 'User',
    required: true
  },
  address: {
    type: 'Address',
    required: false
  }
}, {
  dynamics: {
    User: () => require('./User'),
    Address: () => require('./Address')
  }
})(class School {
});

module.exports = School;
