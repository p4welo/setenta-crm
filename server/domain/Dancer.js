const { attributes } = require('structure');

const Dancer = attributes({
  id: Number,
  sid: String,
  objectState: String,
  firstName: String,
  lastName: String,
  yearOfBirth: Number,
  groupName: String,
  user: {
    type: 'User',
    required: true
  }
}, {
  dynamics: {
    User: () => require('./User')
  }
})(class Dancer {
});

module.exports = Dancer;
