const { attributes } = require('structure');

const PerformancePrice = attributes({
  id: Number,
  sid: String,
  objectState: String,
  type: String,
  value: String,
  description: String,
  performance: {
    type: 'Performance',
    required: true
  }
}, {
  dynamics: {
    Performance: () => require('./Performance')
  }
})(class PerformancePrice {
});

module.exports = PerformancePrice;