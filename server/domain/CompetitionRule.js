const { attributes } = require('structure');

const CompetitionRule = attributes({
  id: Number,
  parentId: Number,
  sid: String,
  objectState: String,
  ruleType: String,
  ownMusicAllowed: Boolean,
  competition: {
    type: 'Competition'
  },
  style: {
    type: 'PerformanceStyle'
  },
  type: {
    type: 'PerformanceType'
  },
  experience: {
    type: 'PerformanceExperience'
  },
  age: {
    type: 'PerformanceAge'
  },
  rules: {
    type: Array,
    itemType: 'CompetitionRule'
  }
}, {
  dynamics: {
    Competition: () => require('./Competition'),
    PerformanceStyle: () => require('./PerformanceStyle'),
    PerformanceType: () => require('./PerformanceType'),
    PerformanceExperience: () => require('./PerformanceExperience'),
    PerformanceAge: () => require('./PerformanceAge'),
    CompetitionRule: () => require('./CompetitionRule')
  }
})(class CompetitionRule {
});

module.exports = CompetitionRule;
