const { attributes } = require('structure');

const PerformanceSection = attributes({
  id: Number,
  sid: String,
  objectState: String,
  day: Number,
  chapter: Number,
  position: Number,
  eliminations: Boolean,
  competition: {
    type: 'Competition',
    required: true
  },
  blocks: {
    type: Array,
    itemType: 'PerformanceBlock'
  },
  performances: {
    type: Array,
    default: []
  }
}, {
  dynamics: {
    Competition: () => require('./Competition'),
    PerformanceBlock: () => require('./PerformanceBlock')
  }
})(class PerformanceSection {
});

module.exports = PerformanceSection;
