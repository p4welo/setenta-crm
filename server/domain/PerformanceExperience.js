const { attributes } = require('structure');

const PerformanceExperience = attributes({
  id: Number,
  sid: String,
  name: String,
  competition: {
    type: 'Competition',
    required: true
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class PerformanceExperience {
});

module.exports = PerformanceExperience;
