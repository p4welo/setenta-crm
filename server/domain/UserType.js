const { attributes } = require('structure');

const UserType = attributes({
  id: Number,
  sid: String,
  name: String,
  users: {
    type: Array,
    itemType: 'User'
  }
}, {
  dynamics: {
    User: () => require('./User'),
    UserType: () => UserType
  }
})(class UserType {
});

module.exports = UserType;
