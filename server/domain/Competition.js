  const { attributes } = require('structure');
const dayjs = require('dayjs');

const Competition = attributes({
  id: Number,
  sid: String,
  name: String,
  start: Date,
  end: Date,
  isPublic: Boolean,
  registrationEnd: Date,
  audioUploadEnd: Date,
  registrationOpened: Boolean,
  resultPublished: Boolean,
  startingListPublished: Boolean,
  website: String,
  regulations: String,
  mapUrl: String,
  phone: String,
  place: String,
  street: String,
  zip: String,
  city: String,
  paymentModel: String,
  ageLevelTolerance: Number,
  user: {
    type: 'User'
  }
}, {
  dynamics: {
    User: () => require('./User')
  }
})(class Competition {
  isRegistrationOpened() {
    return !this.isOutdated() &&
        (dayjs().isBefore(dayjs(this.registrationEnd)) || this.registrationOpened);
  }

  isAudioUploadEnabled() {
    return !!this.audioUploadEnd;
  }

  isOutdated() {
    return dayjs().isAfter(dayjs(this.end));
  }
});

module.exports = Competition;
