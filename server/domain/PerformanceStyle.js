const { attributes } = require('structure');

const PerformanceStyle = attributes({
  id: Number,
  sid: String,
  name: String,
  showTitle: Boolean,
  competition: {
    type: 'Competition',
    required: true
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class PerformanceStyle {
});

module.exports = PerformanceStyle;
