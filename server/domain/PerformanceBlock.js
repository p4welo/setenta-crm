const { attributes } = require('structure');

const PerformanceBlock = attributes({
  id: Number,
  sid: String,
  objectState: String,
  style: String,
  type: String,
  experience: String,
  age: String
}, {
  dynamics: {
  }
})(class PerformanceBlock {
  equalsPerformance(performance) {
    return performance.style === this.style &&
        performance.type === this.type &&
        performance.experience === this.experience &&
        performance.ageLevel === this.age;
  }
});

module.exports = PerformanceBlock;
