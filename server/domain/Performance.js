const { attributes } = require('structure');

const Performance = attributes({
  id: Number,
  sid: String,
  objectState: String,
  name: String,
  title: String,
  style: String,
  type: String,
  experience: String,
  ageLevel: String,
  place: String,
  number: Number,
  user: {
    type: 'User',
    required: true
  },
  competition: {
    type: 'Competition',
    required: true
  },
  dancers: {
    type: Array,
    itemType: 'Dancer'
  },
  createdAt: Date,

  school: String,
  city: String,
  phone: String,
  email: String,
  audioSid: String,
  audio:  {
    type: 'Audio'
  },
  prices: {
    type: Array,
    itemType: 'PerformancePrice'
  }
}, {
  dynamics: {
    Audio: () => require('./Audio'),
    User: () => require('./User'),
    Dancer: () => require('./Dancer'),
    Competition: () => require('./Competition'),
    PerformancePrice: () => require('./PerformancePrice')
  }
})(class Performance {
});

module.exports = Performance;
