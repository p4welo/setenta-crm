const { attributes } = require('structure');

const CompetitionLicence = attributes({
  id: Number,
  sid: String,
  objectState: String,
  musicUpload: Boolean,
  results: Boolean,
  competition: {
    type: 'Competition'
  }
}, {
  dynamics: {
    Competition: () => require('./Competition')
  }
})(class CompetitionLicence {
});

module.exports = CompetitionLicence;