const { attributes } = require('structure');

const Audio = attributes({
  id: Number,
  sid: String,
  objectState: String,
  encoding: String,
  mimetype: String,
  destination: String,
  filename: String,
  path: String,
  size: Number,
  user: {
    type: 'User',
    required: true
  }
}, {
  dynamics: {
    User: () => require('./User')
  }
})(class Audio {
});

module.exports = Audio;
