const { attributes } = require('structure');

const CompetitionPayment = attributes({
  id: Number,
  sid: String,
  date: Date,
  amount: Number,
  user: {
    type: 'User'
  },
  competition: {
    type: 'Competition'
  }
}, {
  dynamics: {
    Competition: () => require('./Competition'),
    User: () => require('./User')
  }
})(class CompetitionPayment {
});

module.exports = CompetitionPayment;
