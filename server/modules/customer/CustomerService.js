const Customer = require('../../models/Customer');

const findAll = () =>
  Customer.find({}).exec();

const create = (customer) =>
  Customer.create(customer);

const getById = (id) =>
  Customer.findById(id).exec();

const getByEmail = (email) =>
  Customer.findOne({'contacts.value': email}).exec();

const removeById = (id) =>
  Customer.findById(id).remove().exec();

const update = (customer) =>
  getById(customer._id)
    .then((oldCustomer) => {
      Object.assign(oldCustomer, customer, {updatedAt: Date.now()});
      return Customer.save(oldCustomer);
    });

const registerToCourse = (customerData, course) => {
  const createIfNotFound = (customer) => {
    if (!customer) {
      return create({
        firstName: customerData.firstName,
        lastName: customerData.lastName
      });
    }
    else {
      return Promise.resolve(customer);
    }
  };
  const addContactInfo = (customer) => {
    return Customer.findByIdAndUpdate(
      customer._id,
      {
        $push: {
          "contacts": {
            $each: [
              {type: 'email', value: customerData.email},
              {type: 'phone', value: customerData.phone}
            ]
          }
        }
      },
      {new: true}
    ).exec();
  };
  const addToCourse = (customer, error) => {
    return Customer.findByIdAndUpdate(
      {_id: customer._id},
      {$push: {courses: course._id}, updatedAt: Date.now()},
      {new: true}
    ).exec();
  };

  return getByEmail(customerData.email)
    .then(createIfNotFound)
    .then(addContactInfo)
    .then(addToCourse);
};

module.exports = {
  create,
  findAll,
  getByEmail,
  getById,
  registerToCourse,
  removeById,
  update
};
