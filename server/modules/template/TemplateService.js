const Template = require('../../models/Template');

const findByClientId = (clientId) => Template.find({clientId}).exec();
const create = (template) => Template.create(template);

module.exports = {
  findByClientId,
  create
};
