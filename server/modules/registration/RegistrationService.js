const Registration = require('../../models/Registration');

const findAll = () =>
  Registration.find({}).exec();

const findByCourse = (course) =>
    Registration.find({course}).exec();

const create = (registration) =>
  Registration.create(registration);

const getById = (id) =>
  Registration.findById(id).exec();

const removeById = (id) =>
  Registration.findById(id).remove().exec();

const registerToCourse = (customer, course) => {
  Object.assign(customer, {course});
  return create(customer);
};

module.exports = {
  create,
  findAll,
  getById,
  removeById,
  registerToCourse,
  findByCourse
};
