const Course = require('../../models/Course');

const findAll = () => Course.find({}).exec();
const findForSchedule = () => Course.find({objectState: 'ACTIVE', isPublic: true}).exec();
const findByClientId = (clientId) => Course.find({clientId}).exec();
const create = (course) => Course.create(course);
const getById = (id) => Course.findById(id).exec();
const removeById = (id) => Course.findByIdAndUpdate(
    id,
    {objectState: 'DELETED', deletedAt: Date.now()},
    {upsert: true, new: true}
);
const update = (id, properties) => {
  properties.updatedAt = Date.now();
  return Course.findByIdAndUpdate(id, properties, {upsert: true, new: true});
};
const updateList = (ids, properties) => {
  properties.updatedAt = Date.now();
  return Course.update({ _id: { $in: ids } }, properties, { multi: true, upsert: true, new: true });
};

module.exports = {
  create,
  findAll,
  findForSchedule,
  findByClientId,
  getById,
  removeById,
  update,
  updateList
};
