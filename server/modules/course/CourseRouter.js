const express = require('express');
const { map } = require('ramda');
const CourseService = require('./CourseService');
const RegistrationService = require('../registration/RegistrationService');
const SmsService = require('../../services/SmsService');
const SlackService = require('../../services/SlackService');
const EmailService = require('../../services/EmailService');
const RecaptchaService = require('../../services/RecaptchaService');

const publicObjectMapper = ({_id, day, name, level, instructor, isPublic, isRegistrationOpen, state, hourFrom, minuteFrom, hourTo, minuteTo, timeFrom, timeTo, updatedAt, description, shortDescription}) => ({
  _id,
  day,
  name,
  level,
  instructor,
  isRegistrationOpen: isRegistrationOpen || false,
  state,
  timeFrom,
  description,
  shortDescription,
  timeTo
});

const listCoursesForSchedule = (request, response) => {
  CourseService.findForSchedule()
      .then((courses, error) => response.json(map(publicObjectMapper, courses)));
};

const registerToCourse = (request, response) => {
  const customerData = request.body;
  RecaptchaService.validate(customerData.recaptcha)
      .then(() => CourseService.getById(request.params.id))
      .then((course) => RegistrationService.registerToCourse(customerData, course))
      .then((customer) => EmailService.clientNotificationAfterCourseRegistration(customer))
      .then((customer) => EmailService.sendCourseRegistrationConfirmation(customer))
      .then((customer) => SmsService.customerCourseRegistrationNotification(customer))
      .then((customer) => {
        SlackService.courseRegistration(customer);
        response.json(customer);
      })
      .catch((errorCodes, arg) => {
        response.json(
            {
              arg,
              captcha: errorCodes
            }
        );
      });
};

module.exports = express.Router()
    .get('/', listCoursesForSchedule)
    .post('/:id/register', registerToCourse);
