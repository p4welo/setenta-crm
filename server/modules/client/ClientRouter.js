const express = require('express');
const passport = require('passport');
const CourseService = require('../course/CourseService');
const ClientService = require('../client/ClientService');

const listCourses = (request, response) => {
  // const clientId = request.params.clientId;
  CourseService.findAll()
      .then((courses, error) => response.json(courses));
};

const createCourse = (request, response) => {
  const clientId = request.params.clientId;
  //console.log(clientId);
  //ClientService.getById(clientId)
  //    .then((client, error) => {
  //      console.log(client);
  const course = request.body;
  course.clientId = clientId;
  CourseService.create(course)
      //})
      .then((course, error) => {
        console.log(course);
        response.json(course)
      });
};

const removeCourse = (request, response) => {
  CourseService.removeById(request.params.id)
      .then(() => response.send('ok'));
};

const updateCourse = (request, response) => CourseService.update(request.params.id, request.body)
    .then((course, error) => response.json(course));

const getCourse = (request, response) =>
    CourseService.getById(request.params.id)
        .then((course, error) => response.json(course));

const updateCourses = (request, response) => {
  CourseService.updateList(request.body.ids, request.body)
      .then((course, error) => response.json(course));
};

const router = express.Router()
    .get(   '/:clientId/course/list', passport.authenticate('jwt', {session: false}), listCourses)
    .post(  '/:clientId/course',      passport.authenticate('jwt', {session: false}), createCourse)
    .get(   '/:clientId/course/:id',  passport.authenticate('jwt', {session: false}), getCourse)
    .delete('/:clientId/course/:id',  passport.authenticate('jwt', {session: false}), removeCourse)
    .put(   '/:clientId/course/:id',  passport.authenticate('jwt', {session: false}), updateCourse)
    .put(   '/:clientId/course',      passport.authenticate('jwt', {session: false}), updateCourses);


module.exports = router;
