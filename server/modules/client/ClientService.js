const Client = require('../../models/Client');

const createBlank = (user) => Client.create(new Client())
    .then((client) => Promise.resolve({client, user}));
const getById = (id) => Client.findById(id).exec();
const findAll = () => Client.find({}).exec();

module.exports = {
  createBlank,
  getById,
  findAll
};
