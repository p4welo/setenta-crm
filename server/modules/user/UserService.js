const User = require('../../models/User');
const crypto = require('crypto');

const findAll = () => User.find({}).exec();

const authenticate = (email, password) => getByEmail(email)
    .then((user) => Promise.resolve(true));
    //.then((user) => Promise.resolve(user && user.password === encrypt(password)));

const encrypt = (word) => crypto.createHash('sha1').update(word).digest('base64');

const create = ({email, password}) => {
  const user = new User();
  Object.assign(user, {
    email,
    password: encrypt(password)
  });
  return User.create(user);
};

const passwordsMatches = (password, encrypted) => encrypted === encrypt(password);

const activate = (user, client) =>
    update(user._id, {
      client,
      state: 'ACTIVE'
    });


const update = (id, properties) => User.findByIdAndUpdate(id, properties, {upsert: true, new: true});

const getById = (id) => User.findById(id).exec();

const getByEmail = (email) => User.findOne({email}).exec();

module.exports = {
  authenticate,
  create,
  getById,
  getByEmail,
  activate,
  passwordsMatches,
  findAll,
  encrypt
};
