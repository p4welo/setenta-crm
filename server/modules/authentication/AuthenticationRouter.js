const express = require('express');
const passport = require('passport');
const jwt = require("jwt-simple");
const EmailService = require('../../services/EmailService');
const RecaptchaService = require('../../services/RecaptchaService');
const UserService = require('../user/UserService');
const ClientService = require('../client/ClientService');
const config = require('../../config/app');

const viewObjectMapper = ({_id, email, state}) => ({
  _id,
  email,
  state
});

const login = (request, response) => {
  var email = request.body.username;
  var password = request.body.password;
  if (email && password) {
    UserService.getByEmail(email)
        .then((user) => {
          if (user && UserService.passwordsMatches(password, user.password)) {
            var payload = {
              id: user._id
            };
            var token = jwt.encode(payload, config.auth.secretKey);
            response.json({
              token: token
            });
          } else {
            response.sendStatus(401);
          }
        });
  } else {
    response.sendStatus(401);
  }
};
const loggedIn = (request, response) => response.send(request.isAuthenticated() ? request.user : '0');
const registerUser = (request, response) => {
  //const {recaptcha} = request.body;
  //RecaptchaService.validate(recaptcha)
  //    .then(() => UserService.create(user))
  const userData = request.body;
  UserService.getByEmail(userData.email)
      .then((found) => {
        if (found) {
          response.status(400).json({message: 'Email exists'});
        }
        return UserService.create(request.body);
      })
      .then((user) => EmailService.sendActivationEmail(user))
      .then((user, error) => response.json(viewObjectMapper(user)));
};
const activateUser = (request, response) => {
  UserService.getById(request.params.id)
      .then((user) => {
        if (!user || user.state !== 'INACTIVE') {
          response.status(404).send();
        }
        return ClientService.createBlank(user);
      })
      .then(({client, user}) => UserService.activate(user, client))
      .then(response.json);
};

const router = express.Router()
    .post('/login', login)
    .get('/loggedin', loggedIn)
    .post('/register', registerUser)
    .get('/activate/:id', activateUser);


module.exports = router;