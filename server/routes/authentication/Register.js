const UserService = require('../../modules/user/UserService');
const EmailService = require('../../services/EmailService');

module.exports = (request, response) => {
  //const {recaptcha} = request.body;
  //RecaptchaService.validate(recaptcha)
  //    .then(() => UserService.create(user))
  const userData = request.body;

  UserService.getByEmail(userData.email)
      .then((found) => {
        if (found) {
          throw new Error('Email exists');
        }
        return UserService.create(userData);
      })
      .then((user) => EmailService.sendActivationEmail(user))
      .then(({_id, email, state}, error) => response.status(200).json({_id, email, state}))
      .catch(({message}) => response.status(400).json({message}));
};