const User = require('../../models/User');
const UserService = require('../../modules/user/UserService');
const AuthenticationService = require('../../services/AuthenticationService');
const SlackService = require('../../services/SlackService');
const { UserMapper } = rootRequire('dao/mappers');

module.exports = (request, response) => {
  const email = request.body.username;
  const password = request.body.password;
  const type = request.body.type;

  if (email && password && type) {
    User.findOne({email}).exec()
        .then((user) => {
          if (user && UserService.passwordsMatches(password, user.password)
              && user.type === type) {
            const token = AuthenticationService.getToken(UserMapper.serialize(user));
            const {email, client} = user;

            SlackService.loginSuccess(user);
            response.status(200).json({token, user: {email, client}});
          } else {
            response.sendStatus(401);
          }
        })
        .catch(() => response.sendStatus(401));
  } else {
    response.sendStatus(401);
  }
};