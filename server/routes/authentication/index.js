const auth = require('express').Router();
const { authenticate } = require('../../middleware/auth');
const Register = require('./Register');
const Activate = require('./Activate');
const Login = require('./Login');
const GetCurrentUser = require('./GetCurrentUser');
const {getObjectById} = require('../../services/DataObjectService');

auth.param  ('userId', getObjectById('User'));

// auth.post   ('/register', Register);
// auth.get    ('/activate/:userId', Activate);
auth.post   ('/login', Login);
auth.get    ('/', authenticate(), GetCurrentUser);

module.exports = auth;