const User = require('../../models/User');
const Client = require('../../models/Client');
const UserStates = require('../../models/constants/UserStates');

const activate = (user, client) => User.findByIdAndUpdate(
    user._id,
    { client, state: 'ACTIVE' },
    { upsert: true, new: true }
);

module.exports = (request, response) => {
  const user = request.User;

  if (user.state !== UserStates.INACTIVE) {
    response.sendStatus(404);
  }
  Client.create(new Client())
      .then((client) => activate(user, client))
      .then(response.status(200).json);
};