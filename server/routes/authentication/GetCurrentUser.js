const User = require('../../models/User');

module.exports = (request, response) => {
  User.findById(request.user.id).populate('client').exec()
      .then((user) => response.status(200).json(user));
};