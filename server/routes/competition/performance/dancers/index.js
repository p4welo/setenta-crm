const performance = require('express').Router();
const { getObjectById } = require('../../../../services/DataObjectService');
const CreateDancer = require('./CreateDancer');
const GetDancers = require('./GetDancers');
const GetDancerById = require('./GetDancerById');
const DeleteDancer = require('./DeleteDancer');

performance.param ('dancerId', getObjectById('CompetitionDancer'));

performance.get  ('/', GetDancers);
performance.post  ('/', CreateDancer);

performance.get   ('/:dancerId', GetDancerById);
// performance.put   ('/:dancerId', authenticate(), UpdateCourse);
performance.delete('/:dancerId', DeleteDancer);

module.exports = performance;
