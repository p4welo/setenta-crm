const Dancer = require('../../../../models/competition/Dancer');
const DataObjectStates = require('../../../../models/constants/DataObjectStates');

module.exports = (request, response) => {
  const dancerId = request.CompetitionDancer._id;

  Dancer.findByIdAndUpdate(
      dancerId,
      {objectState: DataObjectStates.DELETED, deletedAt: Date.now()},
      {upsert: true, new: true}
  )
      .then(() => response.status(200).json(dancerId))
      .catch(({message}) => response.status(400).json({message}))
};