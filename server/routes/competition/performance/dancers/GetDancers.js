const School = require('../../../../models/competition/School');
const Dancer = require('../../../../models/competition/Dancer');

module.exports = (request, response) => findSchoolByUserId(request.user.id)
    .then((school) => findDancersBySchool(school))
    .then((dancerList) => returnDancerList(response, dancerList));

const findSchoolByUserId = (userId) => School.find({user: ObjectId(userId)}).exec();
const findDancersBySchool = (school) => Dancer.find({school}).exec();
const returnDancerList = (response, dancerList) => response.status(200).json(dancerList);