const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const School = require('../../../../models/competition/School');
const Dancer = require('../../../../models/competition/Dancer');

module.exports = (request, response) => {
  School.find({ user: ObjectId(request.user.id) }).exec()
      .then((school) => {
        if (!school) {
          throw new Error('school.not.found');
        }
        else {
          return createDancer(Object.assign({ school }, request.body))
        }
      })
      .then((dancer) => returnDancer(response, dancer))
      .catch(({ message }) => response.status(400).json({ message }))
};

const createDancer = (dancer) => Dancer.create(dancer);
const returnDancer = (response, dancer) => response.status(200).json(dancer);