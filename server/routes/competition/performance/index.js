const performance = require('express').Router();
const Dancers = require('./dancers');
const {authenticate} = require('../../../middleware/auth');
const { getObjectById } = require('../../../services/DataObjectService');
const CreatePerformance = require('./CreatePerformance');
const GetPerformances = require('./GetPerformances');
const DeletePerformance = require('./DeletePerformance');

performance.param ('performanceId', getObjectById('CompetitionPerformance'));

performance.use('/dancers', Dancers);

performance.get  ('/', authenticate(), GetPerformances);
performance.post  ('/', authenticate(), CreatePerformance);
performance.delete('/:performanceId', authenticate(), DeletePerformance);

module.exports = performance;
