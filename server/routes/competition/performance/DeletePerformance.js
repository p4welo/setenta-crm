const Performance = require('../../../models/competition/Performance');
const Dancer = require('../../../models/competition/Dancer');
const DataObjectStates = require('../../../models/constants/DataObjectStates');
const UserTypes = require('../../../models/constants/UserTypes');
const ErrorMessages = require('../../../models/constants/ErrorMessages');
const { checkUserType } = require('../../../services/SecurityService');

module.exports = async (request, response) => {
  try {
    // await checkUserType(request.user.id, UserTypes.CRM);

    const performance = request.CompetitionPerformance;
    await deleteDancers(performance.dancers);
    await deletePerformance(performance._id);
    return response.status(200).json(performance._id);
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};

const deletePerformance = (id) => {
  return Performance.findByIdAndUpdate(
      id,
      { objectState: DataObjectStates.DELETED, deletedAt: Date.now() },
      { upsert: true, new: true }
  );
};
const deleteDancers = (dancers) => {
  return Dancer.update(
      { _id: { $in: dancers } },
      { objectState: DataObjectStates.DELETED, deletedAt: Date.now() },
      { multi: true, upsert: true, new: true }
  );
};