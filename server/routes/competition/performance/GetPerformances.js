const DataObjectStates = require('../../../models/constants/DataObjectStates');
const School = require('../../../models/competition/School');
const Performance = require('../../../models/competition/Performance');

module.exports = async (request, response) => {
  try {
    const school = await findSchoolByUserId(request.user.id);
    const performanceList = await findPerformancesBySchool(school);
    return response.status(200).json(performanceList);
  }
  catch ({ message }) {
    response.status(400).json({ message })
  }
};

const findSchoolByUserId = (userId) => School.findOne({ user: userId }).exec();
const findPerformancesBySchool = (school) => Performance.find({
  school,
  objectState: { $ne: DataObjectStates.DELETED }
}).populate('dancers').exec();