const School = require('../../../models/competition/School');
const Dancer = require('../../../models/competition/Dancer');
const Performance = require('../../../models/competition/Performance');
const ErrorMessages = require('../../../models/constants/ErrorMessages');

module.exports = async (request, response) => {
  try {
    const school = await findSchoolByUserId(request.user.id);
    if (!school) {
      throw new Error('school.not.found');
    }
    const dancers = await createDancers(request.body.dancers, school);
    const performance = await createPerformance(dancers, school, request.body);
    returnPerformance(response, performance);
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};

const findSchoolByUserId = (userId) => School.findOne({user: userId}).exec();
const createDancers = (dancers, school) => Dancer.create(dancers.map((dancer) => (
    {
      ...dancer,
      school
    }
)));
const createPerformance = (dancers, school, performance) => Performance.create(
    {
      ...performance,
      dancers,
      school
    }
);
const returnPerformance = (response, performance) => response.status(200).json(performance);