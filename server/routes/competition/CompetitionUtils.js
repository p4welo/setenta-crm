const { countBy, reduce, toLower, forEach } = require('ramda');
const { Types } = require('./CompetitionConstant');

function getDancersCharge(dancers) {
  const grouppedDancers = countBy((dancer) => toLower(dancer.firstName + dancer.lastName), dancers);

  const charges = Object.keys(grouppedDancers).map((key) => {
    const value = grouppedDancers[key];
    return {
      name: key,
      amount: value,
      value: 25 + (value - 1) * 10
    }
  });

  const addCharge = (sum, charge) => sum + charge.value;
  return reduce(addCharge, 0, charges);
}

const blockFitsPerformance = (block, performance) => {
  if (block.style === performance.style
      && block.type === performance.type
      && block.ageLevel === performance.ageLevel) {

    return !([Types.SOLO, Types.DUO].indexOf(performance.type) > -1
        && block.expLevel !== performance.expLevel);
  }
  return false;
};

const pushPerformanceIntoRightScheduleBlock = (performance, blocks) => {
  const block = blocks.find((scheduleBlock) => blockFitsPerformance(scheduleBlock, performance));
  if (block) {
    block.performances.push(performance);
  }
};

function groupPerformancesBySchedule(performances, scheduleBlocks) {
  const result = scheduleBlocks.map((block) => {
    const obj = block.toObject();
    return {
      ...obj,
      performances: []
    }
  });
  forEach((performance) => pushPerformanceIntoRightScheduleBlock(performance, result), performances);
  return result;
}

module.exports = {
  getDancersCharge,
  groupPerformancesBySchedule
};