const { getScheduleBlocks } = require('../../../services/competition/ScheduleService');
const { getSchoolActivePerformances } = require('../../../services/competition/PerformanceService');
const { groupPerformancesBySchedule } = require('../CompetitionUtils');

module.exports = async (request, response) => {
  try {
    const school = request.CompetitionSchool;
    const performances = await getSchoolActivePerformances(school);
    const scheduleBlocks = await getScheduleBlocks();
    response.status(200).json(groupPerformancesBySchedule(performances, scheduleBlocks));
  }
  catch ({ message }) {
    response.status(400).json({ message })
  }
};