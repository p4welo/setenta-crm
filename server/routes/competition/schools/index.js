const schools = require('express').Router();
const { getObjectById } = require('../../../services/DataObjectService');
const { authenticate } = require('../../../middleware/auth');
const CreateSchool = require('./CreateSchool');
const GetCurrentSchool = require('./GetCurrentSchool');
const GetSchoolById = require('./GetSchoolById');
const GetStartingNumbers = require('./GetStartingNumbers');
const GetDancers = require('./GetDancers');

schools.param('schoolId', getObjectById('CompetitionSchool'));

schools.get('/current', authenticate(), GetCurrentSchool);
schools.get('/:schoolId', GetSchoolById);
schools.get('/:schoolId/starting', GetStartingNumbers);
schools.get('/:schoolId/dancers', GetDancers);
schools.post('/', CreateSchool);

module.exports = schools;
