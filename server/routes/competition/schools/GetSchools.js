const School = require('../../../models/competition/School');

module.exports = (request, response) => {
  School.find({}).exec()
      .then((schools, error) => response.status(200).json(schools));
};