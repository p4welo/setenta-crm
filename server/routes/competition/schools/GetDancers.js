const { getSchoolGroupedDancers } = require('../../../services/competition/DancerService');

module.exports = async (request, response) => {
  try {
    const school = request.CompetitionSchool;
    const dancers = await getSchoolGroupedDancers(school);
    response.status(200).json(dancers);
  }
  catch ({ message }) {
    response.status(400).json({ message })
  }
};