const AuthenticationService = require('../../../services/AuthenticationService');
const Status = require('http-status');
const { UserTypeDao, UserDao, SchoolDao, AddressDao } = require('../../../dao');
const { UserMapper, SchoolMapper, AddressMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  const { email, password } = request.body;

  // FETCH USER TYPE
  let userType;
  try {
    userType = await UserTypeDao.getByName('COMPETITION');
  }
  catch (error) {
    userTypeNotFoundError(response, error);
  }

  // CREATE USER
  let user;
  try {
    const userData = UserMapper.toDatabase(email, password, userType);
    user = await UserDao.create(userData);
  }
  catch (error) {
    if (error.name === 'SequelizeUniqueConstraintError') {
      userAlreadyExistsError(response, error);
    }
    else {
      await transaction.rollback();
      cannotCreateUserError(response, error);
    }
  }

  // CREATE SCHOOL
  const { name, phone } = request.body;
  let school;
  try {
    const schoolData = SchoolMapper.toDatabase(name, phone, user);
    school = await SchoolDao.create(schoolData);
  }
  catch (error) {
    cannotCreateSchoolError(response, error);
  }

  // CREATE ADDRESS
  const { street, zip, city } = request.body;
  let address;
  try {
    const addressData = AddressMapper.toDatabase(school, city, zip, street);
    address = await AddressDao.create(addressData);
  }
  catch (error) {
    cannotCreateAddressError(response, error);
  }

  const token = AuthenticationService.getToken(user, 'sid');
  response.status(Status.OK).json({ token });
};

const userTypeNotFoundError = (response, error) => {
  response.status(Status.NOT_FOUND).json({
    type: 'user.type.not.found',
    details: error.details
  });
};

const userAlreadyExistsError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'user.already.exists',
    details: error.details
  });
};

const cannotCreateUserError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.user',
    details: error.details
  });
};

const cannotCreateAddressError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.address',
    details: error.details
  });
};

const cannotCreateSchoolError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.school',
    details: error.details
  });
};