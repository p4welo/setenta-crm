const ErrorMessages = require('../../../models/constants/ErrorMessages');
const { findSchoolByUserId } = require('../../../services/competition/SchoolService');

module.exports = async (request, response) => {
  try {
    const school = await findSchoolByUserId(request.user.id);
    response.status(200).json(school);
  }
  catch ({ message }) {
    response.status(400).json({ message })
  }

};
