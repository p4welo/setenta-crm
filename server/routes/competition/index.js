const competition = require('express').Router();
const {authenticate} = require('../../middleware/auth');
const Schools = require('./schools');
const Performances = require('./performance');
const Finances = require('./finances');
const Schedule = require('./schedule');
const Reports = require('./reports');

competition.use('/schools', Schools);
competition.use('/performances', Performances);
competition.use('/finances', authenticate(), Finances);
competition.use('/schedule', authenticate(), Schedule);
competition.use('/reports', authenticate(), Reports);

module.exports = competition;