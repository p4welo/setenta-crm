const reports = require('express').Router();
const GetStartingList = require('./GetStartingList');

reports.get  ('/starting', GetStartingList);

module.exports = reports;
