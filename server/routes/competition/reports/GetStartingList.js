const { getScheduleBlocks } = require('../../../services/competition/ScheduleService');
const { getActivePerformances } = require('../../../services/competition/PerformanceService');
const { groupPerformancesBySchedule } = require('../CompetitionUtils');
const { checkUserType } = require('../../../services/SecurityService');
const UserTypes = require('../../../models/constants/UserTypes');
const ErrorMessages = require('../../../models/constants/ErrorMessages');

module.exports = async (request, response) => {
  try {
    await checkUserType(request.user.id, UserTypes.CRM);
    const performances = await getActivePerformances();
    const scheduleBlocks = await getScheduleBlocks();
    response.status(200).json(groupPerformancesBySchedule(performances, scheduleBlocks));
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};