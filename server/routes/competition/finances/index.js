const schools = require('express').Router();
const GetSchoolSummary = require('./GetSchoolSummary');
const GetAdminSummary = require('./GetAdminSummary');

// schools.get  ('/details', GetFinanceDetails);
schools.get  ('/summary', GetAdminSummary);
schools.get  ('/', GetSchoolSummary);

module.exports = schools;