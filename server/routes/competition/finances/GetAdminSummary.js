const UserTypes = require('../../../models/constants/UserTypes');
const ErrorMessages = require('../../../models/constants/ErrorMessages');
const { getGroupedPerformances } = require('../../../services/competition/PerformanceService');
const { checkUserType } = require('../../../services/SecurityService');
const { getSchoolCharges } = require('../../../utils/competition/FinanceUtils');

module.exports = async (request, response) => {
  try {
    await checkUserType(request.user.id, UserTypes.CRM);
    const performances = await getGroupedPerformances();
    const calculated = getSchoolCharges(performances);
    response.status(200).json(calculated);
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};
