const { getDancersCharge } = require('../../../utils/competition/FinanceUtils');
const { findSchoolByUserId } = require('../../../services/competition/SchoolService');
const { findDancersBySchool } = require('../../../services/competition/DancerService');

module.exports = (request, response) => findSchoolByUserId(request.user.id)
    .then((school) => findDancersBySchool(school))
    .then((dancers) => returnCharge(response, dancers));

const returnCharge = (response, dancers) => response.status(200).json(getDancersCharge(dancers));