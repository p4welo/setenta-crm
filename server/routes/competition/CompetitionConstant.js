const Days = {
  SATURDAY: 'SATURDAY',
  SUNDAY: 'SUNDAY'
};

const Styles = {
  HIP_HOP: 'HIP_HOP',
  JAZZ: 'JAZZ',
  CONTEMPORARY: 'CONTEMPORARY',
  SHOW_DANCE: 'SHOW_DANCE',
  BALLET: 'BALLET',
  DISCO_DANCE: 'DISCO_DANCE',
  ART: 'ART',
  MODERN: 'MODERN'
};

const Types = {
  SOLO: 'SOLO',
  DUO: 'DUO',
  MINI_FORMATION: 'MINI_FORMATION',
  FORMATION: 'FORMATION'
};

const AgeLevels = {
  TO_8: 'TO_8',
  FROM_9_TO_11: 'FROM_9_TO_11',
  FROM_12_TO_15: 'FROM_12_TO_15',
  FROM_16: 'FROM_16'
};

const ExpLevels = {
  DEBUT: 'DEBUT',
  MASTER: 'MASTER'
};

module.exports = {
  Days,
  Styles,
  Types,
  AgeLevels,
  ExpLevels
};