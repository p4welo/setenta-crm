const { getActivePerformances } = require('../../../services/competition/PerformanceService');
const { getScheduleBlocks } = require('../../../services/competition/ScheduleService');
const { groupPerformancesBySchedule } = require('../CompetitionUtils');
const Performance = require('../../../models/competition/Performance');
const UserTypes = require('../../../models/constants/UserTypes');
const ErrorMessages = require('../../../models/constants/ErrorMessages');
const { checkUserType } = require('../../../services/SecurityService');

module.exports = async (request, response) => {
  try {
    await checkUserType(request.user.id, UserTypes.CRM);

    const performances = await getActivePerformances();
    const performanceObjects = performances.map((performance) => performance.toObject());
    const scheduleBlocks = await getScheduleBlocks();
    const finalBlocks = groupPerformancesBySchedule(performanceObjects, scheduleBlocks);
    let number = 1;
    finalBlocks.forEach((block) => {
      block.performances.forEach(async (performance) => {
        performance.number = number++;
        await Performance.findByIdAndUpdate(
            performance._id,
            { number: performance.number },
            { upsert: true, new: true }
        );
      });
    });
    response.status(200).json(finalBlocks);
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};
