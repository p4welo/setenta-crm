const UserTypes = require('../../../models/constants/UserTypes');
const Schedule = require('../../../models/competition/Schedule');
const ErrorMessages = require('../../../models/constants/ErrorMessages');
const { checkUserType } = require('../../../services/SecurityService');

module.exports = async (request, response) => {
  try {
    await checkUserType(request.user.id, UserTypes.CRM);
    const schedule = await Schedule.create(request.body);
    response.status(200).json(schedule);
  }
  catch ({ message }) {
    response.status(message === ErrorMessages.UNAUTHORIZED ? 401 : 400).json({ message })
  }
};
