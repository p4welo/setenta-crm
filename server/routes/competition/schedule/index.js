const schedule = require('express').Router();
const { getObjectById } = require('../../../services/DataObjectService');
const GenerateNumbers = require('./GenerateNumbers');

schedule.param ('scheduleId', getObjectById('CompetitionSchedule'));

schedule.get  ('/', GenerateNumbers);
// schedule.post  ('/', CreatePerformance);
// schedule.delete('/:scheduleId', DeletePerformance);

module.exports = schedule;
