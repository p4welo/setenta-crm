const Registration = require('../../models/Registration');
const Course = require('../../models/Course');
const _ = require('lodash');
const Status = require('http-status');
const moment = require('moment');

module.exports = async (request, response) => {
  // const clientId = request.Client._id;

  const courses = await Course.find({
    objectState: 'ACTIVE',
    isPublic: true,
    // clientId
  }).exec();

  const registrations = await Registration.find({
    course: { $in: courses },
    createdAt: { $gte: moment().startOf('day').subtract(7, 'day') }
  }).sort({ createdAt: -1 }).populate('course').exec();

  const todayMorning = moment().startOf('day');

  response.status(Status.OK).json(registrations.map((registration) => {
    const day = moment(registration.createdAt).startOf('day');
    return {
      ...registration.toObject(),
      daysBefore: todayMorning.diff(day, 'days')
    };
  }));
};