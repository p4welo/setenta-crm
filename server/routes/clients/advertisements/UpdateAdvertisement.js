const Advertisement = require('../../../models/PartnerAdvertisement');

module.exports = (request, response) => {
  const advertisementId = request.PartnerAdvertisement._id;
  const properties = request.body;

  properties.updatedAt = Date.now();
  return Advertisement.findByIdAndUpdate(advertisementId, properties, {upsert: true, new: true})
      .then((advertisement) => response.status(200).json(advertisement))
      .catch((err) => console.log(err));
};