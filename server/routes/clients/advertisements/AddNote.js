const Advertisement = require('../../../models/PartnerAdvertisement');

module.exports = (request, response) => {
  const advertisementId = request.PartnerAdvertisement._id;
  console.log(request.user);
  const note = Object.assign({}, request.body, {
    createdBy: request.user.username
  });
  Advertisement.findByIdAndUpdate(advertisementId, {
        $push: {"notes": note}
      },
      {safe: true, upsert: true, new: true})
      .then((result) => response.status(200).json(result));
};