const Advertisement = require('../../../models/PartnerAdvertisement');

module.exports = (request, response) => {
  const clientId = request.Client._id;

  Advertisement.find().exec()
  //Advertisement.find({clientId}).exec()
      .then((advertisements, error) => response.status(200).json(advertisements));
};