const advertisements = require('express').Router();
const {authenticate} = require('../../../middleware/auth');
const {getObjectById} = require('../../../services/DataObjectService');
const AddNote = require('./AddNote');
const UpdateAdvertisement = require('./UpdateAdvertisement');
const GetAdvertisements = require('./GetAdvertisements');
const CreateAdvertisement = require('./CreateAdvertisement');

advertisements.param('advertisementId', getObjectById('PartnerAdvertisement'));

advertisements.put('/:advertisementId', authenticate(), UpdateAdvertisement);
advertisements.put('/:advertisementId/notes', authenticate(), AddNote);
advertisements.post('/', CreateAdvertisement);
advertisements.get('/', authenticate(), GetAdvertisements);

module.exports = advertisements;