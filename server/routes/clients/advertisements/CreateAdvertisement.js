const Advertisement = require('../../../models/PartnerAdvertisement');

module.exports = (request, response) => {
  const clientId = request.Client._id;
  const advertisementData = Object.assign({clientId}, request.body);

  Advertisement.create(advertisementData)
      .then((advertisement, error) => response.status(200).json(advertisement));
};