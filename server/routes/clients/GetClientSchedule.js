const Course = require('../../models/Course');

module.exports = (request, response) => {
  const clientId = request.Client._id;

  Course.find({
    objectState: 'ACTIVE',
    isPublic: true,
    clientId
  }).exec()
      .then((courses, error) => response.status(200).json(courses));
};