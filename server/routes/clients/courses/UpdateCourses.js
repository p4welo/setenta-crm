const CourseService = require('../../../modules/course/CourseService');

module.exports = (request, response) => {

  CourseService.updateList(request.body.ids, request.body)
      .then((course) => response.status(200).json(course));
};