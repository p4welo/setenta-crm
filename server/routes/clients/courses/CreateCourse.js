const CourseService = require('../../../modules/course/CourseService');

module.exports = (request, response) => {
  const client = request.Client;
  const courseData = Object.assign({client}, request.body);

  CourseService.create(courseData)
      .then((course, error) => response.status(200).json(course));
};