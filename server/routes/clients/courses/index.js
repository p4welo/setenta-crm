const courses = require('express').Router();
const {authenticate} = require('../../../middleware/auth');
const {getObjectById} = require('../../../services/DataObjectService');
const Registrations = require('./registrations');
const GetCourseById = require('./GetCourseById');
const GetCourses = require('./GetCourses');
const UpdateCourse = require('./UpdateCourse');
const DeleteCourse = require('./DeleteCourse');
const CreateCourse = require('./CreateCourse');
const UpdateCourses = require('./UpdateCourses');

courses.param ('courseId', getObjectById('Course'));

courses.use   ('/:courseId/registrations', Registrations);
// courses.use   ('/:courseId/registrations', authenticate(), Registrations);

courses.get   ('/:courseId', GetCourseById);
courses.put   ('/:courseId', authenticate(), UpdateCourse);
courses.delete('/:courseId', authenticate(), DeleteCourse);

courses.get   ('/', authenticate(), GetCourses);
courses.put   ('/', authenticate(), UpdateCourses);
courses.post  ('/', authenticate(), CreateCourse);

module.exports = courses;
