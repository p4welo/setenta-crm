const CourseService = require('../../../modules/course/CourseService');
const Course = require('../../../models/Course');
const DataObjectStates = require('../../../models/constants/DataObjectStates');

module.exports = (request, response) => {
  const courseId = request.Course._id;

  Course.findByIdAndUpdate(
      courseId,
      {objectState: DataObjectStates.DELETED, deletedAt: Date.now()},
      {upsert: true, new: true}
  )
      .then(() => response.status(200).json({removed: true}))
      .catch(({message}) => response.status(400).json({message}));
};