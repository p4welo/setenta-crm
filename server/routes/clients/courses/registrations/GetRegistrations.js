const RegistrationService = require('../../../../modules/registration/RegistrationService');

module.exports = (request, response) => {
  const course = request.Course;

  RegistrationService.findByCourse(course)
      .then((registrations) => response.status(200).json(registrations));
};