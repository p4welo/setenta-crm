const RegistrationService = require('../../../../modules/registration/RegistrationService');
const RecaptchaService = require('../../../../services/RecaptchaService');
const EmailService = require('../../../../services/EmailService');
const SmsService = require('../../../../services/SmsService');

module.exports = (request, response) => {
  const course = request.Course;
  const registrationData = Object.assign({course}, request.body);

  RecaptchaService.validate(registrationData.recaptcha)
      .then(() => RegistrationService.create(registrationData))
      .then((customer) => EmailService.clientNotificationAfterCourseRegistration(customer))
      .then((customer) => EmailService.customerConfirmationAfterCourseRegistration(customer))
      .then((customer) => SmsService.customerCourseRegistrationNotification(customer))
      .then((customer) => response.status(200).json(customer))
      .catch((errorCodes, arg) => response.status(400).json({arg, captcha: errorCodes}));
};