const registrations = require('express').Router();
const { getObjectById } = require('../../../../services/DataObjectService');
const { authenticate } = require('../../../../middleware/auth');
const GetRegistrationById = require('./GetRegistrationById');
const GetRegistrations = require('./GetRegistrations');
const CreateRegistration = require('./CreateRegistration');

registrations.param('registrationId', getObjectById('Registration'));

registrations.get('/:registrationId', authenticate(), GetRegistrationById);

registrations.get('/', authenticate(), GetRegistrations);
registrations.post('/', authenticate(), CreateRegistration);

module.exports = registrations;