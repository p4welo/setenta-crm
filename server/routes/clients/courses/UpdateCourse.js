const Course = require('../../../models/Course');

module.exports = (request, response) => {
  const courseId = request.Course._id;
  const properties = request.body;

  properties.updatedAt = Date.now();
  delete properties.__v;
  return Course.findByIdAndUpdate(courseId, properties, {upsert: true, new: true})
      .then((course) => response.status(200).json(course))
      .catch((err) => console.log(err));
};