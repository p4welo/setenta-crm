const Course = require('../../../models/Course');
const DataObjectStates = require('../../../models/constants/DataObjectStates');

module.exports = (request, response) => {
  const clientId = request.Client._id;

  Course.find({
    clientId,
    objectState: {$ne: DataObjectStates.DELETED}
  })
      .exec()
      .then((courses, error) => response.status(200).json(courses));
};