const Client = require('../../models/Client');

module.exports = (request, response) => {
  Client.find({}).exec()
      .then((clients, error) => response.status(200).json(clients));
};