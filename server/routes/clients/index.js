const clients = require('express').Router();
const {authenticate} = require('../../middleware/auth');
const {getObjectById} = require('../../services/DataObjectService');
const GetClientById = require('./GetClientById');
const GetClients = require('./GetClients');
const GetClientSchedule = require('./GetClientSchedule');
const GetClientRegistrations = require('./GetClientRegistrations');
const Courses = require('./courses');
const Templates = require('./templates');
const Advertisements = require('./advertisements');
const Visits = require('./visits');

clients.param('clientId', getObjectById('Client'));

clients.use('/:clientId/courses', Courses);
clients.use('/:clientId/templates', authenticate(), Templates);
clients.use('/:clientId/advertisements', Advertisements);

clients.get('/:clientId/registrations', authenticate(), GetClientRegistrations);
clients.get('/:clientId/schedule', GetClientSchedule);
clients.get('/:clientId', GetClientById);
clients.get('/', GetClients);

module.exports = clients;