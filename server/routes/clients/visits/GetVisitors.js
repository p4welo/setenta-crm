const Visitor = require('../../../models/Visitor');
const { map } = require('ramda');

module.exports = (request, response) => {
  const clientId = request.Client._id;

  Visitor
      .find({clientId})
      .sort({updatedAt: -1})
      .limit(20)
      .exec()
      .then((visitors, error) => response.status(200).json(map((visitor) => {
        return Object.assign(visitor.toObject(), {
          lastVisit: visitor.visits[visitor.visits.length - 1]
        })
      }, visitors)));
};