const visitors = require('express').Router();
const {authenticate} = require('../../../middleware/auth');
const {getObjectById} = require('../../../services/DataObjectService');
const InitVisit = require('./InitVisit');
const GetVisits = require('./GetVisits');

visitors.param('visitorId', getObjectById('Visitor'));

visitors.post('/', InitVisit);
visitors.get('/', authenticate(), GetVisits);

module.exports = visitors;