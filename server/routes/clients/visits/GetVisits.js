const Visit = require('../../../models/Visit');
const { pipe, uniqBy, groupBy } = require('ramda');

module.exports = (request, response) => {
  const clientId = request.Client._id;
  const d = new Date(),
      month = d.getMonth(),
      year = d.getFullYear();

  Visit
      .find({
        clientId,
        createdAt: {$lt: new Date(), $gt: new Date(year + ',' + month)}
      })
      .sort({updatedAt: -1})
      .exec()
      .then((visits, error) => {
        const byDayAndId = (visit) => `${visit.userKey}-${visit.createdAt.getDate()}`;
        const byDay = (visit) => visit.createdAt.getDate();
        const result = pipe(
            uniqBy(byDayAndId),
            groupBy(byDay)
        )(visits);

        response.status(200).json(result)
      });
};