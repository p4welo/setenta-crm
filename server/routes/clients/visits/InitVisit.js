const Visit = require('../../../models/Visit');
const mongoose = require('mongoose');

module.exports = (request, response) => {

  const createVisit = (userKey) => Visit.create({
    userKey,
    url,
    ip,
    resolution,
    clientId
  }).then(returnUserKey);

  const createVisitor = () => createVisit(mongoose.Types.ObjectId());
  const returnUserKey = ({userKey}) => response.status(200).json({userKey});

  const clientId = request.Client._id;
  const {userKey, url, ip, resolution} = request.body;

  if (userKey) {
    Visit.find({userKey}).exec()
        .then((visits) => {
          if (visits && visits.length > 0) {
            return createVisit(userKey);
          }
          else {
            return createVisitor();
          }
        });
  }
  else {
    return createVisitor();
  }
};