const Template = require('../../../models/Template');

module.exports = (request, response) => {
  const clientId = request.Client._id;
  const templateData = Object.assign({clientId}, request.body);

  Template.create(templateData)
      .then((template, error) => response.status(200).json(template));
};