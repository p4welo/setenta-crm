const templates = require('express').Router();
const GetTemplateById = require('./GetTemplateById');
const UpdateTemplate = require('./UpdateTemplate');
const GetTemplates = require('./GetTemplates');
const CreateTemplate = require('./CreateTemplate');
const {getObjectById} = require('../../../services/DataObjectService');

templates.param('templateId', getObjectById('Template'));

templates.get('/:templateId', GetTemplateById);
templates.put('/:templateId', UpdateTemplate);

templates.post('/', CreateTemplate);
templates.get('/', GetTemplates);

module.exports = templates;