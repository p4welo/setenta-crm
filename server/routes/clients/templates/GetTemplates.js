const Template = require('../../../models/Template');

module.exports = (request, response) => {
  const clientId = request.Client._id;

  Template.find({clientId}).exec()
      .then((courses, error) => response.status(200).json(courses));
};