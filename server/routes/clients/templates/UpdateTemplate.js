const Template = require('../../../models/Template');

module.exports = (request, response) => {
  const templateId = request.Template._id;
  const properties = request.body;

  properties.updatedAt = Date.now();
  return Template.findByIdAndUpdate(templateId, properties, {upsert: true, new: true})
      .then((template) => response.status(200).json(template))
};