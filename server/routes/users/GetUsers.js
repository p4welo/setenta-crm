const UserService = require('../../modules/user/UserService');

module.exports = (request, response) => {
  UserService.findAll()
      .then((users, error) => response.status(200).json(users));
};