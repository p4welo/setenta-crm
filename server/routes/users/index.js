const users = require('express').Router();
const GetUserById = require('./GetUserById');
const GetUsers = require('./GetUsers');
const { getObjectById } = require('../../services/DataObjectService');
const { authenticate } = require('../../middleware/auth');

users.param('userId', getObjectById('User'));

users.get('/:userId', authenticate(), GetUserById);
users.get('/', authenticate(), GetUsers);

module.exports = users;