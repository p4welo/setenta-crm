const routes = require('express').Router();
const Authentication = require('./authentication');
const Clients = require('./clients');
const Competition = require('./competition');
const ApiV2 = require('./v2');
const Users = require('./users');

// IMPLEMENTATION v0.1 - DEPRECATED but still used on setenta.wroclaw.pl!
routes.use('/course', require('../modules/course'));
routes.use('/partner', require('../controllers/partnerController'));

// IMPLEMENTATION v1.0
routes.use('/auth', Authentication);
routes.use('/clients', Clients);
routes.use('/users', Users);

// IMPLEMENTATION v1.5 - connected to new DB
routes.use('/competition', Competition);


// IMPLEMENTATION v2.0
routes.use('/v2', ApiV2);


routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});

module.exports = routes;