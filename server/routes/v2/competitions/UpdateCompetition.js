const Status = require('http-status');
const logger = rootRequire('logger');
const CompetitionDao = rootRequire('dao/CompetitionDao');
const { CompetitionMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const properties = request.body;
    const newCompetition = await CompetitionDao.update(competition.id, properties);
    response.status(Status.OK).json(CompetitionMapper.serialize(newCompetition));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};