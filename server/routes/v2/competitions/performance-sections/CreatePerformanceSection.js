const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, PerformanceSectionDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { blocks, position, chapter, day } = request.body;
      const section = await PerformanceSectionDao.create(
          PerformanceSectionMapper.toDatabase(day, chapter, position, competition),
          blocks
      );
      response.status(Status.CREATED).json(PerformanceSectionMapper.serialize(section));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};