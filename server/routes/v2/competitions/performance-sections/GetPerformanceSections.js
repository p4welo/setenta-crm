const Status = require('http-status');
const { groupBy } = require('ramda');
const logger = rootRequire('logger');
const { PerformanceSectionDao, UserDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const sections = await PerformanceSectionDao.findByCompetition(competition);
      const byDay = (section) => section.day;
      const byChapter = (section) => section.chapter;
      const grouppedByDay = groupBy(byDay, sections.map(PerformanceSectionMapper.serialize));
      Object.keys(grouppedByDay).forEach((day) => {
        grouppedByDay[day] = groupBy(byChapter, grouppedByDay[day]);
      });
      response.status(Status.OK).json(grouppedByDay);
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};