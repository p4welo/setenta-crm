const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceSectionDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance_section } = request.db;
    const properties = request.body;
    const newSection = await PerformanceSectionDao.update(performance_section.id, properties);
    response.status(Status.OK).json(PerformanceSectionMapper.serialize(newSection));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};