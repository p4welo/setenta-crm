const routes = require('express').Router();
const GetPerformanceSections = require('./GetPerformanceSections');
const CreatePerformanceSection = require('./CreatePerformanceSection');
const RemovePerformanceSection = require('./RemovePerformanceSection');
const SwapPerformanceSections = require('./SwapPerformanceSections');
const MergePerformanceSections = require('./MergePerformanceSections');
const UpdateSection = require('./UpdateSection');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('performanceSectionSid', getBySid('performance_section'));

routes.get('/', GetPerformanceSections);
routes.post('/', CreatePerformanceSection);

routes.delete('/:performanceSectionSid', RemovePerformanceSection);

routes.put('/:performanceSectionSid/swap', SwapPerformanceSections);
routes.put('/:performanceSectionSid/merge', MergePerformanceSections);
routes.put('/:performanceSectionSid', UpdateSection);

module.exports = routes;
