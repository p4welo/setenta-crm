const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceSectionDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { performance_section, competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      await PerformanceSectionDao.remove(performance_section);
      response.status(Status.NO_CONTENT).json({ removed: true });
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};