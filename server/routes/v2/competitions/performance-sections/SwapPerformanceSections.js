const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceSectionDao, UserDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance_section, competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { destinationSid } = request.body;
      const destinationSection = await PerformanceSectionDao.getBySid(destinationSid);

      const originPosition = performance_section.position;
      const destinationPosition = destinationSection.position;

      await PerformanceSectionDao.update(destinationSection.id, { position: originPosition });
      const result = await PerformanceSectionDao.update(performance_section.id, { position: destinationPosition });

      response.status(Status.OK).json(PerformanceSectionMapper.serialize(result));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};