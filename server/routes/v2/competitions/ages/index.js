const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

const List = require('./GetAgeList');
const Create = require('./CreateAge');

routes.param('ageSid', getBySid('performance_age'));

routes.get('/', List);
routes.post('/', Create);


module.exports = routes;