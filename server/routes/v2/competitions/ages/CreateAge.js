const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceAgeDao, UserDao } = rootRequire('dao');
const { PerformanceAgeMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const user = await UserDao.getBySid(request.user.id);

    if (user.isOwnerOf(competition)) {
      const { name, min, max } = request.body;
      const ageData = PerformanceAgeMapper.toDatabase(name, min, max, competition);
      const age = await PerformanceAgeDao.create(ageData);
      response.status(Status.OK).json(PerformanceAgeMapper.serialize(age));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: 'cannot.create.age' });
  }
};