const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceAgeDao } = rootRequire('dao');
const { PerformanceAgeMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const ages = await PerformanceAgeDao.findByCompetition(competition);

    response.status(Status.OK).json(ages.map(PerformanceAgeMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e.stack });
  }
};
