const routes = require('express').Router();
const GetResults = require('./GetResults');

routes.get('/', GetResults);

module.exports = routes;