const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceDao, PerformanceSectionDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');
const { groupPerformancesBySection, groupSections } = require('../starting-list/StartingListUtils');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const performances = await PerformanceDao.findResultedByCompetition(competition);
    // TODO: filter out no places and exclude user data
    const sections = await PerformanceSectionDao.findByCompetition(competition);
    groupPerformancesBySection(sections, performances);

    const result = groupSections(sections.map(PerformanceSectionMapper.serialize));
    response.status(Status.OK).json(result);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
