const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceExperienceDao, UserDao } = rootRequire('dao');
const { PerformanceExperienceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const user = await UserDao.getBySid(request.user.id);

    if (user.isOwnerOf(competition)) {
      const { name } = request.body;
      const experienceData = PerformanceExperienceMapper.toDatabase(name, competition);
      const experience = await PerformanceExperienceDao.create(experienceData);
      response.status(Status.OK).json(PerformanceExperienceMapper.serialize(experience));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: 'cannot.create.experience'});
  }
};