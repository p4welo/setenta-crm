const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceExperienceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance_experience } = request.db;
    response.status(Status.OK).json(PerformanceExperienceMapper.serialize(performance_experience));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};