const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

const List = require('./GetExperienceList');
const Create = require('./CreateExperience');
const Get = require('./GetExperience');
const Delete = require('./DeleteExperience');

routes.param('experienceSid', getBySid('performance_experience'));

routes.get('/', List);
routes.post('/', Create);

routes.get('/:experienceSid', Get);
routes.delete('/:experienceSid', Delete);

module.exports = routes;