const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceExperienceDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { performance_experience, competition } = request.db;
    const user = await UserDao.getBySid(request.user.id);

    if (user.isOwnerOf(competition)) {
      await PerformanceExperienceDao.remove(performance_experience);
      response.status(Status.NO_CONTENT).json({ removed: true });
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};