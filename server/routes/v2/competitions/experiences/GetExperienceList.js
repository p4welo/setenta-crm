const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceExperienceDao } = rootRequire('dao');
const { PerformanceExperienceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const experiences = await PerformanceExperienceDao.findByCompetition(competition);

    response.status(Status.OK).json(experiences.map(PerformanceExperienceMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
