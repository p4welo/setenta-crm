const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionPaymentDao, UserDao } = rootRequire('dao');
const { CompetitionPaymentMapper } = rootRequire('dao/mappers');
const { CompetitionEmailService } = rootRequire('services');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { date, amount, userSid } = request.body;
      const user = await UserDao.getBySid(userSid);
      const payment = await CompetitionPaymentDao.create(CompetitionPaymentMapper.toDatabase(date, amount, competition, user));
      CompetitionEmailService.afterPaymentCreated(user, competition, payment);
      response.status(Status.CREATED).json(CompetitionPaymentMapper.serialize(payment));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};