const routes = require('express').Router();
const GetPayments = require('./GetPayments');
const CreatePayment = require('./CreatePayment');
const InitPaymentSession = require('./InitPaymentSession');

routes.get('/init', InitPaymentSession);
routes.get('/', GetPayments);
routes.post('/', CreatePayment);

module.exports = routes;