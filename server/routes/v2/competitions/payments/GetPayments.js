const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, CompetitionPaymentDao } = rootRequire('dao');
const { CompetitionPaymentMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    const participantSid = request.query.userSid;
    if (participantSid && currentUser.isOwnerOf(competition)) {
      const participant = await UserDao.getBySid(participantSid);
      const result = await CompetitionPaymentDao.findByCompetitionUser(competition, participant);
      response.status(Status.OK).json(result.map(CompetitionPaymentMapper.serialize));
    }
    else {
      const result = await CompetitionPaymentDao.findByCompetitionUser(competition, currentUser);
      response.status(Status.OK).json(result.map(CompetitionPaymentMapper.serialize));
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.METHOD_NOT_ALLOWED).json({details: e});
  }
};