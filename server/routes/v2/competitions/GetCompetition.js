const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const entity = CompetitionMapper.plainToEntity(competition);
    response.status(Status.OK).json(CompetitionMapper.serialize(entity));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};