const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, CompetitionDao } = require('../../../dao');
const { CompetitionMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { name, start, end, registrationEnd } = request.body;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isAdminType()) {
      const userSid = request.user.id;
      const user = await UserDao.getBySid(userSid);
      const competitionData = CompetitionMapper.toDatabase(name, user, start, end, registrationEnd);
      const competition = await CompetitionDao.create(competitionData);
      response.status(Status.OK).json(CompetitionMapper.serialize(competition));
    } else {
      logger.error('user.not.authorized');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.not.authorized' });
    }
  } catch (error) {
    logger.error(error.stack);
    cannotCreateCompetitionError(response, error);
  }
};

const cannotCreateCompetitionError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.competition',
    details: error.details
  });
};