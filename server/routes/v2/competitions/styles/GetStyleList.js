const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceStyleDao } = rootRequire('dao');
const { PerformanceStyleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const styles = await PerformanceStyleDao.findByCompetition(competition);

    response.status(Status.OK).json(styles.map(PerformanceStyleMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
