const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

const List = require('./GetStyleList');
const Create = require('./CreateStyle');
const Get = require('./GetStyle');
const Delete = require('./DeleteStyle');

routes.param('styleSid', getBySid('performance_style'));

routes.get('/', List);
routes.post('/', Create);

routes.get('/:styleSid', Get);
routes.delete('/:styleSid', Delete);

module.exports = routes;