const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceStyleDao, UserDao } = rootRequire('dao');
const { PerformanceStyleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { name, showTitle } = request.body;

      const styleData = PerformanceStyleMapper.toDatabase(name, competition, showTitle);
      const style = await PerformanceStyleDao.create(styleData);
      response.status(Status.OK).json(PerformanceStyleMapper.serialize(style));
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: 'cannot.create.style' });
  }
};