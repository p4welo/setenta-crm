const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionDao, UserDao } = require('../../../dao');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isAdminType()) {
      await CompetitionDao.remove(competition);
      response.status(Status.OK).json({ removed: true });
    } else {
      logger.error('user.not.authorized');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.not.authorized' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};