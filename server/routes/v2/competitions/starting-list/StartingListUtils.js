const { groupBy, forEach, filter, concat, pipe, sort } = require('ramda');

const orderByPlace = (firstPerformance, secondPerformance) => firstPerformance.place - secondPerformance.place;

const groupPerformancesBySection = (sections = [], performances = []) => {
  forEach((section) => {
    forEach((block) => {
      const found = filter((performance) => block.equalsPerformance(performance))(performances);

      if (found) {
        section.performances = pipe(
            concat(section.performances),
            sort(orderByPlace)
        )(found);
      }
    })(section.blocks)
  })(sections);
};

const groupSectionsByDay = (sections = []) => {
  const byDay = (section) => section.day;
  return groupBy(byDay, sections);
};

const groupSectionsByChapter = (sections = []) => {
  const byChapter = (section) => section.chapter;
  return groupBy(byChapter, sections);
};

const groupSections = (sections = []) => {
  const grouppedByDay = groupSectionsByDay(sections);
  Object.keys(grouppedByDay).forEach((day) => {
    grouppedByDay[day] = groupSectionsByChapter(grouppedByDay[day]);
  });
  return grouppedByDay;
};

const generateNumbers = (sections = []) => {
  let counter = 1;
  forEach((section) => {
    forEach(async (performance) => performance.number = counter++)(section.performances);
  })(sections);
};

module.exports = {
  groupPerformancesBySection,
  groupSections,
  groupSectionsByChapter,
  generateNumbers
};