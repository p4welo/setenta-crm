const routes = require('express').Router();
const Generate = require('./Generate');
const GetStartingList = require('./GetStartingList');

routes.get('/', GetStartingList);
routes.get('/generate', Generate);

module.exports = routes;