const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceDao, PerformanceSectionDao, UserDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');
const { groupPerformancesBySection, groupSections } = require('./StartingListUtils');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const participantSid = request.query.userSid;
    let participant;
    if (participantSid) {
      participant = await UserDao.getBySid(participantSid);
    }

    const performances = await PerformanceDao.findExtendedByCompetition(competition, participant);
    const sections = await PerformanceSectionDao.findByCompetition(competition);

    groupPerformancesBySection(sections, performances);

    const result = groupSections(sections.map(PerformanceSectionMapper.serialize));
    response.status(Status.OK).json(result);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};