const { generateNumbers } = require('./StartingListUtils');

const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceDao, PerformanceSectionDao, UserDao } = rootRequire('dao');
const { PerformanceSectionMapper } = rootRequire('dao/mappers');
const { groupPerformancesBySection, groupSections } = require('./StartingListUtils');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const performances = await PerformanceDao.findExtendedByCompetition(competition);
      const sections = await PerformanceSectionDao.findByCompetition(competition);

      groupPerformancesBySection(sections, performances);
      generateNumbers(sections);

      performances.forEach(async (performance) => {
        await PerformanceDao.update(performance.id, { number: performance.number });
      });

      const result = groupSections(sections.map(PerformanceSectionMapper.serialize));
      response.status(Status.OK).json(result);
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};