const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionRuleDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { competition_rule, competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      await CompetitionRuleDao.remove(competition_rule);
      response.status(Status.OK).json({ removed: true });
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};