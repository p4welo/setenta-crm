const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionRuleDao, UserDao } = rootRequire('dao');
const { CompetitionRuleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition, competition_rule } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const properties = request.body;
      if (properties.hasOwnProperty('ownMusicAllowed')) {
        properties.own_music_allowed = properties.ownMusicAllowed;
      }
      if (properties.hasOwnProperty('objectState')) {
        properties.object_state = properties.objectState;
      }
      const newCompetitionRule = await CompetitionRuleDao.update(competition_rule.id, properties);
      response.status(Status.OK).json(CompetitionRuleMapper.serialize(newCompetitionRule));
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};