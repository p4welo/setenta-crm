const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionRuleDao, PerformanceStyleDao, PerformanceTypeDao, PerformanceExperienceDao, PerformanceAgeDao, UserDao } = rootRequire('dao');
const { CompetitionRuleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const rules = await CompetitionRuleDao.findByCompetition(competition);
      rules.forEach(async (rule) => {
        await CompetitionRuleDao.remove(rule);
      });

      const ages = (await PerformanceAgeDao.findByCompetition(competition)) || [];
      const types = (await PerformanceTypeDao.findByCompetition(competition)) || [];
      const styles = (await PerformanceStyleDao.findByCompetition(competition)) || [];
      const experiences = (await PerformanceExperienceDao.findByCompetition(competition)) || [];

      ages.forEach(async (age) => {
        const data = CompetitionRuleMapper.toDatabase(null, 'AGE', competition, undefined, undefined, undefined, age);
        const ageRule = await CompetitionRuleDao.create(data);

        types.forEach(async (type) => {
          const data = CompetitionRuleMapper.toDatabase(ageRule, 'TYPE', competition, undefined, type);
          const typeRule = await CompetitionRuleDao.create(data);

          styles.forEach(async (style) => {
            const data = CompetitionRuleMapper.toDatabase(typeRule, 'STYLE', competition, style);
            const styleRule = await CompetitionRuleDao.create(data);

            experiences.forEach(async (experience) => {
              const data = CompetitionRuleMapper.toDatabase(styleRule, 'EXPERIENCE', competition, undefined, undefined, experience);
              await CompetitionRuleDao.create(data);
            })
          })
        })
      });
      const result = await CompetitionRuleDao.findByCompetition(competition);
      response.status(Status.OK).json(result.map(CompetitionRuleMapper.serialize));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: 'cannot.create.rules'});
  }
};