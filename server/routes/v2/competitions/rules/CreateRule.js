const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionRuleDao, PerformanceStyleDao, PerformanceTypeDao, PerformanceExperienceDao, PerformanceAgeDao, UserDao } = rootRequire('dao');
const { CompetitionRuleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition, competition_rule } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { ruleType, style, type, experience, age } = request.body;

      if (style) {
        const found = await PerformanceStyleDao.getBySid(style.sid);
        if (found) style.id = found.id;
      } else if (type) {
        const found = await PerformanceTypeDao.getBySid(type.sid);
        if (found) type.id = found.id;
      } else if (experience) {
        const found = await PerformanceExperienceDao.getBySid(experience.sid);
        if (found) experience.id = found.id;
      } else if (age) {
        const found = await PerformanceAgeDao.getBySid(age.sid);
        if (found) age.id = found.id;
      }

      const ruleData = CompetitionRuleMapper.toDatabase(competition_rule, ruleType, competition, style, type, experience, age);
      const rule = await CompetitionRuleDao.create(ruleData);
      response.status(Status.OK).json(CompetitionRuleMapper.serialize(rule));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: 'cannot.create.rule'});
  }
};