const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionRuleMapper } = rootRequire('dao/mappers');
const { CompetitionRuleDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const rules = await CompetitionRuleDao.findByCompetition(competition);
    response.status(Status.OK).json(rules.map(CompetitionRuleMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};

