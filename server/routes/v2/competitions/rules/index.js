const routes = require('express').Router();
const GetCompetitionRules = require('./GetCompetitionRules');
const CreateRule = require('./CreateRule');
const UpdateRule = require('./UpdateRule');
const DeleteRule = require('./DeleteRule');
const AutoFill = require('./AutoFill');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('ruleSid', getBySid('competition_rule'));

routes.get('/', GetCompetitionRules);
routes.post('/', CreateRule);
routes.post('/auto-fill', AutoFill);
routes.post('/:ruleSid', CreateRule);
routes.put('/:ruleSid', UpdateRule);
routes.delete('/:ruleSid', DeleteRule);


module.exports = routes;