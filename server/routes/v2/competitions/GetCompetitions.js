const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionDao, UserDao } = require('../../../dao');
const { CompetitionMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBy({sid: userSid});
    const competitions = await CompetitionDao.findPublicAndMine(user);
    response.status(Status.OK).json(competitions.map(CompetitionMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};