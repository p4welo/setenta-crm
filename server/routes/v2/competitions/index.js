const routes = require('express').Router();

const Performances = require('./performances');
const Rules = require('./rules');
const Finances = require('./finances');
const Payments = require('./payments');
const PerformanceSections = require('./performance-sections');
const StartingList = require('./starting-list');
const Results = require('./results');
const Styles = require('./styles');
const Experiences = require('./experiences');
const Types = require('./types');
const Ages = require('./ages');
const Invoices = require('./invoices');
const Licences = require('./licences');
const { authenticate } = rootRequire('middleware/auth');

const GetCompetitions = require('./GetCompetitions');
const CreateCompetition = require('./CreateCompetition');
const DeleteCompetition = require('./DeleteCompetition');
const GetCompetition = require('./GetCompetition');
const UpdateCompetition = require('./UpdateCompetition');
const { getBySid } = require('../../../services/DataObjectService');

routes.use('/:competitionSid/performances', authenticate(), Performances);
routes.use('/:competitionSid/rules', authenticate(), Rules);
routes.use('/:competitionSid/finances', authenticate(), Finances);
routes.use('/:competitionSid/payments', authenticate(), Payments);
routes.use('/:competitionSid/performance-sections', authenticate(), PerformanceSections);
routes.use('/:competitionSid/starting-list', authenticate(), StartingList);
routes.use('/:competitionSid/results', authenticate(), Results);
routes.use('/:competitionSid/styles', Styles);
routes.use('/:competitionSid/experiences', Experiences);
routes.use('/:competitionSid/types', Types);
routes.use('/:competitionSid/ages', Ages);
routes.use('/:competitionSid/invoices', authenticate(), Invoices);
routes.use('/:competitionSid/licence', authenticate(), Licences);

routes.param('competitionSid', getBySid('competition'));
routes.get('/', authenticate(), GetCompetitions);
routes.post('/', authenticate(), CreateCompetition);

routes.get('/:competitionSid', GetCompetition);
routes.put('/:competitionSid', authenticate(), UpdateCompetition);
routes.delete('/:competitionSid', authenticate(), DeleteCompetition);

module.exports = routes;