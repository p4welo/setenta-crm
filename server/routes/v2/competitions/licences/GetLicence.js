const Status = require('http-status');
const logger = rootRequire('logger');

const { CompetitionLicenceDao } = rootRequire('dao');
const { CompetitionLicenceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const licence = await CompetitionLicenceDao.getByCompetition(competition);

    response.status(Status.OK).json(CompetitionLicenceMapper.serialize(licence));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
