const routes = require('express').Router();
const GetLicence = require('./GetLicence');

routes.get('/', GetLicence);

module.exports = routes;
