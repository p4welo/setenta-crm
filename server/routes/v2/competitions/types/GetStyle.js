const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceStyleMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance_style } = request.db;
    response.status(Status.OK).json(PerformanceStyleMapper.serialize(performance_style));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};