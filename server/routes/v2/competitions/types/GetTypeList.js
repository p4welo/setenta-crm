const Status = require('http-status');
const logger = rootRequire('logger');

const { PerformanceTypeDao } = rootRequire('dao');
const { PerformanceTypeMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const types = await PerformanceTypeDao.findByCompetition(competition);

    response.status(Status.OK).json(types.map(PerformanceTypeMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e.stack });
  }
};
