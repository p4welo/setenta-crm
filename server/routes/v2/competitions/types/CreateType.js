const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceTypeDao, UserDao } = rootRequire('dao');
const { PerformanceTypeMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { name, min, max, showName } = request.body;
      const typeData = PerformanceTypeMapper.toDatabase(name, min, max, showName, competition);
      const type = await PerformanceTypeDao.create(typeData);
      response.status(Status.OK).json(PerformanceTypeMapper.serialize(type));
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (error) {
    logger.error(error.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: 'cannot.create.type' });
  }
};