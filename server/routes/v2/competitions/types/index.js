const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

const List = require('./GetTypeList');
const Create = require('./CreateType');
const Get = require('./GetStyle');
const Delete = require('./DeleteStyle');

routes.param('typeSid', getBySid('performance_type'));

routes.get('/', List);
routes.post('/', Create);


module.exports = routes;