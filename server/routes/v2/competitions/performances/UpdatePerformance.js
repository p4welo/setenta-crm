const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceDao } = rootRequire('dao');
const { PerformanceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance } = request.db;
    const properties = request.body;
    const newPerformance = await PerformanceDao.update(performance.id, properties);
    response.status(Status.OK).json(PerformanceMapper.serialize(newPerformance));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};