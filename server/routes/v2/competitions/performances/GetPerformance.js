const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceMapper } = require('../../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { performance } = request.db;
    response.status(Status.OK).json(PerformanceMapper.serialize(performance));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};