const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao } = rootRequire('dao');
const { PerformanceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);

    const performances = await PerformanceDao.findByUser(competition, user);
    response.status(Status.OK).json(performances.map(PerformanceMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};