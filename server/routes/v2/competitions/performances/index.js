const routes = require('express').Router();
const Audio = require('./audio');
const GetPerformances = require('./GetPerformances');
const CreatePerformance = require('./CreatePerformance');
const GetPerformance = require('./GetPerformance');
const DetelePerformance = require('./DetelePerformance');
const UpdatePerformance = require('./UpdatePerformance');

const { getBySid } = rootRequire('services/DataObjectService');

routes.param('performanceSid', getBySid('performance'));

routes.use('/:performanceSid/audio', Audio);

routes.get('/', GetPerformances);
routes.post('/', CreatePerformance);
routes.get('/:performanceSid', GetPerformance);
routes.delete('/:performanceSid', DetelePerformance);
routes.put('/:performanceSid', UpdatePerformance);

module.exports = routes;