const Status = require('http-status');
const logger = rootRequire('logger');
const { AudioDao, UserDao, PerformanceDao } = rootRequire('dao');
const { AudioMapper, PerformanceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const user = await UserDao.getBySid(request.user.id);
    const { performance } = request.db;

    const oldSid = performance.audio_sid;
    if (oldSid) {
      const currentSong = await AudioDao.getBySid(oldSid);
      if (currentSong) {
        await AudioDao.remove(currentSong);
      }
    }
    const { encoding, mimetype, destination, filename, path, size } = request.file;
    const audio = await AudioDao.create(AudioMapper.toDatabase(encoding, mimetype, destination, filename, path, size, user));
    const newPerformance = await PerformanceDao.update(performance.id, {
      audio_sid: audio.sid
    });
    response.status(Status.OK).json(PerformanceMapper.serialize(newPerformance));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};