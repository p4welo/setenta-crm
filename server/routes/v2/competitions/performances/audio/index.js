const routes = require('express').Router();
const CreateSong = require('./CreateSong');
const { authenticate } = rootRequire('middleware/auth');

const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `./public/uploads`)
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname))
  }
});
const MAX_SIZE = 20 * 1000 * 1000;
const upload = multer({ storage: storage, limits: { fileSize: MAX_SIZE } });

routes.post('/', upload.single('song'), authenticate(), CreateSong);

module.exports = routes;