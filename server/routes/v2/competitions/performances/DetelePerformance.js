const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao } = rootRequire('dao');
const CompetitionUtils = rootRequire('utils/competition/CompetitionUtils');

module.exports = async (request, response) => {
  try {
    const { performance, competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition) || CompetitionUtils.registrationInProgress(competition)) {
      await PerformanceDao.remove(performance);
      response.status(Status.OK).json({ removed: true });
    } else {
      response.status(Status.INTERNAL_SERVER_ERROR).json({ details: 'cannot.delete.performance' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
