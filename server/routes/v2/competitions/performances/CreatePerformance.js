const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao, SchoolDao } = rootRequire('dao');
const { PerformanceMapper } = rootRequire('dao/mappers');
const CompetitionUtils = rootRequire('utils/competition/CompetitionUtils');
const { NotificationService, CompetitionEmailService } = rootRequire('services');

module.exports = async (request, response) => {
  try {
    const user = await UserDao.getBySid(request.user.id);
    const { competition } = request.db;

    if (CompetitionUtils.registrationInProgress(competition)) {
      const { name, dancers, style, type, experience, ageLevel, title } = request.body;

      const performance = await PerformanceDao.create(PerformanceMapper.toDatabase(name, style, type, experience, ageLevel, user, competition, title), dancers);

      if (await isTheFirstPerformanceCreatedByUser(user, competition)) {
        const organizerUser = await UserDao.getBy({id: competition.user_id});
        const school = await SchoolDao.getByUser(user);

        NotificationService.performanceCreatedSuccess(user, competition);
        CompetitionEmailService.afterFirstPerformanceCreated(user, competition); // school
        CompetitionEmailService.newCompetitionUserRegistration(organizerUser, school); // organizer
      }

      response.status(Status.CREATED).json(PerformanceMapper.serialize(performance));
    }
    else {
      response.status(Status.INTERNAL_SERVER_ERROR).json({details: 'cannot.create.performance'});
    }

  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};

const isTheFirstPerformanceCreatedByUser = async (user, competition) => {
  const performances = await PerformanceDao.findByUser(competition, user);
  return performances.length < 2;
};