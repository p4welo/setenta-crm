const routes = require('express').Router();
const CreateInvoice = require('./CreateInvoice');
const GetInvoiceAsPdf = require('./GetInvoiceAsPdf');
const DeleteInvoice = require('./DeleteInvoice');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('invoiceSid', getBySid('invoice'));

routes.get('/:invoiceSid/pdf', GetInvoiceAsPdf);
routes.delete('/:invoiceSid', DeleteInvoice);
routes.post('/', CreateInvoice);

module.exports = routes;