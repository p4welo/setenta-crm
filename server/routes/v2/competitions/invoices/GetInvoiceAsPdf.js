const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao } = rootRequire('dao');
const { InfaktInvoiceService } = rootRequire('services');

module.exports = async (request, response) => {
  try {
    const { competition, invoice } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const pdf = await InfaktInvoiceService.getInvoiceAsPdf('b4f9abe8002de7f8e794d1e77bc18a2d579f065e', invoice.ext_id);
      pdf.data.pipe(response);
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};