const Status = require('http-status');
const logger = rootRequire('logger');
const { InvoiceDao, UserDao } = rootRequire('dao');
const { InvoiceMapper } = rootRequire('dao/mappers');
const { InfaktInvoiceService } = rootRequire('services');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const { nip, name, street, zip, city, title, netPrice, taxPrice, userSid } = request.body;
      const user = await UserDao.getBySid(userSid);
      const invoice = await InfaktInvoiceService.createInvoice(
          'b4f9abe8002de7f8e794d1e77bc18a2d579f065e',
          {
            bankAccount: '23105015751000009143972439',
            name,
            nip,
            street,
            zip,
            city,
            title,
            netPrice: netPrice * 100,
            taxPrice
          }
      );

      const result = await InvoiceDao.create(InvoiceMapper.toDatabase(
          invoice.client_company_name,
          invoice.client_tax_code,
          invoice.client_street,
          invoice.client_post_code,
          invoice.client_city,
          invoice.services[0].name,
          'INFAKT',
          invoice.id,
          invoice.number,
          invoice.net_price,
          invoice.tax_price,
          invoice.gross_price,
          competition,
          user
      ));
      response.status(Status.CREATED).json(InvoiceMapper.serialize(result));
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};