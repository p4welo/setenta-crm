const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionDao, CompetitionPaymentDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const users = await CompetitionDao.getCompetitionInvoiceDatas(competition);
      const payments = await CompetitionPaymentDao.findByCompetition(competition);
      const withPayments = mapWithPaymentInfo(users, payments);
      response.status(Status.OK).json(withPayments);
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};

const mapWithPaymentInfo = (users = [], userPayments = []) => {
  const paymentsByUser = {};
  userPayments.forEach((payment) => {
    const actual = paymentsByUser[payment.user.sid] || 0;
    paymentsByUser[payment.user.sid] = actual + (payment.amount || 0);
  });

  return users.map((user) => ({
    ...user,
    invoiceSid: user.invoice_sid,
    payments: paymentsByUser[user.sid],
  }));
};
