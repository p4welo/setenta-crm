const routes = require('express').Router();
const GetFee = require('./GetFee');
const GetInvoiceDatas = require('./GetInvoiceDatas');

routes.get('/fee', GetFee);
routes.get('/invoice-datas', GetInvoiceDatas);

module.exports = routes;