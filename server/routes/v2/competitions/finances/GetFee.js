const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, DancerDao, CompetitionDao, PerformanceTypeDao, PerformanceDao } = rootRequire('dao');
const { CompetitionMapper } = rootRequire('dao/mappers');
const { PaymentModels } = rootRequire('models/constants');
const CompetitionService = rootRequire('services/CompetitionService');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const { competition } = request.db;

    const entity = CompetitionMapper.plainToEntity(competition);
    const user = await UserDao.getBySid(userSid);

    if (entity.paymentModel === PaymentModels.DANCER_PERFORMANCE_AMOUNT) {
      response.status(Status.OK).json(await calculateFeesPerDancer(competition, user));
    } else if (entity.paymentModel === PaymentModels.SINGLE_DANCER_ENTRANCE) {
      response.status(Status.OK).json(await calculateSingleDancerFees(competition, user));
    } else {
      response.status(Status.OK).json(await calculateFeesPerType(competition, user));
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};

const calculateSingleDancerFees = async (competition, user) => {
  const result = await DancerDao.findByCompetitionUser(competition, user);
  const resultMapper = ({ firstName, lastName, sid }) => ({
    sid,
    firstName,
    lastName,
    fee: CompetitionService.calculateSingleDancerFee(competition)
  });

  return result.map(resultMapper);
};

const calculateFeesPerDancer = async (competition, user) => {
  const result = await CompetitionDao.countPerformancesByDancer(competition, user);
  const resultMapper = ({ firstName, lastName, performance_count, sid }) => ({
    sid,
    firstName,
    lastName,
    performanceCount: +performance_count,
    fee: CompetitionService.calculateDancerFee(+performance_count, competition)
  });

  return result.map(resultMapper);
};

const calculateFeesPerType = async (competition, user) => {
  const types = await PerformanceTypeDao.findByCompetition(competition);
  const performances = await PerformanceDao.findByUser(competition, user);
  return CompetitionService.calculateTypeFees(performances, types, competition);
};
