const Status = require('http-status');
const logger = rootRequire('logger');
const { DancerDao, UserDao } = rootRequire('dao');
const { DancerMapper } = rootRequire('dao/mappers');
const NotificationService = rootRequire('services/NotificationService');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);

    const { firstName, lastName, yearOfBirth, groupName } = request.body;
    const dancerData = DancerMapper.toDatabase(firstName, lastName, yearOfBirth, groupName, user);
    const dancer = await DancerDao.create(dancerData);

    if (await isTheFirstDancerCreatedToday(user)) {
      NotificationService.dancerCreatedSuccess(user);
    }

    response.status(Status.OK).json(DancerMapper.serialize(dancer));
  } catch (error) {
    logger.error(error.stack);
    cannotCreateDancerError(response, error);
  }
};

const cannotCreateDancerError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.dancer',
    details: error.details
  });
};

const isTheFirstDancerCreatedToday = async (user) => {
  const dancers = await DancerDao.findByUser(user); // TODO: zmienic na created TODAY
  return dancers.length < 2;
};