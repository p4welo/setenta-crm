const routes = require('express').Router();
const GetDancers = require('./GetDancers');
const DeleteDancer = require('./DeleteDancer');
const CreateDancer = require('./CreateDancer');
const { getBySid } = require('../../../services/DataObjectService');

routes.param('dancerSid', getBySid('dancer'));

routes.route('/')
    .get(GetDancers)
    .post(CreateDancer);

routes.route('/:dancerSid')
    .delete(DeleteDancer);

module.exports = routes;