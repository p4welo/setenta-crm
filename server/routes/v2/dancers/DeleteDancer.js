const Status = require('http-status');
const logger = rootRequire('logger');
const { DancerDao } = require('../../../dao');

module.exports = async (request, response) => {
  try {
    const { dancer } = request.db;
    await DancerDao.remove(dancer);
    response.status(Status.OK).json({ removed: true });
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};