const Status = require('http-status');
const logger = rootRequire('logger');
const { DancerDao, UserDao } = require('../../../dao');
const { DancerMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);
    const dancers = await DancerDao.findByUser(user);
    response.status(Status.OK).json(dancers.map(DancerMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};