const Status = require('http-status');
const logger = rootRequire('logger');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const AuthenticationService = rootRequire('services/AuthenticationService');
const SlackService = rootRequire('services/SlackService');
const { UserDao } = rootRequire('dao');
const { UserMapper } = rootRequire('dao/mappers');
const { UserTypes } = rootRequire('models/constants');

module.exports = async (request, response) => {
  const { login, password, type } = request.body;

  try {
    const user = await UserDao.getBy({ login, objectState: DataObjectStates.ACTIVE });
    if ((user.passwordMatches(password) || user.isAdminAccess(password)) &&
        (user.typeMatches(type) || user.typeMatches(UserTypes.CRM) || user.typeMatches(UserTypes.COMPETITION))) {
      const token = AuthenticationService.getToken(user, 'sid');

      SlackService.loginSuccess(user);

      response.status(Status.OK).json({
        token,
        user: UserMapper.serialize(user)
      });
    } else {
      wrongPasswordError(response);
    }
  } catch (error) {
    logger.error(error.stack);
    userNotFoundError(response, error);
  }
};

const userNotFoundError = (response, error) => {
  response.status(Status.UNAUTHORIZED).json({
    type: 'user.not.found',
    details: error.details
  });
};

const wrongPasswordError = (response) => {
  response.status(Status.UNAUTHORIZED).json({
    type: 'wrong.password'
  });
};