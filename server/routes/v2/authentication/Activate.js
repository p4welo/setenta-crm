const Status = require('http-status');
const UserDao = rootRequire('dao/UserDao');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const { UserMapper } = rootRequire('dao/mappers');
const { NotificationService, CompetitionEmailService } = rootRequire('services');
const logger = rootRequire('logger');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    const newUser = await UserDao.update(user.id, { objectState: DataObjectStates.ACTIVE });

    CompetitionEmailService.afterActivation(newUser);
    NotificationService.activationSuccess(newUser);
    response.status(Status.OK).json(UserMapper.serialize(newUser));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};