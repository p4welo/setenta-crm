const Status = require('http-status');
const logger = rootRequire('logger');
const UserDao = rootRequire('dao/UserDao');
const SidUtils = rootRequire('utils/SidUtils');
const { CompetitionEmailService } = rootRequire('services');

module.exports = async (request, response) => {
  try {
    const { email } = request.body;
    const user = await UserDao.getBy({ email });
    const newUser = await UserDao.update(user.id, { resetHash: SidUtils.generate() });
    CompetitionEmailService.afterPasswordReset(newUser);
    response.status(Status.OK).json({ type: 'ok' });
  } catch (error) {
    logger.error(error.stack);
    resetPasswordError(response, error);
  }
};

const resetPasswordError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'reset.password.error',
    details: error.details
  });
};
