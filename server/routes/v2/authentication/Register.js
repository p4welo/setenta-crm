const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, UserTypeDao, SchoolDao, AddressDao } = rootRequire('dao');
const { NotificationService, CompetitionEmailService }= rootRequire('services');
const { UserMapper, SchoolMapper, AddressMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  const { email, password, type, firstName, lastName, name, phone, city } = request.body;

  let userType;
  try {
    userType = await UserTypeDao.getByName(type);
  } catch (error) {
    logger.error(error.stack);
    userTypeNotFoundError(response, error);
  }

  let user, school;
  try {
    const userData = UserMapper.toDatabase(email, password, userType, firstName, lastName);
    user = await UserDao.create(userData);
    const schoolData = SchoolMapper.toDatabase(name, phone, user);
    school = await SchoolDao.create(schoolData);
    const addressData = AddressMapper.toDatabase(school, city);
    await AddressDao.create(addressData);
  } catch (error) {
    logger.error(error.stack);
    if (error.name === 'SequelizeUniqueConstraintError') {
      userAlreadyExistsError(response, error);
    } else {
      cannotCreateUserError(response, error);
    }
    return;
  }

  try {
    CompetitionEmailService.afterRegistration(user);
    NotificationService.registrationSuccess(user);
    response.status(Status.OK).json(UserMapper.serialize(user));
  } catch (error) {
    logger.error(error.stack);
  }
};

const userTypeNotFoundError = (response, error) => {
  response.status(Status.NOT_FOUND).json({
    type: 'user.type.not.found',
    details: error.details
  });
};

const userAlreadyExistsError = (response, error) => {
  response.status(Status.BAD_REQUEST).json({
    type: 'user.already.exists',
    details: error.details
  });
};

const cannotCreateUserError = (response, error) => {
  response.status(Status.BAD_REQUEST).json({
    type: 'cannot.create.user',
    details: error.details
  });
};