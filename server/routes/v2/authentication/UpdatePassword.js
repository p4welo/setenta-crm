const Status = require('http-status');
const logger = rootRequire('logger');
const { UserMapper } = require('../../../dao/mappers');
const UserDao = require('../../../dao/UserDao');
const AuthenticationService = require('../../../services/AuthenticationService');

module.exports = async (request, response) => {
  const { password } = request.body;
  const { resetHash } = request.params;

  let user;
  try {
    user = await UserDao.getBy({ resetHash });
  } catch (error) {
    logger.error(error.stack);
    return userNotFoundError(response, error);
  }

  try {
    const newUser = await UserDao.update(user.id, {
      password: AuthenticationService.encrypt(password),
      resetHash: ''
    });
    response.status(Status.OK).json(UserMapper.serialize(newUser));
  } catch (error) {
    logger.error(error.stack);
    cannotUpdateUserError(response, error);
  }
};

const userNotFoundError = (response, error) => {
  response.status(Status.NOT_FOUND).json({
    type: 'user.not.found',
    details: error.details
  });
};

const cannotUpdateUserError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.update.user',
    details: error.details
  });
};