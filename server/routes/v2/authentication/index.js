const routes = require('express').Router();
const Register = require('./Register');
const Login = require('./Login');
const Activate = require('./Activate');
const ResetPassword = require('./ResetPassword');
const UpdatePassword = require('./UpdatePassword');
const { getBySid } = require('../../../services/DataObjectService');

routes.param('userSid', getBySid('user'));

routes.post('/register', Register);
routes.post('/login', Login);
routes.get('/:userSid/activate', Activate);
routes.put('/reset', ResetPassword);
routes.put('/reset/:resetHash', UpdatePassword);
routes.get('/', async (req, res) => {
  res.json({ test: 'OK' });
});

module.exports = routes;