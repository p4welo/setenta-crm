const axios = require('axios');
const Status = require('http-status');
const logger = rootRequire('logger');
// const request = require('request');
const fs = require('fs');
const INFAKT_KEY = 'b4f9abe8002de7f8e794d1e77bc18a2d579f065e';
module.exports = async (req, res) => {
  try {
    // const parameters = {
    //   grant_type: "client_credentials",
    //   client_id: "1271442",
    //   client_secret: "8c9faf710a5e2f2e2686e74dcfc763ee"
    // };
    // const oauth = await axios.post(
    //     'https://secure.payu.com/pl/standard/user/oauth/authorize',
    //     'grant_type=client_credentials&client_id=1271442&client_secret=8c9faf710a5e2f2e2686e74dcfc763ee'
    // );


    // const result = await axios.post(
    //     'https://secure.payu.com/api/v2_1/orders',
    //     `notifyUrl=https%3A%2F%2Fyour.eshop.com%2Fnotify&customerIp=127.0.0.1&merchantPosId=145227&description=RTV%20market&currencyCode=PLN&totalAmount=21000&products=%5Bobject%20Object%5D&products=%5Bobject%20Object%5D`,
    //     {
    //       headers: {
    //         Authorization: `Bearer ${oauth.data.access_token}`,
    //         'Content-Type': 'application/json'
    //       }
    //     }
    // );
    // console.warn(result.data);

    const invoice = await axios.post(
        'https://api.infakt.pl/v3/invoices.json',
        `{"invoice":{"payment_method":"transfer", "bank_account": "70102010130000010200026526","client_company_name": "Szkoła tańca Setenta", "client_tax_code": "6972131230","client_street": "Sienkiewicza 6a","client_city": "Wrocław","client_post_code": "50-335", "services":[{"name": "Przykładowa Usługa", "net_price":12300, "tax_symbol": 0 }]}}`,
        {
          headers: {
            'X-inFakt-ApiKey': INFAKT_KEY,
            'Content-Type': 'application/json'
          }
        }
    );

    const pdf = await axios.get(
        `https://api.infakt.pl/v3/invoices/${invoice.data.id}/pdf.json?document_type=original&locale=pl`,
        {
          headers: {
            'X-inFakt-ApiKey': INFAKT_KEY
          },
          responseType: 'stream'
        },
    );

    pdf.data.pipe(res);

    // fs.createReadStream(pdf).pipe(res);
    // res.status(Status.OK).json({ok: true})

  } catch (e) {
    logger.error(e.stack);
    res.status(Status.METHOD_NOT_ALLOWED).json({ details: e });
  }
};