const routes = require('express').Router();
const Report = require('./report');
const Authentication = require('./authentication');
const Competitions = require('./competitions');
const Dancers = require('./dancers');
const Users = require('./users');
const UserTypes = require('./userTypes');
const Admin = require('./admin');
const Emails = require('./emails');
const Audio = require('./audio');
const InitPaymentSession = require('./InitPaymentSession');
const { authenticate } = rootRequire('middleware/auth');

routes.use('/auth', Authentication);
routes.use('/emails', Emails);
routes.use('/audio', Audio);

routes.use('/admin', authenticate(), Admin);
routes.use('/competitions', Competitions);
routes.use('/dancers', authenticate(), Dancers);
routes.use('/users', authenticate(), Users);
routes.use('/userTypes', authenticate(), UserTypes);

routes.use('/report', Report);

routes.get('/init', InitPaymentSession);

module.exports = routes;