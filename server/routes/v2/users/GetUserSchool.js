const Status = require('http-status');
const logger = rootRequire('logger');
const { SchoolMapper } = rootRequire('dao/mappers');
const { SchoolDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    const school = await SchoolDao.getByUser(user);
    response.status(Status.OK).json(SchoolMapper.serialize(school));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.NOT_FOUND).json({details: e});
  }
};