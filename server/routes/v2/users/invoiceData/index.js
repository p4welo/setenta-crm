const routes = require('express').Router();
const GetInvoiceData = require('./GetInvoiceData');

routes.get('/', GetInvoiceData);

module.exports = routes;