const Status = require('http-status');
const logger = rootRequire('logger');
const { InvoiceDataDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    const invoiceData = await InvoiceDataDao.getByUser(user);
    response.status(Status.OK).json(invoiceData);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};