const Status = require('http-status');
const logger = rootRequire('logger');
const UserDao = require('../../../dao/UserDao');
const { UserMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    const properties = request.body;
    const newUser = await UserDao.update(user.id, properties);
    response.status(Status.OK).json(UserMapper.serialize(newUser));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};