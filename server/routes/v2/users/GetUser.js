const Status = require('http-status');
const logger = rootRequire('logger');
const { UserMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    response.status(Status.OK).json(UserMapper.serialize(user));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.NOT_FOUND).json({details: e});
  }
};