const Status = require('http-status');
const logger = rootRequire('logger');
const { UserTypeDao, UserDao } = require('../../../dao');
const { UserMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  const { email, password, type } = request.body;

  let userType;
  try {
    userType = await UserTypeDao.getByName(type);
  }
  catch (error) {
    logger.error(error.stack);
    userTypeNotFoundError(response, error);
  }

  let user;
  try {
    const userData = UserMapper.toDatabase(email, password, userType);
    user = await UserDao.create(userData);
    response.status(Status.OK).json(UserMapper.serialize(user));
  }
  catch (error) {
    logger.error(error.stack);
    if (error.name === 'SequelizeUniqueConstraintError') {
      userAlreadyExistsError(response, error);
    }
    else {
      cannotCreateUserError(response, error);
    }
  }
};

const userTypeNotFoundError = (response, error) => {
  response.status(Status.NOT_FOUND).json({
    type: 'user.type.not.found',
    details: error.details
  });
};

const userAlreadyExistsError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'user.already.exists',
    details: error.details
  });
};

const cannotCreateUserError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.user',
    details: error.details
  });
};