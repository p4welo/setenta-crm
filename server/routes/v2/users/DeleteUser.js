const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao } = require('../../../dao');

module.exports = async (request, response) => {
  try {
    const { user } = request.db;
    await UserDao.remove(user);
    response.status(Status.OK).end();
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};