const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, SchoolDao, AddressDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);
    const { firstName, lastName, name, city } = request.body;
    const newUser = await UserDao.update(user.id, { firstName, lastName });
    const school = await SchoolDao.getByUser(newUser);
    const newSchool = await SchoolDao.update(school.id, { name });
    const address = await AddressDao.getBySchool(newSchool);
    await AddressDao.update(address.id, { city });

    const result = await UserDao.getBySidExtended(userSid);
    response.status(Status.OK).json(result);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};