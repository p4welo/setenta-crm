const routes = require('express').Router();
const GetMyCompetitions = require('./GetMyCompetitions');

routes.get('/', GetMyCompetitions);

module.exports = routes;