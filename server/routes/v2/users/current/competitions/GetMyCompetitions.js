const Status = require('http-status');
const logger = rootRequire('logger');
const { CompetitionDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);
    const result = await CompetitionDao.findByAttendingUser(user);
    response.status(Status.OK).json(result[0]);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};