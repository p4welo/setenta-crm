const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao } = rootRequire('dao');


module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const result = await UserDao.getBySidExtended(userSid);
    response.status(Status.OK).json(result);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.OK).json({ result: 404 });
  }
};