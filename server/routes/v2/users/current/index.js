const routes = require('express').Router();
const Competitions = require('./competitions');
const InvoiceData = require('./invoiceData');
const GetCurrentUser = require('./GetCurrentUser');
const UpdateCurrentUser = require('./UpdateCurrentUser');

routes.use('/competitions', Competitions);
routes.use('/invoiceData', InvoiceData);

routes.get('/', GetCurrentUser);
routes.put('/', UpdateCurrentUser);

module.exports = routes;