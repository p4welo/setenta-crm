const routes = require('express').Router();
const GetInvoiceData = require('./GetInvoiceData');
const SaveInvoiceData = require('./SaveInvoiceData');

routes.get('/', GetInvoiceData);
routes.put('/', SaveInvoiceData);

module.exports = routes;