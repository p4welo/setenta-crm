const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, InvoiceDataDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);
    const invoiceData = await InvoiceDataDao.getByUser(user);
    response.status(Status.OK).json(invoiceData);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};