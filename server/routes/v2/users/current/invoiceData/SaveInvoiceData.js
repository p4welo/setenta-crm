const Status = require('http-status');
const logger = rootRequire('logger');
const { InvoiceDataDao, UserDao } = rootRequire('dao');
const { InvoiceDataMapper } = rootRequire('dao/mappers');
const NotificationService = rootRequire('services/NotificationService');

module.exports = async (request, response) => {
  try {
    const userSid = request.user.id;
    const user = await UserDao.getBySid(userSid);
    const { name, nip, street, zip, city } = request.body;

    const oldInvoiceData = await InvoiceDataDao.getByUser(user);
    if (!oldInvoiceData) {
      const invoiceDataData = InvoiceDataMapper.toDatabase(name, nip, street, zip, city, user);
      const createdInvoiceData = await InvoiceDataDao.create(invoiceDataData);
      response.status(Status.OK).json(InvoiceDataMapper.serialize(createdInvoiceData));
    }
    else {
      const updatedInvoiceData= await InvoiceDataDao.update(oldInvoiceData.id, {
        name, nip, street, zip, city
      });
      NotificationService.invoiceDataUpdateSuccess(user);
      response.status(Status.OK).json(InvoiceDataMapper.serialize(updatedInvoiceData));
    }
  } catch (error) {
    logger.error(error.stack);
    cannotCreateDancerError(response, error);
  }
};

const cannotCreateDancerError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.dancer',
    details: error.details
  });
};
