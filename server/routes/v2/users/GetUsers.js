const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao } = rootRequire('dao');
const { UserMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const users = await UserDao.findAll();
    response.status(Status.OK).json(users.map(UserMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};