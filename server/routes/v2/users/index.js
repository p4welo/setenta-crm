const routes = require('express').Router();
const CurrentUser = require('./current');
const InvoiceData = require('./invoiceData');
const GetUser = require('./GetUser');
const GetUsers = require('./GetUsers');
const GetUserSchool = require('./GetUserSchool');
const DeleteUser = require('./DeleteUser');
const UpdateUser = require('./UpdateUser');
const CreateUser = require('./CreateUser');
const { getBySid } = require('../../../services/DataObjectService');

routes.param('userSid', getBySid('user'));

routes.use('/current', CurrentUser);
routes.use('/:userSid/invoiceData', InvoiceData);

routes.route('/')
    .get(GetUsers)
    .post(CreateUser);

routes.route('/:userSid')
    .get(GetUser)
    .put(UpdateUser)
    .delete(DeleteUser);
routes.get('/:userSid/schools', GetUserSchool);

module.exports = routes;