const routes = require('express').Router();
const TestReport = require('./TestReport');

routes.get('/test', TestReport);
routes.get('/', async (req, res) => {
  res.json({ test: 'OK' });
});

module.exports = routes;