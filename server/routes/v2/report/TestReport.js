const Status = require('http-status');
const { CompetitionEmailService, SetentaEmailService } = rootRequire('services');
// const SetentaEmailService = rootRequire('services/email/SetentaEmailService');

module.exports = async (request, response) => {
  // CompetitionEmailService.cronWeeklyOrganizerReport();
  CompetitionEmailService.afterActivation({
    email: 'p4welo@gmail.com',
    firstName: 'Paweł',
    sid: '1234567890'
  });
  CompetitionEmailService.afterRegistration({
    email: 'p4welo@gmail.com',
    firstName: 'Paweł',
    sid: '1234567890'
  });
  response.status(Status.OK).json({ sent: true });
};