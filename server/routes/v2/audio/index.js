const routes = require('express').Router();
const GetAudio = require('./GetAudio');
const DeleteAudio = require('./DeleteAudio');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('audioSid', getBySid('audio'));

routes.route('/:audioSid').get(GetAudio);
routes.route('/:audioSid').delete(DeleteAudio);

module.exports = routes;