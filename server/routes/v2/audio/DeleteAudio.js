const Status = require('http-status');
const logger = rootRequire('logger');
const { AudioDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { audio } = request.db;
    await AudioDao.remove(audio);
    response.status(Status.OK).json({ removed: true });
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};