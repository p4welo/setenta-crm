const fs = require('fs');
const Status = require('http-status');
const logger = rootRequire('logger');

module.exports = async (request, response) => {
  try {
    const { audio } = request.db;
    fs.createReadStream(audio.path)
        .pipe(response);
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
