const routes = require('express').Router();
const ReadEmail = require('./ReadEmail');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('emailSid', getBySid('email'));

routes.get('/:emailSid/read', ReadEmail);

// next routes - don't forget to authenticate'

module.exports = routes;