const Status = require('http-status');
const fs = require('fs');
const logger = rootRequire('logger');
const { EmailDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { email } = request.db;
    if (email) {
      await EmailDao.update(email.id, {
        read_count: email.read_count + 1
      });

      fs.readFile('public/assets/pixel.gif', (err, data) => {
        if (err) throw err;
        response.end(data);
      });
    } else {
      response.status(Status.NOT_FOUND).json({ ok: false });
    }

  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};