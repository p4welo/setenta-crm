const routes = require('express').Router();
const Competitions = require('./competitions');

routes.use('/competitions', Competitions);

module.exports = routes;