const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, CompetitionDao, CompetitionPaymentDao, PerformanceDao } = rootRequire('dao');
const { CompetitionMapper } = rootRequire('dao/mappers');
const { PaymentModels } = rootRequire('models/constants');
const CompetitionService = rootRequire('services/CompetitionService');
const { groupBy } = require('ramda');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const entity = CompetitionMapper.plainToEntity(competition);
      const registrations = await CompetitionDao.getCompetitionRegistrations(competition);
      const payments = await CompetitionPaymentDao.findByCompetition(competition);

      let userFees;
      if (entity.paymentModel === PaymentModels.DANCER_PERFORMANCE_AMOUNT) {
        // TOP i TRENDY - platne per dancer zaleznie od ilosci wystepow
        const dancerFees = await CompetitionDao.countCompetitionPerformancesByDancer(competition);
        userFees = feesByDancers(dancerFees, competition);
      } else if (entity.paymentModel === PaymentModels.SINGLE_DANCER_ENTRANCE) {
        // PCEIK - platne per dancer jednorazowa oplata
        const dancerFees = await CompetitionDao.countCompetitionPerformancesByDancer(competition);
        userFees = feesByDancersSingle(dancerFees, competition);
      } else {
        // MAGIA TAŃCA - platne od kategorii
        const performances = await PerformanceDao.findByCompetition(competition);
        const performancesByUser = groupBy((performance) => performance.user.sid)(performances);
        userFees = feesByType(performancesByUser, competition);
      }

      const withFees = mapWithPaymentInfo(registrations, payments, userFees);
      response.status(Status.OK).json(withFees);
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};

const mapWithPaymentInfo = (registrations, userPayments = [], userFees) => {
  const paymentsByUser = {};
  userPayments.forEach((payment) => {
    const actual = paymentsByUser[payment.user.sid] || 0;
    paymentsByUser[payment.user.sid] = actual + (payment.amount || 0);
  });

  return registrations.map((registration) => ({
    ...registration,
    dancersAmount: +registration.dancers,
    performancesAmount: +registration.performances,
    fee: userFees[registration.user_sid],
    payments: paymentsByUser[registration.user_sid],
    sid: registration.user_sid
  }));
};

const feesByDancers = (dancerFees, competition) => {
  const feesByUser = {};
  dancerFees.forEach((dancerFee) => {
    const fee = feesByUser[dancerFee.user_sid] || 0;
    feesByUser[dancerFee.user_sid] = fee + CompetitionService.calculateDancerFee(
        dancerFee.performance_count,
        competition
    )
  });
  return feesByUser;
};
const feesByDancersSingle = (dancerFees, competition) => {
  const feesByUser = {};
  dancerFees.forEach((dancerFee) => {
    const fee = feesByUser[dancerFee.user_sid] || 0;
    feesByUser[dancerFee.user_sid] = fee + CompetitionService.calculateSingleDancerFee(
        competition
    )
  });
  return feesByUser;
};

const feesByType = (performancesByUser, competition) => {
  const feesByUser = {};
  Object.keys(performancesByUser).forEach((userSid) => {
    performancesByUser[userSid].forEach((performance) => {
      const fee = feesByUser[userSid] || 0;
      feesByUser[userSid] = fee + CompetitionService.getTypeFee(performance, competition);
    })
  });
  return feesByUser;
};