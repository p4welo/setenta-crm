const routes = require('express').Router();
const GetCompetitionUsers = require('./GetCompetitionUsers');
const GetUserPerformances = require('./GetUserPerformances');
const GetUserCompetitionDancers = require('./GetUserCompetitionDancers');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('userSid', getBySid('user'));


routes.get('/', GetCompetitionUsers);
routes.get('/:userSid/performances', GetUserPerformances);
routes.get('/:userSid/dancers', GetUserCompetitionDancers);


module.exports = routes;