const Status = require('http-status');
const logger = rootRequire('logger');
const { DancerDao, UserDao } = rootRequire('dao');
const { DancerMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition, user } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const dancers = await DancerDao.findByCompetitionUser(competition, user);
      response.status(Status.OK).json(dancers.map(DancerMapper.serialize));
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};