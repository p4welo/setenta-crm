const routes = require('express').Router();
const FindAll = require('./FindAll');

routes.get('/', FindAll);

module.exports = routes;