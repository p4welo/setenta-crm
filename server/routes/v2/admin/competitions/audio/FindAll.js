const Status = require('http-status');
const fs = require('fs');
const archiver = require('archiver');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {

  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {

      const performances = await PerformanceDao.findWithAudioByCompetition(competition);

      response.writeHead(Status.OK, {
        'Content-Type': 'application/zip',
        'Content-disposition': 'attachment; filename=music.zip'
      });

      const zip = archiver('zip');
      zip.pipe(response);
      performances.filter((p) => !!p.audio && p.audio.path)
          .forEach((performance) => {
            const audio = performance.audio;
            zip.append(fs.createReadStream(audio.path), { name: `${performance.number}.mp3` });
          });
      zip.finalize(function (err, bytes) {
        if (err) {
          throw err;
        }
        logger.info(bytes + ' total bytes');
      });
    } else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};