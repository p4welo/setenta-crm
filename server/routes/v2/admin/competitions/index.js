const routes = require('express').Router();
const PerformanceBlocks = require('./performance-blocks');
const Schools = require('./schools');
const Performances = require('./performances');
const Dancers = require('./dancers');
const Users = require('./users');
const Report = require('./report');
const Audio = require('./audio');
const GetCompetitions = require('./GetCompetitions');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('competitionSid', getBySid('competition'));

routes.use('/:competitionSid/performance-blocks', PerformanceBlocks);
routes.use('/:competitionSid/schools', Schools);
routes.use('/:competitionSid/performances', Performances);
routes.use('/:competitionSid/dancers', Dancers);
routes.use('/:competitionSid/users', Users);
routes.use('/:competitionSid/report', Report);
routes.use('/:competitionSid/audio', Audio);

routes.get('/', GetCompetitions);

module.exports = routes;