const Status = require('http-status');
const logger = rootRequire('logger');
const { DancerDao, UserDao } = rootRequire('dao');
const { DancerMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const dancers = await DancerDao._findByCompetition(competition);
      response.status(Status.OK).json({
        all: dancers.length
      });
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};