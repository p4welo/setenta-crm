const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

// const GetAllDancers = require('./GetAllDancers');
const GetDancersCount = require('./GetDancersCount');

routes.param('dancerSid', getBySid('dancer'));

// routes.get('/', GetAllDancers);
routes.get('/count', GetDancersCount);

module.exports = routes;