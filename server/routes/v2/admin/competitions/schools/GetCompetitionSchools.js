const Status = require('http-status');
const logger = rootRequire('logger');
const { UserDao, CompetitionDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const registrations = await CompetitionDao.getCompetitionRegistrations(competition);
      const dancerFees = await CompetitionDao.countCompetitionPerformancesByDancer(competition);
      response.status(Status.OK).json(mapWithFees(registrations, dancerFees));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};

const mapWithFees = (registrations, dancerFees) => {
  const feesByUser = {};
  dancerFees.forEach((dancerFee) => {
    const fee = feesByUser[dancerFee.user_sid] || 0;
    feesByUser[dancerFee.user_sid] = fee + 25 + (dancerFee.performance_count - 1) * 10
  });
  return registrations.map((registration) => ({
    ...registration,
    dancersAmount: +registration.dancers,
    performancesAmount: +registration.performances,
    fee: feesByUser[registration.user_sid],
    sid: registration.user_sid
  }));
};