const routes = require('express').Router();
const GetCompetitionSchools = require('./GetCompetitionSchools');
const { getBySid } = rootRequire('services/DataObjectService');

routes.param('schoolSid', getBySid('school'));

routes.get('/', GetCompetitionSchools);

module.exports = routes;