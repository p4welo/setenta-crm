const routes = require('express').Router();
const GetPerformancesAmountByDay = require('./GetPerformancesAmountByDay');

routes.get('/performancesByDay', GetPerformancesAmountByDay);

module.exports = routes;