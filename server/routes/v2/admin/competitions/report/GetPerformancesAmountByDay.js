const Status = require('http-status');
const { groupBy, range } = require('ramda');
const dayjs = require('dayjs');
const logger = rootRequire('logger');

const { PerformanceDao } = rootRequire('dao');

module.exports = async (request, response) => {
  const DATE_FORMAT = 'YYYY-MM-DD';
  try {
    const { competition } = request.db;
    const report = await PerformanceDao.dailyAmountByCompetition(competition);

    if (report.length < 1) {
      response.status(Status.OK).json([]);
    }
    else {
      const first = dayjs(report[0].date);
      const last = dayjs(report[report.length - 1].date);
      const diff = last.diff(first, 'day');

      // fill empty dates
      const full = {};
      range(0, diff + 1).map((i) => {
        full[first.add(i, 'day').format(DATE_FORMAT)] = 0;
      });

      report.map((dayReport) => {
        const date = dayjs(dayReport.date);
        full[date.format(DATE_FORMAT)] = +dayReport.amount
      });

      // transform into array again
      response.status(Status.OK).json(Object.keys(full).map(dateKey => {
        return {
          date: dateKey,
          amount: full[dateKey]
        }
      }));
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e.stack });
  }
};
