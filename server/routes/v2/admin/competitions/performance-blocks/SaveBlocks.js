const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceBlockDao, UserDao } = rootRequire('dao');
const { PerformanceBlockMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      await PerformanceBlockDao.clearAllByCompetition(competition);
      const blocksToSave = request.body;
      const result = [];
      blocksToSave.forEach(async (block, index) => {
        const { style, type, experience, age } = block;
        const performanceBlockData = PerformanceBlockMapper.toDatabase(style, type, experience, age, index, competition);
        const savedBlock = await PerformanceBlockDao.create(performanceBlockData);
        result.push(savedBlock);
      });
      response.status(Status.OK).json(result.map(PerformanceBlockMapper.serialize));
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};
