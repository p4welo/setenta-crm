const routes = require('express').Router();
const GetAll = require('./GetAll');
const GetOrdered = require('./GetOrdered');
const SaveBlocks = require('./SaveBlocks');

routes.get('/all', GetAll);
routes.get('/', GetOrdered);
routes.post('/', SaveBlocks);

module.exports = routes;