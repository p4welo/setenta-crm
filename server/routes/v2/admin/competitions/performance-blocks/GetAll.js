const Status = require('http-status');
const logger = rootRequire('logger');
const { PerformanceBlockDao, UserDao } = rootRequire('dao');
const { PerformanceBlockMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const performanceBlocks = await PerformanceBlockDao.findByCompetition(competition);
    response.status(Status.OK).json(performanceBlocks.map(PerformanceBlockMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};