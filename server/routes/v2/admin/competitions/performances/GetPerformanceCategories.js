const Status = require('http-status');
const { groupBy } = require('ramda');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao } = rootRequire('dao');
const { PerformanceMapper } = rootRequire('dao/mappers');

module.exports = async (request, response) => {
  try {
    const SEPARATOR = '|';

    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const performances = await PerformanceDao.findByCompetition(competition);
      const byChapter = (performance) =>
          `${performance.style}${SEPARATOR}${performance.type}${SEPARATOR}${performance.experience}${SEPARATOR}${performance.ageLevel}`;
      const result = groupBy(byChapter, performances.map(PerformanceMapper.serialize));
      response.status(Status.OK).json(result);
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({ details: e });
  }
};