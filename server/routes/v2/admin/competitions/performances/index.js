const routes = require('express').Router();
const { getBySid } = rootRequire('services/DataObjectService');

const GetPerformanceCategories = require('./GetPerformanceCategories');
const GetAllPerformances = require('./GetAllPerformances');
const GetPerformancesCount = require('./GetPerformancesCount');

routes.param('performanceSid', getBySid('performance'));

routes.get('/categories', GetPerformanceCategories);
routes.get('/', GetAllPerformances);
routes.get('/count', GetPerformancesCount);

module.exports = routes;