const Status = require('http-status');
const dayjs = require('dayjs');
const logger = rootRequire('logger');
const { PerformanceDao, UserDao } = rootRequire('dao');

module.exports = async (request, response) => {
  try {
    const { competition } = request.db;
    const currentUser = await UserDao.getBySid(request.user.id);

    if (currentUser.isOwnerOf(competition)) {
      const performances = await PerformanceDao.findByCompetition(competition);
      const yesterday = [];
      const today = performances.filter((performance => {
        return dayjs().isSame(dayjs(performance.createdAt), 'day');
      }));
      response.status(Status.OK).json({
        all: performances.length,
        yesterday: yesterday.length,
        today: today.length
      });
    }
    else {
      logger.error('user.is.not.the.competition.owner');
      response.status(Status.UNAUTHORIZED).json({ details: 'user.is.not.the.competition.owner' });
    }
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};