const Status = require('http-status');
const logger = rootRequire('logger');
const { UserTypeDao } = require('../../../dao');
const { UserTypeMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const { name } = request.body;
    const userTypeData = UserTypeMapper.toDatabase(name);
    const userType = await UserTypeDao.create(userTypeData);
    response.status(Status.OK).json(UserTypeMapper.serialize(userType));
  } catch (error) {
    logger.error(error.stack);
    if (error.name === 'SequelizeUniqueConstraintError') {
      userTypeAlreadyExistsError(response, error);
    } else {
      cannotCreateUserTypeError(response, error);
    }
  }
};

const userTypeAlreadyExistsError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'user.type.already.exists',
    details: error.details
  });
};

const cannotCreateUserTypeError = (response, error) => {
  response.status(Status.INTERNAL_SERVER_ERROR).json({
    type: 'cannot.create.user.type',
    details: error.details
  });
};