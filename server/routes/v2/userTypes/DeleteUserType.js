const Status = require('http-status');
const logger = rootRequire('logger');
const { UserTypeDao } = require('../../../dao');

module.exports = async (request, response) => {
  try {
    const { user_type } = request.db;
    await UserTypeDao.remove(user_type);
    response.status(Status.OK).json({ status: 'OK' });
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};