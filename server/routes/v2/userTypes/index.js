const routes = require('express').Router();
const GetUserTypes = require('./GetUserTypes');
const DeleteUserType = require('./DeleteUserType');
const CreateUserType = require('./CreateUserType');
const { getBySid } = require('../../../services/DataObjectService');

routes.param('userTypeSid', getBySid('user_type'));

routes.route('/')
    .get(GetUserTypes)
    .post(CreateUserType);

routes.route('/:userTypeSid')
    .delete(DeleteUserType);

module.exports = routes;