const Status = require('http-status');
const logger = rootRequire('logger');
const { UserTypeDao } = require('../../../dao');
const { UserTypeMapper } = require('../../../dao/mappers');

module.exports = async (request, response) => {
  try {
    const userTypes = await UserTypeDao.findAll();
    response.status(Status.OK).json(userTypes.map(UserTypeMapper.serialize));
  } catch (e) {
    logger.error(e.stack);
    response.status(Status.INTERNAL_SERVER_ERROR).json({details: e});
  }
};