const models = rootRequire('db/models');
const { InvoiceMapper } = rootRequire('dao/mappers');

const getBy = async (params) => {
  const invoice = await models.invoice.findOne({
    where: params
  });
  return InvoiceMapper.toEntity(invoice);
};

const remove = async (invoice) => {
  const found = await models.invoice.findById(invoice.id);
  await found.destroy();
};

const getByUser = async (user) => {
  return await getBy({user_id: user.id});
};

const create = async (data) => {
  const newInvoice = await models.invoice.create(data);
  const invoice = await models.invoice.findOne({
    where: { id: newInvoice.id },
    include: [{ all: true }]
  });
  return InvoiceMapper.toEntity(invoice);
};

const update = async (id, newData) => {
  const invoice = await models.invoice.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedInvoice = await invoice.update(newData, { transaction });
    const invoiceEntity = InvoiceMapper.toEntity(updatedInvoice);

    await transaction.commit();

    return invoiceEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  update,
  remove,
  getBy,
  getByUser
};