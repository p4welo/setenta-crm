const models = rootRequire('db/models');
const { PerformancePriceMapper } = require('./mappers');

const create = async (performancePriceData) => {
  const performancePriceModel = await models.performance_price.create(performancePriceData);
  return PerformancePriceMapper.toEntity(performancePriceModel);
};

const remove = async (performancePrice) => {
  const found = await models.performance_price.findById(performancePrice.id);
  await found.destroy();
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.performance_price.findOne({
    where: params
  });
  return PerformancePriceMapper.toEntity(found);
};

module.exports = {
  create,
  remove,
  getBySid,
  getBy
};