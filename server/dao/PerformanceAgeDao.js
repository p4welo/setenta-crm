const models = rootRequire('db/models');
const { PerformanceAgeMapper } = require('./mappers');

const create = async (performanceAgeData) => {
  const performanceAgeModel = await models.performance_age.create(performanceAgeData);
  return PerformanceAgeMapper.toEntity(performanceAgeModel);
};

const remove = async (performanceAge) => {
  const found = await models.performance_age.findById(performanceAge.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.performance_age.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['min', 'ASC']
    ],
    include: []
  });
  return found.map(PerformanceAgeMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.performance_age.findOne({
    where: params
  });
  return PerformanceAgeMapper.toEntity(found);
};

module.exports = {
  create,
  findByCompetition,
  remove,
  getBySid,
  getBy
};