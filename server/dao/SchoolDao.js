const models = require('../db/models');
const { SchoolMapper } = require('../dao/mappers');

const create = async (school) => {
  const newSchool = await models.school.create(school);
  return SchoolMapper.toEntity(newSchool);
};

const update = async (id, newData) => {
  const school = await models.school.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedSchool = await school.update(newData, { transaction });
    const schoolEntity = SchoolMapper.toEntity(updatedSchool);

    await transaction.commit();

    return schoolEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const remove = async (school) => {
  const found = await models.school.findById(school.id);
  await found.destroy();
  return;
};

const findAll = async () => {
  const schools = await models.school.findAll({
    include: [
      { model: models.user },
      { model: models.address }
    ]
  });

  return schools.map(SchoolMapper.toEntity);
};

const getByUser = async (user) => {
  return await getBy({user_id: user.id});
};

const getBy = async (params) => {
  const school = await models.school.findOne({
    where: params,
    include: [
      { model: models.user },
      { model: models.address }
    ]
  });
  return SchoolMapper.toEntity(school);
};

module.exports = {
  create,
  remove,
  findAll,
  getBy,
  update,
  getByUser
};