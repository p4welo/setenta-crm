const models = rootRequire('db/models');
const { PaymentModelEntryMapper } = require('./mappers');

const create = async (entryData) => {
  const entryModel = await models.payment_model_entry.create(entryData);
  return PaymentModelEntryMapper.toEntity(entryModel);
};

const remove = async (entry) => {
  const found = await models.payment_model_entry.findById(entry.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.payment_model_entry.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['amount', 'ASC']
    ],
    include: []
  });
  return found.map(PaymentModelEntryMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.payment_model_entry.findOne({
    where: params
  });
  return PaymentModelEntryMapper.toEntity(found);
};

module.exports = {
  create,
  findByCompetition,
  remove,
  getBySid,
  getBy
};