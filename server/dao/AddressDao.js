const models = require('../db/models');
const { AddressMapper } = require('../dao/mappers');

const create = async (address) => {
  const newAddress = await models.address.create(address);
  return AddressMapper.toEntity(newAddress)
};

const update = async (id, newData) => {
  const address = await models.address.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedAddress = await address.update(newData, { transaction });
    const addressEntity = AddressMapper.toEntity(updatedAddress);

    await transaction.commit();

    return addressEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const getBySchool = async (school) => {
  return await getBy({ school_id: school.id });
};

const getBy = async (params) => {
  const address = await models.address.findOne({
    where: params
  });
  return AddressMapper.toEntity(address);
};

module.exports = {
  create,
  update,
  getBy,
  getBySchool
};