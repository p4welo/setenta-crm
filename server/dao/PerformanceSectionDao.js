const models = require('../db/models');
const { PerformanceSectionMapper, PerformanceBlockMapper } = require('./mappers');

const create = async (sectionData, blocksData = []) => {
  const newSection = await models.performance_section.create(sectionData);

  blocksData.map(async ({ style, type, experience, age }) => {
    const blockModel = await models.performance_block.create(PerformanceBlockMapper.toDatabase(style, type, experience, age));
    await newSection.addBlock(blockModel);
  });

  return PerformanceSectionMapper.toEntity(newSection);
};

const findByCompetition = async (competition) => {
  const sections = await models.performance_section.findAll({
    where: {
      competition_id: competition.id
    },
    include: [{
      model: models.performance_block,
      as: 'blocks'
    }],
    order: [
      ['day', 'ASC'],
      ['chapter', 'ASC'],
      ['position', 'ASC'],
    ]
  });
  return sections.map(PerformanceSectionMapper.toEntity);
};

const merge = async (originSid, destinationSid) => {
  const origin = await models.performance_section.findOne({
    where: { sid: originSid },
    include: [{
      model: models.performance_block,
      as: 'blocks'
    }]
  });

  const destination = await models.performance_section.findOne({
    where: { sid: destinationSid }
  });

  origin.blocks.forEach(async (block) => {
    await block.setPerformance_section(destination);
  });

  await remove(origin);
  return PerformanceSectionMapper.toEntity(destination);
};

const remove = async (section) => {
  const found = await models.performance_section.findById(section.id);
  if (found) {
    await found.destroy();
  }
  return;
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const section = await models.performance_section.findOne({
    where: params
  });
  return PerformanceSectionMapper.toEntity(section);
};

const update = async (id, newData) => {
  const section = await models.performance_section.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedSection = await section.update(newData, { transaction });
    const sectionEntity = PerformanceSectionMapper.toEntity(updatedSection);

    await transaction.commit();

    return sectionEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  remove,
  getBy,
  getBySid,
  update,
  merge,
  findByCompetition
};