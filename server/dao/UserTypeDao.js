const models = require('../db/models');
const { UserTypeMapper } = require('../dao/mappers');

const findAll = async () => {
  const users = await models.user_type.findAll();

  return users.map(UserTypeMapper.toEntity);
};

const getByName = async (name) => {
  return await getBy({ name });
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const type = await models.user_type.findOne({
    where: params
  });
  return UserTypeMapper.toEntity(type);
};

const create = async (userType) => {
  const newUserType = await models.user_type.create(userType);
  return UserTypeMapper.toEntity(newUserType);
};

const remove = async (userType) => {
  const type = await models.user_type.findById(userType.id);
  await type.destroy();
  return;
};

module.exports = {
  create,
  getByName,
  getBy,
  getBySid,
  findAll,
  remove
};