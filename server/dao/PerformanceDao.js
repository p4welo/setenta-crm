const models = require('../db/models');
const { PerformanceMapper } = require('../dao/mappers');

const assignDancer = async (performanceModel, dancerEntity) => {
  const dancerModel = await models.dancer.findOne({
    where: { sid: dancerEntity.sid }
  });
  return await performanceModel.addDancer(dancerModel);
};

const create = async (performanceData, dancerEntities = []) => {
  const performanceModel = await models.performance.create(performanceData);
  dancerEntities.forEach(async (entity) => await assignDancer(performanceModel, entity));
  return PerformanceMapper.toEntity(performanceModel);
};

const remove = async (performance) => {
  const found = await models.performance.findById(performance.id);
  await found.destroy();
};

const findByUser = async (competition, user) => {
  const performances = await models.performance.findAll({
    where: {
      user_id: user.id,
      competition_id: competition.id
    },
    include: [{
      model: models.dancer,
      as: 'dancers'
    }]
  });
  return performances.map(PerformanceMapper.toEntity);
};

const findByCompetition = async (competition) => {
  const performanceBlocks = await models.performance.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['created_at', 'ASC'],
    ],
    include: [{
      model: models.dancer,
      as: 'dancers'
    }, {
      model: models.user,
      as: 'user'
    }]
  });
  return performanceBlocks.map(PerformanceMapper.toEntity);
};

const findWithAudioByCompetition = async (competition) => {
  const performances = await models.performance.findAll({
    where: {
      competition_id: competition.id
    },
    include: [{
      model: models.audio,
      as: 'audio'
    }]
  });
  return performances.map(PerformanceMapper.toEntity);
};

const dailyAmountByCompetition = async (competition) => {
  const result = await models.sequelize.query(`
      SELECT date_trunc('day', performances.created_at) AS date,
             count(*) AS amount
      FROM performances
      WHERE competition_id=${competition.id}
      GROUP BY 1
      ORDER BY 1;
    `);
  return result[0];
};

const findAll = async (competition) => {
  const performances = await models.performance.findAll({
    where: {
      competition_id: competition.id
    },
    include: [{
      model: models.dancer,
      as: 'dancers'
    }]
  });
  return performances.map(PerformanceMapper.toEntity);
};

const findExtendedByCompetition = async (competition, user) => {

  const queryParams = user ?
      {
        competition_id: competition.id,
        user_id: user.id
      } :
      { competition_id: competition.id };

  const performanceBlocks = await models.performance.findAll({
    where: queryParams,
    include: [{
      model: models.dancer,
      as: 'dancers'
    }, {
      model: models.user,
      as: 'user',
      include: [
        {
          model: models.school,
          include: [
            {
              model: models.address
            }
          ]
        }
      ]
    }]
  });
  return performanceBlocks.map(PerformanceMapper.toEntity);
};

const findResultedByCompetition = async (competition) => {
  const performanceBlocks = await models.performance.findAll({
    where: {
      competition_id: competition.id,
      place: {
        [models.Sequelize.Op.ne]: null
      }
    },
    include: [{
      model: models.dancer,
      as: 'dancers'
    }, {
      model: models.performance_price,
      as: 'prices'
    }, {
      model: models.user,
      as: 'user',
      attributes: {
        exclude: ['email', 'firstName', 'lastName', 'login', 'object_state']
      },
      include: [
        {
          model: models.school,
          include: [
            {
              model: models.address
            }
          ]
        }
      ]
    }],
    order: [
      ['place', 'ASC']
    ]
  });
  return performanceBlocks.map(PerformanceMapper.toEntity);
};

const update = async (id, newData) => {
  const performance = await models.performance.findById(id);
  const transaction = await models.sequelize.transaction();

  try {
    let updatedPerformance = await performance.update(newData, { transaction });

    if (newData.dancers && newData.dancers.length > 0) {
      await updatedPerformance.setDancers([]);
      newData.dancers.forEach(async (entity) => await assignDancer(updatedPerformance, entity));
    }

    const performanceEntity = PerformanceMapper.toEntity(updatedPerformance);

    await transaction.commit();

    return performanceEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const user = await models.performance.findOne({
    where: params,
    include: [{
      model: models.dancer,
      as: 'dancers'
    }]
  });
  return PerformanceMapper.toEntity(user);
};

module.exports = {
  create,
  findAll,
  findByUser,
  findByCompetition,
  findExtendedByCompetition,
  findResultedByCompetition,
  findWithAudioByCompetition,
  dailyAmountByCompetition,
  update,
  getBySid,
  getBy,
  remove
};