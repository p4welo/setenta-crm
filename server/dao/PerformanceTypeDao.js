const models = rootRequire('db/models');
const { PerformanceTypeMapper } = require('./mappers');

const create = async (performanceTypeData) => {
  const performanceTypeModel = await models.performance_type.create(performanceTypeData);
  return PerformanceTypeMapper.toEntity(performanceTypeModel);
};

const remove = async (performanceType) => {
  const found = await models.performance_type.findById(performanceType.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.performance_type.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['min', 'ASC']
    ],
    include: []
  });
  return found.map(PerformanceTypeMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.performance_type.findOne({
    where: params
  });
  return PerformanceTypeMapper.toEntity(found);
};

module.exports = {
  create,
  findByCompetition,
  remove,
  getBySid,
  getBy
};