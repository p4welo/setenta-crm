module.exports = {
  name: 'top',
  rules: [
    {
      name: 'HIP_HOP',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'JAZZ',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'CONTEMPORARY',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'SHOW_DANCE',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'BALLET',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'DISCO',
      type: 'STYLE',
      rules: [
        {
          name: 'SOLO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 1,
          max: 1
        },
        {
          name: 'DUO',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 2,
          max: 2
        },
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'ART',
      type: 'STYLE',
      rules: [
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [{ name: 'DEBUT', type: 'EXPERIENCE' }],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [{ name: 'DEBUT', type: 'EXPERIENCE' }],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'MODERN',
      type: 'STYLE',
      rules: [
        {
          name: 'MINI_FORMATION',
          type: 'TYPE',
          rules: [{ name: 'DEBUT', type: 'EXPERIENCE' }],
          min: 3,
          max: 7
        },
        {
          name: 'FORMATION',
          type: 'TYPE',
          rules: [{ name: 'DEBUT', type: 'EXPERIENCE' }],
          min: 8,
          max: 25
        }
      ]
    },
  ]
};