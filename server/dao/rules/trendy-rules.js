const RuleType = require('../../models/constants/CompetitionRuleTypes');

module.exports = {
  name: 'trendy',
  rules: [
    {
      name: 'HIP_HOP',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'JAZZ',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'CONTEMPORARY',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'SHOW_DANCE',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ],
          min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'BALLET',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'DISCO',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 8,
          max: 25
        }
      ]
    },
    {
      name: 'THEATER_DANCE',
      type: 'STYLE',
      rules: [
        {
          name: 'Mini formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 3,
          max: 7
        },
        {
          name: 'Formacje',
          type: 'TYPE',
          rules: [
            { name: 'DEBUT', type: 'EXPERIENCE' },
            { name: 'MASTER', type: 'EXPERIENCE' }
          ], min: 8,
          max: 25
        }
      ]
    }
  ]
};