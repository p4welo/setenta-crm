const models = require('../db/models');
const { DancerMapper } = require('../dao/mappers');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');

const create = async (dancer) => {
  const newDancer = await models.dancer.create(dancer);
  return DancerMapper.toEntity(newDancer);
};

const remove = async (dancer) => {
  await update(dancer.id, { objectState: DataObjectStates.DELETED });
};

const update = async (id, newData) => {
  const dancer = await models.dancer.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedDancer = await dancer.update(newData, { transaction });
    const dancerEntity = DancerMapper.toEntity(updatedDancer);

    await transaction.commit();

    return dancerEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const findByUser = async (user) => {
  const dancers = await models.dancer.findAll({
    where: {
      user_id: user.id,
      objectState: DataObjectStates.ACTIVE
    }
  });
  return dancers.map(DancerMapper.toEntity);
};

// TODO: szatnie?
const getDancersAmountPerDay = async (competition, day) => {
  const result = await models.sequelize.query(`
      SELECT s.name,
             count(distinct dp.dancer_id) AS dancers
      FROM users u
               JOIN schools s ON (u.id = s.user_id)
               JOIN addresses a ON (s.id = a.school_id)
               JOIN performances p ON (u.id = p.user_id)
               JOIN dancer_performance dp ON (p.id = dp.performance_id)
      WHERE p.competition_id = ${competition.id}
        AND (p.style, p.type, p.experience, p."ageLevel") IN (
          select style, type, experience, age
          from performance_sections as ps
                   JOIN performance_blocks AS pb
                        ON pb.performance_section_id = ps.id
          WHERE ps.day = ${day}
      )
      GROUP BY (s.id)
  `);
  return result[0];
};

const findAll = async () => {
  const dancers = await models.dancer.findAll({
    include: [
      { model: models.user }
    ]
  });

  return dancers.map(DancerMapper.toEntity);
};

const findByCompetition = async (competition) => {
  const result = await models.sequelize.query(`
    SELECT p.sid, d."firstName", d."lastName" from performances p
        LEFT JOIN dancer_performance dp on p.id = dp.performance_id
        LEFT JOIN dancers d on dp.dancer_id = d.id
    where p.competition_id = ${competition.id}
  `);
  return result[0];
};

const _findByCompetition = async (competition) => {
  const result = await models.sequelize.query(`
      SELECT DISTINCT d.* 
      FROM dancer_performance AS dp
        JOIN dancers AS d ON (d.id=dp.dancer_id)
        JOIN performances AS p ON (dp.performance_id=p.id)
      WHERE p.competition_id = ${competition.id}
  `);
  return result[0];
};

const findByCompetitionUser = async (competition, user) => {
  const result = await models.sequelize.query(`
      SELECT d.*, count(*) as performances 
      FROM dancer_performance AS dp
        JOIN dancers AS d ON (d.id=dp.dancer_id)
        JOIN performances AS p ON (dp.performance_id=p.id)
      WHERE p.competition_id = ${competition.id} AND d.user_id=${user.id}
      GROUP BY d.id
      ORDER BY performances DESC
  `);
  return result[0];
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const dancer = await models.dancer.findOne({
    where: params,
    include: [{
      model: models.user
    }]
  });
  return DancerMapper.toEntity(dancer);
};

module.exports = {
  create,
  remove,
  getBy,
  getBySid,
  findAll,
  findByUser,
  findByCompetition,
  _findByCompetition,
  findByCompetitionUser,
  update
};