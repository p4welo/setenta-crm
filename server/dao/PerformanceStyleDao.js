const models = rootRequire('db/models');
const { PerformanceStyleMapper } = require('./mappers');

const create = async (performanceStyleData) => {
  const performanceStyleModel = await models.performance_style.create(performanceStyleData);
  return PerformanceStyleMapper.toEntity(performanceStyleModel);
};

const remove = async (performanceStyle) => {
  const found = await models.performance_style.findById(performanceStyle.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.performance_style.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['name', 'ASC']
    ],
    include: []
  });
  return found.map(PerformanceStyleMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.performance_style.findOne({
    where: params
  });
  return PerformanceStyleMapper.toEntity(found);
};

module.exports = {
  create,
  findByCompetition,
  remove,
  getBySid,
  getBy
};