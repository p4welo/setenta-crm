const fs = require('fs');
const models = rootRequire('db/models');
const { AudioMapper } = require('./mappers');

const create = async (audioData) => {
  const audioModel = await models.audio.create(audioData);
  return AudioMapper.toEntity(audioModel);
};

const remove = async (audio) => {
  const found = await models.audio.findById(audio.id);
  if (found) {
    fs.unlinkSync(found.path);
  }
  await found.destroy();
};

const getByPerformance = async (performance) => {
  return await getBy({ performance_id: performance.id })
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.audio.findOne({
    where: params
  });
  if (found) {
    return AudioMapper.toEntity(found);
  }
  return undefined;
};

const findAll = async () => {
  const audios = await models.audio.findAll();

  return audios.map(AudioMapper.toEntity);
};

module.exports = {
  create,
  findAll,
  getByPerformance,
  remove,
  getBySid,
  getBy
};