const models = rootRequire('db/models');
const { CompetitionRuleMapper } = require('./mappers');

const create = async (competitionRuleData) => {
  const competitionRuleModel = await models.competition_rule.create(competitionRuleData);
  const competitionRule = await models.competition_rule.findOne({
    where: { id: competitionRuleModel.id },
    include: [{ all: true }]
  });
  return CompetitionRuleMapper.toEntity(competitionRule);
};

const remove = async (rule) => {
  const found = await models.competition_rule.findById(rule.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.competition_rule.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['id', 'ASC']
    ],
    include: [{ all: true }]
  });
  return found.map(CompetitionRuleMapper.toEntity);
};

const update = async (id, newData) => {
  const competitionRule = await models.competition_rule.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedRule = await competitionRule.update(newData, { transaction });
    const ruleEntity = CompetitionRuleMapper.toEntity(updatedRule);

    await transaction.commit();

    return ruleEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  remove,
  update,
  findByCompetition
};
