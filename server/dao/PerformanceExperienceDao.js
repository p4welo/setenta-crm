const models = rootRequire('db/models');
const { PerformanceExperienceMapper } = require('./mappers');

const create = async (performanceExperienceData) => {
  const performanceExperienceModel = await models.performance_experience.create(performanceExperienceData);
  return PerformanceExperienceMapper.toEntity(performanceExperienceModel);
};

const remove = async (performanceExperience) => {
  const found = await models.performance_experience.findById(performanceExperience.id);
  await found.destroy();
};

const findByCompetition = async (competition) => {
  const found = await models.performance_experience.findAll({
    where: {
      competition_id: competition.id
    },
    include: []
  });
  return found.map(PerformanceExperienceMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.performance_experience.findOne({
    where: params
  });
  return PerformanceExperienceMapper.toEntity(found);
};

module.exports = {
  create,
  findByCompetition,
  remove,
  getBy,
  getBySid
};