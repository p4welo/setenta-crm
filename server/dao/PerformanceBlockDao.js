const models = require('../db/models');
const { PerformanceBlockMapper } = require('./mappers');

const clearAllByCompetition = async (competition) => {
  const found = await findByCompetition(competition);
  if (found && found.length > 0) {
    found.forEach(async (block) => {
      await remove(block);
    })
  }
  return;
};

const create = async (block) => {
  const newBlock = await models.performance_block.create(block);
  return PerformanceBlockMapper.toEntity(newBlock);
};

const findByCompetition = async (competition) => {
  const performanceBlocks = await models.performance_block.findAll({
    where: {
      competition_id: competition.id
    },
    order: [
      ['position', 'ASC'],
    ]
  });
  return performanceBlocks.map(PerformanceBlockMapper.toEntity);
};

const remove = async (block) => {
  const found = await models.performance_block.findById(block.id);
  if (found) {
    await found.destroy();
  }
  return;
};

const update = async (id, newData) => {
  const block = await models.performance_block.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedBlock = await block.update(newData, { transaction });
    const blockEntity = PerformanceBlockMapper.toEntity(updatedBlock);

    await transaction.commit();

    return blockEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  remove,
  clearAllByCompetition,
  findByCompetition,
  update
};