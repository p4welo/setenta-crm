const models = require('../db/models');
const { InvoiceDataMapper } = require('../dao/mappers');

const getBy = async (params) => {
  const invoiceData = await models.invoice_data.findOne({
    where: params
  });
  return InvoiceDataMapper.toEntity(invoiceData);
};

const getByUser = async (user) => {
  return await getBy({user_id: user.id});
};

const create = async (invoiceData) => {
  const newInvoiceData = await models.invoice_data.create(invoiceData);
  return InvoiceDataMapper.toEntity(newInvoiceData);
};

const update = async (id, newData) => {
  const invoiceData = await models.invoice_data.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedInvoiceData = await invoiceData.update(newData, { transaction });
    const invoiceDataEntity = InvoiceDataMapper.toEntity(updatedInvoiceData);

    await transaction.commit();

    return invoiceDataEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  update,
  getBy,
  getByUser
};