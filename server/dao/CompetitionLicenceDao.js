const models = rootRequire('db/models');
const { CompetitionLicenceMapper } = require('./mappers');

const create = async (licenceData) => {
  const licenceModel = await models.competition_licence.create(licenceData);
  return CompetitionLicenceMapper.toEntity(licenceModel);
};

const remove = async (licence) => {
  const found = await models.competition_licence.findById(licence.id);
  await found.destroy();
};

const getByCompetition = async (competition) => {
  return await getBy({
    competition_id: competition.id
  });
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const found = await models.competition_licence.findOne({
    where: params
  });
  return CompetitionLicenceMapper.toEntity(found);
};

module.exports = {
  create,
  getByCompetition,
  remove,
  getBySid,
  getBy
};