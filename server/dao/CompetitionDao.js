const models = require('../db/models');
const { CompetitionMapper } = require('./mappers');
const topRules = require('./rules/top-rules');
const trendyRules = require('./rules/trendy-rules');

const findAll = async () => {
  const competitions = await models.competition.findAll({
    include: [{
      model: models.user
    }]
  });

  return competitions.map(CompetitionMapper.toEntity);
};

const findPublicAndMine = async (user) => {
  const competitions = await models.competition.findAll({
    where: {
      [models.Sequelize.Op.or]: [
        {
          is_public: true
        },
        {
          is_public: false,
          user_id: user.id
        }
      ]
    },
    include: [{
      model: models.user
    }]
  });

  return competitions.map(CompetitionMapper.toEntity);
};

const findPublic = async () => {
  const competitions = await models.competition.findAll({
    where: {
      is_public: true
    },
    include: [{
      model: models.user
    }]
  });

  return competitions.map(CompetitionMapper.toEntity);
};

const getByName = async (name) => {
  return await getBy({ name });
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const competition = await models.competition.findOne({
    where: params,
    include: [{
      model: models.user
    }]
  });
  return CompetitionMapper.toEntity(competition);
};

const findByUser = async (user) => {
  const competitions = await models.competition.findAll({
    where: {
      user_id: user.id
    }
  });
  return competitions.map(CompetitionMapper.toEntity);
};

const create = async (competition) => {
  const newCompetition = await models.competition.create(competition);
  return CompetitionMapper.toEntity(newCompetition);
};

const countPerformancesByDancer = async (competition, user) => {
  const performances = await models.sequelize.query(`
    SELECT d.sid, d."firstName", d."lastName", count(*) AS performance_count 
    FROM dancer_performance dp 
      JOIN performances AS p ON (dp.performance_id=p.id) 
      JOIN dancers AS d ON (d.id=dp.dancer_id) 
    WHERE p.competition_id = ${competition.id}
      AND p.user_id = ${user.id} 
    GROUP BY (d.sid, d."firstName", d."lastName", p.competition_id)
  `);
  return performances[0];
};

const getCompetitionInvoiceDatas = async (competition) => {
  const result = await models.sequelize.query(`
      SELECT DISTINCT u.sid, 
                      id.name, 
                      u.email, 
                      id.nip, 
                      id.name, 
                      id.street, 
                      id.zip, 
                      id.city, 
                      id.updated_at,
                      i.sid as invoice_sid
      FROM users u
          JOIN invoice_data id ON (u.id = id.user_id)
          JOIN schools s ON (u.id = s.user_id)
          JOIN performances p ON (u.id = p.user_id)
          JOIN competitions c ON (c.id = p.competition_id)
          LEFT JOIN invoices i ON i.user_id = u.id AND i.competition_id = c.id
      WHERE c.id = ${competition.id}
      ORDER BY id.updated_at;
  `);
  return result[0];
};

const countCompetitionPerformancesByDancer = async (competition) => {
  const performances = await models.sequelize.query(`
    SELECT u.sid user_sid, dp.dancer_id, count(*) AS performance_count
    FROM users u
      JOIN performances p ON u.id=p.user_id
      JOIN dancer_performance dp ON p.id=dp.performance_id
    WHERE p.competition_id = ${competition.id}
    GROUP BY (u.sid, dp.dancer_id)
  `);
  return performances[0];
};

const getCompetitionRegistrations = async (competition) => {
  const registrations = await models.sequelize.query(`
    SELECT a.user_sid, a.email, a.name, a.city, a.performances, b.dancers FROM
    (
      SELECT u.sid AS user_sid,
             u.email as email,
             s.id AS school_id,
             s.name AS name,
             a.city AS city,
             count(*) AS performances 
      FROM users u
      JOIN schools s ON (u.id=s.user_id)
      JOIN addresses a ON (s.id=a.school_id)
      JOIN performances p ON (u.id=p.user_id)
      WHERE p.competition_id = ${competition.id}
      GROUP BY (u.sid, u.email, s.id, s.name, a.city)
    ) a
    INNER JOIN
    (
      SELECT s.id AS school_id,
             count(distinct dp.dancer_id) AS dancers
      FROM users u
      JOIN schools s ON (u.id=s.user_id)
      JOIN addresses a ON (s.id=a.school_id)
      JOIN performances p ON (u.id=p.user_id)
      JOIN dancer_performance dp ON (p.id=dp.performance_id)
      WHERE p.competition_id = ${competition.id}
      GROUP BY (s.id)
    ) b
    ON (a.school_id=b.school_id);
  `);
  return registrations[0];
};

const findByAttendingUser = async (user) => {
  return await models.sequelize.query(`
    SELECT c.sid, c.name, p.user_id, count(*) as performance_amount
    FROM performances p
      JOIN competitions c ON c.id=p.competition_id
    WHERE p.user_id=${user.id}
    GROUP BY (c.sid, c.name, p.user_id)
  `);
};

const update = async (id, newData) => {
  const competition = await models.competition.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const params = {
      ...newData,
      is_registration_started: newData.registrationOpened,
      is_result_published: newData.resultPublished,
      is_starting_list_published: newData.startingListPublished
    };
    const updatedCompetition = await competition.update(params, { transaction });
    const competitionEntity = CompetitionMapper.toEntity(updatedCompetition);

    await transaction.commit();

    return competitionEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

module.exports = {
  create,
  findAll,
  findPublic,
  findPublicAndMine,
  getBy,
  getBySid,
  getByName,
  findByAttendingUser,
  findByUser,
  countPerformancesByDancer,
  countCompetitionPerformancesByDancer,
  getCompetitionRegistrations,
  update,
  getCompetitionInvoiceDatas
};