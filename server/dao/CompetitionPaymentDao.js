const models = require('../db/models');
const { CompetitionPaymentMapper} = require('./mappers');

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const payment = await models.competition_payment.findOne({
    where: params,
    include: [{
      model: models.user
    }]
  });
  return CompetitionPaymentMapper.toEntity(payment);
};

const findByCompetition = async (competition) => {
  const payments = await models.competition_payment.findAll({
    where: {
      competition_id: competition.id
    },
    include: [{
      model: models.user
    }]
  });
  return payments.map(CompetitionPaymentMapper.toEntity);
};

const findByCompetitionUser = async (competition, user) => {
  const payments = await models.competition_payment.findAll({
    where: {
      competition_id: competition.id,
      user_id: user.id
    }
  });
  return payments.map(CompetitionPaymentMapper.toEntity);
};

const create = async (payment) => {
  const newPayment = await models.competition_payment.create(payment);
  return CompetitionPaymentMapper.toEntity(newPayment);
};

module.exports = {
  create,
  getBy,
  getBySid,
  findByCompetitionUser,
  findByCompetition
};