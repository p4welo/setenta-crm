const models = require('../db/models');
const { EmailMapper } = require('../dao/mappers');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');

const create = async (email) => {
  const newEmail = await models.email.create(email);
  return EmailMapper.toEntity(newEmail);
};

const remove = async (email) => {
  await update(email.id, { objectState: DataObjectStates.DELETED });

};

const update = async (id, newData) => {
  const email = await models.email.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedEmail = await email.update(newData, { transaction });
    const emailEntity = EmailMapper.toEntity(updatedEmail);

    await transaction.commit();

    return emailEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const findByUser = async (user) => {
  const emails = await models.email.findAll({
    where: {
      user_id: user.id,
      object_state: DataObjectStates.ACTIVE
    }
  });
  return emails.map(EmailMapper.toEntity);
};

const findAll = async () => {
  const emails = await models.email.findAll({
    include: [
      { model: models.user }
    ]
  });

  return emails.map(EmailMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const email = await models.email.findOne({
    where: params,
    include: [{
      model: models.user
    }]
  });
  return EmailMapper.toEntity(email);
};

module.exports = {
  create,
  remove,
  getBy,
  getBySid,
  findAll,
  findByUser,
  update
};