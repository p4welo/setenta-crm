const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const PerformanceStyle = rootRequire('domain/PerformanceStyle');
const SidUtils = rootRequire('utils/SidUtils');

const PerformanceStyleMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, name, competition, show_title } = dataValues;

    return new PerformanceStyle({
      id,
      sid,
      objectState: object_state,
      showTitle: show_title,
      name,
      competition
    });
  },

  serialize(style) {
    const { sid, objectState, name, showTitle } = style;
    return {
      sid,
      objectState,
      name,
      showTitle
    }
  },

  toDatabase(name, competition, showTitle) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      show_title: showTitle,
      name,
      competition_id: competition.id,
    };
  }
};

module.exports = PerformanceStyleMapper;