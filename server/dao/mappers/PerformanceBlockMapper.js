const PerformanceBlock = require('../../domain/PerformanceBlock');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');

const PerformanceBlockMapper = {
  toEntity({ dataValues }) {
    const { id, sid, objectState, style, type, experience, age, position, competition } = dataValues;

    return new PerformanceBlock({
      id,
      sid,
      objectState,
      style,
      type,
      experience,
      age
    });
  },

  serialize(block) {
    const { style, type, experience, age, position } = block;
    return {
      style,
      type,
      experience,
      age
    }
  },

  toDatabase(style, type, experience, age, position, competition) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      style,
      type,
      experience,
      age
    }
  }
};

module.exports = PerformanceBlockMapper;