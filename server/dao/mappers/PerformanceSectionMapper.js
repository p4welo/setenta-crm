const PerformanceSection = require('../../domain/PerformanceSection');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const PerformanceMapper = require('./PerformanceMapper');

const PerformanceSectionMapper = {
  toEntity({ dataValues }) {
    const { id, sid, objectState, day, chapter, position, blocks, competition, eliminations } = dataValues;

    return new PerformanceSection({
      id,
      sid,
      objectState,
      day,
      chapter,
      position,
      blocks,
      competition,
      eliminations
    });
  },

  serialize(section) {
    const { sid, day, chapter, position, blocks, performances, eliminations } = section;
    return {
      sid,
      day,
      chapter,
      position,
      blocks,
      eliminations,
      performances: performances ? performances.map(PerformanceMapper.serialize) : []
    }
  },

  toDatabase(day, chapter, position, competition) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      day,
      chapter,
      position,
      competition_id: competition.id
    }
  }
};

module.exports = PerformanceSectionMapper;
