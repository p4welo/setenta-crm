const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const PaymentModelEntry = rootRequire('domain/PaymentModelEntry');
const SidUtils = rootRequire('utils/SidUtils');

const PaymentModelEntryMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, amount, value, date, competition } = dataValues;

    return new PaymentModelEntry({
      id,
      sid,
      objectState: object_state,
      date,
      amount,
      value,
      competition
    });
  },

  serialize(entry) {
    const { sid, objectState, amount, value, date } = entry;
    return {
      sid,
      objectState,
      amount,
      value,
      date
    }
  },

  toDatabase(amount, value, competition) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      amount,
      value,
      competition_id: competition.id,
    };
  }
};

module.exports = PaymentModelEntryMapper;