const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const CompetitionLicence = rootRequire('domain/CompetitionLicence');
const SidUtils = rootRequire('utils/SidUtils');

const CompetitionLicenceMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, music_upload, results, competition } = dataValues;

    return new CompetitionLicence({
      id,
      sid,
      objectState: object_state,
      musicUpload: music_upload,
      results,
      competition
    });
  },

  serialize(entry) {
    const { musicUpload, results } = entry;
    return {
      musicUpload,
      results
    }
  },

  toDatabase(musicUpload, results, competition) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      music_upload: musicUpload,
      results,
      competition_id: competition.id,
    };
  }
};

module.exports = CompetitionLicenceMapper;