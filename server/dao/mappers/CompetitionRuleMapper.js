const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const CompetitionRule = rootRequire('domain/CompetitionRule');
const SidUtils = rootRequire('utils/SidUtils');
const PerformanceStyleMapper = require('./PerformanceStyleMapper');
const PerformanceTypeMapper = require('./PerformanceTypeMapper');
const PerformanceExperienceMapper = require('./PerformanceExperienceMapper');
const PerformanceAgeMapper = require('./PerformanceAgeMapper');

const CompetitionRuleMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, rule_type, performance_style, performance_type, performance_experience, performance_age, rules, competition, parent_id, own_music_allowed } = dataValues;

    return new CompetitionRule({
      id,
      parentId: parent_id,
      sid,
      objectState: object_state,
      ruleType: rule_type,
      ownMusicAllowed: own_music_allowed,
      style: performance_style ? PerformanceStyleMapper.toEntity(performance_style) : undefined,
      type: performance_type ? PerformanceTypeMapper.toEntity(performance_type) : undefined,
      experience: performance_experience ? PerformanceExperienceMapper.toEntity(performance_experience) : undefined,
      age: performance_age ? PerformanceAgeMapper.toEntity(performance_age) : undefined,
      rules: rules ? rules.map(CompetitionRuleMapper.toEntity) : [],
      competition
    });
  },

  serialize(rule) {
    const { id, sid, objectState, ruleType, style, type, experience, age, rules, parentId, ownMusicAllowed } = rule;
    return {
      id,
      parentId,
      sid,
      objectState,
      ruleType,
      ownMusicAllowed,
      style: style ? PerformanceStyleMapper.serialize(style) : undefined,
      type: type ? PerformanceTypeMapper.serialize(type) : undefined,
      experience: experience ? PerformanceExperienceMapper.serialize(experience) : undefined,
      age: age ? PerformanceAgeMapper.serialize(age) : undefined,
      rules: rules ? rules.map(CompetitionRuleMapper.serialize) : []
    }
  },

  toDatabase(parent, ruleType, competition, style, type, experience, age) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      rule_type: ruleType,
      performance_style_id: style ? style.id : undefined,
      performance_type_id: type ? type.id : undefined,
      performance_experience_id: experience ? experience.id : undefined,
      performance_age_id: age ? age.id : undefined,
      parent_id: parent ? parent.id : undefined,
      competition_id: competition.id,
    };
  }
};

module.exports = CompetitionRuleMapper;