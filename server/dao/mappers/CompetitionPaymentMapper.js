const CompetitionPayment = require('../../domain/CompetitionPayment');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const UserMapper = require('./UserMapper');
const CompetitionMapper = require('./CompetitionMapper');

const CompetitionPaymentMapper = {
  toEntity({ dataValues }) {
    const { id, sid, date, amount, competition, user } = dataValues;

    return new CompetitionPayment({
      id,
      sid,
      date,
      amount,
      competition,
      user
    });
  },

  serialize(payment) {
    const { sid, date, amount, competition, user } = payment;
    return {
      sid,
      date,
      amount,
      competition: competition ? CompetitionMapper.serialize(competition) : undefined,
      user: user ? UserMapper.serialize(user) : undefined
    }
  },

  toDatabase(date, amount, competition, user) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      date,
      amount,
      user_id: user.id,
      competition_id: competition.id,
    }
  }
};

module.exports = CompetitionPaymentMapper;