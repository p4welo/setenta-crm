const School = require('../../domain/School');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const AddressMapper = require('./AddressMapper');
const UserMapper = require('./UserMapper');

const SchoolMapper = {
  toEntity({ dataValues }) {
    const { id, sid, name, phone, user, address } = dataValues;

    return new School({
      id,
      sid,
      name,
      phone,
      user,
      address
    });
  },

  serialize(school) {
    const { sid, name, phone, address, user } = school;
    return {
      sid,
      name,
      phone,
      email: user ? user.email : undefined,
      address: address ? AddressMapper.serialize(address) : undefined
    }
  },

  toDatabase(name, phone, user) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      name,
      phone,
      user_id: user.id
    }
  }
};

module.exports = SchoolMapper;