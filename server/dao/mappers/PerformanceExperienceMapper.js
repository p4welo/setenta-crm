const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const PerformanceExperience = rootRequire('domain/PerformanceExperience');
const SidUtils = rootRequire('utils/SidUtils');
const CompetitionMapper = require('./CompetitionMapper');

const PerformanceExperienceMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, name, competition } = dataValues;

    return new PerformanceExperience({
      id,
      sid,
      objectState: object_state,
      name,
      competition
    });
  },

  serialize(experience) {
    const { sid, objectState, name } = experience;
    return {
      sid,
      objectState,
      name
    }
  },

  toDatabase(name, competition) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      name,
      competition_id: competition.id,
    };
  }
};

module.exports = PerformanceExperienceMapper;