const Email = require('../../domain/Email');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');

const EmailMapper = {
  toEntity({ dataValues }) {
    const { id, sid, from, to, subject, content, read_count, user } = dataValues;

    return new Email({
      id,
      sid,
      from,
      to,
      subject,
      content,
      user,
      readCount: read_count
    });
  },

  serialize(email) {
    const { sid, from, to, subject, content, user, readCount } = email;
    return {
      sid,
      from,
      to,
      subject,
      content,
      user,
      readCount
    }
  },

  toDatabase(from, to, subject, content, user, read_count = 0) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      from,
      to,
      subject,
      content,
      user_id: user.id,
      read_count
    }
  }
};

module.exports = EmailMapper;