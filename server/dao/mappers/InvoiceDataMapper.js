const InvoiceData = require('../../domain/InvoiceData');
const SidUtils = require('../../utils/SidUtils');
const DataObjectStates = require('../../models/constants/DataObjectStates');

const InvoiceDataMapper = {
  toEntity(object) {
    if (!object) {
      return undefined;
    }
    const { dataValues } = object;
    const { id, sid, name, nip, street, zip, city, user } = dataValues;
    return new InvoiceData({
      id,
      sid,
      name,
      nip,
      street,
      zip,
      city,
      user
    });
  },

  serialize(invoiceData) {
    const { sid, name, nip, street, zip, city } = invoiceData;
    return {
      sid,
      name,
      nip,
      street,
      zip,
      city
    }
  },

  toDatabase(name, nip, street, zip, city, user) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      name,
      nip,
      street,
      zip,
      city,
      user_id: user.id
    }
  }
};

module.exports = InvoiceDataMapper;