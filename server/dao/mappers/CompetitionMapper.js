const Competition = require('../../domain/Competition');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const UserMapper = require('./UserMapper');

const CompetitionMapper = {
  toEntity({ dataValues }) {
    const { id, sid, name, user, start, end, is_public, registrationEnd, website, regulations, map_url, phone, place, street, zip, city, is_registration_started, is_result_published, is_starting_list_published, payment_model, age_level_tolerance, audio_upload_end } = dataValues;

    return new Competition({
      id,
      sid,
      name,
      user,
      start,
      end,
      paymentModel: payment_model,
      isPublic: is_public,
      registrationEnd,
      audioUploadEnd: audio_upload_end,
      registrationOpened: is_registration_started,
      resultPublished: is_result_published,
      startingListPublished: is_starting_list_published,
      website,
      regulations,
      mapUrl: map_url,
      phone,
      place,
      street,
      zip,
      city,
      ageLevelTolerance: age_level_tolerance
    });
  },

  plainToEntity(dataValues) {
    return this.toEntity({ dataValues });
  },

  serialize(competition) {
    const { sid, name, user, start, end, isPublic, registrationEnd, audioUploadEnd, website, regulations, mapUrl, phone, place, street, zip, city, resultPublished, startingListPublished, paymentModel, ageLevelTolerance } = competition;
    return {
      sid,
      name,
      start,
      end,
      registrationEnd,
      audioUploadEnd,
      website,
      regulations,
      mapUrl,
      phone,
      place,
      street,
      zip,
      city,
      isPublic,
      paymentModel,
      registrationOpened: competition.isRegistrationOpened(),
      audioUploadEnabled: competition.isAudioUploadEnabled(),
      resultPublished,
      startingListPublished,
      ageLevelTolerance,
      user: user ? UserMapper.serialize(user) : undefined
    }
  },

  toDatabase(name, user, start, end, registrationEnd, website, regulations, phone, place, street, zip, city, paymentModel, ageLevelTolerance, audioUploadEnd) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      name,
      start,
      end,
      registrationEnd,
      audio_upload_end: audioUploadEnd,
      website,
      regulations,
      phone,
      place,
      street,
      zip,
      city,
      age_level_tolerance: ageLevelTolerance,
      payment_model: paymentModel,
      user_id: user.id
    }
  }
};

module.exports = CompetitionMapper;