const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const SidUtils = rootRequire('utils/SidUtils');
const PerformancePrice = rootRequire('domain/PerformancePrice');

const PerformancePriceMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, type, value, description, performance } = dataValues;

    return new PerformancePrice({
      id,
      sid,
      objectState: object_state,
      type,
      value,
      description,
      performance
    });
  },

  serialize(price) {
    const { sid, objectState, type, value, description } = price;
    return {
      sid,
      objectState,
      type,
      value,
      description
    };
  },

  toDatabase(type, value, description, performance) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      type,
      value,
      description,
      performance_id: performance.id
    }
  }
};

module.exports = PerformancePriceMapper;