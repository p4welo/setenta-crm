const config = rootRequire('config/app');
const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const Audio = rootRequire('domain/Audio');
const SidUtils = rootRequire('utils/SidUtils');

const AudioMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, encoding, mimetype, destination, filename, path, size } = dataValues;

    return new Audio({
      id,
      sid,
      objectState: object_state,
      encoding,
      mimetype,
      destination,
      filename,
      path,
      size
    });
  },

  serialize(song) {
    const { sid, filename } = song;
    return {
      sid,
      url: `${config.baseUrl}/uploads/${filename}`
    }
  },

  toDatabase(encoding, mimetype, destination, filename, path, size, user) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      encoding,
      mimetype,
      destination,
      filename,
      path,
      size,
      user_id: user.id
    };
  }
};

module.exports = AudioMapper;