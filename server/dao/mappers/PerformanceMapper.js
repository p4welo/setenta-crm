const Performance = require('../../domain/Performance');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const UserMapper = require('./UserMapper');
const DancerMapper = require('./DancerMapper');
const PerformancePriceMapper = require('./PerformancePriceMapper');

const PerformanceMapper = {
  toEntity({ dataValues }) {
    const { id, sid, objectState, name, title, style, type, experience, ageLevel, number, place, user, dancers, competition, school, phone, city, email, created_at, audio_sid, audio, prices } = dataValues;

    return new Performance({
      id,
      sid,
      objectState,
      name,
      title,
      style,
      type,
      experience,
      ageLevel,
      number,
      place,
      user: user ? UserMapper.toEntity(user) : undefined,
      dancers,
      competition,
      createdAt: created_at,
      audioSid: audio_sid,
      audio,
      prices,

      school,
      city,
      phone,
      email
    });
  },

  serialize(performance) {
    const { sid, name, title, style, type, experience, ageLevel, number, place, user, dancers, school, phone, city, email, audioSid, prices } = performance;
    return {
      sid,
      name,
      title,
      style,
      type,
      experience,
      ageLevel,
      number,
      place,
      user: user ? UserMapper.serialize(user) : undefined,
      dancers: dancers ? dancers.map(DancerMapper.serialize) : [],
      prices: prices ? prices.map(PerformancePriceMapper.serialize) : [],
      school,
      city,
      phone,
      email,
      audioSid
    }
  },

  toDatabase(name, style, type, experience, ageLevel, user, competition, title) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      name,
      title,
      style,
      type,
      experience,
      ageLevel,
      user_id: user.id,
      competition_id: competition.id
    }
  }
};

module.exports = PerformanceMapper;