const Dancer = require('../../domain/Dancer');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');

const DancerMapper = {
  toEntity({ dataValues }) {
    const { id, sid, firstName, lastName, yearOfBirth, groupName, user } = dataValues;

    return new Dancer({
      id, sid, firstName, lastName, yearOfBirth, groupName, user
    });
  },

  serialize(dancer) {
    const { sid, firstName, lastName, yearOfBirth, groupName, performances } = dancer;
    return {
      sid,
      firstName,
      lastName,
      yearOfBirth,
      groupName,
      performanceAmount: +performances
    }
  },

  toDatabase(firstName, lastName, yearOfBirth, groupName, user) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      firstName,
      lastName,
      yearOfBirth,
      groupName,
      user_id: user.id
    }
  }
};

module.exports = DancerMapper;