const Invoice = require('../../domain/Invoice');
const SidUtils = require('../../utils/SidUtils');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const UserMapper = require('./UserMapper');

const InvoiceMapper = {
  toEntity(object) {
    if (!object) {
      return undefined;
    }
    const { dataValues } = object;
    const { id, sid, object_state, name, nip, street, zip, city, title, provider, ext_id, number, net_price, tax_price, gross_price, competition, user } = dataValues;
    return new Invoice({
      id,
      sid,
      objectState: object_state,
      name,
      nip,
      street,
      zip,
      city,
      title,
      provider,
      extId: ext_id,
      number,
      netPrice: net_price,
      taxPrice: tax_price,
      grossPrice: gross_price,
      competition,
      user
    });
  },

  serialize(invoice) {
    const { sid, number, name, nip, street, zip, city, title, netPrice, taxPrice, grossPrice, user } = invoice;
    return {
      sid,
      number,
      name,
      nip,
      street,
      zip,
      city,
      title,
      netPrice,
      taxPrice,
      grossPrice,
      user: user ? UserMapper.serialize(user) : undefined
    }
  },

  toDatabase(name, nip, street, zip, city, title, provider, ext_id, number, net_price, tax_price, gross_price, competition, user) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      name,
      nip,
      street,
      zip,
      city,
      title,
      provider,
      ext_id,
      number,
      net_price,
      tax_price,
      gross_price,
      competition_id: competition.id,
      user_id: user.id
    }
  }
};

module.exports = InvoiceMapper;