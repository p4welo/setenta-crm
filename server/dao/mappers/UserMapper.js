const User = require('../../domain/User');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');
const AuthenticationService = require('../../services/AuthenticationService');
const SchoolMapper = require('./SchoolMapper');

const UserMapper = {
  toEntity({ dataValues }) {
    const { id, sid, objectState, login, email, password, resetHash, type, firstName, lastName, school } = dataValues;

    return new User({
      id,
      sid,
      objectState,
      login,
      email,
      password,
      firstName,
      lastName,
      resetHash,
      type,
      school: school ? SchoolMapper.toEntity(school) : undefined
    });
  },

  serialize(user) {
    const { sid, email, login, type, objectState, firstName, lastName, name, city, school } = user;
    return {
      sid,
      email,
      login,
      firstName,
      lastName,
      name,
      city,
      client: '5998733647375b02dafe9b14',
      school: school ? SchoolMapper.serialize(school) : undefined,
      type: type ? type.name : undefined,
      objectState
    }
  },

  toDatabase(email, password, type, firstName, lastName) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.INACTIVE,
      login: email,
      password: AuthenticationService.encrypt(password),
      email,
      firstName,
      lastName,
      type_id: type.id
    };
  }
};

module.exports = UserMapper;