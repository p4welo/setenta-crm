const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const PerformanceType = rootRequire('domain/PerformanceType');
const SidUtils = rootRequire('utils/SidUtils');

const PerformanceTypeMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, name, min, max, show_name, competition } = dataValues;

    return new PerformanceType({
      id,
      sid,
      objectState: object_state,
      name,
      min,
      max,
      showName: show_name,
      competition
    });
  },

  serialize(type) {
    const { sid, objectState, name, min, max, showName } = type;
    return {
      sid,
      objectState,
      name,
      min,
      max,
      showName
    }
  },

  toDatabase(name, min, max, showName, competition) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      name,
      min,
      max,
      show_name: showName,
      competition_id: competition.id,
    };
  }
};

module.exports = PerformanceTypeMapper;