const DataObjectStates = rootRequire('models/constants/DataObjectStates');
const SidUtils = rootRequire('utils/SidUtils');
const PerformanceAge = rootRequire('domain/PerformanceAge');

const PerformanceAgeMapper = {
  toEntity({ dataValues }) {
    const { id, sid, object_state, name, min, max, competition } = dataValues;

    return new PerformanceAge({
      id,
      sid,
      objectState: object_state,
      name,
      min,
      max,
      competition
    });
  },

  serialize(age) {
    const { sid, objectState, name, min, max } = age;
    return {
      sid,
      objectState,
      name,
      min,
      max
    };
  },

  toDatabase(name, min, max, competition) {
    return {
      sid: SidUtils.generate(),
      object_state: DataObjectStates.ACTIVE,
      name,
      min,
      max,
      competition_id: competition.id,
    };
  }
};

module.exports = PerformanceAgeMapper;