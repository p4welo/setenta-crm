const UserType = require('../../domain/UserType');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');

const UserTypeMapper = {
  toEntity({ dataValues }) {
    const { id, sid, name, users } = dataValues;

    return new UserType({
      id,
      sid,
      name,
      users
    });
  },

  serialize(type) {
    const { sid, name, users } = type;
    return {
      sid, name, users
    }
  },

  toDatabase(name) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      name
    }
  }
};

module.exports = UserTypeMapper;