const Address = require('../../domain/Address');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const SidUtils = require('../../utils/SidUtils');

const AddressMapper = {
  toEntity({ dataValues }) {
    const { id, sid, street, zip, city, school } = dataValues;

    return new Address({
      id,
      sid,
      street,
      zip,
      city,
      school
    });
  },

  serialize(address) {
    const { sid, street, zip, city, school } = address;
    return {
      sid,
      street,
      zip,
      city,
      school: school ? school : undefined,
    }
  },

  toDatabase(school, city, zip, street) {
    return {
      sid: SidUtils.generate(),
      objectState: DataObjectStates.ACTIVE,
      street,
      zip,
      city,
      school_id: school.id
    }
  }
};

module.exports = AddressMapper;