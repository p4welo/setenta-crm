const models = require('../db/models');
const { UserMapper } = require('../dao/mappers');

const findAll = async () => {
  const users = await models.user.findAll({
    include: [{
      model: models.user_type,
      as: 'type'
    }]
  });

  return users.map(UserMapper.toEntity);
};

const getBySid = async (sid) => {
  return await getBy({ sid });
};

const getBy = async (params) => {
  const user = await models.user.findOne({
    where: params,
    include: [{
      model: models.user_type,
      as: 'type'
    }]
  });
  return UserMapper.toEntity(user);
};

const update = async (id, newData) => {
  const user = await models.user.findById(id);

  const transaction = await models.sequelize.transaction();

  try {
    const updatedUser = await user.update(newData, { transaction });
    const userEntity = UserMapper.toEntity(updatedUser);

    await transaction.commit();

    return userEntity;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
};

const remove = async (user) => {
  const found = await models.user.findById(user.id);
  await found.destroy();
  return;
};

const getBySidExtended = async (sid) => {
  const result = await models.sequelize.query(`
      select u.*, s.name, a.city from users as u
        left join schools as s on (u.id=s.user_id)
        left join addresses as a on (s.id=a.school_id)
      where u.sid='${sid}';
    `);
  return result[0][0];
};

const create = async (user) => {
  const newUser = await models.user.create(user);
  return UserMapper.toEntity(newUser);
};

module.exports = {
  create,
  findAll,
  getBy,
  getBySid,
  getBySidExtended,
  update,
  remove
};