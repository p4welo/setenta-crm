alter table performances
    add audio_sid varchar(255);

alter table performances
    add constraint performances_audios__fk
        foreign key (audio_sid) references audios (sid)
            on delete set null;

