const moment = require('moment');
const logger = rootRequire('logger');
const {
  UserTypeDao,
  UserDao,
  CompetitionDao,
  DancerDao,
  PerformanceDao,
  SchoolDao,
  AddressDao
} = rootRequire('dao');
const {
  UserTypeMapper,
  UserMapper,
  CompetitionMapper,
  DancerMapper,
  PerformanceMapper,
  SchoolMapper,
  AddressMapper
} = rootRequire('dao/mappers');
const { UserTypes } = rootRequire('models/constants');

const createSampleType = async (name) => {
  let type;
  try {
    type = await UserTypeDao.getByName(name);
  } catch (error) {
    type = await UserTypeDao.create(UserTypeMapper.toDatabase(name))
  }
  return type;
};

const createSampleUser = async (email, password, type, schoolName, phone, city) => {
  let user;
  try {
    user = await UserDao.getBy({ email });
  } catch (error) {
    user = await UserDao.create({
      ...UserMapper.toDatabase(email, password, type),
      objectState: 'ACTIVE'
    });
    if (schoolName && city) {
      const school = await SchoolDao.create({
        ...SchoolMapper.toDatabase(schoolName, phone, user)
      });
      const address = await AddressDao.create({
        ...AddressMapper.toDatabase(school, city)
      })
    }
  }
  return user;
};

const createSampleCompetition = async (user, name, start, end, registrationEnd, website, phone, place, street, zip, city) => {
  let competition;
  try {
    competition = await CompetitionDao.getByName(name);
  } catch (error) {
    competition = await CompetitionDao.create(CompetitionMapper.toDatabase(name, user, start, end, registrationEnd, website, phone, place, street, zip, city));
  }
  return competition;
};

const createSampleDancer = async (firstName, lastName, yearOfBirth, groupName, user) => {
  let dancer;
  try {
    dancer = await DancerDao.getBy({ firstName, lastName, yearOfBirth, groupName });
  } catch (error) {
    dancer = await DancerDao.create(DancerMapper.toDatabase(firstName, lastName, yearOfBirth, groupName, user));
  }
  return dancer;
};

const createSamplePerformance = async (name, style, type, experience, ageLevel, dancers, competition, user) => {
  let performance;
  performance = await PerformanceDao.create(PerformanceMapper.toDatabase(name, style, type, experience, ageLevel, user, competition), dancers);

  return performance;
};

module.exports = async () => {
  const crmType = await createSampleType(UserTypes.CRM);
  const crmUser = await createSampleUser('p4welo@gmail.com', 'pawelo88', crmType);
  await createSampleUser('sekretariat@setenta.wroclaw.pl', 'bojatanczycchce', crmType);

  const competitionType = await createSampleType(UserTypes.COMPETITION);

  const schoolType = await createSampleType(UserTypes.SCHOOL);
  // await createSampleUser(
  //     'pawel.radomski1@gmail.com',
  //     'pawelo88',
  //     schoolType,
  //     'Szkoła tańca Setenta',
  //     '793432042',
  //     'Wrocław'
  // );

  // await createSampleCompetition(
  //     crmUser,
  //     'I Festiwal tańca formacji i miniformacji TRENDY',
  //     moment('2019-04-06').startOf('day').toDate(),
  //     moment('2019-04-06').endOf('day').toDate(),
  //     moment('2019-03-17').endOf('day').toDate(),
  //     'http://trendy.setenta.wroclaw.pl',
  //     '695081437',
  //     'Szkoła Podstawowa im. Odkrywców i Podróżników',
  //     'ul. Parkowa 2/4',
  //     '56-400',
  //     'Borowa k. Wrocławia'
  // );
};