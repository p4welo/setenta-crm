module.exports = (sequelize, DataTypes) => {

  const PerformanceExperience = sequelize.define('performance_experience', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    underscored: true
  });

  PerformanceExperience.associate = (models) => {
    models.performance_experience.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return PerformanceExperience;
};