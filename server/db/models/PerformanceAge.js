module.exports = (sequelize, DataTypes) => {

  const PerformanceAge = sequelize.define('performance_age', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    min: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    max: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    underscored: true
  });

  PerformanceAge.associate = (models) => {
    models.performance_age.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return PerformanceAge;
};