module.exports = (sequelize, DataTypes) => {

  const PaymentModelEntry = sequelize.define('payment_model_entry', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    amount: DataTypes.INTEGER,
    value: DataTypes.FLOAT,
    date: DataTypes.DATE
  });

  PaymentModelEntry.associate = (models) => {
    models.payment_model_entry.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return PaymentModelEntry;
};