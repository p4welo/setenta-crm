module.exports = (sequelize, DataTypes) => {

  const CompetitionRule = sequelize.define('competition_rule', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rule_type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    own_music_allowed: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  }, {
    underscored: true
  });

  CompetitionRule.associate = (models) => {
    models.competition_rule.belongsTo(models.competition);
    models.competition_rule.belongsTo(models.performance_style);
    models.competition_rule.belongsTo(models.performance_type);
    models.competition_rule.belongsTo(models.performance_experience);
    models.competition_rule.belongsTo(models.performance_age);
    models.competition_rule.hasMany(models.competition_rule, { as: 'rules', foreignKey: 'parent_id', onDelete: 'cascade' });
  };

  return CompetitionRule;
};