module.exports = (sequelize, DataTypes) => {

  const PerformanceStyle = sequelize.define('performance_style', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    show_title: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  }, {
    underscored: true
  });

  PerformanceStyle.associate = (models) => {
    models.performance_style.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return PerformanceStyle;
};