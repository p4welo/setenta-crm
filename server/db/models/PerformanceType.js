module.exports = (sequelize, DataTypes) => {

  const PerformanceType = sequelize.define('performance_type', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    min: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    max: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    show_name: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  }, {
    underscored: true
  });

  PerformanceType.associate = (models) => {
    models.performance_type.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return PerformanceType;
};