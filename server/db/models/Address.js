module.exports = (sequelize, DataTypes) => {

  const Address = sequelize.define('address', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    street: DataTypes.STRING,
    zip: DataTypes.STRING,
    city: DataTypes.STRING
  }, {
    underscored: true
  });

  Address.associate = (models) => {
    models.address.belongsTo(models.school, { onDelete: 'cascade' });
  };
  return Address;
};