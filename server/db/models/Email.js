module.exports = (sequelize, DataTypes) => {

  const Email = sequelize.define('email', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    from: DataTypes.STRING,
    to: DataTypes.STRING,
    subject: DataTypes.STRING,
    content: DataTypes.STRING,
    read_count: {
      type: DataTypes.INTEGER,
      default: 0
    }
  }, {
    underscored: true
  });

  Email.associate = (models) => {
    models.email.belongsTo(models.user, { onDelete: 'cascade' });
  };

  return Email;
};