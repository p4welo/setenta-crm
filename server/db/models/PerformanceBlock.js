module.exports = (sequelize, DataTypes) => {

  const PerformanceBlock = sequelize.define('performance_block', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    style: DataTypes.STRING,
    type: DataTypes.STRING,
    age: DataTypes.STRING,
    experience: DataTypes.STRING
  }, {
    underscored: true
  });

  PerformanceBlock.associate = (models) => {
    models.performance_block.belongsTo(models.performance_section, { onDelete: 'cascade' });
  };
  return PerformanceBlock;
};