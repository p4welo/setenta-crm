module.exports = (sequelize, DataTypes) => {

  const CompetitionPayment = sequelize.define('competition_payment', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: false
    }
  }, {
    underscored: true
  });

  CompetitionPayment.associate = (models) => {
    models.competition_payment.belongsTo(models.user);
    models.competition_payment.belongsTo(models.competition);
  };

  return CompetitionPayment;
};