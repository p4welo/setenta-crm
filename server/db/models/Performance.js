module.exports = (sequelize, DataTypes) => {

  const Performance = sequelize.define('performance', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: DataTypes.STRING,
    title: DataTypes.STRING,
    style: DataTypes.STRING,
    type: DataTypes.STRING,
    experience: DataTypes.STRING,
    ageLevel: DataTypes.STRING,
    number: DataTypes.STRING,
    place: DataTypes.INTEGER,
    audio_sid: DataTypes.STRING
  }, {
    underscored: true
  });

  Performance.associate = (models) => {
    models.performance.belongsToMany(models.dancer, { through: 'dancer_performance' });
    models.performance.belongsTo(models.user, { onDelete: 'cascade' });
    models.performance.belongsTo(models.competition, { onDelete: 'cascade' });
    models.performance.belongsTo(models.audio, {foreignKey: 'audio_sid', targetKey: 'sid'});
    models.performance.hasMany(models.performance_price, { as: 'prices' });
  };

  return Performance;
};