module.exports = (sequelize, DataTypes) => {

  const UserType = sequelize.define('user_type', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  }, {
    underscored: true
  });

  return UserType;
};