module.exports = (sequelize, DataTypes) => {

  const Dancer = sequelize.define('dancer', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    yearOfBirth: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    groupName: DataTypes.STRING
  }, {
    underscored: true
  });

  Dancer.associate = (models) => {
    models.dancer.belongsTo(models.user, { onDelete: 'cascade' });
  };

  return Dancer;
};