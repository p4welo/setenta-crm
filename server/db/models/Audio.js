module.exports = (sequelize, DataTypes) => {

  const Audio = sequelize.define('audio', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    encoding: DataTypes.STRING,
    mimetype: DataTypes.STRING,
    destination: DataTypes.STRING,
    filename: DataTypes.STRING,
    path: DataTypes.STRING,
    size: DataTypes.INTEGER
  }, {
    underscored: true
  });

  Audio.associate = (models) => {
    models.audio.belongsTo(models.user, { onDelete: 'cascade' });
  };

  return Audio;
};