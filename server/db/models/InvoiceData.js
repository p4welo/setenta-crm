module.exports = (sequelize, DataTypes) => {

  const InvoiceData = sequelize.define('invoice_data', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: DataTypes.STRING,
    nip: DataTypes.STRING,
    street: DataTypes.STRING,
    zip: DataTypes.STRING,
    city: DataTypes.STRING
  }, {
    underscored: true
  });

  InvoiceData.associate = (models) => {
    models.invoice_data.belongsTo(models.user, { onDelete: 'cascade' });
  };

  return InvoiceData;
};