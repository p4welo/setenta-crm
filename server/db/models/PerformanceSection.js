module.exports = (sequelize, DataTypes) => {

  const PerformanceSection = sequelize.define('performance_section', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    chapter: DataTypes.INTEGER,
    day: DataTypes.INTEGER,
    position: DataTypes.INTEGER,
    eliminations: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  }, {
    underscored: true
  });

  PerformanceSection.associate = (models) => {
    models.performance_section.belongsTo(models.competition, { onDelete: 'cascade' });
    models.performance_section.hasMany(models.performance_block, { as: 'blocks' });
  };
  return PerformanceSection;
};