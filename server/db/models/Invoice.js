module.exports = (sequelize, DataTypes) => {

  const Invoice = sequelize.define('invoice', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    object_state: {
      type: DataTypes.STRING,
      allowNull: false
    },

    name: DataTypes.STRING,
    nip: DataTypes.STRING,
    street: DataTypes.STRING,
    zip: DataTypes.STRING,
    city: DataTypes.STRING,

    title: DataTypes.STRING,

    provider: DataTypes.STRING,
    ext_id: DataTypes.INTEGER,
    number: DataTypes.STRING,
    net_price: DataTypes.INTEGER,
    tax_price: DataTypes.INTEGER,
    gross_price: DataTypes.INTEGER
  }, {
    underscored: true
  });

  Invoice.associate = (models) => {
    models.invoice.belongsTo(models.competition, { onDelete: 'cascade' });
    models.invoice.belongsTo(models.user, { onDelete: 'cascade' });
  };

  return Invoice;
};