module.exports = (sequelize, DataTypes) => {

  const PerformancePrice = sequelize.define('performance_price', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    type: DataTypes.STRING,
    value: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    underscored: true
  });

  PerformancePrice.associate = (models) => {
    models.performance_price.belongsTo(models.performance, { onDelete: 'cascade' });
  };

  return PerformancePrice;
};