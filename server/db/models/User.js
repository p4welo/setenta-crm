const { fillSidIfNeeded, fillObjectStateIfNeeded } = require('../../utils/DbUtils');

module.exports = (sequelize, DataTypes) => {

    const User = sequelize.define('user', {
      sid: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      objectState: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      resetHash: DataTypes.STRING,
      login: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING
    }, {
      underscored: true,
      hooks: {
        beforeValidate(user) {
          fillSidIfNeeded(user);
          fillObjectStateIfNeeded(user);
        }
      }
    });

    User.associate = (models) => {
      models.user.belongsTo(models.user_type, {as: 'type', onDelete: 'cascade'});
      models.user.hasOne(models.school);
    };

    return User;
};
