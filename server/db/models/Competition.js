module.exports = (sequelize, DataTypes) => {

  const Competition = sequelize.define('competition', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    is_public: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    is_registration_started: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    is_starting_list_published: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    is_result_published: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    registrationEnd: DataTypes.DATE,
    website: DataTypes.STRING,
    regulations: DataTypes.STRING,
    map_url: DataTypes.STRING,
    phone: DataTypes.STRING,
    place: DataTypes.STRING,
    street: DataTypes.STRING,
    zip: DataTypes.STRING,
    city: DataTypes.STRING,
    payment_model: DataTypes.STRING,
    age_level_tolerance: DataTypes.INTEGER,
    audio_upload_end: DataTypes.DATE
  }, {
    underscored: true
  });

  Competition.associate = (models) => {
    models.competition.belongsTo(models.user);
  };

  return Competition;
};