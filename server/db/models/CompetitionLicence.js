module.exports = (sequelize, DataTypes) => {

  const CompetitionLicence = sequelize.define('competition_licence', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    music_upload: DataTypes.BOOLEAN,
    results: DataTypes.BOOLEAN
  }, {
    underscored: true
  });

  CompetitionLicence.associate = (models) => {
    models.competition_licence.belongsTo(models.competition, { onDelete: 'cascade' });
  };

  return CompetitionLicence;
};