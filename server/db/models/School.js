module.exports = (sequelize, DataTypes) => {

  const School = sequelize.define('school', {
    sid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    objectState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    underscored: true
  });

  School.associate = (models) => {
    models.school.belongsTo(models.user, { onDelete: 'cascade' });
    models.school.hasOne(models.address);
  };

  return School;
};