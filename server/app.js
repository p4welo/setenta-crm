const mongoose = require('mongoose');
const www = require('./www');
const config = require('./config/app');
const models = require('./db/models');
const DbInit = require('./db/DbInit');
const logger = require('./logger');

const init = async () => {
  try {
    mongoose.connect(config.db);
    await models.sequelize.sync(); // DO NOT TOUCH!
    await www.listen(config.port);
    await DbInit();
    logger.info('app listening on port ' + config.port);
  } catch (e) {
    logger.error(e.stack);
  }
};

init();