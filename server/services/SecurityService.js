const ErrorMessages = require('../models/constants/ErrorMessages');
const UserDao = require('../dao/UserDao');

const checkUserType = async (userId, type) => {
  const user = await UserDao.getBySid(userId);
  if (user && user.type.name === type) {
    return user;
  }
  throw new Error(ErrorMessages.UNAUTHORIZED);
};

module.exports = {
  checkUserType
};