const calculateDancerFee = (performancesAmount, competition) => {
  // KLASYCZNA WYCENA NP. TRENDY:
  const cost = {
    first: 25,
    next: 10
  };

  // WYCENA DLA TOP:
  if (competition.name.indexOf('TOP') > -1) {
    cost.first = 25;
    cost.next = 15;
  }

  // WYCENA DLA GOLD CONTEST:
  else if (competition.name.indexOf('GOLD') > -1) {
    cost.first = 20;
    cost.next = 10;
  }

  // WYCENA DLA SZKOŁY POEZJA:
  else if (competition.sid.indexOf('beb4994ae87e7a37f4fe4784fdbc6f26') > -1) {
    cost.first = 30;
    cost.next = 30;
  }

  // WYCENA DLA OLSZTYNEK:
  else if (competition.sid.indexOf('1b673477610e1ac07a54a0230cf19dfe') > -1) {
    cost.first = 25;
    cost.next = 15;
  }

  return cost.first + (performancesAmount - 1) * cost.next;
};

const calculateSingleDancerFee = (competition) => {
  // PCEIK - POJEDYNCZE WEJŚCIE DLA TANCERZA
  return 25;
};

const getTypeFee = (performance, competition) => {
  const dancersAmount = performance.dancers.length;
  if (competition.name.indexOf('MAGIA TAŃCA') > -1) {
    // WYCENA DLA MAGIA TANCA:
    switch (dancersAmount) {
      case 1:
        return 40;
      case 2:
        return 60;
      case 3:
        return 80;
      default:
        return 20 * dancersAmount;
    }
  } else if (competition.name.indexOf('PASJA TAŃCA') > -1) {
    // WYCENA DLA PASJI TAŃCA:
    switch (dancersAmount) {
      case 1:
        return 25;
      case 2:
        return 50;
      default:
        return 120;
    }
  } else if (competition.name.indexOf('NA SCENIE') > -1) {
    // WYCENA DLA NA SCENIE:
    if (dancersAmount === 1) {
      return 40;
    } else {
      return 20 * dancersAmount;
    }
  } else {
    if (dancersAmount === 1) {
      return 40;
    } else {
      return 20 * dancersAmount;
    }
  }
};

const calculateTypeFees = (performances = [], types = [], competition) => {
  const typeMap = {};
  types.forEach((type) => {
    typeMap[type.name] = {
      amount: 0,
      fee: 0,
      type: type.name
    };
  });

  performances.forEach((performance) => {
    const old = typeMap[performance.type];
    const amount = old.amount;
    const fee = old.fee;

    typeMap[performance.type].amount = amount + 1;
    typeMap[performance.type].fee = fee + getTypeFee(performance, competition);
  });

  return Object.keys(typeMap).map((typeKey => {
    const type = typeMap[typeKey];
    return {
      type: typeMap[typeKey].type,
      fee: type.fee,
      performanceCount: type.amount
    }
  }));
};

module.exports = {
  calculateDancerFee,
  calculateTypeFees,
  calculateSingleDancerFee,
  getTypeFee
};
