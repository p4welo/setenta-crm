module.exports = {
  CompetitionEmailService: require('./email/CompetitionEmailService'),
  SetentaEmailService: require('./email/SetentaEmailService'),
  EmailService: require('./EmailService'),
  SlackService: require('./SlackService'),
  NotificationService: require('./NotificationService'),
  InfaktInvoiceService: require('./invoice/InfaktInvoiceService')
};