const mongoose = require('mongoose');
const models = require('../db/models');

module.exports = {
  getObjectById: (name) => {
    return (request, response, next, value) => {
      const ObjectModel = mongoose.model(name);
      ObjectModel.findById(value).exec()
          .then((result) => {
            request[name] = result;
            next();
          })
          .catch((error) => {
            response.status(404).send(`Invalid ${name} ID`);
          });
    };
  },

  getBySid: (name) => {
    return async (request, response, next, value) => {
      try {
        const result = await models[name].findOne({ where: { sid: value } });
        request.db = {
          ...request.db,
          [name]: result.dataValues
        };
      }
      catch (error) {
        response.status(404).send(`Invalid ${name} ID`);
      }

      next();
    };
  }
};