const { getSchoolActivePerformances } = require('./PerformanceService');
const { countBy, toLower, trim } = require('ramda');
const DataObjectStates = require('../../models/constants/DataObjectStates');
const Dancer = require('../../models/competition/Dancer');

const findDancersBySchool = (school) => Dancer.find({
  school,
  objectState: { $ne: DataObjectStates.DELETED }
}).exec();

const groupByFirstAndLastName = (dancers) => {
  const grouppedDancers = countBy((dancer) => {
    return toLower(trim(dancer.toObject().firstName) + trim(dancer.toObject().lastName))
  }, dancers);
  return Object.keys(grouppedDancers).map((key) => {
    const value = grouppedDancers[key];
    return {
      name: key,
      amount: value
    }
  });
};

const groupDancers = (performances) => {
  const dancers = [];
  performances.forEach((performance) => {
    dancers.push(...performance.dancers)
  });
  return groupByFirstAndLastName(dancers);
};

const getSchoolGroupedDancers = async (school) => {
  const performances = await getSchoolActivePerformances(school);
  return groupDancers(performances);
};

module.exports = {
  findDancersBySchool,
  getSchoolGroupedDancers
};