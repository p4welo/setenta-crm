const Schedule = require('../../models/competition/Schedule');

function getScheduleBlocks() {
  return Schedule.find({}).sort({order: 1}).exec();
}

module.exports = {
  getScheduleBlocks
};