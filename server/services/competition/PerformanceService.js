const { isNotDeleted } = require('../../utils/DataObjectUtils');
const { groupBy, keys, head } = require('ramda');

const Performance = require('../../models/competition/Performance');

const getGroupedPerformances = async () => {
  const performances = await getActivePerformances();
  const bySchool = (performance) => {
    return performance.school._id;
  };
  const grouppedBySchool = groupBy(bySchool, performances);
  return keys(grouppedBySchool).map((key) => {
    const schoolPerformances = grouppedBySchool[key];
    return {
      school: head(schoolPerformances).school,
      performances: schoolPerformances
    }
  })
};

const getSchoolActivePerformances = async (school) => {
  return await Performance.find({
    school,
    ...isNotDeleted
  })
      .populate('dancers')
      .exec();
};

const getActivePerformances = async () => {
  return await Performance.find(isNotDeleted)
      .populate('school')
      .populate('dancers')
      .exec();
};

module.exports = {
  getGroupedPerformances,
  getActivePerformances,
  getSchoolActivePerformances
};