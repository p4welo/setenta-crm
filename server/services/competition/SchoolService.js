const School = require('../../models/competition/School');

const findSchoolByUserId = (userId) => School.findOne({ user: userId }).exec();

module.exports = {
  findSchoolByUserId
};