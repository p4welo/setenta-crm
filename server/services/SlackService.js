const { IncomingWebhook } = require('@slack/client');

const webhook = new IncomingWebhook('https://hooks.slack.com/services/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');
const kiwiWebhook = new IncomingWebhook('https://hooks.slack.com/services/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');

class SlackService {

  loginSuccess(user) {
    const slackHook = user.isAdminType() ? webhook : kiwiWebhook;

    slackHook.send({
      attachments: [
        {
          text: `${user.email} - Login`
        }
      ]
    });
  }

  success(text) {
    this._sendKiwiAttachment({
      color: "good",
      text
    });
  }

  warning(text) {
    this._sendKiwiAttachment({
      color: "warning",
      text
    });
  }

  danger(text) {
    this._sendKiwiAttachment({
      color: "danger",
      text
    });
  }

  courseRegistration(customer) {
    const {firstName, lastName, course} = customer;
    const days = ['poniedziałek', 'wtorek', 'środę', 'czwartek', 'piątek', 'sobotę', 'niedzielę'];
    const formatCourse = ({name, day, timeFrom}) =>
        `${name} w ${days[day]} o ${timeFrom}`;

    webhook.send({
      "attachments": [
        {
          "color": "good",
          "title": ":boom: Nowy zapis na kurs :boom:",
          "text": `${firstName} ${lastName} zapisał się na zajęcia ${formatCourse(course)}.`
        }
      ]
    });
  }

  _sendKiwiAttachment(attachment) {
    kiwiWebhook.send({
      attachments: [attachment]
    });
  }
}
module.exports = new SlackService();