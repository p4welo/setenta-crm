const SMSAPI = require('smsapi');
const config = require('../config/app');

class SmsService {
  constructor() {
    this.smsapi = new SMSAPI({
      server: config.sms.apiServer
    });
  }

  authenticate() {
    return this.smsapi.authentication
        .login(config.sms.username, config.sms.password);
  }

  sendMessage(content) {
    return () => this.smsapi.message
        .sms()
        .to(config.sms.recipient)
        .from(config.sms.sender)
        .message(content)
        .execute();
  }

  customerCourseRegistrationNotification(customer) {
    const {firstName, lastName, phone, course} = customer;
    const days = ['poniedziałek', 'wtorek', 'środę', 'czwartek', 'piątek', 'sobotę', 'niedzielę'];

    const formatCourse = ({name, day, timeFrom}) =>
        `${name} w ${days[day]} o ${timeFrom}`;

    this.authenticate()
        .then(this.sendMessage(
            `SETENTA - Zapisy z poziomu strony: ${firstName} ${lastName} zapisał się na zajęcia ${formatCourse(course)}. Telefon: ${phone}. Pozdrawiam, Mąż :)`
        ))
        .then(console.log)
        .catch(console.log);
    return Promise.resolve(customer);
  }
}
module.exports = new SmsService();
