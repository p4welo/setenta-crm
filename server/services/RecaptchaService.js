const config = require('../config/app');
const reCAPTCHA = require('recaptcha2');

class RecaptchaService {
  constructor() {
    this.recaptcha = new reCAPTCHA(config.recaptcha);
  }

  validate(key) {
    return this.recaptcha.validate(key);
  }
}
module.exports = new RecaptchaService();
