const axios = require('axios');

class InfaktInvoiceService {
  constructor() {

  }

  async createInvoice(authKey, { bankAccount, name, nip, street, zip, city, title, netPrice, taxPrice }) {
    const invoice = await axios.post(
        'https://api.infakt.pl/v3/invoices.json',
        // `{"invoice":{"payment_method":"transfer", "bank_account": "70102010130000010200026526","client_company_name": "Szkoła tańca Setenta", "client_tax_code": "6972131230","client_street": "Sienkiewicza 6a","client_city": "Wrocław","client_post_code": "50-335", "services":[{"name": "Przykładowa Usługa", "net_price":12300, "tax_symbol": 0 }]}}`,
        `{"invoice":{"payment_method":"transfer", "bank_account": "${bankAccount}","client_company_name": "${name}", "client_tax_code": "${nip}","client_street": "${street}","client_city": "${city}","client_post_code": "${zip}", "services":[{"name": "${title}", "net_price":${netPrice}, "tax_symbol": ${taxPrice} }]}}`,
        {
          headers: {
            'X-inFakt-ApiKey': authKey,
            'Content-Type': 'application/json'
          }
        }
    );
    return invoice.data;
  }

  async getInvoiceAsPdf(authKey, extId) {
    const pdf = await axios.get(
        `https://api.infakt.pl/v3/invoices/${extId}/pdf.json?document_type=original&locale=pl`,
        {
          headers: {
            'X-inFakt-ApiKey': authKey
          },
          responseType: 'stream'
        },
    );
    return pdf;
  }

  async deleteInvoice(authKey, extId) {
    const pdf = await axios.delete(
        `https://api.infakt.pl/v3/invoices/${extId}.json`,
        {
          headers: {
            'X-inFakt-ApiKey': authKey
          }
        },
    );
    return pdf;
  }
}

module.exports = new InfaktInvoiceService();