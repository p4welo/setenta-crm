const appConfig = rootRequire('config/app');
const EmailService = require('./EmailService');

class CompetitionEmailService extends EmailService {
  constructor(props) {
    super(props);
  }

  // SCHOOL
  afterRegistration(user) {
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: 'Aktywacja konta',
      to: user.email
    };
    const template = this.getCompetitionTemplate('afterRegistration');
    this.compileAndSend(config, template, {
      link: `${appConfig.frontendUrl}/#/activate/${user.sid}`,
      user
    }, user);
  }

  afterActivation(user) {
    // witamy w kartazgloszen.pl
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: 'Witamy w kartazgloszen.pl',
      to: user.email
    };
    const template = this.getCompetitionTemplate('afterActivation');
    this.compileAndSend(config, template, {
      user
    }, user);
  }

  afterPasswordReset(user) {
    // kliknij link aby zresetowac haslo
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: 'Zmiana hasła użytkownika',
      to: user.email
    };
    const template = this.getCompetitionTemplate('afterPasswordReset');
    this.compileAndSend(config, template, {
      link: `${appConfig.frontendUrl}/#/password/new/${user.resetHash}`,
      user
    }, user);
  }

  afterFirstPerformanceCreated(user, competition) {
    // otrzymalismy Twoje pierwsze zgloszenie
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: `${competition.name} - Przyjęto nowe zgłoszenie`,
      to: user.email
    };
    const template = this.getCompetitionTemplate('afterFirstPerformanceCreated');
    this.compileAndSend(config, template, {
      user,
      competition
    }, user);
  }

  afterPaymentCreated(user, competition, payment) {
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: `${competition.name} - Otrzymano wpłatę`,
      to: user.email
    };
    const template = this.getCompetitionTemplate('afterPaymentCreated');
    this.compileAndSend(config, template, {
      user,
      competition,
      payment
    }, user);
  }

  // ORGANIZER
  newCompetitionUserRegistration(organizer, school) {
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: `Nowy uczestnik Twojego turnieju`,
      to: organizer.email
    };
    const template = this.getCompetitionTemplate('newCompetitionUserRegistration');
    this.compileAndSend(config, template, {
      organizer,
      school
    }, organizer);
  }

  cronWeeklyOrganizerReport() {
    // TODO: create weekly report
    const config = {
      ...EmailService.CONFIG.KARTA_ZGLOSZEN,
      subject: 'test report'
    };
    const template = this.getCompetitionTemplate('cronWeeklyOrganizerReport');
    this.compileAndSend(config, template, {});
  }


  getCompetitionTemplate(filename) {
    return this.getTemplate(`competition/${filename}.hbs`)
  }
}

module.exports = new CompetitionEmailService();