const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

const appConfig = rootRequire('config/app');
const { EmailDao } = rootRequire('dao');
const { EmailMapper } = rootRequire('dao/mappers');

module.exports = class EmailService {
  static get CONFIG() {
    return {
      KARTA_ZGLOSZEN: {
        from: '"Karta zgłoszeń" <kontakt@kartazgloszen.pl>',
        to: 'p4welo@gmail.com',
        subject: 'Karta zgłoszeń'
      },
      SETENTA: {
        from: '"Szkoła tańca Setenta" <kontakt@setenta.wroclaw.pl>',
        to: 'p4welo@gmail.com',
        subject: 'Szkoła tańca Setenta'
      }
    }
  }

  constructor() {
    this.transporter = nodemailer.createTransport(appConfig.email);
  }

  getTemplate(filePath) {
    const source = fs.readFileSync(path.join(__dirname, filePath), 'utf8');
    return handlebars.compile(source);
  }

  async compileAndSend(config, template, data, user) {
    const emailEntity = await EmailDao.create(EmailMapper.toDatabase(config.from, config.to, config.subject, '', user));

    const email = {
      ...config,
      html: template({
        ...data,
        followUrl: `${appConfig.baseUrl}/api/v2/emails/${emailEntity.sid}/read`
      })
    };

    this.transporter.sendMail(email, (error, info) => {
      if (error) {
        return Promise.reject(error);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
    });
    return Promise.resolve();
  }
};