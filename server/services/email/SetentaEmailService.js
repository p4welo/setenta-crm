const config = rootRequire('config/app');
const EmailService = require('./EmailService');

class SetentaEmailService extends EmailService {
  constructor(props) {
    super(props);
  }

  test() {
    const config = {
      ...EmailService.CONFIG.SETENTA,
      subject: 'setenta test report'
    };
    const template = this.getCompetitionTemplate('cronWeeklyOrganizerReport');
    this.compileAndSend(config, template, {});
  }

  getCompetitionTemplate(filename) {
    return this.getTemplate(`competition/${filename}.hbs`)
  }
}

module.exports = new SetentaEmailService();