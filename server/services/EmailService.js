const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');
const config = require('../config/app');

const EMAIL_ACCOUNT = {
  ACTIVE_PORTAL: {
    from: '"Szkoła tańca Setenta" <kontakt@setenta.wroclaw.pl>',
    to: 'p4welo@gmail.com',
    subject: 'ACTIVE PORTAL'
  },

  SETENTA: {
    from: '"Szkoła tańca Setenta" <kontakt@setenta.wroclaw.pl>',
    to: 'p4welo@gmail.com',
    subject: 'Szkoła tańca Setenta'
  },

  KARTA_ZGLOSZEN: {
    from: '"Karta zgłoszeń" <kontakt@kartazgloszen.pl>',
    to: 'p4welo@gmail.com',
    subject: 'Karta zgłoszeń'
  }
};

class OldEmailService {
  constructor() {
    this.transporter = nodemailer.createTransport(config.email);
    this.resetOptions(EMAIL_ACCOUNT.ACTIVE_PORTAL);
  }

  resetOptions(accountConfig) {
    this.mailOptions = accountConfig;
  }

  sendEmail(args) {
    this.transporter.sendMail(this.mailOptions, (error, info) => {
      if (error) {
        return Promise.reject(error);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
    });
    return Promise.resolve(args);
  }

  sendResetPasswordEmail(user) {
    this.resetOptions(EMAIL_ACCOUNT.KARTA_ZGLOSZEN);

    const source = fs.readFileSync(path.join(__dirname, 'templates/ResetPassword.hbs'), 'utf8');
    const template = handlebars.compile(source);
    this.mailOptions.html = template({
      link: `${config.baseUrl}/#!/resetPassword/${user.resetHash}`,
      user,
    });
    const sender = 'Karta zgłoszeń';
    this.mailOptions.subject = `Zmiana hasła dostępu - ${sender}`;
    this.mailOptions.to = user.email;

    return this.sendEmail(user);
  }

  // SETENTA - rejestracja na kurs (mail do klienta)
  sendCourseRegistrationConfirmation(customer) {
    this.resetOptions(EMAIL_ACCOUNT.SETENTA);
    const { course, email } = customer;
    const { day, timeFrom, _id } = course;
    const days = ['Poniedziałek', 'Wtorek', 'Środę', 'Czwartek', 'Piątek', 'Sobotę', 'Niedzielę'];

    const source = fs.readFileSync(path.join(__dirname, 'templates/CourseRegistration.hbs'), 'utf8');
    const template = handlebars.compile(source);
    this.mailOptions.html = template({
      course: {
        name: course.name,
        day: days[day],
        time: timeFrom
      },
      id: _id
    });
    this.mailOptions.subject = `Potwierdzenie zapisów`;
    this.mailOptions.to = email;

    return this.sendEmail(customer);
  }

  sendActivationEmail(user) {
    return this.sendEmail(user);
  }

  customerConfirmationAfterCourseRegistration(customer) {
    this.resetOptions(EMAIL_ACCOUNT.SETENTA);
    const {firstName, lastName, email, phone, course} = customer;
    const {day, timeFrom, _id} = course;
    const days = ['Poniedziałek', 'Wtorek', 'Środę', 'Czwartek', 'Piątek', 'Sobotę', 'Niedzielę'];
    this.mailOptions.to = email;
    this.mailOptions.subject = "Potwierdzenie zapisów";
    this.mailOptions.html = `
      <p>Witaj ${firstName} ${lastName}!</p>
      <p>Przesyłamy podsumowanie zapisów na zajęcia taneczne:</p>
      <p>
        <span>Zajęcia:</span>
        <strong>${course.name}</strong>
      </p>
      <p>
        <span>Termin:</span>
        <strong>${days[day]} ${timeFrom}</strong>
      </p>
      <p>W momencie gdy grupa się zbierze otrzymasz smsa z potwierdzeniem (numer podany podczas rejestracji - ${phone})</p>
      <p>Wszelkie zmiany/uwagi prosimy zgłaszać w odpowiedzi na tego maila.</p>
      <p>Do zobaczenia! :)</p>
      <p>
        <strong>Szkoła tańca Setenta</strong>
      </p>
      <p>
        <small>Id rezerwacji: ${_id}</small>
      </p>
    `;
    return this.sendEmail(customer);
  }

  customerConfirmationAfterAdvertisementPublish(advertisement) {
    this.resetOptions();
    const {advertiser, contact} = advertisement;

    this.mailOptions.to = contact.email;
    this.mailOptions.subject = "Wysłanie zgłoszenia";
    this.mailOptions.html = `
      <p>Witaj ${advertiser.firstName} ${advertiser.lastName}!</p>
      <p>Otrzymaliśmy od Ciebie zgłoszenie poszukiwanego partnera do tańca.</p>
      <p>Bierzemy się do działania i rozpoczynamy poszukiwania! :)</p>
      <p>Oczekuj kontaktu z naszej strony (numer podany podczas zgłoszenia - ${contact.mobile})</p>
      <p>Wszelkie zmiany/uwagi prosimy zgłaszać w odpowiedzi na tego maila.</p>
      <p>Do zobaczenia! :)</p>
      <p>
        <strong>Szkoła tańca Setenta</strong>
      </p>
    `;
    return this.sendEmail(advertisement);
  }

  // ACTIVE PORTAL - powiadomienie po zgłoszeniu zapotrzebowania na partnera do tańca
  clientNotificationAfterAdvertisementPublish(advertisement) {
    this.resetOptions();

    this.mailOptions.html = `
      <p>PARTNER DO TAŃCA:</p>
      <p>${advertisement}</p>
    `;
    return this.sendEmail(advertisement);
  }

  // ACTIVE PORTAL - powiadomienie po zapisie klienta na kurs
  clientNotificationAfterCourseRegistration(customer) {
    this.resetOptions(EMAIL_ACCOUNT.ACTIVE_PORTAL);
    const {firstName, lastName, phone, course} = customer;
    const days = ['poniedziałek', 'wtorek', 'środę', 'czwartek', 'piątek', 'sobotę', 'niedzielę'];

    const formatCourse = ({name, day, timeFrom}) => `${name} w ${days[day]} o ${timeFrom}`;
    this.mailOptions.html = `
      <p>ZAPISY: ${firstName} ${lastName} zapisał się na zajęcia ${formatCourse(course)}. Telefon: ${phone}.</p>
      <p>${customer}</p>
    `;
    return this.sendEmail(customer);
  }

  // COMPETITION
  sendCompetitionRegistrationEmail(user) {
    this.resetOptions(EMAIL_ACCOUNT.KARTA_ZGLOSZEN);

    const source = fs.readFileSync(path.join(__dirname, 'templates/CompetitionRegistration.hbs'), 'utf8');
    const template = handlebars.compile(source);
    this.mailOptions.html = template({
      link: `${config.frontendUrl}/#/activate/${user.sid}`,
      user,
    });
    const sender = 'Karta zgłoszeń';
    this.mailOptions.subject = `Witamy - ${sender}`;
    this.mailOptions.to = user.email;

    return this.sendEmail(user);
  }
}
module.exports = new OldEmailService();
