const User = require('../models/User');

const getUserById = (id) => User.findById(id).exec();

module.exports = {
  getUserById
};