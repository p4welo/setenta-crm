const SlackService = require('./SlackService');

class NotificationService {
  invoiceDataUpdateSuccess(user) {
    SlackService.success(`${user.login} - Aktualizacja danych do faktury`);
  }

  registrationSuccess(user) {
    SlackService.success(`${user.login} - Rejestracja :boom:`);
  }

  activationSuccess(user) {
    SlackService.success(`${user.login} - Aktywacja :sunny:`);
  }

  dancerCreatedSuccess(user) {
    SlackService.success(`${user.login} - Utworzenie pierwszego tancerza`);
  }

  performanceCreatedSuccess(user, competition) {
    SlackService.success(`${user.login} - Zgłoszenie pierwszego występu na turniej - ${competition.name}`);
  }
}

module.exports = new NotificationService();