const jwt = require('jwt-simple');
const crypto = require('crypto');
const Config = require('../config/app');

class AuthenticationService {

  constructor() {
    this.authConfig = Config.auth;
  }

  getToken(user, field = '_id') {
    const { email, login, type, firstName, lastName, name, city } = user;
    const payload = { id: user[field], user: {
        email,
        login,
        firstName,
        lastName,
        name,
        city,
        type: type ? type.name : undefined
      }};
    return jwt.encode(payload, this.authConfig.secretKey);
  }

  encrypt(word) {
    return crypto
        .createHash(this.authConfig.hashFunction)
        .update(word)
        .digest(this.authConfig.digestFunction);
  }
}

module.exports = new AuthenticationService();
