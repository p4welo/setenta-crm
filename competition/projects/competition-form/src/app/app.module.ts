import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompetitionFormState } from './store/competition-form.state';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
      NgxsModule.forRoot([
           CompetitionFormState
      ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
