import { State } from '@ngxs/store';

export interface CompetitionFormStateModel {
  sid: string;
}

@State<CompetitionFormStateModel>({
  name: 'competition-form',
  defaults: {
    sid: ''
  }
})
export class CompetitionFormState {

}