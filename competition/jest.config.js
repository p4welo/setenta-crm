module.exports = {
  verbose: false,
  preset: "jest-preset-angular",
  setupTestFrameworkScriptFile: "<rootDir>/src/setupJest.ts",
  testEnvironment: "jest-environment-jsdom-global",
  bail: false,
  globals: {
    "ts-jest": {
      "tsConfigFile": "src/tsconfig.spec.json"
    },
    "__TRANSFORM_HTML__": true
  },
  "moduleNameMapper": {
    "@local/(.*)": "<rootDir>/src/$1"
  },
  transformIgnorePatterns: [
    "node_modules/(?!@ngrx|ramda/es|@angular/common)"
  ],
  coverageDirectory: "coverage",
  collectCoverageFrom: [
    "src/**/*.ts",
    "!src/**/*.module.ts",
    "!src/**/*.routes.ts",
    "!src/**/*.mocks.ts",
    "!src/**/*.mock.ts",
    "!src/**/*.model.ts",
    "!src/**/*animation.ts",
    "!src/**/index.ts",
    "!src/jestGlobalMocks.ts",
    "!src/setupJest.ts",
    "!src/polyfills.ts",
    "!src/**/*.d.ts",
    "!**/node_modules/**"
  ],
  coverageReporters: [
    "json",
    "lcov",
    "cobertura"
  ],
  coverageThreshold: {
    global: {
      statements: 60,
      branches: 45,
      functions: 50,
      lines: 60
    }
  }
};
