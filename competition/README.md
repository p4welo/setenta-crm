Ostatnia aktualizacja: 24 listopada 2019

# API
## Adres serwera
Serwer znajduje się pod adresem `https://p4welo.usermd.net` gdzie powinny być kierowane wszystkie
 zapytania
 

## Logowanie
Logowanie należy wykonać w celu uzyskania tokenu autoryzacyjnego, dzięki któremu uzyskamy dostęp 
do zasobów danego użytkownika
```
POST /api/v2/auth/login
```

### Parametry
| parametr   | typ    | wymagany |
|------------|--------|----------|
| `login`    | string | tak      |
| `password` | string | tak      |


### Odpowiedź
`token` token JWT który dołączamy do każdego zapytania

### Przykładowe użycie
```
curl 'https://p4welo.usermd.net/api/v2/auth/login' \
-H 'Content-Type: application/json' \
-X POST \
-D '{"login":"user@email.com","password":"haslo"}'
```
## Moje turnieje
Pobieranie listy turniejów, które są organizowane przez bieżącego użytkownika

```
GET /api/v2/admin/competitions
```

### Parametry
Brak parametrów

### Odpowiedź
`Competition[]`


### Przykładowe użycie
```
curl 'https://p4welo.usermd.net/api/v2/admin/competitions' \
-H 'accept: application/json' \
-H 'authorization: JWT {token}' 
```

## Listy startowe
Pobieranie list startowych danego turnieju (identyfikowanego po polu 
`sid`)

```
GET /api/v2/competitions/{competitionSid}/starting-list
```

### Parametry
Brak parametrów

### Odpowiedź
Odpowiedź przychodzi w postaci drzewa pogrupowanego względem dnia
(`dayId`), rozdziału (`chapterId`) i sekcji (`sectionId`), które
przyjmują wartości liczbowe. W praktyce - rozdział to blok występów
pomiędzy przerwami na rozdanie nagród. Sekcja to grupa występów, która
często składa się z kilku kategorii (jeśli zostały połączone np. z
powodu małej ilości występów).

```
{
  [dayId]: {
    [chapterId]: {
      [sectionId]: CompetitionSection
    }
  }
}
```

### Przykładowe użycie
```
curl 'https://p4welo.usermd.net/api/v2/competitions/{competitionSid}/starting-list' \
-H 'accept: application/json' \
-H 'authorization: JWT {token}' 
```

# Model danych
## Competition

| parametr             | typ     | opis                                                     |
|----------------------|---------|----------------------------------------------------------|
| `ageLevelTolerance`  | number  | procentowa tolerancja przy wyliczaniu kategorii wiekowej |
| `city`               | string  | miejscowość                                              |
| `end`                | date    | koniec turnieju                                          |
| `mapUrl`             | string  | link do mapy                                             |
| `name`               | string  | nazwa turnieju                                           |
| `paymentModel`       | string  | model wyliczania płatności                               |
| `phone`              | string  | telefon do organizatora                                  |
| `place`              | string  | miejsce zawodów (nazwa obiektu)                          |
| `registrationEnd`    | date    | data graniczna, do kiedy są otwarte zapisy               |
| `registrationOpened` | boolean | flaga określająca czy zapisy są otwarte                  |
| `regulations`        | string  | link do regulaminu                                       |
| `resultPublished`    | boolean | flaga określająca czy wyniki są dostępne publicznie      |
| `sid`                | string  | secured ID                                               |
| `start`              | date    | start turnieju                                           |
| `street`             | string  | ulica                                                    |
| `website`            | string  | adres strony                                             |
| `zip`                | string  | kod pocztowy                                             |

## CompetitionSection
Sekcja (kategoria) w danym bloku startowym

| parametr       | typ                | opis                                                         |
|----------------|--------------------|--------------------------------------------------------------|
| `blocks`       | CompetitionBlock[] | kategorie wchodzące w skład sekcji                           |
| `chapter`      | number             |                                                              |
| `day`          | number             |                                                              |
| `eliminations` | boolean            | flaga określająca czy dana sekcja posiada bloki eliminacyjne |
| `performances` | Performance[]      | lista występów danej sekcji (kategorii)                      |
| `position`     | number             | numer określający konkretną pozycję w danym bloku startowym  |
| `sid`          | string             | secured ID                                                   |

## CompetitionBlock
Kategoria określająca jednoznacznie występ

| parametr         | typ                | opis                                                         |
|------------------|--------------------|--------------------------------------------------------------|
| `age`            | string             | kategoria wiekowa np. "9-11 lat"                             |
| `experience`     | string             | poziom doświadczenia np. "Debiuty"                           |
| `sid`            | string             | secured ID                                                   |
| `style`          | string             | styl występu np. "Jazz"                                      |
| `type`           | string             | kategoria np. "Mini formacje"                                |

## Performance

## Dancer
