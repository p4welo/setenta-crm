import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompetitionFormRoutingModule } from './competition-form-routing.module';
import { CompetitionFormComponent } from './competition-form.component';


@NgModule({
  declarations: [CompetitionFormComponent],
  imports: [
    CommonModule,
    CompetitionFormRoutingModule
  ]
})
export class CompetitionFormModule { }
