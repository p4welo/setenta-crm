import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompetitionFormComponent } from './competition-form.component';


const routes: Routes = [{
  path: '',
  component: CompetitionFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompetitionFormRoutingModule { }
