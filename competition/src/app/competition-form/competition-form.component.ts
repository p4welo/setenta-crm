import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-competition-form',
  templateUrl: './competition-form.component.html',
  styleUrls: ['./competition-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
