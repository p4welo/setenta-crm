import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
  getInvoiceData,
  isInvoiceDataLoading
} from '../store/selectors';
import { InvoiceData } from '../../model';

import { InvoiceDataFetchStartAction } from '../store/actions';

@Injectable()
export class HomeService {

  public invoiceData$: Observable<InvoiceData> = this.store$.select(getInvoiceData);
  public invoiceDataLoading$: Observable<boolean> = this.store$.select(isInvoiceDataLoading);

  constructor(private store$: Store<any>) {}

  public dispatchInvoiceDataFetchStart(): void {
    this.store$.dispatch(new InvoiceDataFetchStartAction());
  }
}