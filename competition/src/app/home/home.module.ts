import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminDancersCountComponent } from '@competition/home/components/admin-dancers-count';
import { AdminDaysLeftComponent } from '@competition/home/components/admin-days-left';
import { HomePerformancesChartComponent } from '@competition/home/components/admin-performances-chart';
import { AdminPerformancesCountComponent } from '@competition/home/components/admin-performances-count';
import { AdminRecentSchoolsComponent } from '@competition/home/components/admin-recent-schools';
import { AdminStoreModule } from '@competition/modules/admin-store/admin-store.module';
import { AuthModule } from '@competition/modules/auth';
import { DancerStoreModule } from '@competition/modules/dancer-store';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';

import { HomeService } from '@competition/home/services';
import { CoreModule } from '@competition/core';

import { HomeEffects } from './store/effects';
import { reducers } from './store/reducers';
import { routes } from './home.routes';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('home', reducers),
    EffectsModule.forFeature([HomeEffects]),
    CoreModule,
    DancerStoreModule,
    AuthModule,
    TranslateModule,
    AdminStoreModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HomeComponent,
    HomePerformancesChartComponent,
    AdminPerformancesCountComponent,
    AdminDaysLeftComponent,
    AdminDancersCountComponent,
    AdminRecentSchoolsComponent
  ],
  providers: [ HomeService ]
})
export class HomeModule {
  public static routes = routes;
}
