import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AdminService } from '@competition/modules/admin-store/services';
import { AuthService } from '@competition/modules/auth/services';
import { DancersFetchStart } from '@competition/modules/dancer-store/actions';
import { DancerService } from '@competition/modules/dancer-store/services';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AdminCompetitionService, FinanceService, NavigationService } from '../core/services';

import { Competition, Dancer_, InvoiceData } from '../model';
import { HomeService } from './services';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

  public dancersInitialized$: Observable<boolean> = this.dancerService.initialized$;
  public dancers$: Observable<Dancer_[]> = this.dancerService.dancers$;

  public invoiceDataLoading$: Observable<boolean> = this.homeService.invoiceDataLoading$;
  public invoiceData$: Observable<InvoiceData> = this.homeService.invoiceData$;

  public adminCompetitions$: Observable<Competition[]> = this.adminService.upcomingCompetitions$;

  public loadingDetails: boolean = false;

  // public competitions: Competition[];
  // public chartValues: number[];
  // public chartLabels: string[];

  constructor(
      private homeService: HomeService,
      private financeService: FinanceService,
      private adminCompetitionService: AdminCompetitionService,
      private authService: AuthService,
      private adminService: AdminService,
      private navigationService: NavigationService,
      private dancerService: DancerService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    if (this.isAdminUser) {
      this.adminService.dispatchCompetitionsFetchStart();
    }

    this.store$.dispatch(new DancersFetchStart());
    this.homeService.dispatchInvoiceDataFetchStart();
  }

  public get isAdminUser(): boolean {
    return this.authService.isAdminUser();
  }

  public goToDetails(competition: Competition): void {
    this.loadingDetails = true;
    this.navigationService.navigateToAdminCompetitionDetails(competition);
  }

  // private loadMyCompetitions(): void {
  //   this.adminCompetitionService.getMyList().subscribe((competitions: Competition[]) => {
  //     this.competitions = competitions;
  //     if (competitions.length > 0) {
  //       this.loadChartData(competitions[0]);
  //     }
  //   });
  // }

  // private loadChartData(competition: Competition): void {
  //   this.adminCompetitionService.getPerformancesByDayReport(competition.sid).subscribe((data) => {
  //     this.chartLabels = data.map(d => d.date);
  //     this.chartValues = data.map(d => d.amount);
  //   })
  // }
}
