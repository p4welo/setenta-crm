import { HomeComponent } from './home.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

describe('Component: PropertyDetailsComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let debugElement: DebugElement;

  const config = {
    declarations: [],
    imports: [],
    providers: []
  };

  const setup = () => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
  };

  beforeEach(async(() => {
    TestBed
        .configureTestingModule(config)
        .compileComponents()
        .then(setup);
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });
});