import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit, ViewRef
} from '@angular/core';
import { Competition } from '@competition/model';
import * as dayjs from 'dayjs';

@Component({
  selector: 'home-admin-days-left',
  templateUrl: './admin-days-left.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDaysLeftComponent implements OnInit {
  @Input() public competition: Competition;

  public loading: boolean = true;
  public days: number = 0;

  constructor(private cd: ChangeDetectorRef) {
  }

  public ngOnInit(): void {
    setTimeout(() => {
      this.days = dayjs(this.competition.start).diff(dayjs(), 'day') + 1;
      this.loading = false;
      if (this.cd && !(this.cd as ViewRef).destroyed) {
        this.cd.detectChanges();
      }
    }, 200);
  }
}