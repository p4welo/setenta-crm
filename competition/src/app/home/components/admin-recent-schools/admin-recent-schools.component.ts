import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import {
  adminGetCompetitionPerformances,
} from '@competition/core/utils';
import { Competition } from '@competition/model';

@Component({
  selector: 'home-admin-recent-schools',
  templateUrl: './admin-recent-schools.component.html'
})
export class AdminRecentSchoolsComponent implements OnInit {
  @Input() public competition: Competition;

  public loading: boolean = true;

  constructor(
      private http: HttpClient
  ) {}

  public ngOnInit(): void {
    this.http.get(adminGetCompetitionPerformances(this.competition.sid))
        .subscribe((performances) => {
          console.warn(performances);
        })
  }
}