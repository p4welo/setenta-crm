import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit, ViewRef
} from '@angular/core';
import { adminGetCompetitionPerformancesCount } from '@competition/core/utils';
import { Competition } from '@competition/model';

export interface PerformancesCount {
  all: number;
  today: number;
  yesterday: number;
}

@Component({
  selector: 'home-admin-performances-count',
  templateUrl: './admin-performances-count.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPerformancesCountComponent implements OnInit {
  @Input() public competition: Competition;

  public loading: boolean = true;
  public count: number;
  public today: number;

  constructor(
      private http: HttpClient,
      private cd: ChangeDetectorRef
  ) {
  }

  public ngOnInit(): void {
    this.http.get(adminGetCompetitionPerformancesCount(this.competition.sid))
        .subscribe((result: PerformancesCount) => {
          this.count = result.all || 0;
          this.today = result.today || 0;
          this.loading = false;
          if (this.cd && !(this.cd as ViewRef).destroyed) {
            this.cd.detectChanges();
          }
        })
  }
}
