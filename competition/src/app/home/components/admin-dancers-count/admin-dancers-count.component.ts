import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit, ViewRef
} from '@angular/core';
import { adminGetCompetitionDancersCount, } from '@competition/core/utils';
import { PerformancesCount } from '@competition/home/components/admin-performances-count';
import { Competition } from '@competition/model';

@Component({
  selector: 'home-admin-dancers-count',
  templateUrl: './admin-dancers-count.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDancersCountComponent implements OnInit {
  @Input() public competition: Competition;

  public loading: boolean = true;
  public count: number;

  constructor(
      private http: HttpClient,
      private cd: ChangeDetectorRef
  ) {
  }

  public ngOnInit(): void {
    this.http.get(adminGetCompetitionDancersCount(this.competition.sid))
        .subscribe((result: PerformancesCount) => {
          this.count = result.all || 0;
          this.loading = false;
          if (this.cd && !(this.cd as ViewRef).destroyed) {
            this.cd.detectChanges();
          }
        })
  }
}