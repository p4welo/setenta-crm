import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {
  ChartModel
} from '@competition/home/components/admin-performances-chart/chart.model';
import { Competition } from '@competition/model';

import { Chart } from 'chart.js';

@Component({
  selector: 'app-performances-chart',
  templateUrl: './home-performances-chart.component.html',
})
export class HomePerformancesChartComponent implements OnInit, OnChanges {

  @Input() public competition: Competition;
  @Input() public labels: string[];
  @Input() public values: number[];

  public chart: ChartModel;

  public ngOnInit(): void {
    this.initChart();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    let needsUpdate = false;
    if (changes.labels && changes.labels.currentValue) {
      this.chart.data.labels = this.labels;
      needsUpdate = true;
    }
    if (changes.values && changes.values.currentValue) {
      this.chart.data.datasets[0].data = this.values;
      needsUpdate = true;
    }
    if (needsUpdate) {
      this.chart.update();
    }
  }

  private initChart(): void {
    this.chart = new Chart('line-chart', {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: '# of Votes',
          data: [],
          backgroundColor: ['rgba(255, 99, 132, 0.2)'],
          borderColor: ['rgba(255, 99, 132, 1)'],
          borderWidth: 2
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}