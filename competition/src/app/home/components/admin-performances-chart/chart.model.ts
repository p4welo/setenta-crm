export interface ChartDataElement {
  date: string,
  amount: number
}

export interface ChartModel {
  data: {
    labels: string[];
    datasets: any[];
  },
  update: any;
}