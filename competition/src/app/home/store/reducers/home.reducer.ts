import { InvoiceData } from '@competition/model';
import { HomeActions, HomeActionTypes } from '../actions';

export interface HomeState {
  invoiceDataLoading: boolean;
  invoiceData: InvoiceData;
}

export const initialState: HomeState = {
  invoiceDataLoading: false,
  invoiceData: undefined
};

export function home(state: HomeState = initialState, action: HomeActions): HomeState {
  switch (action.type) {
    case HomeActionTypes.INVOICE_DATA_FETCH_START:
      return {
        ...state,
        invoiceDataLoading: true
      };
    case HomeActionTypes.INVOICE_DATA_FETCH_SUCCESS:
      return {
        ...state,
        invoiceData: action.payload,
        invoiceDataLoading: false
      };
    case HomeActionTypes.INVOICE_DATA_FETCH_ERROR:
      return {
        ...state,
        invoiceDataLoading: false
      };
    default:
      return state;
  }
}
