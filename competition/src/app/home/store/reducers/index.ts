import { createFeatureSelector } from '@ngrx/store';

import * as fromHome from './home.reducer';

export const reducers = fromHome.home;

export const getHomeState = createFeatureSelector<fromHome.HomeState>('home');
