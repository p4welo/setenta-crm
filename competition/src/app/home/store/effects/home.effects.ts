import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getMyInvoiceDataUrl } from '@competition/core/utils';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  HomeActions,
  HomeActionTypes, InvoiceDataFetchErrorAction, InvoiceDataFetchSuccessAction
} from '@competition/home/store/actions';
import { InvoiceData } from '@competition/model';

@Injectable()
export class HomeEffects {
  constructor(
      private actions$: Actions<Action>,
      private http: HttpClient
  ) {
  }

  @Effect()
  public fetchInvoiceData$: Observable<HomeActions> = this.actions$.pipe(
      ofType(HomeActionTypes.INVOICE_DATA_FETCH_START),
      switchMap(() => this.http.get<InvoiceData>(getMyInvoiceDataUrl()).pipe(
          map((invoiceData: InvoiceData) => new InvoiceDataFetchSuccessAction(invoiceData)),
          catchError(() => of(new InvoiceDataFetchErrorAction())),
      ))
  );
}