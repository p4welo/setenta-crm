import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromHome from '../reducers/home.reducer';

export const getInvoiceData = createSelector(
    fromFeature.getHomeState,
    (state: fromHome.HomeState) => state.invoiceData
);

export const isInvoiceDataLoading = createSelector(
    fromFeature.getHomeState,
    (state: fromHome.HomeState) => state.invoiceDataLoading
);