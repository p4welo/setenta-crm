import { InvoiceData } from '@competition/model';
import { Action } from '@ngrx/store';

export const HomeActionTypes = {
  INVOICE_DATA_FETCH_START: '[Home] invoice data fetch start',
  INVOICE_DATA_FETCH_SUCCESS: '[Home] invoice data fetch success',
  INVOICE_DATA_FETCH_ERROR: '[Home] invoice data fetch error',
  MY_ADMIN_COMPETITIONS_FETCH_START: '[Home] my admin competitions fetch start',
  MY_ADMIN_COMPETITIONS_FETCH_SUCCESS: '[Home] my admin competitions fetch success',
  MY_ADMIN_COMPETITIONS_FETCH_ERROR: '[Home] my admin competitions fetch error',
};

export class InvoiceDataFetchStartAction implements Action {
  public type: string = HomeActionTypes.INVOICE_DATA_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class InvoiceDataFetchSuccessAction implements Action {
  public type: string = HomeActionTypes.INVOICE_DATA_FETCH_SUCCESS;

  constructor(public payload: InvoiceData) {
  }
}

export class InvoiceDataFetchErrorAction implements Action {
  public type: string = HomeActionTypes.INVOICE_DATA_FETCH_ERROR;

  constructor(public payload?: any) {
  }
}

export type HomeActions =
    | InvoiceDataFetchStartAction
    | InvoiceDataFetchSuccessAction
    | InvoiceDataFetchErrorAction
    ;
