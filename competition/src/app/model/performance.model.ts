import { Dancer_ } from './dancer.model';
import { User } from './user.model';

export interface Performance {
  sid?: string;
  name?: string;
  title?: string;
  type?: string;
  style?: string;
  number?: number;
  place?: string;
  ageLevel?: string;
  experience?: string;
  dancers: Dancer_[];
  user?: User;
  audioSid?: string;
  prices?: PerformancePrice[];
  visible?: boolean;
}

export interface PerformancePrice {
  sid: string;
  type: string;
  value: string;
  description: string;
}

export interface PerformanceSong {
  sid: string;
  filename: string;
  url: string;
}

export interface PerformanceBlock {
  style?: string;
  type?: string;
  experience?: string;
  age?: string;
  position?: number; // TODO: remove

  performances?: Performance[] // TODO: remove
}

export interface PerformanceSection {
  sid?: string;
  day: string;
  chapter: string;
  position: number;
  eliminations?: boolean;
  blocks: PerformanceBlock[];
  performances?: Performance[];
}

export interface PerformanceSectionWrapper {
  [dayId: string]: {
    [chapterId: string]: PerformanceSection[];
  }
}

export interface DashboardPerformancesCount {
  all: number;
  today: number;
  yesterday: number;
}