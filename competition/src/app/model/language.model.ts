export interface Language {
  code: string;
  label: string;
}

export const LANGUAGES: Language[] = [
  { code: 'pl', label: 'common.language.pl' },
  { code: 'en', label: 'common.language.en' }
];
