import { Address } from './address.model';

export interface School {
  sid: string;
  name: string;
  phone: string;
  email?: string;
  address: Address;
}