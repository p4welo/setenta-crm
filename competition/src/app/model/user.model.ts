import { School } from './school.model';

export interface User {
  sid: string;
  firstName: string;
  lastName: string;
  email: string;
  name: string;
  city: string;

  school?: School;
}