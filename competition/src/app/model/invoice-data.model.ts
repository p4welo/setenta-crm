export interface InvoiceData {
  sid: string;
  name: string;
  street: string;
  zip: string;
  city: string;
  nip: string;

  updated_at?: string;
  email?: string;
  payments?: number;
  invoiceSid?: string;
}

export interface Invoice {
  nip: string;
  name: string;
  street: string;
  zip: string;
  city: string;
  title: string;
  netPrice: number;
  taxPrice: number;
  userSid?: string;
}