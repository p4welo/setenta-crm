import { School } from './school.model';

export interface Dancer_ {
  sid?: string;
  firstName: string;
  lastName: string;
  yearOfBirth: string;
  groupName?: string;

  checked?: boolean;
}