export interface Competition {
  sid: string;
  name: string;
  start: Date;
  end: Date;
  registrationEnd: Date;
  audioUploadEnd: Date;
  isPublic: boolean;
  registrationOpened: boolean;
  resultPublished: boolean;
  startingListPublished: boolean;
  paymentModel: string;
  website: string;
  regulations: string;
  mapUrl: string;
  phone: string;
  place: string;
  street: string;
  zip: string;
  city: string;
  ageLevelTolerance: number;
}

export interface DancerPerformanceFee {
  sid: string;
  firstName?: string;
  lastName?: string;
  type?: string;
  performanceCount: number;
  fee: number;
}

export interface CompetitionPayment {
  sid: string;
  date: Date,
  amount: number;
  userSid: string;
}

export interface CompetitionStyle {
  sid: string;
  name?: string;
  showTitle?: boolean;
}

export interface CompetitionLicence {
  sid?: string;
  musicUpload: boolean;
  results: boolean;
}

export interface CompetitionType {
  sid: string;
  name: string;
  min: number;
  max: number;
  showName?: boolean;
}

export interface CompetitionExperience {
  sid: string;
  name: string;
}

export interface CompetitionAge {
  sid: string;
  name: string;
  min: number;
  max: number;
}

export interface AdminCompetitionParticipant {
  sid: string;
  userSid: string;
  name: string;
  city: string;
  email: string;
  dancersAmount: number;
  performancesAmount: number;
  fee: number;
  payments: number;

  detailsOpened: boolean;
}

export interface CompetitionRule {
  id?: number;
  parentId?: number;
  sid?: string;
  objectState?: string;
  ruleType: string;
  style?: CompetitionStyle;
  type?: CompetitionType;
  experience?: CompetitionExperience;
  age?: CompetitionAge;
  rules?: CompetitionRule[];

  ownMusicAllowed?: boolean;
  checked?: boolean;
  expanded?: boolean;
}

export const RuleTypes = {
  STYLE: 'STYLE',
  TYPE: 'TYPE',
  EXPERIENCE: 'EXPERIENCE',
  AGE: 'AGE'
};

export interface Rule {
  name: string;
  type: string;
  min?: number;
  max?: number;
  rules?: Rule[];
}

export interface PerformanceRule {
  rules: Rule[];
}