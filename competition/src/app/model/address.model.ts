export interface Address {
  sid: string;
  street: string;
  zip: string;
  city: string;
}