import { Dancer_ } from '@competition/model';

export const dancersMock: Dancer_[] = [
  {
    firstName: 'Anna',
    lastName: 'Nowak',
    yearOfBirth: '1999'
  },
  {
    firstName: 'Maria',
    lastName: 'Kowalska',
    yearOfBirth: '1999'
  },
  {
    firstName: 'Joanna',
    lastName: 'Kowalska',
    yearOfBirth: '2000'
  },
  {
    firstName: 'Marta',
    lastName: 'Kowalska',
    yearOfBirth: '2000'
  },
  {
    firstName: 'Barbara',
    lastName: 'Kowalska',
    yearOfBirth: '2001'
  },
  {
    firstName: 'Katarzyna',
    lastName: 'Kowalska',
    yearOfBirth: '2001'
  },
  {
    firstName: 'Antonina',
    lastName: 'Kowalska',
    yearOfBirth: '2002'
  },
  {
    firstName: 'Martyna',
    lastName: 'Kowalska',
    yearOfBirth: '2002'
  }
];

export const dancerMock: Dancer_ = dancersMock[0];