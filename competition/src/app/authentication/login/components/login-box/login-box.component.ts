import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginForm } from '@competition/modules/auth/model';
import { path } from 'ramda';

@Component({
  selector: 'app-login-box',
  templateUrl: './login-box.component.html',
  styleUrls: ['./login-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginBoxComponent implements OnInit {

  @Input() public error: string;
  @Input() public isLoading: boolean;
  @Output() public onSubmit: EventEmitter<LoginForm> = new EventEmitter<LoginForm>();

  public ngForm: FormGroup;
  public showErrors: boolean = false;

  constructor(private formBuilder: FormBuilder) {
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      type: 'SCHOOL'
    });
  }

  public submit(form: LoginForm): void {
    if (this.ngForm.valid) {
      this.onSubmit.emit(form);
    }
    else {
      this.showErrors = true;
    }
  }

  public isInvalid(field: string): boolean {
    return this.showErrors && this.ngForm.get(field).invalid;
  }

  public getError(field: string): any {
    return path(['errors'], this.ngForm.get(field));
  }
}
