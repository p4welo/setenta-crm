import { Routes } from '@angular/router';

import { LoginContainerComponent } from './login.component';

export const routes: Routes = [
  { path: '', component: LoginContainerComponent }
];
