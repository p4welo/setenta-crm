import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '@competition/modules/auth';
import { TranslateModule } from "@ngx-translate/core";

import { CoreModule } from '@competition/core';

import { routes } from './login.routes';
import { LoginBoxComponent } from './components/login-box/login-box.component';
import { LoginContainerComponent } from './login.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    AuthModule,
    TranslateModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    LoginContainerComponent,
    LoginBoxComponent
  ],
  providers: [
  ]
})
export class LoginModule {
  public static routes = routes;
}
