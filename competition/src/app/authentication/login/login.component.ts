import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthLoginStart } from '@competition/modules/auth/actions';
import { LoginForm } from '@competition/modules/auth/model';
import { AuthService } from '@competition/modules/auth/services';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginContainerComponent {

  public error$: Observable<string> = this.authService.error$;
  public isLoading$: Observable<boolean> = this.authService.isLoading$;

  constructor(
      private authService: AuthService,
      private store$: Store<any>
  ) {}

  login(form: LoginForm) {
    form.type = 'SCHOOL';
    this.store$.dispatch(new AuthLoginStart(form))
  }
}
