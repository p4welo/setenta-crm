import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreModule } from '@competition/core';

import { LogoutComponent } from './logout.component';
import { routes } from './logout.routes';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    LogoutComponent
  ]
})
export class LogoutModule {
  public static routes = routes;
}