import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthLogoutStart } from '@competition/modules/auth/actions';
import { Store } from '@ngrx/store';

@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoutComponent implements OnInit {

  constructor(
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AuthLogoutStart());
  }
}