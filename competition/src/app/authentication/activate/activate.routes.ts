import { Routes } from '@angular/router';

import { ActivateComponent } from './activate.component';

export const routes: Routes = [
  { path: ':userSid', component: ActivateComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];