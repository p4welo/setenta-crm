import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@competition/core';

import { ActivateService } from './activate.service';
import { routes } from './activate.routes';
import { ActivateComponent } from './activate.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    HttpClientModule,
    TranslateModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ActivateComponent],
  providers: [ActivateService]
})
export class ActivateModule {
  public static routes = routes;
}