import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { getActivationUrl } from '@competition/core/utils/index';

@Injectable()
export class ActivateService {
  constructor(
      private http: HttpClient) {
  }

  public activate(id: string): Observable<any> {
    return this.http.get(getActivationUrl(id));
  }
}