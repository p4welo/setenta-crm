import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ActivateService } from './activate.service';

@Component({
  templateUrl: './activate.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivateComponent implements OnInit {
  public loading: boolean = true;
  public error: boolean = false;

  constructor(
      private route: ActivatedRoute,
      private activateService: ActivateService,
      private cd: ChangeDetectorRef
  ) {
  }

  public ngOnInit(): void {
    const sid: string = this.route.snapshot.paramMap.get('userSid');
    this.activateService.activate(sid).subscribe(
        () => {
          this.loading = false;
          this.error = false;
          this.cd.detectChanges();
        },
        () => {
          this.loading = false;
          this.error = true;
          this.cd.detectChanges();
        }
    );
  }
}