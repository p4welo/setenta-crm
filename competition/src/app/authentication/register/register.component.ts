import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { RegistrationForm } from './model/registration-form.model';
import { RegistrationService } from './services/registration.service';

@Component({
  selector: 'app-finance',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent {

  public error$: Observable<string>;
  public isLoading$: Observable<boolean>;
  public success$: Observable<boolean>;

  constructor(private registrationService: RegistrationService) {
    this.error$ = this.registrationService.error$;
    this.success$ = this.registrationService.success$;
    this.isLoading$ = this.registrationService.isLoading$;
  }

  public register(registrationForm: RegistrationForm) {
    this.registrationService.dispatchRegistrationFormSubmit(registrationForm);
  }
}
