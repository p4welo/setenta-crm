export interface RegistrationForm {
  firstName: string;
  lastName: string;
  name: string;
  phone: string;
  city: string;
  email: string;
  password: string;
  agreement: boolean;
}
