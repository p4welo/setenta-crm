import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  ActionTypes,
  RegistrationActions,
  RegistrationSubmitErrorAction,
  RegistrationSubmitSuccessAction
} from '../actions/index';
import { RegistrationForm } from '../model/index';
import { RegistrationSubmitStartAction } from '../actions/index';
import { getRegistrationUrl } from '../../../core/utils/index';

@Injectable()
export class RegistrationEffects {

  constructor(private actions$: Actions,
              private http: HttpClient) {}

  @Effect()
  public sendRegistrationData$: Observable<RegistrationActions> = this.actions$.pipe(
      ofType(ActionTypes.SUBMIT_START),
      map((action: RegistrationSubmitStartAction) => action.payload),
      switchMap((registrationForm: RegistrationForm) => this.executePost(registrationForm))
  );

  private executePost(registrationForm: RegistrationForm): Observable<Action> {
    return this.http.post(getRegistrationUrl(), registrationForm).pipe(
        map(this.dispatchRegistrationSuccess),
        catchError(this.handleError)
    );
  }

  private dispatchRegistrationSuccess(res: any): Action {
    return new RegistrationSubmitSuccessAction(res);
  }

  private handleError(e: any): Observable<Action> {
    return of(new RegistrationSubmitErrorAction(e.error.type));
  }
}
