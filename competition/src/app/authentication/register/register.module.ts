import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { CoreModule } from '../../core';
import { routes } from './register.routes';
import { getInitialState, reducers } from './reducers/index';
import { RegisterComponent } from './register.component';
import { RegisterBoxComponent } from './components/register-box/register-box.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RegistrationEffects } from './effects/registration.effects';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationService } from './services/registration.service';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
    StoreModule.forFeature('registrationState', reducers, {
      initialState: getInitialState
    }),
    EffectsModule.forFeature([RegistrationEffects]),
    RouterModule.forChild(routes),
    TranslateModule,
    HttpClientModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    RegistrationService
  ],
  declarations: [
    RegisterComponent,
    RegisterBoxComponent
  ]
})
export class RegisterModule {
  public static routes = routes;
}
