import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RegistrationForm } from '../model/index';
import { RegistrationSubmitStartAction } from '../actions/register.actions';
import { Observable } from 'rxjs';
import { getError, getLoading, getSuccess } from '../reducers/index';

@Injectable()
export class RegistrationService {

  public error$: Observable<string> = this.selectErrorState();
  public success$: Observable<boolean> = this.selectSuccessState();
  public isLoading$: Observable<boolean> = this.selectLoadingState();

  constructor(private store$: Store<any>) {
  }

  public dispatchRegistrationFormSubmit(registrationForm: RegistrationForm): void {
    this.store$.dispatch(new RegistrationSubmitStartAction(registrationForm));
  }

  private selectErrorState(): Observable<string> {
    return this.store$.select(getError);
  }

  private selectSuccessState(): Observable<boolean> {
    return this.store$.select(getSuccess);
  }

  private selectLoadingState(): Observable<boolean> {
    return this.store$.select(getLoading);
  }
}
