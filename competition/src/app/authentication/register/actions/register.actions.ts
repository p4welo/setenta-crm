import { Action } from '@ngrx/store';
import { RegistrationForm } from '../model/registration-form.model';
import { UserSession } from '../../../model/user-session.model';

export const ActionTypes = {
  SUBMIT_START: '[Registration] submit start',
  SUBMIT_SUCCESS: '[Registration] submit success',
  SUBMIT_ERROR: '[Registration] submit error'
};

export class RegistrationSubmitStartAction implements Action {
  public type: string = ActionTypes.SUBMIT_START;

  constructor(public payload: RegistrationForm) {
  }
}

export class RegistrationSubmitSuccessAction implements Action {
  public type: string = ActionTypes.SUBMIT_SUCCESS;

  constructor(public payload: UserSession) {
  }
}

export class RegistrationSubmitErrorAction implements Action {
  public type: string = ActionTypes.SUBMIT_ERROR;

  constructor(public payload?: any) {
  }
}

export type RegistrationActions =
    | RegistrationSubmitStartAction
    | RegistrationSubmitSuccessAction
    | RegistrationSubmitErrorAction;