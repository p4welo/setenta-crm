import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { path, trim } from 'ramda';
import { RegistrationForm } from '../../model/index';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-box',
  templateUrl: './register-box.component.html',
  styleUrls: ['./register-box.component.scss']
})
export class RegisterBoxComponent implements OnInit {

  @Input() public error: string;
  @Input() public isLoading: boolean;
  @Output() public onSubmit: EventEmitter<RegistrationForm> = new EventEmitter<RegistrationForm>();

  public ngForm: FormGroup;
  public showErrors: boolean = false;

  constructor(private formBuilder: FormBuilder) {
  }

  public ngOnInit() {
    this.ngForm = this.formBuilder.group({
      firstName: ['', [
        Validators.required,
        Validators.maxLength(15)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.maxLength(25)
      ]],
      name: ['', [
        Validators.required,
        Validators.maxLength(50)
      ]],
      phone: ['', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9)
      ]],
      city: ['', [
        Validators.required,
        Validators.maxLength(25)
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', Validators.required],
      repassword: ['', [
        Validators.required,
        this.matchValidator('password')
      ]],
      agreement: [false, Validators.required],
      type: 'SCHOOL'
    });
  }

  public submit(form: RegistrationForm): void {
    if (this.ngForm.valid) {
      this.onSubmit.emit({
        ...form,
        email: trim(form.email),
        firstName: trim(form.firstName),
        lastName: trim(form.lastName),
        name: trim(form.name),
        city: trim(form.city)
      });
    } else {
      this.showErrors = true;
    }
  }

  public isError(field: string, type: string): boolean {
    return path(['errors', type], this.ngForm.get(field));
  }

  public isInvalid(field: string): boolean {
    return this.showErrors && this.ngForm.get(field).invalid;
  }

  public getError(field: string): any {
    return path(['errors'], this.ngForm.get(field));
  }

  public get userAlreadyExists(): boolean {
    return this.error === 'user.already.exists';
  }

  private matchValidator(fieldName: string) {
    let fcfirst: FormControl;
    let fcSecond: FormControl;

    return function matchValidator(control: FormControl) {

      if (!control.parent) {
        return null;
      }

      // INITIALIZING THE VALIDATOR.
      if (!fcfirst) {
        //INITIALIZING FormControl first
        fcfirst = control;
        fcSecond = control.parent.get(fieldName) as FormControl;

        //FormControl Second
        if (!fcSecond) {
          throw new Error('matchValidator(): Second control is not found in the parent group!');
        }

        fcSecond.valueChanges.subscribe(() => {
          fcfirst.updateValueAndValidity();
        });
      }

      if (!fcSecond) {
        return null;
      }

      if (fcSecond.value !== fcfirst.value) {
        return {
          matchOther: true
        };
      }

      return null;
    }
  }
}
