import { ActionTypes, RegistrationActions } from '../actions/index';
import { RegistrationForm } from '../model/index';
import { path } from 'ramda';

export interface RegistrationState {
  loading: boolean;
  success: boolean;
  form?: RegistrationForm;
  error?: any;
}

export const initialState: RegistrationState = {
  loading: false,
  success: false
};

export const getError = state => path(['registration', 'error'], state);
export const getSuccess = state => path(['registration', 'success'], state);
export const getLoading = state => path(['registration', 'loading'], state);

export function registration(state: RegistrationState = initialState, action: RegistrationActions) {
  switch (action.type) {
    case ActionTypes.SUBMIT_START:
      return {
        ...state,
        loading: true,
        form: action.payload
      };
    case ActionTypes.SUBMIT_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true
      };
    case ActionTypes.SUBMIT_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: action.payload
      };
    default:
      return state;
  }
}
