import * as fromRegistration from './registration.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface RegistrationState {
  registration: fromRegistration.RegistrationState;
}

export const reducers = {
  registration: fromRegistration.registration,
};

export interface State {
  registrationState: RegistrationState;
}

export const initialState = {
  ...fromRegistration.initialState
};

export function getInitialState(): any {
  return initialState;
}

export const getRegistration = createFeatureSelector<RegistrationState>('registrationState');

export const getError = createSelector(
    getRegistration,
    fromRegistration.getError
);
export const getSuccess = createSelector(
    getRegistration,
    fromRegistration.getSuccess
);
export const getLoading = createSelector(
    getRegistration,
    fromRegistration.getLoading
);