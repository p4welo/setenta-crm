import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getResetPasswordUrl } from '@competition/core/utils';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  public ngForm: FormGroup;

  public loading: boolean = false;
  public success: boolean = false;
  public error: boolean = false;

  private destroy$ = new Subject();

  constructor(
      private formBuilder: FormBuilder,
      private http: HttpClient
  ) {
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public submit(form: { email: string }): void {
    if (this.ngForm.valid) {
      this.loading = true;
      const { email } = form;
      this.http
          .put(getResetPasswordUrl(), { email })
          .pipe(takeUntil(this.destroy$))
          .subscribe(
              (result) => {
                this.success = true;
              },
              (error) => {
                this.error = true;
              });
    }
  }
}