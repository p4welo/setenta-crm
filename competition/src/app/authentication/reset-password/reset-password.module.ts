import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { routes } from './reset-password.routes';
import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/index';
import { RouterModule } from '@angular/router';
import { ResetPasswordComponent } from './reset-password.component';
import { NewPasswordComponent } from './new-password.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
  ],
  declarations: [
    ResetPasswordComponent,
    NewPasswordComponent
  ],
  providers: [
  ]
})
export class ResetPasswordModule {

}