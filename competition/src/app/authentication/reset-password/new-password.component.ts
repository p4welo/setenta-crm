import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { getNewPasswordUrl } from '@competition/core/utils';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnDestroy {
  public ngForm: FormGroup;

  public loading: boolean = false;
  public success: boolean = false;
  public error: boolean = false;

  private readonly resetSid: string;
  private destroy$ = new Subject();

  constructor(
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private route: ActivatedRoute,
  ) {
    this.resetSid = this.route.snapshot.paramMap.get('resetSid');
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      password: ['', [Validators.required]]
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public submit(form: { password: string }): void {
    if (this.ngForm.valid) {
      this.loading = true;
      const { password } = form;
      this.http.put(getNewPasswordUrl(this.resetSid), { password })
          .pipe(takeUntil(this.destroy$))
          .subscribe(
              (result) => {
                this.success = true;
              },
              (error) => {
                this.error = true;
              });
    }
  }
}