import { Routes } from '@angular/router';
import { ResetPasswordComponent } from './reset-password.component';
import { NewPasswordComponent } from './new-password.component';

export const routes: Routes = [
  { path: 'reset', component: ResetPasswordComponent },
  { path: 'new/:resetSid', component: NewPasswordComponent,
    pathMatch: 'full' },
  {
    path: '',
    redirectTo: '/reset',
    pathMatch: 'full'
  }
];
