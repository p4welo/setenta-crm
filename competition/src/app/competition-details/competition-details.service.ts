import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  getAges,
  getCompetition,
  getCompetitionSid,
  getExperiences, getLicence,
  getNewRules,
  getPerformances,
  getResults,
  getStyles,
  getStylesWithTitle,
  getTypes,
  isCompetitionLoading,
  isFirstSuccessVisible, isMusicUploadOpened,
  isPerformanceUploadInProgress,
  isRegistrationOpened, isResultsLoading,
  performancesContainExperience
} from './store/selectors';
import {
  Competition, CompetitionAge,
  CompetitionExperience, CompetitionLicence, CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Performance, PerformanceSectionWrapper
} from '../model';
import {
  getCompetitionUrl
} from '../core/utils';
import { Observable } from 'rxjs';

@Injectable()
export class CompetitionDetailsService {

  public competition$: Observable<Competition> = this.store$.select(getCompetition);
  public competitionSid$: Observable<string> = this.store$.select(getCompetitionSid);
  public performances$: Observable<Performance[]> = this.store$.select(getPerformances);
  public licence$: Observable<CompetitionLicence> = this.store$.select(getLicence);
  public styles$: Observable<CompetitionStyle[]> = this.store$.select(getStyles);
  public stylesWithTitle$: Observable<CompetitionStyle[]> = this.store$.select(getStylesWithTitle);
  public types$: Observable<CompetitionType[]> = this.store$.select(getTypes);
  public experiences$: Observable<CompetitionExperience[]> = this.store$.select(getExperiences);
  public ages$: Observable<CompetitionAge[]> = this.store$.select(getAges);
  public rules$: Observable<CompetitionRule[]> = this.store$.select(getNewRules);
  public results$: Observable<PerformanceSectionWrapper> = this.store$.select(getResults);
  public registrationOpened$: Observable<boolean> = this.store$.select(isRegistrationOpened);
  public musicUploadOpened$: Observable<boolean> = this.store$.select(isMusicUploadOpened);
  public performanceUploadInProgress$: Observable<boolean> = this.store$.select(isPerformanceUploadInProgress);
  public performancesContainExperience$: Observable<boolean> = this.store$.select(performancesContainExperience);
  public firstSuccessVisible$: Observable<boolean> = this.store$.select(isFirstSuccessVisible);
  public competitionLoading$: Observable<boolean> = this.store$.select(isCompetitionLoading);
  public resultsLoading$: Observable<boolean> = this.store$.select(isResultsLoading);

  constructor(
      private http: HttpClient,
      private store$: Store<any>
      ) {
  }

  public get(sid: string): Observable<Competition> {
    return this.http.get<Competition>(getCompetitionUrl(sid));
  }
}