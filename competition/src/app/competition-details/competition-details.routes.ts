import { Routes } from '@angular/router';
import { CompetitionRegistrationComponent } from './competition-registration';
import { CompetitionFinancesComponent } from './competition-finances';
import { CompetitionInfoComponent } from './competition-info';
import { CompetitionResultsComponent } from './competition-results';
import { CompetitionDetailsComponent } from './competition-details.component';

export const routes: Routes = [
  { path: '', component: CompetitionDetailsComponent,
    children: [
      { path: '', redirectTo: 'info', pathMatch: 'full' },
      { path: 'info', component: CompetitionInfoComponent },
      { path: 'registration', component: CompetitionRegistrationComponent },
      { path: 'finances', component: CompetitionFinancesComponent },
      { path: 'results', component: CompetitionResultsComponent }
    ]}
];