import { Competition, Dancer_ } from '../model';
import { AgeLevel } from './competition-rules.const';
import { getDancersAgeLevel, getPerformanceLeadingAge } from './competition.utils';

describe('Utils: CompetitionUtils', () => {
  it('should calculate proper ageLevel', () => {
    const dancers: Dancer_[] = [
      { firstName: 'Jan', lastName: 'Kowalski', yearOfBirth: '2005' } as Dancer_,
      { firstName: 'Mariusz', lastName: 'Citko', yearOfBirth: '2007' } as Dancer_
    ];

    const ageLevels: AgeLevel[] = [
      { name: 'TO_8', min: 0, max: 8 } as AgeLevel,
      { name: 'FROM_9_TO_11', min: 9, max: 11 } as AgeLevel,
      { name: 'FROM_12_TO_15', min: 12, max: 15 } as AgeLevel,
      { name: 'FROM_16', min: 16, max: 100 } as AgeLevel
    ];
    const competition = {
      ageLevelTolerance: 20
    };

    const calculatedAgeLevel: AgeLevel = getDancersAgeLevel(dancers, ageLevels, competition as Competition);
    expect(calculatedAgeLevel).toEqual(ageLevels[2]);
  });

  it('should calculate proper leading age of the performance', () => {
    const dancers: Dancer_[] = [
      { firstName: 'Jan', lastName: 'Kowalski', yearOfBirth: '2005' } as Dancer_,
      { firstName: 'Mariusz', lastName: 'Citko', yearOfBirth: '2007' } as Dancer_
    ];
    const competition = {
      ageLevelTolerance: 20
    };
    const age: number = getPerformanceLeadingAge(dancers, competition as Competition);
    expect(age).toBe(14);
  });
});