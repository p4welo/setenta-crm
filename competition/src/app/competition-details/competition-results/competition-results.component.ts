import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { path } from 'ramda';
import { Observable } from 'rxjs';
import { CompetitionDetailsService } from '../competition-details.service';
import {
  ResultsFetchStart,
  StylesFetchStart,
  TypesFetchStart
} from '@competition/competition-details/store/actions';
import { CompetitionsService } from '@competition/core/services';
import { competitionDayName } from '@competition/core/utils';
import {
  Competition,
  CompetitionStyle,
  CompetitionType,
  PerformanceBlock,
  PerformanceSection,
  PerformanceSectionWrapper
} from '@competition/model';


@Component({
  templateUrl: './competition-results.component.html',
  styleUrls: ['./competition-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionResultsComponent implements OnInit {

  public competition$: Observable<Competition> = this.competitionDetailsService.competition$;
  public results$: Observable<PerformanceSectionWrapper> = this.competitionDetailsService.results$;
  public resultsLoading$: Observable<boolean> = this.competitionDetailsService.resultsLoading$;
  public types$: Observable<CompetitionType[]> = this.competitionDetailsService.types$;
  public stylesWithTitle$: Observable<CompetitionStyle[]> = this.competitionDetailsService.stylesWithTitle$;

  constructor(
      private route: ActivatedRoute,
      private competitionsService: CompetitionsService,
      private competitionDetailsService: CompetitionDetailsService,
      private store$: Store<any>
  ) {
  }

  public chapters(sections: PerformanceSectionWrapper): string[] {
    return Object.keys(sections)
  }

  public ngOnInit(): void {
    this.store$.dispatch(new TypesFetchStart());
    this.store$.dispatch(new StylesFetchStart());
    this.store$.dispatch(new ResultsFetchStart());
  }

  public getDayNumbers(list: PerformanceSectionWrapper): string[] {
    return Object.keys(list);
  }

  public getDayName(dayNumber: string, competition: Competition): string {
    return competitionDayName(competition, +dayNumber);
  }

  public getChapterNumbers(list: PerformanceSectionWrapper, dayNumber: string): string[] {
    return list ? Object.keys(list[dayNumber]) : [];
  }

  public noPerformancesInside(sections: PerformanceSection[] = []): boolean {

    return sections.filter((section: PerformanceSection) => section.performances.length > 0).length === 0;
  }

  public getSections(list: PerformanceSectionWrapper, dayNumber: string, chapterNumber: string): PerformanceSection[] {
    return list[dayNumber][chapterNumber];
  }

  public getCity(performance: Performance): string {
    return path(['user', 'school', 'address', 'city'], performance);
  }

  public canShowTitle(section: PerformanceSection, stylesWithTitle: CompetitionStyle[]): boolean {
    if (stylesWithTitle && stylesWithTitle.length > 0) {
      let found = false;
      stylesWithTitle.forEach((styleWithTitle: CompetitionStyle) => {
        section.blocks.forEach((block: PerformanceBlock) => {
          if (block.style === styleWithTitle.name) {
            found = true;
            return;
          }
        });
      });
      return found;
    }
    return false;
  }

  public getSchool(performance: Performance): string {
    return path(['user', 'school', 'name'], performance);
  }
}