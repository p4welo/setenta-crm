import { ascend, prop, sort } from 'ramda';
import { Competition, CompetitionPayment, Dancer_, DancerPerformanceFee } from '../model';
import {
  AgeLevel,
} from './competition-rules.const';

export const getDancersAgeLevel = (dancers: Dancer_[] = [], ageLevels: AgeLevel[], competition: Competition): AgeLevel => {
  const age = getPerformanceLeadingAge(dancers, competition);
  if (age) {
    const foundLevels = ageLevels.filter((level: AgeLevel) => level.min <= age && level.max >= age);
    if (foundLevels.length > 0) {
      return foundLevels[0];
    }
  }
  return undefined;
};

export const getPerformanceLeadingAge = (dancers: Dancer_[], competition: Competition): number => {
  const length = dancers.length;
  if (length > 0) {
    const sorted: Dancer_[] = sort(ascend(prop('yearOfBirth')), dancers);
    const numberToIgnore = length > 2 ?
        Math.round(length * competition.ageLevelTolerance / 100) :
        0;
    return new Date().getFullYear() - +sorted[numberToIgnore].yearOfBirth;
  }
  return undefined;
};

export const sumFees = (fees: DancerPerformanceFee[] = []) => {
  return fees.reduce((accumulator: number, current: DancerPerformanceFee) => accumulator + current.fee, 0);
};

export const sumPayments = (payments: CompetitionPayment[] = []) => {
  return payments.reduce((accumulator: number, current: CompetitionPayment) => accumulator + current.amount, 0);
};


