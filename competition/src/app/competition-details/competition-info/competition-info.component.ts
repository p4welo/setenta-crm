import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionStyle,
  CompetitionType
} from '../../model';

import { CompetitionDetailsService } from '../competition-details.service';
import {
  AgesFetchStart,
  ExperiencesFetchStart,
  StylesFetchStart,
  TypesFetchStart
} from '../store/actions';

@Component({
  selector: 'app-competition-info',
  templateUrl: './competition-info.component.html',
  styleUrls: ['./competition-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionInfoComponent implements OnInit {

  public competition$: Observable<Competition> = this.competitionDetailsService.competition$;
  public styles$: Observable<CompetitionStyle[]> = this.competitionDetailsService.styles$;
  public types$: Observable<CompetitionType[]> = this.competitionDetailsService.types$;
  public experiences$: Observable<CompetitionExperience[]> = this.competitionDetailsService.experiences$;
  public ages$: Observable<CompetitionAge[]> = this.competitionDetailsService.ages$;
  public licence$: Observable<CompetitionLicence> = this.competitionDetailsService.licence$;

  constructor(
      private route: ActivatedRoute,
      private store$: Store<any>,
      private competitionDetailsService: CompetitionDetailsService
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new StylesFetchStart());
    this.store$.dispatch(new TypesFetchStart());
    this.store$.dispatch(new ExperiencesFetchStart());
    this.store$.dispatch(new AgesFetchStart());
  }
}