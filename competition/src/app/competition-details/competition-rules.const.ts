
export const RuleType = {
  STYLE: 'STYLE',
  TYPE: 'TYPE',
  EXPERIENCE: 'EXPERIENCE',
  AGE: 'AGE'
};

export const PerformanceTypes = {
  SOLO: 'SOLO',
  DUO: 'DUO',
  FORMATION: 'FORMATION',
  MINI_FORMATION: 'MINI_FORMATION'
};

export const PerformanceStyles = {
  HIP_HOP: 'HIP_HOP',
  JAZZ: 'JAZZ',
  CONTEMPORARY : 'CONTEMPORARY',
  DISCO: 'DISCO',
  BALLET: 'BALLET',
  SHOW_DANCE: 'SHOW_DANCE',
  ART: 'ART',
  MODERN: 'MODERN',
  THEATER_DANCE: 'THEATER_DANCE'
};

export const ExperienceLevels = {
  MASTER: 'MASTER',
  DEBUT: 'DEBUT'
};

export interface AgeLevel {
  name: string;
  min: number;
  max: number;
}

export interface Rule {
  name: string;
  type: string;
  min?: number;
  max?: number;
  rules?: Rule[];
}

// TODO: deprecated
export interface PerformanceRule {
  rules: Rule[];
}

export const AGE_LEVELS: AgeLevel[] = [
  { name: 'TO_8', min: 0, max: 8 },
  { name: 'FROM_9_TO_11', min: 9, max: 11 },
  { name: 'FROM_12_TO_15', min: 12, max: 15 },
  { name: 'FROM_16', min: 16, max: 100 }
];
