import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence, CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Dancer_,
  Performance, PerformanceSectionWrapper
} from '@competition/model';

export interface CompetitionDetailsModuleState {
  competition?: Competition;
  competitionSid?: string;
  performances: Performance[];
  licence: CompetitionLicence;
  styles: CompetitionStyle[];
  types: CompetitionType[];
  experiences: CompetitionExperience[];
  ages: CompetitionAge[];
  rules: CompetitionRule[];
  results?: PerformanceSectionWrapper;
  loading: {
    competition: boolean;
    results: boolean;
  };
  registration: {
    uploadInProgress: boolean;
    removeAudioSid?: string;
    firstSuccessVisible: boolean;
  };
}