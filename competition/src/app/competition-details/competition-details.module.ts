import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  CompetitionRegistrationComponent,
} from '@competition/competition-details/competition-registration';
import { AuthModule } from '@competition/modules/auth';
import { DancerStoreModule } from '@competition/modules/dancer-store';
import { MatchMediaModule } from '@competition/modules/match-media';
import { PerformanceFormModule } from '@competition/modules/performance-form';
import { CompetitionDetailsEffects } from './store/effects';
import { PerformanceSongModule } from '@competition/modules/performance-song';
import { reducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FileUploadModule } from 'ng2-file-upload';
import { CompetitionFinancesComponent } from './competition-finances';
import { CompetitionInfoComponent } from './competition-info';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { CompetitionDetailsService } from './competition-details.service';
import { routes } from './competition-details.routes';
import { CompetitionDetailsComponent } from './competition-details.component';
import { CompetitionResultsComponent } from './competition-results';
import { CoreModule } from '../core';

@NgModule({
  imports: [
    CommonModule,
    AuthModule,
    CoreModule,
    HttpClientModule,
    TranslateModule,
    PerformanceFormModule,
    FormsModule,
    ScrollingModule,
    DancerStoreModule,
    MatchMediaModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    FileUploadModule,
    PerformanceSongModule,
    StoreModule.forFeature('competitionDetails', reducers),
    EffectsModule.forFeature([CompetitionDetailsEffects]),
    TooltipModule.forRoot()
  ],
  declarations: [
    CompetitionDetailsComponent,
    CompetitionInfoComponent,
    CompetitionRegistrationComponent,
    CompetitionFinancesComponent,
    CompetitionResultsComponent
  ],
  providers: [
    CompetitionDetailsService
  ]
})
export class CompetitionDetailsModule {

}