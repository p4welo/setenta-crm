import { ChangeDetectionStrategy, Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompetitionDetailsService } from '@competition/competition-details/competition-details.service';
import {
  CreatePerformanceStart,
  DeletePerformanceStart,
  EditPerformanceStart,
  PerformancesFetchStart,
  PerformanceSongUploadStart,
  PerformanceSongUploadSuccess,
  RemoveSongStart,
  RulesFetchStart,
  StylesFetchStart,
  TypesFetchStart
} from '@competition/competition-details/store/actions';
import {
  CompetitionsService,
  NotificationService,
  PerformanceService,
  SessionService
} from '@competition/core/services';
import {
  Competition, CompetitionLicence, CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Dancer_,
  Performance
} from '@competition/model';
import { DancersFetchStart } from '@competition/modules/dancer-store/actions';
import { DancerService } from '@competition/modules/dancer-store/services';
import { PerformanceSongService } from '@competition/modules/performance-song/services';
import { Store } from '@ngrx/store';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { getSortedList, getSortingColumnClass } from '../../core/utils/table.utils';

@Component({
  templateUrl: './competition-registration.component.html',
  styleUrls: ['./competition-registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionRegistrationComponent implements OnInit {

  public performances: Performance[];
  public sortingProperty: string;
  public sortingAscending: boolean = true;

  public canAddMusic: boolean = true;
  public addFormVisible: boolean = false;

  public uploadModalRef: BsModalRef;
  public uploadPerformance: Performance;
  public uploadFilename: string;

  public performanceToEdit: Performance;

  public uploader: FileUploader = new FileUploader({
    itemAlias: 'song'
  });

  public playingSongSid$: Observable<string> = this.performanceSongService.playingSongSid$;
  public competition$: Observable<Competition> = this.competitionDetailsService.competition$;
  public dancers$: Observable<Dancer_[]> = this.dancerService.filteredDancers$;
  public dancersInitialized$: Observable<boolean> = this.dancerService.initialized$;
  public performances$: Observable<Performance[]> = this.competitionDetailsService.performances$;
  public stylesWithTitle$: Observable<CompetitionStyle[]> = this.competitionDetailsService.stylesWithTitle$;
  public types$: Observable<CompetitionType[]> = this.competitionDetailsService.types$;
  public rules$: Observable<CompetitionRule[]> = this.competitionDetailsService.rules$;
  public registrationOpened$: Observable<boolean> = this.competitionDetailsService.registrationOpened$;
  public musicUploadOpened$: Observable<boolean> = this.competitionDetailsService.musicUploadOpened$;
  public uploadInProgress$: Observable<boolean> = this.competitionDetailsService.performanceUploadInProgress$;
  public canShowExperience$: Observable<boolean> = this.competitionDetailsService.performancesContainExperience$;
  public firstSuccessVisible$: Observable<boolean> = this.competitionDetailsService.firstSuccessVisible$;
  public licence$: Observable<CompetitionLicence> = this.competitionDetailsService.licence$;

  constructor(
      private route: ActivatedRoute,
      private store$: Store<any>,
      private performanceService: PerformanceService,
      private competitionDetailsService: CompetitionDetailsService,
      private competitionsService: CompetitionsService,
      private notificationService: NotificationService,
      private sessionService: SessionService,
      private modalService: BsModalService,
      private dancerService: DancerService,
      private performanceSongService: PerformanceSongService
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new PerformancesFetchStart());
    this.store$.dispatch(new DancersFetchStart());
    this.store$.dispatch(new StylesFetchStart());
    this.store$.dispatch(new TypesFetchStart());
    this.store$.dispatch(new RulesFetchStart());

    this.uploader.onAfterAddingFile = (file) => {
      this.uploadFilename = file.file.name;
      file.withCredentials = false;
    };
    this.uploader.onSuccessItem = (item: FileItem, response: string) => {
      this.store$.dispatch(new PerformanceSongUploadSuccess(JSON.parse(response)));
      this.hideUploadModal();
    }
  }

  // ADD FORM
  public showAddForm(): void {
    this.performanceToEdit = {} as Performance;
    this.addFormVisible = true;
  }

  public edit(performance: Performance): void {
    this.performanceToEdit = performance;
    this.addFormVisible = true;
  }

  public hideAddForm(): void {
    this.addFormVisible = false;
  }

  // SORTING
  public sortBy(property: string): void {
    if (this.sortingProperty === property) {
      this.sortingAscending = !this.sortingAscending;
    } else {
      this.sortingProperty = property;
      this.sortingAscending = true;
    }
  }

  public showUploadModal(performance: Performance, template: TemplateRef<any>): void {
    this.uploadPerformance = performance;
    this.uploadModalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  public removeAudio(performance: Performance): void {
    if (confirm('Czy na pewno chcesz usunąć muzykę do występu?')) {
      this.store$.dispatch(new RemoveSongStart({ performance }));
    }
  }

  public hideUploadModal(): void {
    this.uploadFilename = '';
    this.uploadModalRef.hide();
  }

  public upload(): void {
    if (this.uploadFilename) {
      this.store$.dispatch(new PerformanceSongUploadStart({
        uploader: this.uploader,
        performanceSid: this.uploadPerformance.sid
      }));
    }
  }

  public trim(field: string): string {
    return field ? field.trim() : '';
  }

  public getSortingClass(property: string): string {
    return getSortingColumnClass(property, this.sortingProperty, this.sortingAscending);
  }

  // TABLE
  // TODO: na razie nieuzywane
  public get list(): Performance[] {
    return getSortedList(this.performances, this.sortingProperty, this.sortingAscending);
  }

  public delete(performance: Performance): void {
    if (confirm('Czy na pewno chcesz usunąć występ?')) {
      this.store$.dispatch(new DeletePerformanceStart(performance));
      this.hideAddForm();
    }
  }

  public create(performance: Performance): void {
    if (performance.sid && confirm('Czy na pewno chcesz zaktualizować występ?')) {
      this.store$.dispatch(new EditPerformanceStart(performance));
    }
    else {
      this.store$.dispatch(new CreatePerformanceStart(performance));
    }
    this.hideAddForm();
  }
}