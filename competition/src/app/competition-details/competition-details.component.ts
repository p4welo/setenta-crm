import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompetitionLicenceFetchStart, SelectCompetitionStart } from './store/actions';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Competition, CompetitionLicence } from '../model';
import { CompetitionDetailsService } from './competition-details.service';

@Component({
  templateUrl: './competition-details.component.html',
  styleUrls: ['./competition-details.component.scss']
})
export class CompetitionDetailsComponent implements OnInit {

  public competition$: Observable<Competition> = this.competitionDetailsService.competition$;
  public licence$: Observable<CompetitionLicence> = this.competitionDetailsService.licence$;

  constructor(
      private route: ActivatedRoute,
      private store$: Store<any>,
      private competitionDetailsService: CompetitionDetailsService
  ) {
  }

  public ngOnInit(): void {
    const competitionSid = this.route.snapshot.paramMap.get('competitionSid');
    this.store$.dispatch(new SelectCompetitionStart(competitionSid));
    this.store$.dispatch(new CompetitionLicenceFetchStart(competitionSid));
  }
}
