import { CompetitionDetailsModuleState } from '@competition/competition-details/model';
import { CompetitionStyle, Performance } from '@competition/model';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as dayjs from 'dayjs';

export const getCompetitionDetails = createFeatureSelector<CompetitionDetailsModuleState>('competitionDetails');

export const getCompetitionSid = createSelector(
    getCompetitionDetails,
    state => state.competitionSid
);

export const getPerformances = createSelector(
    getCompetitionDetails,
    state => state.performances
);

export const getCompetition = createSelector(
    getCompetitionDetails,
    state => state.competition
);

export const getStyles = createSelector(
    getCompetitionDetails,
    state => state.styles
);

export const getStylesWithTitle = createSelector(
    getStyles,
    styles => styles.filter((style: CompetitionStyle) => style.showTitle)
);

export const getTypes = createSelector(
    getCompetitionDetails,
    state => state.types
);

export const getExperiences = createSelector(
    getCompetitionDetails,
    state => state.experiences
);

export const getAges = createSelector(
    getCompetitionDetails,
    state => state.ages
);

export const getLicence = createSelector(
    getCompetitionDetails,
    state => state.licence
);

export const getRules = createSelector(
    getCompetitionDetails,
    state => state.rules
);

export const getNewRules = createSelector(
    getCompetitionDetails,
    state => nest(state.rules)
);

const nest = (items, id = 0, link = 'parentId') => items
    .filter(item => item[link] === id)
    .map(item => ({ ...item, rules: nest(items, item.id) }));

export const getResults = createSelector(
    getCompetitionDetails,
    state => state.results
);

export const isRegistrationOpened = createSelector(
    getCompetition,
    competition => competition && competition.registrationOpened
);

export const isMusicUploadOpened = createSelector(
    getCompetition,
    competition => competition && !dayjs().isAfter(dayjs(competition.audioUploadEnd))
);

export const isPerformanceUploadInProgress = createSelector(
    getCompetitionDetails,
    state => state.registration.uploadInProgress
);

export const isFirstSuccessVisible = createSelector(
    getCompetitionDetails,
    state => state.registration.firstSuccessVisible
);

export const isCompetitionLoading = createSelector(
    getCompetitionDetails,
    state => state.loading.competition
);

export const isResultsLoading = createSelector(
    getCompetitionDetails,
    state => state.loading.results
);

export const performancesContainExperience = createSelector(
    getPerformances,
    performances =>
        performances.filter((performance: Performance) => !!performance.experience).length > 0
);
