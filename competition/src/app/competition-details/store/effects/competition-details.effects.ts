import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NotificationService } from '@competition/core/services';
import {
  getAudioUrl,
  getCompetitionAgesUrl,
  getCompetitionExperiencesUrl, getCompetitionLicenceUrl,
  getCompetitionResultListUrl,
  getCompetitionRulesUrl,
  getCompetitionStylesUrl,
  getCompetitionTypesUrl,
  getCompetitionUrl,
  getPerformanceListUrl,
  getPerformanceSongUrl,
  getPerformanceUrl
} from '@competition/core/utils';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Performance,
  PerformanceSectionWrapper
} from '@competition/model';
import { AuthService } from '@competition/modules/auth/services';
import { StopSongStart } from '@competition/modules/performance-song/actions';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {
  AgesFetchError,
  AgesFetchSuccess,
  CompetitionDetailsActions,
  CompetitionDetailsActionTypes, CompetitionLicenceFetchError, CompetitionLicenceFetchSuccess,
  CreatePerformanceError,
  CreatePerformanceSuccess,
  DeletePerformanceError,
  DeletePerformanceSuccess,
  EditPerformanceError,
  EditPerformanceStart,
  EditPerformanceSuccess,
  ExperiencesFetchError,
  ExperiencesFetchSuccess,
  PerformancesFetchError,
  PerformancesFetchStart,
  PerformancesFetchSuccess,
  RemoveSongError,
  RemoveSongSuccess,
  ResultsFetchError,
  ResultsFetchSuccess,
  RulesFetchError,
  RulesFetchSuccess,
  SelectCompetitionError,
  SelectCompetitionSuccess,
  StylesFetchError,
  StylesFetchSuccess,
  TypesFetchError,
  TypesFetchSuccess
} from '../actions';
import { CompetitionDetailsService } from '../../competition-details.service';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';

@Injectable()
export class CompetitionDetailsEffects {
  constructor(
      private actions$: Actions<{ type: any; payload: any }>,
      private competitionDetailsService: CompetitionDetailsService,
      private http: HttpClient,
      private notificationService: NotificationService,
      private authService: AuthService
  ) {
  }

  @Effect()
  public fetchCompetition$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.SELECT_COMPETITION_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<Competition>(getCompetitionUrl(competitionSid)).pipe(
              map((response) => new SelectCompetitionSuccess(response)),
              catchError((error) => of(new SelectCompetitionError(error)))
          ))
  );

  @Effect()
  public fetchPerformances$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.PERFORMANCES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<Performance[]>(getPerformanceListUrl(competitionSid)).pipe(
              map((response) => new PerformancesFetchSuccess(response)),
              catchError((error) => of(new PerformancesFetchError(error)))
          )
      )
  );

  public fetchLicence$ = createEffect(() => this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.COMPETITION_LICENCE_FETCH_START),
      switchMap((action: CompetitionDetailsActions) =>
          this.http.get<CompetitionLicence>(getCompetitionLicenceUrl(action.payload)).pipe(
              map((response) => new CompetitionLicenceFetchSuccess(response)),
              catchError((error) => of(new CompetitionLicenceFetchError(error)))
          )
      ),
  ));

  @Effect()
  public createPerformance$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.CREATE_PERFORMANCE_START),
      withLatestFrom(
          this.competitionDetailsService.competitionSid$,
          this.competitionDetailsService.registrationOpened$
      ),
      filter(([action, competitionSid, registrationOpened]: [CompetitionDetailsActions, string, boolean]) =>
          registrationOpened),
      switchMap(([action, competitionSid, registrationOpened]: [CompetitionDetailsActions, string, boolean]) =>
          this.http.post<Performance>(getPerformanceListUrl(competitionSid), action.payload).pipe(
              map((response) => new CreatePerformanceSuccess(response)),
              catchError((error) => of(new CreatePerformanceError(error)))
          ))
  );

  @Effect()
  public editPerformance$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.EDIT_PERFORMANCE_START),
      map((action: EditPerformanceStart) => action.payload),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([performance, competitionSid]: [Performance, string]) => {
        const sid = performance.sid;
        const properties = {
          ...performance,
          sid: undefined
        };
        return this.http.put<Performance>(getPerformanceUrl(competitionSid, sid), properties).pipe(
            map((response) => new EditPerformanceSuccess(response)),
            catchError((error) => of(new EditPerformanceError(error)))
        );
      })
  );

  @Effect()
  public fetchPerformancesOnCreate$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(
          CompetitionDetailsActionTypes.CREATE_PERFORMANCE_SUCCESS,
          CompetitionDetailsActionTypes.EDIT_PERFORMANCE_SUCCESS
      ),
      map(() => new PerformancesFetchStart())
  );

  @Effect({ dispatch: false })
  public performanceCreated$: Observable<any> = this.actions$.pipe(
      ofType(
          CompetitionDetailsActionTypes.CREATE_PERFORMANCE_SUCCESS,
          CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_SUCCESS,
          CompetitionDetailsActionTypes.EDIT_PERFORMANCE_SUCCESS
      ),
      tap(() => this.notificationService.success("Pomyślnie zapisano"))
  );

  @Effect()
  public deletePerformance$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.DELETE_PERFORMANCE_START),
      withLatestFrom(
          this.competitionDetailsService.competitionSid$,
          this.competitionDetailsService.registrationOpened$
      ),
      filter(([action, competitionSid, registrationOpened]: [CompetitionDetailsActions, string, boolean]) =>
          registrationOpened),
      switchMap(([action, competitionSid, registrationOpened]: [CompetitionDetailsActions, string, boolean]) =>
          this.http.delete(getPerformanceUrl(competitionSid, action.payload.sid)).pipe(
              map((response) => new DeletePerformanceSuccess(response)),
              catchError((error) => of(new DeletePerformanceError(error)))
          ))
  );

  @Effect({ dispatch: false })
  public performanceDeleted$: Observable<any> = this.actions$.pipe(
      ofType(
          CompetitionDetailsActionTypes.DELETE_PERFORMANCE_SUCCESS,
          CompetitionDetailsActionTypes.REMOVE_SONG_SUCCESS
      ),
      tap(() => this.notificationService.success("Pomyślnie usunięto"))
  );

  @Effect()
  public fetchStyles$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.STYLES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<CompetitionStyle[]>(getCompetitionStylesUrl(competitionSid)).pipe(
              map((response) => new StylesFetchSuccess(response)),
              catchError((error) => of(new StylesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchTypes$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.TYPES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<CompetitionType[]>(getCompetitionTypesUrl(competitionSid)).pipe(
              map((response) => new TypesFetchSuccess(response)),
              catchError((error) => of(new TypesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchExperiences$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.EXPERIENCES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<CompetitionExperience[]>(getCompetitionExperiencesUrl(competitionSid)).pipe(
              map((response) => new ExperiencesFetchSuccess(response)),
              catchError((error) => of(new ExperiencesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchAges$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.AGES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<CompetitionAge[]>(getCompetitionAgesUrl(competitionSid)).pipe(
              map((response) => new AgesFetchSuccess(response)),
              catchError((error) => of(new AgesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchRules$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.RULES_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<CompetitionRule[]>(getCompetitionRulesUrl(competitionSid)).pipe(
              map((response) => new RulesFetchSuccess(response)),
              catchError((error) => of(new RulesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchResults$: Observable<CompetitionDetailsActions> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.RESULTS_FETCH_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      switchMap(([action, competitionSid]: [CompetitionDetailsActions, string]) =>
          this.http.get<PerformanceSectionWrapper>(getCompetitionResultListUrl(competitionSid)).pipe(
              map((response) => new ResultsFetchSuccess(response)),
              catchError((error) => of(new ResultsFetchError(error)))
          )
      )
  );

  @Effect()
  public performanceSongUpload$: Observable<any> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_START),
      withLatestFrom(this.competitionDetailsService.competitionSid$),
      tap(([action, competitionSid]: [CompetitionDetailsActions, string]) => {
        const token = this.authService.getAuthToken();
        const url = `${getPerformanceSongUrl(competitionSid, action.payload.performanceSid)}`;
        const uploader = action.payload.uploader;
        uploader.setOptions({
          ...uploader.options,
          authToken: `JWT ${token}`,
          url
        });
        uploader.uploadAll();
      }),
      map(() => new StopSongStart())
  );

  @Effect()
  public removePerformanceSong$: Observable<any> = this.actions$.pipe(
      ofType(CompetitionDetailsActionTypes.REMOVE_SONG_START),
      switchMap((action: CompetitionDetailsActions) =>
          this.http.delete(getAudioUrl(action.payload.performance.audioSid)).pipe(
              switchMap(() => [
                new RemoveSongSuccess({ performance: action.payload.performance }),
                new StopSongStart()
              ]),
              catchError((error) => of(new RemoveSongError(error)))
          )
      )
  );
}