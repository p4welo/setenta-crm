import { append } from 'ramda';

import { CompetitionDetailsModuleState } from '../../model';
import { CompetitionDetailsActions, CompetitionDetailsActionTypes } from '../actions';

export const initialState: CompetitionDetailsModuleState = {
  performances: [],
  licence: {
    musicUpload: false,
    results: false
  },
  styles: [],
  types: [],
  experiences: [],
  ages: [],
  rules: [],
  loading: {
    competition: false,
    results: false
  },
  registration: {
    uploadInProgress: false,
    firstSuccessVisible: false
  }
};

export function reducers(
    state: CompetitionDetailsModuleState = initialState,
    action: CompetitionDetailsActions
): CompetitionDetailsModuleState {
  switch (action.type) {
    case CompetitionDetailsActionTypes.SELECT_COMPETITION_START:
      return {
        ...state,
        competitionSid: action.payload,
        loading: {
          ...state.loading,
          competition: true
        }
      };
    case CompetitionDetailsActionTypes.SELECT_COMPETITION_SUCCESS:
      return {
        ...state,
        competition: action.payload,
        loading: {
          ...state.loading,
          competition: false
        }
      };
    case CompetitionDetailsActionTypes.COMPETITION_LICENCE_FETCH_SUCCESS:
      return {
        ...state,
        licence: action.payload
      };
    case CompetitionDetailsActionTypes.RESULTS_FETCH_START:
      return {
        ...state,
        loading: {
          ...state.loading,
          results: true
        }
      };
    case CompetitionDetailsActionTypes.PERFORMANCES_FETCH_SUCCESS:
      return {
        ...state,
        performances: action.payload
      };
    case CompetitionDetailsActionTypes.CREATE_PERFORMANCE_SUCCESS:
      return {
        ...state,
        performances: append(action.payload, state.performances),
        registration: {
          ...state.registration,
          firstSuccessVisible: state.performances && state.performances.length < 1
        }
      };
    case CompetitionDetailsActionTypes.EDIT_PERFORMANCE_SUCCESS:
      return {
        ...state,
        performances: state.performances.map((p) => p.sid === action.payload.sid ? action.payload : p)
      };
    case CompetitionDetailsActionTypes.DELETE_PERFORMANCE_START:
      return {
        ...state,
        performances: state.performances.filter((performance) => performance.sid !== action.payload.sid)
      };
    case CompetitionDetailsActionTypes.STYLES_FETCH_SUCCESS:
      return {
        ...state,
        styles: action.payload
      };
    case CompetitionDetailsActionTypes.TYPES_FETCH_SUCCESS:
      return {
        ...state,
        types: action.payload
      };
    case CompetitionDetailsActionTypes.EXPERIENCES_FETCH_SUCCESS:
      return {
        ...state,
        experiences: action.payload
      };
    case CompetitionDetailsActionTypes.AGES_FETCH_SUCCESS:
      return {
        ...state,
        ages: action.payload
      };
    case CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_START:
      return {
        ...state,
        registration: {
          ...state.registration,
          uploadInProgress: true
        }
      };
    case CompetitionDetailsActionTypes.REMOVE_SONG_START:
      return {
        ...state,
        registration: {
          ...state.registration,
          removeAudioSid: action.payload.performance.sid
        }
      };
    case CompetitionDetailsActionTypes.REMOVE_SONG_SUCCESS:
      return {
        ...state,
        performances: state.performances.map((performance) => {
          if (performance.sid === action.payload.performance.sid) {
            return {
              ...performance,
              audioSid: undefined
            }
          }
          return performance;
        }),
        registration: {
          ...state.registration,
          removeAudioSid: undefined
        }
      };
    case CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_ERROR:
      return {
        ...state,
        registration: {
          ...state.registration,
          uploadInProgress: false
        }
      };
    case CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_SUCCESS:
      return {
        ...state,
        performances: state.performances.map((performance) => {
          if (performance.sid === action.payload.sid) {
            return {
              ...performance,
              audioSid: action.payload.audioSid
            }
          }
          else {
            return performance;
          }
        }),
        registration: {
          ...state.registration,
          uploadInProgress: false
        }
      };
    case CompetitionDetailsActionTypes.RULES_FETCH_SUCCESS:
      return {
        ...state,
        rules: action.payload
      };
    case CompetitionDetailsActionTypes.RESULTS_FETCH_SUCCESS:
      return {
        ...state,
        results: action.payload,
        loading: {
          ...state.loading,
          results: false
        }
      };
    default:
      return state;
  }
}