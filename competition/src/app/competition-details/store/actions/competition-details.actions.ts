import { Action } from '@ngrx/store';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Performance,
  PerformanceSectionWrapper
} from '@competition/model';

export const CompetitionDetailsActionTypes = {
  SELECT_COMPETITION_START: '[Competition Details] Competition select start',
  SELECT_COMPETITION_SUCCESS: '[Competition Details] Competition select success',
  SELECT_COMPETITION_ERROR: '[Competition Details] Competition select error',

  PERFORMANCES_FETCH_START: '[Competition Details] Performances fetch start',
  PERFORMANCES_FETCH_SUCCESS: '[Competition Details] Performances fetch success',
  PERFORMANCES_FETCH_ERROR: '[Competition Details] Performances fetch error',

  CREATE_PERFORMANCE_START: '[Competition Details] Create performance start',
  CREATE_PERFORMANCE_SUCCESS: '[Competition Details] Create performance success',
  CREATE_PERFORMANCE_ERROR: '[Competition Details] Create performance error',

  EDIT_PERFORMANCE_START: '[Competition Details] Edit performance start',
  EDIT_PERFORMANCE_SUCCESS: '[Competition Details] Edit performance success',
  EDIT_PERFORMANCE_ERROR: '[Competition Details] Edit performance error',

  DELETE_PERFORMANCE_START: '[Competition Details] Delete performance start',
  DELETE_PERFORMANCE_SUCCESS: '[Competition Details] Delete performance success',
  DELETE_PERFORMANCE_ERROR: '[Competition Details] Delete performance error',

  COMPETITION_LICENCE_FETCH_START: '[Competition Details] Licence fetch start',
  COMPETITION_LICENCE_FETCH_SUCCESS: '[Competition Details] Licence fetch success',
  COMPETITION_LICENCE_FETCH_ERROR: '[Competition Details] Licence fetch error',

  STYLES_FETCH_START: '[Competition Details] Styles fetch start',
  STYLES_FETCH_SUCCESS: '[Competition Details] Styles fetch success',
  STYLES_FETCH_ERROR: '[Competition Details] Styles fetch error',

  TYPES_FETCH_START: '[Competition Details] Types fetch start',
  TYPES_FETCH_SUCCESS: '[Competition Details] Types fetch success',
  TYPES_FETCH_ERROR: '[Competition Details] Types fetch error',

  EXPERIENCES_FETCH_START: '[Competition Details] Experiences fetch start',
  EXPERIENCES_FETCH_SUCCESS: '[Competition Details] Experiences fetch success',
  EXPERIENCES_FETCH_ERROR: '[Competition Details] Experiences fetch error',

  AGES_FETCH_START: '[Competition Details] Ages fetch start',
  AGES_FETCH_SUCCESS: '[Competition Details] Ages fetch success',
  AGES_FETCH_ERROR: '[Competition Details] Ages fetch error',

  RULES_FETCH_START: '[Competition Details] Rules fetch start',
  RULES_FETCH_SUCCESS: '[Competition Details] Rules fetch success',
  RULES_FETCH_ERROR: '[Competition Details] Rules fetch error',

  RESULTS_FETCH_START: '[Competition Details] Results fetch start',
  RESULTS_FETCH_SUCCESS: '[Competition Details] Results fetch success',
  RESULTS_FETCH_ERROR: '[Competition Details] Results fetch error',

  SONG_UPLOAD_WINDOW_SHOW: '[Competition Details] Song upload window show',

  PERFORMANCE_SONG_UPLOAD_START: '[Competition Details] Performance song upload start',
  PERFORMANCE_SONG_UPLOAD_SUCCESS: '[Competition Details] Performance song upload success',
  PERFORMANCE_SONG_UPLOAD_ERROR: '[Competition Details] Performance song upload error',

  REMOVE_SONG_START: '[Competition Details] Remove song start',
  REMOVE_SONG_SUCCESS: '[Competition Details] Remove song success',
  REMOVE_SONG_ERROR: '[Competition Details] Remove song error',
};
// PerformanceSongUploadStart
export class SelectCompetitionStart implements Action {
  public type: string = CompetitionDetailsActionTypes.SELECT_COMPETITION_START;

  constructor(public payload: string) {
  }
}

export class SelectCompetitionSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.SELECT_COMPETITION_SUCCESS;

  constructor(public payload: Competition) {
  }
}

export class SelectCompetitionError implements Action {
  public type: string = CompetitionDetailsActionTypes.SELECT_COMPETITION_ERROR;

  constructor(public payload: Error) {
  }
}

export class CompetitionLicenceFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.COMPETITION_LICENCE_FETCH_START;

  constructor(public payload: string) {
  }
}

export class CompetitionLicenceFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.COMPETITION_LICENCE_FETCH_SUCCESS;

  constructor(public payload: CompetitionLicence) {
  }
}

export class CompetitionLicenceFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.COMPETITION_LICENCE_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class PerformancesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class PerformancesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCES_FETCH_SUCCESS;

  constructor(public payload: Performance[]) {
  }
}

export class PerformancesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreatePerformanceStart implements Action {
  public type: string = CompetitionDetailsActionTypes.CREATE_PERFORMANCE_START;

  constructor(public payload?: any) {
  }
}

export class CreatePerformanceSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.CREATE_PERFORMANCE_SUCCESS;

  constructor(public payload: Performance) {
  }
}

export class CreatePerformanceError implements Action {
  public type: string = CompetitionDetailsActionTypes.CREATE_PERFORMANCE_ERROR;

  constructor(public payload: Error) {
  }
}

export class EditPerformanceStart implements Action {
  public type: string = CompetitionDetailsActionTypes.EDIT_PERFORMANCE_START;

  constructor(public payload: Performance) {
  }
}

export class EditPerformanceSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.EDIT_PERFORMANCE_SUCCESS;

  constructor(public payload: Performance) {
  }
}

export class EditPerformanceError implements Action {
  public type: string = CompetitionDetailsActionTypes.EDIT_PERFORMANCE_ERROR;

  constructor(public payload: Error) {
  }
}

export class DeletePerformanceStart implements Action {
  public type: string = CompetitionDetailsActionTypes.DELETE_PERFORMANCE_START;

  constructor(public payload: Performance) {
  }
}

export class DeletePerformanceSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.DELETE_PERFORMANCE_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class DeletePerformanceError implements Action {
  public type: string = CompetitionDetailsActionTypes.DELETE_PERFORMANCE_ERROR;

  constructor(public payload: Error) {
  }
}

export class StylesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.STYLES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class StylesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.STYLES_FETCH_SUCCESS;

  constructor(public payload: CompetitionStyle[]) {
  }
}

export class StylesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.STYLES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class TypesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.TYPES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class TypesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.TYPES_FETCH_SUCCESS;

  constructor(public payload: CompetitionType[]) {
  }
}

export class TypesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.TYPES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class ExperiencesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.EXPERIENCES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class ExperiencesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.EXPERIENCES_FETCH_SUCCESS;

  constructor(public payload: CompetitionExperience[]) {
  }
}

export class ExperiencesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.EXPERIENCES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class AgesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.AGES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AgesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.AGES_FETCH_SUCCESS;

  constructor(public payload: CompetitionAge[]) {
  }
}

export class AgesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.AGES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class RulesFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.RULES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class RulesFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.RULES_FETCH_SUCCESS;

  constructor(public payload: CompetitionRule[]) {
  }
}

export class RulesFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.RULES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class ResultsFetchStart implements Action {
  public type: string = CompetitionDetailsActionTypes.RESULTS_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class ResultsFetchSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.RESULTS_FETCH_SUCCESS;

  constructor(public payload: PerformanceSectionWrapper) {
  }
}

export class ResultsFetchError implements Action {
  public type: string = CompetitionDetailsActionTypes.RESULTS_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class PerformanceSongUploadStart implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_START;

  constructor(public payload: { uploader: any; performanceSid: string; }) {
  }
}

export class PerformanceSongUploadSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_SUCCESS;

  constructor(public payload: Performance) {
  }
}

export class PerformanceSongUploadError implements Action {
  public type: string = CompetitionDetailsActionTypes.PERFORMANCE_SONG_UPLOAD_ERROR;

  constructor(public payload: Error) {
  }
}

export class RemoveSongStart implements Action {
  public type: string = CompetitionDetailsActionTypes.REMOVE_SONG_START;

  constructor(public payload: { performance: Performance }) {
  }
}

export class RemoveSongSuccess implements Action {
  public type: string = CompetitionDetailsActionTypes.REMOVE_SONG_SUCCESS;

  constructor(public payload: { performance: Performance }) {
  }
}

export class RemoveSongError implements Action {
  public type: string = CompetitionDetailsActionTypes.REMOVE_SONG_ERROR;

  constructor(public payload: Error) {
  }
}

export type CompetitionDetailsActions =
    | SelectCompetitionStart
    | SelectCompetitionSuccess
    | SelectCompetitionError
    | CompetitionLicenceFetchStart
    | CompetitionLicenceFetchSuccess
    | CompetitionLicenceFetchError
    | PerformancesFetchStart
    | PerformancesFetchSuccess
    | PerformancesFetchError
    | CreatePerformanceStart
    | CreatePerformanceSuccess
    | CreatePerformanceError
    | StylesFetchStart
    | StylesFetchSuccess
    | StylesFetchError
    | TypesFetchStart
    | TypesFetchSuccess
    | TypesFetchError
    | ExperiencesFetchStart
    | ExperiencesFetchSuccess
    | ExperiencesFetchError
    | AgesFetchStart
    | AgesFetchSuccess
    | AgesFetchError
    | RulesFetchStart
    | RulesFetchSuccess
    | RulesFetchError
    | RemoveSongStart
    | RemoveSongSuccess
    | RemoveSongError
    | PerformanceSongUploadStart
    | PerformanceSongUploadSuccess
    | PerformanceSongUploadError
    ;