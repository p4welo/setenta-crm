import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompetitionDetailsService } from '@competition/competition-details/competition-details.service';
import { PaymentModels } from '@competition/core/constants/finance.const';
import { FinanceService } from '@competition/core/services';
import { path } from 'ramda';
import { first } from 'rxjs/operators';
import {
  filterDancerFees,
  getSortedList,
  getSortingColumnClass
} from '../../core/utils/table.utils';
import { Competition, CompetitionPayment, DancerPerformanceFee } from '../../model';
import { sumFees, sumPayments } from '../competition.utils';

@Component({
  selector: 'app-competition-finances',
  templateUrl: './competition-finances.component.html',
  styleUrls: ['./competition-finances.component.scss']
})
export class CompetitionFinancesComponent implements OnInit{

  public competition: Competition;
  public payments: CompetitionPayment[];
  public dancerFees: DancerPerformanceFee[];
  public sortingProperty: string = 'fee';
  public sortingAscending: boolean = false;
  public dancerFilter: string;

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private financeService: FinanceService,
      private competitionDetailsService: CompetitionDetailsService
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.competitionDetailsService.get(this.competitionSid)
        .subscribe((competition: Competition) => {
          this.competition = competition;
        });
    this.financeService.getCompetitionFee(this.competitionSid)
        .pipe(first())
        .subscribe((fees: DancerPerformanceFee[]) => {
          this.dancerFees = fees;
        });
    this.loadPayments();
  }

  public get overallFee(): number {
    return sumFees(this.dancerFees || []);
  }

  public get fullyPaid(): boolean {
    return this.overallFee - sumPayments(this.payments) <= 0;
  }

  public get isDancerModel(): boolean {
    return path(['paymentModel'], this.competition) === PaymentModels.DANCER_PERFORMANCE_AMOUNT;
  }

  public get isSingleDancerModel(): boolean {
    return path(['paymentModel'], this.competition) === PaymentModels.SINGLE_DANCER_ENTRANCE;
  }

  // SORTING
  public sortBy(property: string): void {
    if (this.sortingProperty === property) {
      this.sortingAscending = !this.sortingAscending;
    } else {
      this.sortingProperty = property;
      this.sortingAscending = true;
    }
  }

  public getSortingClass(property: string): string {
    return getSortingColumnClass(property, this.sortingProperty, this.sortingAscending);
  }

  // TABLE
  public get list(): DancerPerformanceFee[] {
    const filtered: DancerPerformanceFee[] = filterDancerFees(this.dancerFees || [], this.dancerFilter);
    return getSortedList(filtered, this.sortingProperty, this.sortingAscending);
  }

  private loadPayments(): void {
    this.financeService.getCompetitionPayments(this.competitionSid)
        .pipe(first())
        .subscribe(value => this.payments = value);
  }
}