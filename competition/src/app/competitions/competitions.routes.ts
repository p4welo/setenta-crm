import { Routes } from '@angular/router';
import { CompetitionsComponent } from './competitions.component';

export const routes: Routes = [
  { path: '', component: CompetitionsComponent, pathMatch: 'full' },
  { path: ':competitionSid', loadChildren: () => import('../competition-details/').then(m => m.CompetitionDetailsModule) }
];