import { Component, OnInit } from '@angular/core';
import { CompetitionsService } from '@competition/core/services';
import { getSortedList, isOutdated, sortDescendingByDate } from '@competition/core/utils';
import { Competition } from '../model';

@Component({
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.scss']
})
export class CompetitionsComponent implements OnInit {

  public competitions: Competition[];

  constructor(private competitionsService: CompetitionsService) {
  }

  public ngOnInit(): void {
    this.competitionsService.list().subscribe((data: Competition[]) => {
      this.competitions = data;
    });
  }

  public get list(): Competition[] {
    return getSortedList(this.competitions, 'start')
        .filter((competition: Competition) => !this.isOutdated(competition));
  }

  public get outdatedList(): Competition[] {
    return getSortedList(this.competitions, 'start')
        .filter((competition: Competition) => this.isOutdated(competition))
        .sort(sortDescendingByDate);
  }

  public isOutdated(competition: Competition): boolean {
    return isOutdated(competition);
  }
}