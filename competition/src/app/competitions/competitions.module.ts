import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { AuthModule } from '@competition/modules/auth';
import { CoreModule } from '@competition/core';

import { routes } from './competitions.routes';
import { CompetitionsComponent } from './competitions.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    AuthModule,
    HttpClientModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild(routes),
  ],
  providers: [],
  declarations: [
    CompetitionsComponent
  ]
})
export class CompetitionsModule {
  public static routes = routes;
}