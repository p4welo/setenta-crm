import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '@competition/model/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserProfileComponent implements OnInit {
  @Input() public user: User;
  @Input() public loading: boolean;

  @Output() public save: EventEmitter<User> = new EventEmitter<User>();

  public ngForm: FormGroup;
  public profileEdit: boolean = false;

  constructor(private formBuilder: FormBuilder) {
  }

  public ngOnInit() {
    this.ngForm = this.formBuilder.group({
      name: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      city: ['', Validators.required]
    });
  }

  public edit(): void {
    if (this.user) {
      const { firstName, lastName, name, city } = this.user;
      this.ngForm.setValue({
        firstName, lastName, name, city
      });
    }
    this.profileEdit = true;
  }

  public cancel(): void {
    this.profileEdit = false;
  }

  public onSave(user: User): void {
    this.save.emit(user);
    this.profileEdit = false;
  }
}