import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvoiceData } from '@competition/model';

@Component({
  selector: 'app-user-invoice-data',
  templateUrl: './user-invoice-data.component.html',
  styleUrls: ['./user-invoice-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserInvoiceDataComponent implements OnInit {
  @Input() public invoiceData: InvoiceData;
  @Input() public loading: boolean;

  @Output() public save: EventEmitter<InvoiceData> = new EventEmitter<InvoiceData>();

  public ngForm: FormGroup;
  public invoiceEdit: boolean = false;

  constructor(private formBuilder: FormBuilder) {
  }

  public ngOnInit() {
    this.ngForm = this.formBuilder.group({
      name: ['', Validators.required],
      nip: ['', Validators.required],
      street: ['', Validators.required],
      zip: ['', Validators.required],
      city: ['', Validators.required]
    });
  }

  public edit(): void {
    if (this.invoiceData) {
      const { name, nip, street, zip, city } = this.invoiceData;
      this.ngForm.setValue({
        name, nip, street, zip, city
      });
    }
    this.invoiceEdit = true;
  }

  public cancel(): void {
    this.invoiceEdit = false;
  }

  public onSave(invoiceData: InvoiceData): void {
    this.save.emit(invoiceData);
    this.invoiceEdit = false;
  }
}