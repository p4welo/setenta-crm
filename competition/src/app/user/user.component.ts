import { Component, OnInit } from '@angular/core';
import {
  FinanceService,
  NotificationService,
  UserService
} from '@competition/core/services';
import { InvoiceData, User } from '@competition/model';
import { AuthGetCurrentUserStart } from '@competition/modules/auth/actions';
import { AuthService } from '@competition/modules/auth/services';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public selectedTab: number = 0;
  public user$: Observable<User> = this.authService.currentUser$;
  public invoiceData$: Observable<InvoiceData>;

  public invoiceLoading: boolean = false;
  public userLoading: boolean = false;

  constructor(
      private financeService: FinanceService,
      private notificationService: NotificationService,
      private userService: UserService,
      private authService: AuthService,
      private store$: Store<any>
  ) {}

  public ngOnInit(): void {
    this.loadInvoiceData();
  }

  public onInvoiceDataSave(invoiceData: InvoiceData): void {
    this.invoiceLoading = true;
    this.financeService.saveUserInvoiceData(invoiceData).subscribe(() => {
      this.notificationService.success("Pomyślnie zapisano");
      this.loadInvoiceData();
      this.invoiceLoading = false;
    })
  }

  public onUserSave(user: User): void {
    this.userLoading = true;
    this.userService.update(user).subscribe(() => {
        this.notificationService.success("Pomyślnie zapisano");
        this.loadUser();
        this.userLoading = false;
    })
  }

  private loadUser(): void {
    this.store$.dispatch(new AuthGetCurrentUserStart());
  }

  private loadInvoiceData(): void {
    this.invoiceData$ = this.financeService.getUserInvoiceData();
  }
}