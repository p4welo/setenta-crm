import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from '@competition/core';
import { AuthModule } from '@competition/modules/auth';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { UserInvoiceDataComponent, UserProfileComponent } from './components';
import { routes } from './user.routes';
import { UserComponent } from './user.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    AuthModule,
    ReactiveFormsModule,
    TranslateModule,
    NgxMaskModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    UserComponent,
    UserInvoiceDataComponent,
    UserProfileComponent
  ],
  providers: []
})
export class UserModule {

}