import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from "@ngx-translate/core";
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { NgxMaskModule } from 'ngx-mask'
import {
  CompetitionStatusLabelComponent,
  ConfirmModalComponent,
  DancerFormComponent,
  LabeledFieldComponent,
  LoadingContainerComponent,
  MainPageComponent,
  NavbarComponent,
  PerformanceNameCellComponent,
  SelectFieldComponent,
  SidebarComponent,
  TextFieldComponent
} from './components';
import { FooterComponent } from './components/footer';
import {
  AdminCompetitionService,
  CompetitionsService,
  FinanceService,
  NavigationService,
  NotificationService,
  PerformanceService,
  PrintService,
  SessionService,
  UserService
} from './services';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  declarations: [
    MainPageComponent,
    NavbarComponent,
    ConfirmModalComponent,
    LabeledFieldComponent,
    TextFieldComponent,
    SelectFieldComponent,
    FooterComponent,
    SidebarComponent,
    DancerFormComponent,
    CompetitionStatusLabelComponent,
    PerformanceNameCellComponent,
    LoadingContainerComponent
  ],
  exports: [
    MainPageComponent,
    NavbarComponent,
    ConfirmModalComponent,
    LabeledFieldComponent,
    TextFieldComponent,
    SelectFieldComponent,
    FooterComponent,
    SidebarComponent,
    DancerFormComponent,
    CompetitionStatusLabelComponent,
    PerformanceNameCellComponent,
    LoadingContainerComponent
  ],
  providers: [
    AdminCompetitionService,
    NavigationService,
    SessionService,
    NotificationService,
    PerformanceService,
    UserService,
    FinanceService,
    CompetitionsService,
    PrintService
  ]
})
export class CoreModule {
}
