import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { registrationEnded, registrationInProgress } from '@competition/core/utils';
import { Competition } from '@competition/model';

@Component({
  selector: 'app-competition-status-label',
  templateUrl: './competition-status-label.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionStatusLabelComponent {
  @Input() public competition: Competition;

  public get registrationInProgress(): boolean {
    if (this.competition) {
      return registrationInProgress(this.competition);
    }
  }

  public get registrationEnded(): boolean {
    if (this.competition) {
      return registrationEnded(this.competition);
    }
  }
}