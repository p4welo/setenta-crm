import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Input() public simpleVersion: boolean = false;
  @Output() public onLogout: EventEmitter<any> = new EventEmitter<any>();

  public isVisibleOnMobile: boolean = false;

  constructor() { }

  public toggle(): void {
    this.isVisibleOnMobile = !this.isVisibleOnMobile;
  }

  public logout(): void {
    this.onLogout.emit();
  }
}
