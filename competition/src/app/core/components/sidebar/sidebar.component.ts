import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthGetCurrentUserStart } from '@competition/modules/auth/actions';
import { AuthService } from '@competition/modules/auth/services';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../../model';
import { UserService } from '../../services';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit{

  public user$: Observable<User> = this.authService.currentUser$;

  constructor(
      private userService: UserService,
      private authService: AuthService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AuthGetCurrentUserStart());
  }

  public get isAdminUser(): boolean {
    return this.authService.isAdminUser();
  }
}