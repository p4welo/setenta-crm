import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trim, path } from 'ramda';
import { Dancer_ } from '../../../model';

@Component({
  selector: 'app-dancer-form',
  templateUrl: './dancer-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DancerFormComponent implements OnInit {

  @Input()
  public loading: boolean = false;

  @Output()
  public save: EventEmitter<Dancer_> = new EventEmitter<Dancer_>();

  @Output()
  public cancel: EventEmitter<void> = new EventEmitter<void>();

  public ngForm: FormGroup;
  public showErrors: boolean = false;

  constructor(
      private formBuilder: FormBuilder
  ) {
  }

  public ngOnInit(): void {
    this.initForm();
  }

  public onSubmit(dancer: Dancer_): void {
    if (this.ngForm.valid) {
      this.save.emit({
        firstName: trim(dancer.firstName),
        lastName: trim(dancer.lastName),
        yearOfBirth: trim(dancer.yearOfBirth),
        groupName: trim(dancer.groupName)
      });
      this.initForm();
    } else {
      this.showErrors = true;
    }
  }

  public onCancelClick(): void {
    this.cancel.emit();
  }

  public isInvalid(field: string): boolean {
    return this.showErrors && this.ngForm.get(field).invalid;
  }

  public getError(field: string): any {
    return path(['errors'], this.ngForm.get(field));
  }

  private initForm(): void {
    const currentYear = new Date().getFullYear();
    this.ngForm = this.formBuilder.group({
      firstName: ['', [
        Validators.required,
        Validators.maxLength(15)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.maxLength(25)
      ]],
      yearOfBirth: ['', [
        Validators.required,
        Validators.min(currentYear - 100),
        Validators.max(currentYear)
      ]],
      groupName: [''],
    });
  }
}
