import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Language, LANGUAGES } from '../../../model/language.model';

@Component({
  selector: 'app-main-page',
  styleUrls: ['./main-page.component.scss'],
  templateUrl: './main-page.component.html'
})
export class MainPageComponent implements OnInit, OnDestroy {

  @Input() public title: string;
  @Input() public subtitle: string;
  @Input() public iconId: string;
  @Input() public backTo: string;
  @Input() public menu: boolean = false;

  public languages: Language[] = LANGUAGES;
  public menuCollapsed: boolean = true;
  public sidebarVisible: boolean = false;
  public urlChangeSubscription: Subscription = new Subscription();

  constructor(
      private translate: TranslateService,
      private router: Router
  ) {}

  public ngOnInit(): void {
    if (this.menu) {
      this.urlChangeSubscription.add(
          this.router.events
              .pipe(filter(e => e instanceof RouterEvent))
              .subscribe(() => this.menuCollapsed = true)
      );
    }
  }

  public ngOnDestroy(): void {
    this.urlChangeSubscription.unsubscribe();
  }

  public toggleSidebar(): void {
    this.sidebarVisible = !this.sidebarVisible;
  }

  public setLanguage(language: Language): void {
    this.translate.use(language.code);
  }
}