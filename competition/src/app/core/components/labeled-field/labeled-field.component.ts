import {
  AfterContentInit,
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  Input
} from '@angular/core';
import { FormFieldComponent } from './form-field.component';

@Component({
  selector: 'app-labeled-field',
  templateUrl: './labeled-field.component.html',
  styleUrls: ['./labeled-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LabeledFieldComponent implements AfterContentInit, AfterViewChecked {

  @ContentChild(FormFieldComponent, { static: false })
  public formField: FormFieldComponent;

  @Input()
  public invalid: boolean = false;

  @Input()
  public error: any;

  @Input()
  public margins: boolean = true;

  public label: string;
  public pinned: boolean = false;
  public required: boolean = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  public ngAfterContentInit(): void {
    this.label = this.formField.placeholder;
    this.required = this.formField.required;
    this.pinned = this.isNotNull();

    this.formField.stateChanges.subscribe(() => {
      this.pinned = this.isNotNull();
    });
  }

  public ngAfterViewChecked(): void {
    this.pinned = this.isNotNull();
    this.changeDetectorRef.detectChanges();
  }

  public get errorKey(): string {
    if (this.invalid) {
      if (this.error.required) {
        return 'common.labeled-field.error.required';
      }
      if (this.error.email) {
        return 'common.labeled-field.error.email';
      }
      if (this.error.passwordMatch) {
        return 'common.labeled-field.error.passwordMatch';
      }
      if (this.error.min) {
        return 'common.labeled-field.error.min';
      }
      if (this.error.max) {
        return 'common.labeled-field.error.max';
      }
      if (this.error.minlength) {
        return 'common.labeled-field.error.minLength';
      }
      if (this.error.maxlength) {
        return 'common.labeled-field.error.maxlength';
      }
    }
  }

  private isNotNull(): boolean {
    return this.formField.value !== '';
  }
}