import { Subject } from 'rxjs';

export abstract class FormFieldComponent {

  public readonly stateChanges: Subject<void>;
  public placeholder: string;
  public required: boolean;

  abstract get value(): string;
}
