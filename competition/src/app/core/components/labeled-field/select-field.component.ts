import { Component, ElementRef, HostListener, Input, OnDestroy } from '@angular/core';
import { FormFieldComponent } from '@competition/core/components/labeled-field/form-field.component';
import { Subject } from 'rxjs';

@Component({
  selector: '[appSelectField]',
  template: '<ng-content></ng-content>',
  styleUrls: ['./select-field.component.scss'],
  providers: [{provide: FormFieldComponent, useExisting: SelectFieldComponent}]
})
export class SelectFieldComponent implements FormFieldComponent, OnDestroy {

  @Input()
  public placeholder: string;

  @Input()
  public required: boolean;

  public readonly stateChanges: Subject<void> = new Subject();

  constructor(protected elementRef: ElementRef) {
  }

  public ngOnDestroy(): void {
    this.stateChanges.next();
    this.stateChanges.unsubscribe();
  }

  @HostListener('input')
  public handleInput(): void {
    this.stateChanges.next();
  }

  get value(): string {
    return this.elementRef.nativeElement.value;
  }
}