import { Component, ElementRef, HostListener, Input } from '@angular/core';
import { FormFieldComponent } from './form-field.component';
import { Subject } from 'rxjs';

@Component({
  selector: '[appTextField]',
  template: '<ng-content></ng-content>',
  styleUrls: ['./text-field.component.scss'],
  providers: [{provide: FormFieldComponent, useExisting: TextFieldComponent}]
})
export class TextFieldComponent implements FormFieldComponent {

  @Input()
  public placeholder: string;

  @Input()
  public required: boolean;

  public readonly stateChanges: Subject<void> = new Subject<void>();

  constructor(protected elementRef: ElementRef) {
  }

  @HostListener('blur')
  public handleBlur(): void {
    this.stateChanges.next();
  }

  get value(): string {
    return this.elementRef.nativeElement.value;
  }
}