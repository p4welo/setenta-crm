import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CompetitionType, Performance } from '../../../model';
import { path, find, propEq } from 'ramda';

@Component({
  selector: 'app-performance-name-cell',
  templateUrl: './performance-name-cell.component.html',
  styleUrls: ['./performance-name-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerformanceNameCellComponent {
  @Input() public performance: Performance;
  @Input() public types: CompetitionType[] = [];

  public get showName(): boolean {
    if (this.types && this.performance) {
      const name = path(['type'], this.performance);
      const found: CompetitionType = find(propEq('name', name))(this.types);
      return found && found.showName;
    }
    return false;
  }
}