import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading-container',
  templateUrl: './loading-container.component.html'
})
export class LoadingContainerComponent {
  @Input() public loaded: boolean;
}