import { Injectable } from '@angular/core';
import PNotify from 'pnotify/dist/es/PNotify';
import PNotifyButtons from 'pnotify/dist/es/PNotifyButtons';

@Injectable()
export class NotificationService {

  private pnotify: PNotify;

  constructor() {
    PNotifyButtons; // Initiate the module. Important!
    this.pnotify = PNotify;
  }

  public success(message: string): void {
    this.pnotify.success({
      addClass: 'bg-success',
      title: 'Sukces',
      text: message
    });
  }

  public error(message: string): void {
    this.pnotify.error({
      addClass: 'bg-danger',
      title: 'Błąd',
      text: message
    });
  }
}