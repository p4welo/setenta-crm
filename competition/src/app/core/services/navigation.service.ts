import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class NavigationService {

  constructor(
      private router: Router
  ) {}

  public navigateToHomePage(): Promise<boolean> {
    return this.router.navigate(['/']);
  }

  public navigateToLoginPage(): Promise<boolean> {
    return this.router.navigate(['/login']);
  }

  public navigateToAdminCompetitionDetails(competition): Promise<boolean> {
    return this.router.navigate(['/admin/competitions', competition.sid]);
  }
}
