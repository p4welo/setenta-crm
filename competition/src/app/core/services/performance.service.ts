import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getPerformanceListUrl, getPerformanceUrl } from '../utils/index';
import { Performance } from '../../model/index';
import { Observable } from 'rxjs';

@Injectable()
export class PerformanceService {

  constructor(
      private http: HttpClient) {
  }

  public create(performance: Performance, competitionSid: string): Observable<any> {
    return this.http.post(getPerformanceListUrl(competitionSid), performance);
  }

  public delete(performance: Performance, competitionSid: string): Observable<any> {
    return this.http.delete(getPerformanceUrl(competitionSid, performance.sid));
  }
}