import { Injectable } from '@angular/core';

@Injectable()
export class PrintService {

  constructor() {
  }

  public print(): void {
    const titleBackup = document.title;
    document.title = 'www.kartazgloszen.pl - Skuteczna automatyzacja Twojego turnieju';
    window.print();
    document.title = titleBackup;
  }
}