import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../model';
import { getCurrentUserUrl } from '../utils';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  constructor(
      private http: HttpClient) {
  }

  public update(user: User): Observable<User> {
    return this.http.put<User>(getCurrentUserUrl(), user);
  }
}