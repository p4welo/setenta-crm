import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  Competition,
  PerformanceSection,
  PerformanceSectionWrapper,
  CompetitionRule, CompetitionType, CompetitionStyle, CompetitionExperience, CompetitionAge
} from '../../model';
import {
  getCompetitionListUrl,
  getCompetitionStartingListUrl,
  getCompetitionPerformanceSectionsUrl,
  getCompetitionRulesUrl,
  getMyCompetitionsUrl,
  getCompetitionPerformanceSectionUrl,
  getGenerateCompetitionStartingListUrl,
  getCompetitionResultListUrl,
  getCompetitionUrl,
  getCompetitionTypesUrl,
  getCompetitionStylesUrl,
  getCompetitionExperiencesUrl,
  getCompetitionAgesUrl, adminGetPerformancesByDayReport
} from '../utils';
import { Observable } from 'rxjs';

@Injectable()
export class CompetitionsService {

  constructor(
      private http: HttpClient) {
  }

  public list(): Observable<Competition[]> {
    return this.http.get<Competition[]>(getCompetitionListUrl());
  }

  public myCompetitions(): Observable<Competition[]> {
    return this.http.get<Competition[]>(getMyCompetitionsUrl());
  }

  public getCompetitionPerformanceSections(competitionSid: string): Observable<PerformanceSectionWrapper> {
    return this.http.get<PerformanceSectionWrapper>(getCompetitionPerformanceSectionsUrl(competitionSid));
  }

  public createCompetitionPerformanceSection(competitionSid: string, section: PerformanceSection): Observable<PerformanceSection> {
    return this.http.post<PerformanceSection>(getCompetitionPerformanceSectionsUrl(competitionSid), section);
  }

  // TODO: move me to admin service
  public removeCompetitionPerformanceSection(competitionSid: string, sectionSid: string): Observable<any> {
    return this.http.delete<any>(getCompetitionPerformanceSectionUrl(competitionSid, sectionSid));
  }

  public getCompetitionStartingList(competitionSid: string, userSid: string): Observable<PerformanceSectionWrapper> {
    let params = new HttpParams();
    if (userSid) {
      params = params.append('userSid', userSid);
    }
    return this.http.get<PerformanceSectionWrapper>(
        getCompetitionStartingListUrl(competitionSid),
        { params }
    );
  }

  public getCompetition(competitionSid: string): Observable<Competition> {
    return this.http.get<Competition>(getCompetitionUrl(competitionSid));
  }

  public getCompetitionResultList(competitionSid: string): Observable<PerformanceSectionWrapper> {
    return this.http.get<PerformanceSectionWrapper>(getCompetitionResultListUrl(competitionSid));
  }

  public generateCompetitionStartingList(competitionSid: string): Observable<PerformanceSectionWrapper> {
    return this.http.get<PerformanceSectionWrapper>(getGenerateCompetitionStartingListUrl(competitionSid));
  }

  public getRules(competitionSid: string): Observable<CompetitionRule[]> {
    return this.http.get<CompetitionRule[]>(getCompetitionRulesUrl(competitionSid));
  }

  public getCompetitionTypes(competitionSid: string): Observable<CompetitionType[]> {
    return this.http.get<CompetitionType[]>(getCompetitionTypesUrl(competitionSid));
  }

  public getCompetitionStyles(competitionSid: string): Observable<CompetitionStyle[]> {
    return this.http.get<CompetitionStyle[]>(getCompetitionStylesUrl(competitionSid));
  }

  public getCompetitionExperiences(competitionSid: string): Observable<CompetitionExperience[]> {
    return this.http.get<CompetitionExperience[]>(getCompetitionExperiencesUrl(competitionSid));
  }

  public getCompetitionAges(competitionSid: string): Observable<CompetitionAge[]> {
    return this.http.get<CompetitionAge[]>(getCompetitionAgesUrl(competitionSid));
  }

  public adminGetPerformancesByDayReport(competitionSid: string): Observable<any> {
    return this.http.get<any>(adminGetPerformancesByDayReport(competitionSid));
  }
}