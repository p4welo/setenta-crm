import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChartDataElement } from '../../home/components/admin-performances-chart/chart.model';
import {
  adminGetCompetitionListUrl,
  adminGetCompetitionPerformances, adminGetCompetitionUserDancers,
  adminGetCompetitionUserPerformances,
  adminGetCompetitionUsers,
  adminGetPerformanceBlocks, adminGetPerformancesByDayReport,
  getCompetitionAgesUrl,
  getCompetitionExperiencesUrl,
  getCompetitionInvoiceDatas,
  getCompetitionRuleUrl,
  getCompetitionStylesUrl,
  getCompetitionTypesUrl,
  getCompetitionUrl,
  getMergeCompetitionPerformanceSectionsUrl,
  getPaymentsUrl,
  getSwapCompetitionPerformanceSectionsUrl,
  getUserInvoiceDataUrl,
  getUserPaymentsUrl,
  getUserSchoolUrl
} from '../utils';
import {
  AdminCompetitionParticipant,
  Competition,
  CompetitionAge,
  CompetitionExperience,
  CompetitionPayment,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  InvoiceData,
  Performance,
  School,
} from '../../model';
import { Observable } from 'rxjs';

@Injectable()
export class AdminCompetitionService {

  constructor(private http: HttpClient) {
  }

  public getMyList(): Observable<Competition[]> {
    return this.http.get<Competition[]>(adminGetCompetitionListUrl());
  }

  public getCompetition(competitionSid: string): Observable<Competition> {
    return this.http.get<Competition>(getCompetitionUrl(competitionSid));
  }

  public getCompetitionStyles(competitionSid: string): Observable<CompetitionStyle[]> {
    return this.http.get<CompetitionStyle[]>(getCompetitionStylesUrl(competitionSid));
  }

  public getCompetitionExperiences(competitionSid: string): Observable<CompetitionExperience[]> {
    return this.http.get<CompetitionExperience[]>(getCompetitionExperiencesUrl(competitionSid));
  }

  public getCompetitionAges(competitionSid: string): Observable<CompetitionAge[]> {
    return this.http.get<CompetitionAge[]>(getCompetitionAgesUrl(competitionSid));
  }

  public getCompetitionParticipants(competitionSid: string): Observable<AdminCompetitionParticipant[]> {
    return this.http.get<AdminCompetitionParticipant[]>(adminGetCompetitionUsers(competitionSid));
  }

  public getCompetitionInvoiceDatas(competitionSid: string): Observable<any[]> {
    return this.http.get<any[]>(getCompetitionInvoiceDatas(competitionSid));
  }

  public getPerformanceCategoriesUrl(competitionSid: string): Observable<Performance[]> {
    return this.http.get<Performance[]>(adminGetCompetitionPerformances(competitionSid));
  }

  public getUserPerformances(competitionSid: string, userSid: string): Observable<Performance[]> {
    return this.http.get<Performance[]>(adminGetCompetitionUserPerformances(competitionSid, userSid));
  }

  public getUserDancers(competitionSid: string, userSid: string): Observable<any[]> {
    return this.http.get<any[]>(adminGetCompetitionUserDancers(competitionSid, userSid));
  }

  public getUserInvoiceData(userSid: string): Observable<InvoiceData> {
    return this.http.get<InvoiceData>(getUserInvoiceDataUrl(userSid));
  }

  public getUserSchool(userSid: string): Observable<School> {
    return this.http.get<School>(getUserSchoolUrl(userSid));
  }

  public getUserPayments(competitionSid: string, userSid: string): Observable<CompetitionPayment[]> {
    return this.http.get<CompetitionPayment[]>(getUserPaymentsUrl(competitionSid, userSid));
  }

  public createUserPayment(competitionSid: string, payment: CompetitionPayment): Observable<CompetitionPayment> {
    return this.http.post<CompetitionPayment>(getPaymentsUrl(competitionSid), payment);
  }

  public getPerformanceBlocks(competitionSid: string): Observable<any[]> {
    return this.http.get<any[]>(adminGetPerformanceBlocks(competitionSid));
  }

  public savePerformanceBlocks(competitionSid: string, performanceBlocks: any[]): Observable<any[]> {
    return this.http.post<any[]>(adminGetPerformanceBlocks(competitionSid), performanceBlocks);
  }

  public saveCompetitionRule(competitionSid: string, parentSid: string, rule: CompetitionRule): Observable<any> {
    return this.http.post<any>(getCompetitionRuleUrl(competitionSid, parentSid), rule);
  }

  public removeCompetitionRule(competitionSid: string, ruleSid: string): Observable<any> {
    return this.http.delete(getCompetitionRuleUrl(competitionSid, ruleSid));
  }

  public saveCompetitionStyle(competitionSid: string, style: CompetitionStyle): Observable<any> {
    return this.http.post<any>(getCompetitionStylesUrl(competitionSid), style);
  }

  public updateCompetition(competitionSid: string, properties: any): Observable<Competition> {
    return this.http.put<Competition>(getCompetitionUrl(competitionSid), properties);
  }

  public saveCompetitionType(competitionSid: string, type: CompetitionType): Observable<any> {
    return this.http.post<any>(getCompetitionTypesUrl(competitionSid), type);
  }

  public saveCompetitionExperience(competitionSid: string, experience: CompetitionExperience): Observable<any> {
    return this.http.post<any>(getCompetitionExperiencesUrl(competitionSid), experience);
  }

  public saveCompetitionAge(competitionSid: string, age: CompetitionAge): Observable<any> {
    return this.http.post<any>(getCompetitionAgesUrl(competitionSid), age);
  }

  public swapSections(competitionSid: string, originSid: string, destinationSid: string): Observable<any> {
    return this.http.put<any>(getSwapCompetitionPerformanceSectionsUrl(competitionSid, originSid), {destinationSid});
  }

  public mergeSections(competitionSid: string, originSid: string, destinationSid: string): Observable<any> {
    return this.http.put<any>(getMergeCompetitionPerformanceSectionsUrl(competitionSid, originSid), {destinationSid});
  }

  public getPerformancesByDayReport(competitionSid: string): Observable<ChartDataElement[]> {
    return this.http.get<ChartDataElement[]>(adminGetPerformancesByDayReport(competitionSid));
  }
}