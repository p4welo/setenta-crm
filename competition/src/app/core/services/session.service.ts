import { Injectable } from '@angular/core';

/**
 * @deprecated use storageModule instead
 */
@Injectable()
export class SessionService {

  private static readonly AUTH_TOKEN = 'setenta.authToken';
  private DEFAULT_COOKIE_EXPIRY: number = 600; // 10 mins

  private getSessionStorage(key: string): string {
    let value: string = null;
    const storage = this.getStorage();
    if (storage.getItem(key)) {
      value = storage.getItem(key);
    } else {
      document.cookie.split(';').some((item) => {
        const keyPosition = item.indexOf(key);
        if (item.indexOf(key) > -1) {
          // keyPosition can be 0 or 1, +1 for the '=' added after the key.
          value = decodeURIComponent(item.substring(keyPosition + key.length + 1, item.length));
          return true;
        }
        return false;
      });
    }
    return value;
  }

  private setSessionStorage(key: string, value: string, expiresIn?: number): void {
    try {
      this.getStorage().setItem(key, value);
    } catch (e) {
      const now = new Date();
      const expires: number = expiresIn || this.DEFAULT_COOKIE_EXPIRY;
      now.setTime(now.getTime() + (expires * 1000));
      document.cookie = `${key}=${encodeURIComponent(value)}; expires=${expires}; path=/`;
    }
  }

  private removeSessionStorage(key: string): void {
    this.getStorage().removeItem(key);
  }

  public getAuthToken(): string {
    return this.getSessionStorage(SessionService.AUTH_TOKEN);
  }

  public setAuthToken(token: string): void {
    return this.setSessionStorage(SessionService.AUTH_TOKEN, token);
  }

  public clearAuthToken(): void {
    return this.removeSessionStorage(SessionService.AUTH_TOKEN);
  }

  private getStorage() {
    return window.sessionStorage;
  }
}