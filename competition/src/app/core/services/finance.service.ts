import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CompetitionPayment, InvoiceData } from '../../model';
import { getCompetitionFeeUrl, getMyInvoiceDataUrl, getPaymentsUrl } from '../utils';

@Injectable()
export class FinanceService {
  constructor(
      private http: HttpClient) {
  }

  public getCompetitionFee(competitionId: string): Observable<any> {
    return this.http.get(getCompetitionFeeUrl(competitionId));
  }

  public getCompetitionPayments(competitionId: string): Observable<any> {
    return this.http.get<CompetitionPayment[]>(getPaymentsUrl(competitionId));
  }

  public getUserInvoiceData(): Observable<InvoiceData> {
    return this.http.get<InvoiceData>(getMyInvoiceDataUrl());
  }

  public saveUserInvoiceData(invoiceData: InvoiceData): Observable<InvoiceData> {
    return this.http.put<InvoiceData>(getMyInvoiceDataUrl(), invoiceData);
  }
}