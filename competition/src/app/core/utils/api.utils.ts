import { getServerUrl } from './utils';

// ADMIN
export const getAdminUrl = () => `${getServerUrl()}/api/v2/admin`;
export const adminGetCompetitionListUrl = () => `${getAdminUrl()}/competitions`;
export const adminGetCompetitionUrl = (id: string) => `${adminGetCompetitionListUrl()}/${id}`;
export const adminGetCompetitionSchools = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/users`;
export const adminGetCompetitionPerformances = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/performances`;
export const adminGetCompetitionPerformanceCategories = (competitionId: string) => `${adminGetCompetitionPerformances(competitionId)}/categories`;
export const adminGetCompetitionPerformancesCount = (competitionId: string) => `${adminGetCompetitionPerformances(competitionId)}/count`;
export const adminGetCompetitionUsers = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/users`;
export const adminGetCompetitionAudio = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/audio`;
export const adminGetCompetitionReport = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/report`;
export const adminGetCompetitionDancers = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/dancers`;
export const adminGetCompetitionDancersCount = (competitionId: string) => `${adminGetCompetitionDancers(competitionId)}/count`;
export const adminGetPerformancesByDayReport = (competitionId: string) => `${adminGetCompetitionReport(competitionId)}/performancesByDay`;
export const adminGetPerformanceBlocks = (competitionId: string) => `${adminGetCompetitionUrl(competitionId)}/performance-blocks`;
export const adminGetCompetitionUserPerformances = (competitionId: string, userId: string) => `${adminGetCompetitionUsers(competitionId)}/${userId}/performances`;
export const adminGetCompetitionUserDancers = (competitionId: string, userId: string) => `${adminGetCompetitionUsers(competitionId)}/${userId}/dancers`;


// SCHOOL
export const getSchoolUrl = () => `${getServerUrl()}/api/v2`;
export const getRegistrationUrl = () => `${getSchoolUrl()}/auth/register`;
export const getActivationUrl = (id: string) => `${getSchoolUrl()}/auth/${id}/activate`;
export const getLoginUrl = () => `${getSchoolUrl()}/auth/login`;
export const getResetPasswordUrl = () => `${getSchoolUrl()}/auth/reset`;
export const getNewPasswordUrl = (sid: string) => `${getResetPasswordUrl()}/${sid}`;


export const getUserListUrl = () => `${getSchoolUrl()}/users`;
export const getUserUrl = (id: string) => `${getUserListUrl()}/${id}`;
export const getUserInvoiceDataUrl = (id: string) => `${getUserUrl(id)}/invoiceData`;
export const getUserSchoolUrl = (id: string) => `${getUserUrl(id)}/schools`;
export const getCurrentUserUrl = () => `${getUserListUrl()}/current`;
export const getMyInvoiceDataUrl = () => `${getCurrentUserUrl()}/invoiceData`;
export const getMyCompetitionsUrl = () => `${getCurrentUserUrl()}/competitions`;

export const getDancerListUrl = () => `${getSchoolUrl()}/dancers`;
export const getDancerUrl = (id: string) => `${getDancerListUrl()}/${id}`;

export const getAudioUrl = (sid: string) => `${getSchoolUrl()}/audio/${sid}`;

// export const getPerformanceSongUrl = (sid: string) => `${getSchoolUrl()}/performances/${sid}/songs`;

export const getCompetitionListUrl = () => `${getSchoolUrl()}/competitions`;
export const getCompetitionUrl = (id: string) => `${getCompetitionListUrl()}/${id}`;
export const getCompetitionLicenceUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/licence`;
export const getCompetitionStylesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/styles`;
export const getCompetitionTypesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/types`;
export const getCompetitionExperiencesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/experiences`;
export const getCompetitionAgesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/ages`;
export const getPaymentsUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/payments`;
export const getUserPaymentsUrl = (competitionId: string, userSid: string) => `${getPaymentsUrl(competitionId)}?userSid=${userSid}`;

export const getCompetitionFinancesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/finances`;
export const getCompetitionRulesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/rules`;
export const getGenerateCompetitionRulesUrl = (competitionId: string) => `${getCompetitionRulesUrl(competitionId)}/auto-fill`;
export const getCompetitionRuleUrl = (competitionId: string, ruleId: string) => `${getCompetitionRulesUrl(competitionId)}/${ruleId}`;
export const getCompetitionInvoicesUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/invoices`;
export const getCompetitionInvoiceUrl = (competitionId: string, invoiceSid: string) => `${getCompetitionInvoicesUrl(competitionId)}/${invoiceSid}`;
export const getCompetitionInvoicePdfUrl = (competitionId: string, invoiceSid: string) => `${getCompetitionInvoiceUrl(competitionId, invoiceSid)}/pdf`;
export const getCompetitionPerformanceSectionsUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/performance-sections`;
export const getCompetitionPerformanceSectionUrl = (competitionId: string, sectionId: string) => `${getCompetitionPerformanceSectionsUrl(competitionId)}/${sectionId}`;
export const getSwapCompetitionPerformanceSectionsUrl = (competitionId: string, sectionId: string) => `${getCompetitionPerformanceSectionUrl(competitionId, sectionId)}/swap`;
export const getMergeCompetitionPerformanceSectionsUrl = (competitionId: string, sectionId: string) => `${getCompetitionPerformanceSectionUrl(competitionId, sectionId)}/merge`;
export const getCompetitionStartingListUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/starting-list`;
export const getCompetitionResultListUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/results`;
export const getGenerateCompetitionStartingListUrl = (competitionId: string) => `${getCompetitionStartingListUrl(competitionId)}/generate`;
export const getPerformanceListUrl = (competitionId: string) => `${getCompetitionUrl(competitionId)}/performances`;

export const getCompetitionFeeUrl = (competitionId: string) => `${getCompetitionFinancesUrl(competitionId)}/fee`;
export const getCompetitionInvoiceDatas = (competitionId: string) => `${getCompetitionFinancesUrl(competitionId)}/invoice-datas`;
export const getPerformanceUrl = (competitionId: string, id: string) => `${getPerformanceListUrl(competitionId)}/${id}`;
export const getPerformanceSongUrl = (competitionId: string, performanceSid: string) => `${getPerformanceUrl(competitionId, performanceSid)}/audio`;
