import { environment } from '../../../environments/environment';

export const isProduction = () => !!environment.production;

export const getServerUrl = () => environment.serverUrl;