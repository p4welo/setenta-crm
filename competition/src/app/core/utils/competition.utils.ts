import * as dayjs from 'dayjs';
import { Competition } from '../../model';

export const isOutdated = (competition: Competition) => {
  return dayjs().isAfter(dayjs(competition.end));
};

export const registrationInProgress = (competition: Competition) => {
  return competition.registrationOpened;
};

export const registrationEnded = (competition: Competition) => {
  return !registrationInProgress(competition);
};

export const sortDescendingByDate = (competitionA: Competition, competitionB: Competition) => {
  return competitionB.start === competitionA.start ? 0 : competitionA.start > competitionB.start ? -1 : 1;
};