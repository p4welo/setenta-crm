import { Competition } from '@competition/model';
import * as dayjs from 'dayjs';
import { path } from 'ramda';

export const competitionDayName = (competition: Competition, dayNumber: number) => {
  const startDate = path(['start'], competition);
  if (!startDate) {
    return '';
  }
  return dayjs(startDate).add(dayNumber, 'day').format('dddd');
};
