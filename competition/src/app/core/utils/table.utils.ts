import { Dancer_, DancerPerformanceFee } from '../../model';
import { sort, prop, ascend, descend } from 'ramda';

export const getSortingColumnClass = (column: string, sortProperty: string, isSortAscending: boolean = true): string => {
  if (column === sortProperty) {
    return isSortAscending ? 'sorting_asc' : 'sorting_desc';
  }
  return 'sorting';
};

export const getSortedList = (list: any[] = [], sortProperty: string, isSortAscending: boolean = true) => {
  if (!list || list.length < 1) return [];
  const compareFn = isSortAscending ? ascend : descend;
  return sort(compareFn(prop(sortProperty)), list);
};

export const filterDancers = (dancers: Dancer_[], filterValue: string) => {
  if (!filterValue) {
    return dancers;
  }
  else {
    return dancers
        .filter(
            (dancer: Dancer_) => `${dancer.firstName.toLowerCase()} ${dancer.lastName.toLowerCase()}`
                .indexOf(filterValue.toLowerCase()) > -1
        );
  }
};

export const filterDancerFees = (fees: DancerPerformanceFee[], filterValue: string) => {
  if (!filterValue) {
    return fees;
  }
  else {
    return fees
        .filter(
            (fee: DancerPerformanceFee) => `${fee.firstName.toLowerCase()} ${fee.lastName.toLowerCase()}`
                .indexOf(filterValue.toLowerCase()) > -1
        );
  }
};