import {
  AGE_LEVELS, AgeLevel,
  Rule,
  RuleType
} from '@competition/competition-details/competition-rules.const';
import { PerformanceBlock } from '@competition/model';

export const flattenPerformancesStructure = (rules: Rule[]): PerformanceBlock[] => {
  const blocks: PerformanceBlock[] = [];
  let props: PerformanceBlock = {};

  const handle = (rules, props) => {
    rules.forEach((rule: Rule) => {
      props[fn[rule.type]] = rule.name;
      if (rule.rules && rule.rules.length > 0) {
        handle(rule.rules, props);
      } else {
        const { style, type, experience, age } = props;
        blocks.push({ style, type, experience, age });
        delete props[fn[rule.type]];
      }
    });
  };

  const fn = {
    [RuleType.STYLE]: 'style',
    [RuleType.TYPE]: 'type',
    [RuleType.AGE]: 'age',
    [RuleType.EXPERIENCE]: 'experience',
  };

  handle(rules, props);

  const result = [];
  blocks.forEach((block: PerformanceBlock) => {
    AGE_LEVELS.forEach((ageLevel: AgeLevel) => {
      result.push({
        ...block,
        age: ageLevel.name
      });
    })
  });
  return result;
};

