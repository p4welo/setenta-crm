import { Routes } from '@angular/router';
import { DancersComponent } from './dancers.component';

export const routes: Routes = [
  { path: '', component: DancersComponent }
];