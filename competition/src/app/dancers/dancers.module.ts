import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '@competition/modules/auth';
import { DancerStoreModule } from '@competition/modules/dancer-store';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule } from 'ngx-bootstrap';
import { MatchMediaModule } from '@competition/modules/match-media';
import { CoreModule } from '@competition/core';
import { routes } from './dancers.routes';
import { DancersComponent } from './dancers.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    AuthModule,
    DancerStoreModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    HttpClientModule,
    TranslateModule,
    MatchMediaModule
  ],
  providers: [],
  declarations: [DancersComponent]
})
export class DancersModule {
  public static routes = routes;
}