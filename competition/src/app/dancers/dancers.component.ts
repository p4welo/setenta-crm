import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
  CreateDancerStart,
  DancersFetchStart,
  DeleteDancerStart,
  SetDancerFilter,
  SetSortProperty
} from '@competition/modules/dancer-store/actions';
import { DancerService } from '@competition/modules/dancer-store/services';
import { MatchMediaService } from '@competition/modules/match-media/match-media.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { NotificationService } from '../core/services';
import { getSortingColumnClass } from '../core/utils/table.utils';
import { Dancer_ } from '../model';

@Component({
  templateUrl: './dancers.component.html',
  styleUrls: ['./dancers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DancersComponent implements OnInit {

  public addFormVisible: boolean = false;
  public dancers: Dancer_[];

  public media$: Observable<any> = this.matchMediaService.resize$;
  public dancers$: Observable<Dancer_[]> = this.dancerService.dancers$;
  public filteredDancers$: Observable<Dancer_[]> = this.dancerService.filteredDancers$;
  public initialized$: Observable<boolean> = this.dancerService.initialized$;
  public submitInProgress$: Observable<boolean> = this.dancerService.submitInProgress$;
  public sortAscending$: Observable<boolean> = this.dancerService.sortAscending$;
  public sortProperty$: Observable<string> = this.dancerService.sortProperty$;

  constructor(
      private formBuilder: FormBuilder,
      private notificationService: NotificationService,
      private matchMediaService: MatchMediaService,
      private dancerService: DancerService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new DancersFetchStart());
  }

  public sortBy(property: string): void {
    this.store$.dispatch(new SetSortProperty(property));
  }

  public getSortingClass(property: string, sortProperty: string, sortAscending: boolean): string {
    return getSortingColumnClass(property, sortProperty, sortAscending);
  }

  public onSubmit(dancer: Dancer_): void {
    this.store$.dispatch(new CreateDancerStart(dancer));
  }

  public onDancerFilterInput(event: any): void {
    this.store$.dispatch(new SetDancerFilter(event.target.value || ''));

  }

  public delete(dancer: Dancer_): void {
    if (confirm('Czy na pewno chcesz usunąć tancerza?')) {
      this.store$.dispatch(new DeleteDancerStart(dancer));
    }
  }

  public showAddForm(): void {
    this.addFormVisible = true;
  }

  public hideAddForm(): void {
    this.addFormVisible = false;
  }
}