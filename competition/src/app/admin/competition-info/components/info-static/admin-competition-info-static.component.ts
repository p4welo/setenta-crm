import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Competition } from '@competition/model';

@Component({
  selector: 'admin-competition-info-static',
  templateUrl: './admin-competition-info-static.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCompetitionInfoStaticComponent {
  @Input() public competition: Competition;
}