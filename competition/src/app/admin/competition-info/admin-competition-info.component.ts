import { ChangeDetectionStrategy, Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  AdminAgesFetchStart,
  CreateAgeStart,
  CreateExperienceStart,
  CreateStyleStart,
  CreateTypeStart,
  AdminExperiencesFetchStart,
  AdminStylesFetchStart,
  AdminTypesFetchStart, AdminCompetitionLicenceFetchStart
} from '@competition/admin/store';
import { CompetitionsService, NotificationService } from '@competition/core/services';

import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionStyle,
  CompetitionType
} from '@competition/model';
import { Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { AdminCompetitionService } from '../services';

@Component({
  selector: 'admin-competition-info',
  templateUrl: './admin-competition-info.component.html',
  styleUrls: ['./admin-competition-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCompetitionInfoComponent implements OnInit {

  public competition$: Observable<Competition> = this.adminCompetitionService.competition$;
  public styles$: Observable<CompetitionStyle[]> = this.adminCompetitionService.styles$;
  public types$: Observable<CompetitionType[]> = this.adminCompetitionService.types$;
  public experiences$: Observable<CompetitionExperience[]> = this.adminCompetitionService.experiences$;
  public ages$: Observable<CompetitionAge[]> = this.adminCompetitionService.ages$;
  public licence$: Observable<CompetitionLicence> = this.adminCompetitionService.licence$;

  public addStyleModalRef: BsModalRef;
  public ngStyleForm: FormGroup;

  public addExperienceModalRef: BsModalRef;
  public ngExperienceForm: FormGroup;

  public addTypeModalRef: BsModalRef;
  public ngTypeForm: FormGroup;

  public addAgeModalRef: BsModalRef;
  public ngAgeForm: FormGroup;

  public checked: boolean = false;

  constructor(
      private route: ActivatedRoute,
      private modalService: BsModalService,
      private formBuilder: FormBuilder,
      private adminCompetitionService: AdminCompetitionService,
      private competitionsService: CompetitionsService,
      private notificationService: NotificationService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AdminStylesFetchStart());
    this.store$.dispatch(new AdminTypesFetchStart());
    this.store$.dispatch(new AdminExperiencesFetchStart());
    this.store$.dispatch(new AdminAgesFetchStart());

    this.ngStyleForm = this.formBuilder.group({
      name: ['', Validators.required],
      showTitle: [false]
    });
    this.ngExperienceForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
    this.ngTypeForm = this.formBuilder.group({
      name: ['', Validators.required],
      min: ['', Validators.required],
      max: ['', Validators.required],
      showName: [false]
    });
    this.ngAgeForm = this.formBuilder.group({
      name: ['', Validators.required],
      min: ['', Validators.required],
      max: ['', Validators.required]
    });
  }

  // public toggleResults(resultPublished: boolean): void {
  //   this.adminCompetitionService.updateCompetition(
  //       this.competitionSid,
  //       { resultPublished: !resultPublished }
  //   )
  //       .subscribe((data) => {
  //         // this.competition = data;
  //         this.notificationService.success('Pomyślnie zapisano');
  //       })
  // }

  public show(template: TemplateRef<any>): BsModalRef {
    return this.modalService.show(template);
  }

  public saveStyle(style: CompetitionStyle): void {
    this.store$.dispatch(new CreateStyleStart(style));
    this.ngStyleForm.reset();
    this.addStyleModalRef.hide();
  }

  public saveType(type: CompetitionType): void {
    this.store$.dispatch(new CreateTypeStart(type));
    this.ngTypeForm.reset();
    this.addTypeModalRef.hide();
  }

  public saveExperience(experience: CompetitionExperience): void {
    this.store$.dispatch(new CreateExperienceStart(experience));
    this.ngExperienceForm.reset();
    this.addExperienceModalRef.hide();
  }

  public saveAge(age: CompetitionAge): void {
    this.store$.dispatch(new CreateAgeStart(age));
    this.ngAgeForm.reset();
    this.addAgeModalRef.hide();
  }
}