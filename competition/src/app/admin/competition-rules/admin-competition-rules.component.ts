import { ChangeDetectionStrategy, Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '@competition/admin/services';
import {
  AdminAgesFetchStart,
  AdminExperiencesFetchStart,
  AdminRulesFetchStart,
  AdminStylesFetchStart,
  AdminTypesFetchStart
} from '@competition/admin/store';
import { CompetitionsService } from '@competition/core/services';
import {
  CompetitionAge,
  CompetitionExperience,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType
} from '@competition/model';
import { Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { forkJoin, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-rules.component.html',
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCompetitionRulesComponent implements OnInit {

  public styles$: Observable<CompetitionStyle[]> = this.adminCompetitionService.styles$;
  public types$: Observable<CompetitionType[]> = this.adminCompetitionService.types$;
  public experiences$: Observable<CompetitionExperience[]> = this.adminCompetitionService.experiences$;
  public ages$: Observable<CompetitionAge[]> = this.adminCompetitionService.ages$;
  public rules$: Observable<CompetitionRule[]> = this.adminCompetitionService.rules$;
  public newRules$: Observable<CompetitionRule[]> = this.adminCompetitionService.newRules$;

  public selectedTypeName: string;
  public types = [
    { name: 'AGE' },
    { name: 'TYPE' },
    { name: 'STYLE' },
    { name: 'EXPERIENCE' }
  ];

  public selection = {};
  public addModalRef: BsModalRef;

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private modalService: BsModalService,
      private formBuilder: FormBuilder,
      private adminCompetitionService: AdminCompetitionService,
      private competitionsService: CompetitionsService,
      private store$: Store<any>
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AdminStylesFetchStart());
    this.store$.dispatch(new AdminTypesFetchStart());
    this.store$.dispatch(new AdminExperiencesFetchStart());
    this.store$.dispatch(new AdminAgesFetchStart());
    this.store$.dispatch(new AdminRulesFetchStart());
  }

  public get somethingSelected(): boolean {
    return Object.keys(this.selection).length > 0;
  }

  public onSelect(rule: CompetitionRule): void {
    rule.checked = !rule.checked;
    if (rule.checked) {
      this.selection[rule.sid] = true;
    } else {
      delete this.selection[rule.sid];
    }
  }

  public showAddModal(template: TemplateRef<any>): void {
    this.addModalRef = this.modalService.show(template);
  }

  public removeSelected(): void {
    const requests = Object.keys(this.selection).map((ruleSid: string) => {
      return this.adminCompetitionService.removeCompetitionRule(this.competitionSid, ruleSid);
    });
    forkJoin(requests)
        .pipe(first())
        .subscribe(() => {
          this.store$.dispatch(new AdminRulesFetchStart());
        })
  }

  public saveRule(ruleType: string, style: CompetitionStyle, type?: CompetitionType, experience?: CompetitionExperience, age?: CompetitionAge): void {
    const rule = {
      ruleType,
      style,
      type,
      experience,
      age
    };
    let requests = [];
    if (!this.somethingSelected) {
      requests = [this.adminCompetitionService.saveCompetitionRule(this.competitionSid, '', rule)];
    } else {
      requests = Object.keys(this.selection).map((parentSid: string) => {
        return this.adminCompetitionService.saveCompetitionRule(this.competitionSid, parentSid, rule);
      });
    }

    forkJoin(requests)
        .pipe(first())
        .subscribe(() => {
          this.store$.dispatch(new AdminRulesFetchStart());
          this.addModalRef.hide();
        })
  }
}