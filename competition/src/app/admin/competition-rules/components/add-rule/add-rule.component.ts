import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {
  CompetitionAge,
  CompetitionExperience,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType
} from '@competition/model';

@Component({
  selector: 'app-add-rule',
  templateUrl: './add-rule.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminAddRuleComponent {
  @Input() public ages: CompetitionAge[] = [];
  @Input() public types: CompetitionType[] = [];
  @Input() public styles: CompetitionStyle[] = [];
  @Input() public experiences: CompetitionExperience[] = [];

  @Input() public parentRule: CompetitionRule;

  @Output() public save: EventEmitter<CompetitionRule> = new EventEmitter();

  public saveRule(ruleType: string, style: CompetitionStyle, type?: CompetitionType, experience?: CompetitionExperience, age?: CompetitionAge): void {
    const rule = {
      ruleType,
      style,
      type,
      experience,
      age,
      // parentSid: this.parentRule ? this.parentRule.sid : undefined
    };
    this.save.emit(rule);
  }
}