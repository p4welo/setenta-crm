import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CompetitionRule } from '@competition/model';

@Component({
  selector: 'admin-rule-node',
  templateUrl: './admin-rule-node.component.html',
  styleUrls: ['./admin-rule-node.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRuleNodeComponent {
  @Input() public rule: CompetitionRule;

  @Output() public select: EventEmitter<CompetitionRule> = new EventEmitter();
  @Output() public expand: EventEmitter<CompetitionRule> = new EventEmitter();

  public get selected(): boolean {
    return this.rule.checked;
  }

  public onSelect(rule: CompetitionRule): void {
    console.warn('select', rule.checked);
    this.select.emit(rule);
  }

  public onExpand(rule: CompetitionRule): void {
    this.expand.emit(rule);
  }

  public trackByFn(index, item) {
    return item.sid
  }
}