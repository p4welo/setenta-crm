import { ChangeDetectionStrategy, Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '@competition/admin/services';
import {
  AdminAgesFetchStart,
  AdminExperiencesFetchStart,
  AdminRulesFetchStart,
  AdminStylesFetchStart,
  AdminTypesFetchStart,
  CollapseRule,
  CreateRuleStart,
  DeselectRule,
  ExpandRule,
  GenerateRulesStart,
  RemoveRuleStart,
  SelectRule,
  UpdateRuleStart,
} from '@competition/admin/store';
import { CompetitionsService } from '@competition/core/services';
import {
  CompetitionAge,
  CompetitionExperience,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType
} from '@competition/model';
import { Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './new-admin-competition-rules.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAdminCompetitionRulesComponent implements OnInit {

  public styles$: Observable<CompetitionStyle[]> = this.adminCompetitionService.styles$;
  public types$: Observable<CompetitionType[]> = this.adminCompetitionService.types$;
  public experiences$: Observable<CompetitionExperience[]> = this.adminCompetitionService.experiences$;
  public ages$: Observable<CompetitionAge[]> = this.adminCompetitionService.ages$;
  public rules$: Observable<CompetitionRule[]> = this.adminCompetitionService.newRules$;
  public selectedRule$: Observable<CompetitionRule> = this.adminCompetitionService.selectedRule$;
  public isRulesLoading$: Observable<boolean> = this.adminCompetitionService.isRulesLoading$;

  public addModalRef: BsModalRef;

  constructor(
      private route: ActivatedRoute,
      private modalService: BsModalService,
      private formBuilder: FormBuilder,
      private adminCompetitionService: AdminCompetitionService,
      private competitionsService: CompetitionsService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AdminStylesFetchStart());
    this.store$.dispatch(new AdminTypesFetchStart());
    this.store$.dispatch(new AdminExperiencesFetchStart());
    this.store$.dispatch(new AdminAgesFetchStart());
    this.store$.dispatch(new AdminRulesFetchStart());
  }

  public add(rule: CompetitionRule): void {
    this.store$.dispatch(new CreateRuleStart(rule));
    this.addModalRef.hide();
  }

  public remove(): void {
    if (confirm('Czy na pewno usunąć zaznaczoną regułę i reguły wewnętrzne?')) {
      this.store$.dispatch(new RemoveRuleStart());
    }
  }

  public generate(): void {
    if (confirm('Czy na pewno usunąć bieżące reguły i wygenerować nowe?')) {
      this.store$.dispatch(new GenerateRulesStart());
    }
  }

  public toggleSelection(rule: CompetitionRule): void {
    if (rule.checked) {
      this.store$.dispatch(new DeselectRule(rule));
    } else {
      this.store$.dispatch(new SelectRule(rule));
    }
  }

  public toggleExpansion(rule: CompetitionRule): void {
    if (rule.expanded) {
      this.store$.dispatch(new CollapseRule(rule));
    } else {
      this.store$.dispatch(new ExpandRule(rule));
    }
  }

  public toggleObjectState(rule: CompetitionRule): void {
    const objectState = rule.objectState === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    const enableRule = objectState === 'ACTIVE';
    if (confirm(`Czy na pewno chcesz ${enableRule ? 'otworzyć' : 'zamknąć'} zapisy w wybranej kategorii?`)) {
      this.store$.dispatch(new UpdateRuleStart({ objectState }));
    }
  }

  public toggleOwnMusicAllowed(rule: CompetitionRule): void {
    const ownMusicAllowed = !rule.ownMusicAllowed;
    this.store$.dispatch(new UpdateRuleStart({ ownMusicAllowed }));
  }

  public showAddModal(template: TemplateRef<any>): void {
    this.addModalRef = this.modalService.show(template);
  }

  public trackByFn(index, item) {
    return item.sid
  }
}