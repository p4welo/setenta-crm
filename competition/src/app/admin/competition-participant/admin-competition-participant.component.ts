import { Component, OnInit, TemplateRef } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import {
  AdminAgesFetchStart,
  AdminExperiencesFetchStart,
  AdminStylesFetchStart,
  AdminTypesFetchStart,
  GenerateSchoolCardStart,
  GenerateSchoolNumbersStart,
  SelectParticipantStart,
  UserDancersFetchStart,
  UserPaymentsFetchStart,
  UserPerformancesFetchStart,
  UserSchoolFetchStart
} from '@competition/admin/store';
import {
  CompetitionsService,
  NotificationService,
  PerformanceService
} from '@competition/core/services';
import { getSortingColumnClass } from '@competition/core/utils';

import {
  CompetitionAge,
  CompetitionExperience,
  CompetitionPayment,
  CompetitionStyle,
  CompetitionType,
  Dancer_,
  InvoiceData,
  Performance,
  School
} from '@competition/model';
import { PageSize } from '@competition/model/print.model';
import { MatchMediaService } from '@competition/modules/match-media/match-media.service';
import { PerformanceSongService } from '@competition/modules/performance-song/services';
import { Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { AdminCompetitionService } from '../services';

@Component({
  templateUrl: './admin-competition-participant.component.html',
  styleUrls: ['./admin-competition-participant.component.scss']
})
export class AdminCompetitionParticipantComponent implements OnInit {

  public invoiceData$: Observable<InvoiceData>;
  public performances: Performance[];
  public payments: CompetitionPayment[];
  public canAddMusic: boolean = true;

  public school$: Observable<School> = this.adminCompetitionService.userSchool$;
  public performances$: Observable<Performance[]> = this.adminCompetitionService.userPerformances$;
  public dancers$: Observable<Dancer_[]> = this.adminCompetitionService.userDancers$;
  public payments$: Observable<CompetitionPayment[]> = this.adminCompetitionService.userPayments$;
  public styles$: Observable<CompetitionStyle[]> = this.adminCompetitionService.styles$;
  public types$: Observable<CompetitionType[]> = this.adminCompetitionService.types$;
  public experiences$: Observable<CompetitionExperience[]> = this.adminCompetitionService.experiences$;
  public ages$: Observable<CompetitionAge[]> = this.adminCompetitionService.ages$;

  public playingSongSid$: Observable<string> = this.performanceSongService.playingSongSid$;

  public media$: Observable<any> = this.matchMediaService.resize$;

  public sortingProperty: string;

  public sortingAscending: boolean = true;
  public editPerformanceModalRef: BsModalRef;

  public selectedPerformance: Performance;

  private competitionSid: string;
  private readonly participantSid: string;

  constructor(
      private store$: Store<any>,
      private route: ActivatedRoute,
      private adminCompetitionService: AdminCompetitionService,
      private competitionsService: CompetitionsService,
      private notificationService: NotificationService,
      private modalService: BsModalService,
      private performanceService: PerformanceService,
      private matchMediaService: MatchMediaService,
      private performanceSongService: PerformanceSongService
  ) {
    this.route.parent.paramMap
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
    this.participantSid = this.route.snapshot.paramMap.get('participantSid');
  }

  public ngOnInit(): void {
    this.store$.dispatch(new SelectParticipantStart(this.route.snapshot.paramMap.get('participantSid')));
    this.store$.dispatch(new UserSchoolFetchStart());
    this.store$.dispatch(new UserPaymentsFetchStart());
    this.store$.dispatch(new UserPerformancesFetchStart());
    this.store$.dispatch(new UserDancersFetchStart());
    this.store$.dispatch(new AdminStylesFetchStart());
    this.store$.dispatch(new AdminTypesFetchStart());
    this.store$.dispatch(new AdminExperiencesFetchStart());
    this.store$.dispatch(new AdminAgesFetchStart());

    this.invoiceData$ = this.adminCompetitionService.getUserInvoiceData(this.participantSid);
  }


  public canShowExperience(performances: Performance[] = []): boolean {
    return performances.filter((performance: Performance) => !!performance.experience).length > 0;
  }

  public canShowTitle(performances: Performance[] = []): boolean {
    return performances.filter((performance: Performance) => !!performance.title).length > 0;
  }

  public edit(performance: Performance, template: TemplateRef<any>): void {
    this.selectedPerformance = performance;
    this.editPerformanceModalRef = this.modalService.show(template);
  }

  public remove(performance: Performance): void {
    if (confirm('Czy na pewno chcesz usunąć występ?')) {
      this.performanceService.delete(performance, this.competitionSid).subscribe(() => {
        this.notificationService.success("Pomyślnie usunięto");
        this.store$.dispatch(new UserPerformancesFetchStart());
      });
    }
  }

  public updatePerformance(performance: Performance): void {
    this.adminCompetitionService.updatePerformance(this.competitionSid, this.selectedPerformance.sid, performance)
        .subscribe(() => {
          this.notificationService.success("Pomyślnie zapisano");
          this.store$.dispatch(new UserPerformancesFetchStart());
        });
    this.editPerformanceModalRef.hide();
  }

  // SORTING
  public sortBy(property: string): void {
    if (this.sortingProperty === property) {
      this.sortingAscending = !this.sortingAscending;
    } else {
      this.sortingProperty = property;
      this.sortingAscending = true;
    }
  }

  public getSortingClass(property: string): string {
    return getSortingColumnClass(property, this.sortingProperty, this.sortingAscending);
  }

  public savePayment(payment: CompetitionPayment): void {
    this.adminCompetitionService
        .createUserPayment(this.competitionSid, {
          ...payment,
          userSid: this.participantSid
        })
        .subscribe(() => {
          this.store$.dispatch(new UserPaymentsFetchStart());
          this.notificationService.success("Pomyślnie dodano");
        });
  }

  public deletePayment(payment: CompetitionPayment): void {

  }

  public printNumbersA4(): void {
    this.store$.dispatch(new GenerateSchoolNumbersStart(PageSize.A4));
  }

  public printNumbersA5(): void {
    this.store$.dispatch(new GenerateSchoolNumbersStart(PageSize.A5));
  }

  public printSchoolCard(): void {
    this.store$.dispatch(new GenerateSchoolCardStart());
  }
}