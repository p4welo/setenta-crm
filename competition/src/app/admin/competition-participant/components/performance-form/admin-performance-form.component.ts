import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  CompetitionAge,
  CompetitionExperience,
  CompetitionStyle,
  CompetitionType,
  Performance
} from '@competition/model';

@Component({
  selector: 'app-admin-performance-form',
  templateUrl: './admin-performance-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPerformanceFormComponent implements OnInit, OnChanges {
  @Input() public performance: Performance;

  @Input() public styles: CompetitionStyle[] = [];
  @Input() public experiences: CompetitionExperience[] = [];
  @Input() public types: CompetitionType[] = [];
  @Input() public ages: CompetitionAge[] = [];

  @Output() public save: EventEmitter<Performance> = new EventEmitter();
  @Output() public cancel: EventEmitter<void> = new EventEmitter();

  public ngForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder
  ) {
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      name: [''],
      style: [''],
      type: [''],
      experience: [''],
      ageLevel: [''],
      number: [''],
      title: ['']
    });
    this.ngForm.reset(this.performance);
  }

  public ngOnChanges(changes: SimpleChanges): void {

  }

  public canShowTitle(styleName): boolean {
    if (this.styles) {
      const found = this.styles.filter((style) => style.name === styleName);
      return found && found.length > 0 && found[0].showTitle;
    }
    return false;
  }

  public cancelClick(): void {
    this.cancel.emit();
  }

  public submit(performance: Performance): void {
    if (confirm('Czy na pewno chcesz wprowadzić zmiany w zgłoszeniu?')) {
      this.save.emit(performance);
    }
  }
}