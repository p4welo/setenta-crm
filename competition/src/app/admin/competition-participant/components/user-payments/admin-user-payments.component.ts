import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompetitionPayment, User } from '@competition/model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'admin-user-payments',
  templateUrl: './admin-user-payments.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminUserPaymentsComponent implements OnInit {
  @Input() public payments: CompetitionPayment[];
  @Input() public user: User;

  @Output() public save: EventEmitter<CompetitionPayment> = new EventEmitter();
  @Output() public delete: EventEmitter<CompetitionPayment> = new EventEmitter();

  public addPaymentModalRef: BsModalRef;
  public ngForm: FormGroup;

  constructor(
      private modalService: BsModalService,
      private formBuilder: FormBuilder
  ) {
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      date: ['', Validators.required],
      amount: ['', Validators.required]
    });
  }

  public get sum(): number {
    if (!this.payments) return 0;
    return this.payments.reduce((accumulator: number, current: CompetitionPayment) => accumulator + current.amount, 0);
  }

  public addPayment(template: TemplateRef<any>): void {
    this.addPaymentModalRef = this.modalService.show(template);
  }

  public savePayment(payment: CompetitionPayment): void {

    this.save.emit({
      ...payment,
      amount: +('' + payment.amount).split(',').join('.')
    });
    this.addPaymentModalRef.hide();
  }

  public deletePayment(payment: CompetitionPayment): void {
    if (confirm('Czy na pewno usunąć wpłatę?')) {
      this.delete.emit(payment);
      // this.addPaymentModalRef.hide();
    }
  }
}