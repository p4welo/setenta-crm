import {
  AdminCompetitionParticipant,
  Competition, CompetitionPayment,
  Dancer_,
  Performance,
  School
} from '@competition/model';
import { PageSize } from '@competition/model/print.model';
import { Action } from '@ngrx/store';

export const AdminParticipantActionTypes = {
  PARTICIPANTS_FETCH_START: '[Admin Participant] Participants fetch start',
  PARTICIPANTS_FETCH_SUCCESS: '[Admin Participant] Participants fetch success',
  PARTICIPANTS_FETCH_ERROR: '[Admin Participant] Participants fetch error',

  SELECT_PARTICIPANT_START: '[Admin Participant] Select participant start',
  SELECT_PARTICIPANT_SUCCESS: '[Admin Participant] Select participant success',
  SELECT_PARTICIPANT_ERROR: '[Admin Participant] Select participant error',

  USER_SCHOOL_FETCH_START: '[Admin Participant] User school fetch start',
  USER_SCHOOL_FETCH_SUCCESS: '[Admin Participant] User school fetch success',
  USER_SCHOOL_FETCH_ERROR: '[Admin Participant] User school fetch error',

  USER_PERFORMANCES_FETCH_START: '[Admin Participant] User performances fetch start',
  USER_PERFORMANCES_FETCH_SUCCESS: '[Admin Participant] User performances fetch success',
  USER_PERFORMANCES_FETCH_ERROR: '[Admin Participant] User performances fetch error',

  USER_DANCERS_FETCH_START: '[Admin Participant] User dancers fetch start',
  USER_DANCERS_FETCH_SUCCESS: '[Admin Participant] User dancers fetch success',
  USER_DANCERS_FETCH_ERROR: '[Admin Participant] User dancers fetch error',

  USER_PAYMENTS_FETCH_START: '[Admin Participant] User payments fetch start',
  USER_PAYMENTS_FETCH_SUCCESS: '[Admin Participant] User payments fetch success',
  USER_PAYMENTS_FETCH_ERROR: '[Admin Participant] User payments fetch error',

  GENERATE_SCHOOL_CARD_START: '[Admin Participant] Generate school card start',
  GENERATE_SCHOOL_CARD_SUCCESS: '[Admin Participant] Generate school card success',
  GENERATE_SCHOOL_CARD_ERROR: '[Admin Participant] Generate school card error',

  GENERATE_SCHOOL_NUMBERS_START: '[Admin Participant] Generate school performance numbers start',
  GENERATE_SCHOOL_NUMBERS_SUCCESS: '[Admin Participant] Generate school performance numbers success',
  GENERATE_SCHOOL_NUMBERS_ERROR: '[Admin Participant] Generate school performance numbers error',
};

export class ParticipantsFetchStart implements Action {
  public type: string = AdminParticipantActionTypes.PARTICIPANTS_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class ParticipantsFetchSuccess implements Action {
  public type: string = AdminParticipantActionTypes.PARTICIPANTS_FETCH_SUCCESS;

  constructor(public payload: AdminCompetitionParticipant[]) {
  }
}

export class ParticipantsFetchError implements Action {
  public type: string = AdminParticipantActionTypes.PARTICIPANTS_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class SelectParticipantStart implements Action {
  public type: string = AdminParticipantActionTypes.SELECT_PARTICIPANT_START;

  constructor(public payload: string ) {
  }
}

export class SelectParticipantSuccess implements Action {
  public type: string = AdminParticipantActionTypes.SELECT_PARTICIPANT_SUCCESS;

  constructor(public payload: Competition ) {
  }
}

export class SelectParticipantError implements Action {
  public type: string = AdminParticipantActionTypes.SELECT_PARTICIPANT_ERROR;

  constructor(public payload: Error ) {
  }
}

export class UserSchoolFetchStart implements Action {
  public type: string = AdminParticipantActionTypes.USER_SCHOOL_FETCH_START;

  constructor(public payload?: any ) {
  }
}

export class UserSchoolFetchSuccess implements Action {
  public type: string = AdminParticipantActionTypes.USER_SCHOOL_FETCH_SUCCESS;

  constructor(public payload: School ) {
  }
}

export class UserSchoolFetchError implements Action {
  public type: string = AdminParticipantActionTypes.USER_SCHOOL_FETCH_ERROR;

  constructor(public payload: Error ) {
  }
}

export class UserPerformancesFetchStart implements Action {
  public type: string = AdminParticipantActionTypes.USER_PERFORMANCES_FETCH_START;

  constructor(public payload?: any ) {
  }
}

export class UserPerformancesFetchSuccess implements Action {
  public type: string = AdminParticipantActionTypes.USER_PERFORMANCES_FETCH_SUCCESS;

  constructor(public payload: Performance[] ) {
  }
}

export class UserPerformancesFetchError implements Action {
  public type: string = AdminParticipantActionTypes.USER_PERFORMANCES_FETCH_ERROR;

  constructor(public payload: Error ) {
  }
}

export class UserDancersFetchStart implements Action {
  public type: string = AdminParticipantActionTypes.USER_DANCERS_FETCH_START;

  constructor(public payload?: any ) {
  }
}

export class UserDancersFetchSuccess implements Action {
  public type: string = AdminParticipantActionTypes.USER_DANCERS_FETCH_SUCCESS;

  constructor(public payload: Dancer_[] ) {
  }
}

export class UserDancersFetchError implements Action {
  public type: string = AdminParticipantActionTypes.USER_DANCERS_FETCH_ERROR;

  constructor(public payload: Error ) {
  }
}

export class UserPaymentsFetchStart implements Action {
  public type: string = AdminParticipantActionTypes.USER_PAYMENTS_FETCH_START;

  constructor(public payload?: any ) {
  }
}

export class UserPaymentsFetchSuccess implements Action {
  public type: string = AdminParticipantActionTypes.USER_PAYMENTS_FETCH_SUCCESS;

  constructor(public payload: CompetitionPayment[] ) {
  }
}

export class UserPaymentsFetchError implements Action {
  public type: string = AdminParticipantActionTypes.USER_PAYMENTS_FETCH_ERROR;

  constructor(public payload: Error ) {
  }
}

export class GenerateSchoolCardStart implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_CARD_START;

  constructor(public payload?: any) {
  }
}

export class GenerateSchoolCardSuccess implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_CARD_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class GenerateSchoolCardError implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_CARD_ERROR;

  constructor(public payload?: any) {
  }
}

export class GenerateSchoolNumbersStart implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_NUMBERS_START;

  constructor(public payload: PageSize) {
  }
}

export class GenerateSchoolNumbersSuccess implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_NUMBERS_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class GenerateSchoolNumbersError implements Action {
  public type: string = AdminParticipantActionTypes.GENERATE_SCHOOL_NUMBERS_ERROR;

  constructor(public payload?: any) {
  }
}

export type AdminParticipantActions =
    | ParticipantsFetchStart
    | ParticipantsFetchSuccess
    | ParticipantsFetchError
    | SelectParticipantStart
    | SelectParticipantStart
    | SelectParticipantError
    | UserPerformancesFetchStart
    | UserPerformancesFetchSuccess
    | UserPerformancesFetchError
    | UserSchoolFetchStart
    | UserSchoolFetchSuccess
    | UserSchoolFetchError
    | GenerateSchoolCardStart
    | GenerateSchoolCardSuccess
    | GenerateSchoolCardError
    | GenerateSchoolNumbersStart
    | GenerateSchoolNumbersSuccess
    | GenerateSchoolNumbersError
;
