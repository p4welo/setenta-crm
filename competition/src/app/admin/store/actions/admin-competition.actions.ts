import { Action } from '@ngrx/store';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType, Invoice, InvoiceData
} from '@competition/model';

export const AdminCompetitionActionTypes = {
  COMPETITIONS_FETCH_START: '[Admin Competition] Competitions fetch start',
  COMPETITIONS_FETCH_SUCCESS: '[Admin Competition] Competitions fetch success',
  COMPETITIONS_FETCH_ERROR: '[Admin Competition] Competitions fetch error',

  SELECT_COMPETITION_START: '[Admin Competition] Competition select start',
  SELECT_COMPETITION_SUCCESS: '[Admin Competition] Competition select success',
  SELECT_COMPETITION_ERROR: '[Admin Competition] Competition select error',

  UPDATE_COMPETITION_START: '[Admin Competition] Competition update start',
  UPDATE_COMPETITION_SUCCESS: '[Admin Competition] Competition update success',
  UPDATE_COMPETITION_ERROR: '[Admin Competition] Competition update error',

  STYLES_FETCH_START: '[Admin Competition] Styles fetch start',
  STYLES_FETCH_SUCCESS: '[Admin Competition] Styles fetch success',
  STYLES_FETCH_ERROR: '[Admin Competition] Styles fetch error',

  COMPETITION_LICENCE_FETCH_START: '[Admin Competition] Licence fetch start',
  COMPETITION_LICENCE_FETCH_SUCCESS: '[Admin Competition] Licence fetch success',
  COMPETITION_LICENCE_FETCH_ERROR: '[Admin Competition] Licence fetch error',

  CREATE_STYLE_START: '[Admin Competition] Create style start',
  CREATE_STYLE_SUCCESS: '[Admin Competition] Create style success',
  CREATE_STYLE_ERROR: '[Admin Competition] Create style error',

  TYPES_FETCH_START: '[Admin Competition] Types fetch start',
  TYPES_FETCH_SUCCESS: '[Admin Competition] Types fetch success',
  TYPES_FETCH_ERROR: '[Admin Competition] Types fetch error',

  CREATE_TYPE_START: '[Admin Competition] Create type start',
  CREATE_TYPE_SUCCESS: '[Admin Competition] Create type success',
  CREATE_TYPE_ERROR: '[Admin Competition] Create type error',

  EXPERIENCES_FETCH_START: '[Admin Competition] Experiences fetch start',
  EXPERIENCES_FETCH_SUCCESS: '[Admin Competition] Experiences fetch success',
  EXPERIENCES_FETCH_ERROR: '[Admin Competition] Experiences fetch error',

  CREATE_EXPERIENCE_START: '[Admin Competition] Create experience start',
  CREATE_EXPERIENCE_SUCCESS: '[Admin Competition] Create experience success',
  CREATE_EXPERIENCE_ERROR: '[Admin Competition] Create experience error',

  AGES_FETCH_START: '[Admin Competition] Ages fetch start',
  AGES_FETCH_SUCCESS: '[Admin Competition] Ages fetch success',
  AGES_FETCH_ERROR: '[Admin Competition] Ages fetch error',

  CREATE_AGE_START: '[Admin Competition] Create age start',
  CREATE_AGE_SUCCESS: '[Admin Competition] Create age success',
  CREATE_AGE_ERROR: '[Admin Competition] Create age error',

  ADMIN_INVOICE_DATA_FETCH_START: '[Admin Competition] Invoice data fetch start',
  ADMIN_INVOICE_DATA_FETCH_SUCCESS: '[Admin Competition] Invoice data fetch success',
  ADMIN_INVOICE_DATA_FETCH_ERROR: '[Admin Competition] Invoice data fetch error',

  CREATE_INVOICE_START: '[Admin Competition] Create invoice start',
  CREATE_INVOICE_SUCCESS: '[Admin Competition] Create invoice success',
  CREATE_INVOICE_ERROR: '[Admin Competition] Create invoice error',

  DELETE_INVOICE_START: '[Admin Competition] Delete invoice start',
  DELETE_INVOICE_SUCCESS: '[Admin Competition] Delete invoice success',
  DELETE_INVOICE_ERROR: '[Admin Competition] Delete invoice error',

  DOWNLOAD_INVOICE_START: '[Admin Competition] Download invoice start',
  DOWNLOAD_INVOICE_SUCCESS: '[Admin Competition] Download invoice success',
  DOWNLOAD_INVOICE_ERROR: '[Admin Competition] Download invoice error',

  RULES_FETCH_START: '[Admin Competition] Rules fetch start',
  RULES_FETCH_SUCCESS: '[Admin Competition] Rules fetch success',
  RULES_FETCH_ERROR: '[Admin Competition] Rules fetch error',

  CREATE_RULE_START: '[Admin Competition] Create rule start',
  CREATE_RULE_SUCCESS: '[Admin Competition] Create rule success',
  CREATE_RULE_ERROR: '[Admin Competition] Create rule error',

  UPDATE_RULE_START: '[Admin Competition] Update rule start',
  UPDATE_RULE_SUCCESS: '[Admin Competition] Update rule success',
  UPDATE_RULE_ERROR: '[Admin Competition] Update rule error',

  GENERATE_RULES_START: '[Admin Competition] Generate rules start',
  GENERATE_RULES_SUCCESS: '[Admin Competition] Generate rules success',
  GENERATE_RULES_ERROR: '[Admin Competition] Generate rules error',

  REMOVE_RULE_START: '[Admin Competition] Remove rule start',
  REMOVE_RULE_SUCCESS: '[Admin Competition] Remove rule success',
  REMOVE_RULE_ERROR: '[Admin Competition] Remove rule error',

  EXPAND_RULE: '[Admin Competition] Expand rule',
  COLLAPSE_RULE: '[Admin Competition] Collapse rule',
  SELECT_RULE: '[Admin Competition] Select rule',
  DESELECT_RULE: '[Admin Competition] Deselect rule',

  DOWNLOAD_AUDIO_START: '[Admin Competition] Download audio start',
  DOWNLOAD_AUDIO_SUCCESS: '[Admin Competition] Download audio success',
  DOWNLOAD_AUDIO_ERROR: '[Admin Competition] Download audio error'
};

export class AdminCompetitionsFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITIONS_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminCompetitionsFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITIONS_FETCH_SUCCESS;

  constructor(public payload: Competition[]) {
  }
}

export class AdminCompetitionsFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITIONS_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminSelectCompetitionStart implements Action {
  public type: string = AdminCompetitionActionTypes.SELECT_COMPETITION_START;

  constructor(public payload: string ) {
  }
}

export class AdminSelectCompetitionSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.SELECT_COMPETITION_SUCCESS;

  constructor(public payload: Competition ) {
  }
}

export class AdminSelectCompetitionError implements Action {
  public type: string = AdminCompetitionActionTypes.SELECT_COMPETITION_ERROR;

  constructor(public payload: Error ) {
  }
}

export class UpdateCompetitionStart implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_COMPETITION_START;

  constructor(public payload: any) {
  }
}

export class UpdateCompetitionSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_COMPETITION_SUCCESS;

  constructor(public payload: Competition) {
  }
}

export class UpdateCompetitionError implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_COMPETITION_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminStylesFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.STYLES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminStylesFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.STYLES_FETCH_SUCCESS;

  constructor(public payload: CompetitionStyle[]) {
  }
}

export class AdminStylesFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.STYLES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminCompetitionLicenceFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITION_LICENCE_FETCH_START;

  constructor(public payload: string) {
  }
}

export class AdminCompetitionLicenceFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITION_LICENCE_FETCH_SUCCESS;

  constructor(public payload: CompetitionLicence) {
  }
}

export class AdminCompetitionLicenceFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.COMPETITION_LICENCE_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateStyleStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_STYLE_START;

  constructor(public payload: CompetitionStyle) {
  }
}

export class CreateStyleSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_STYLE_SUCCESS;

  constructor(public payload: CompetitionStyle) {
  }
}

export class CreateStyleError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_STYLE_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminTypesFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.TYPES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminTypesFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.TYPES_FETCH_SUCCESS;

  constructor(public payload: CompetitionType[]) {
  }
}

export class AdminTypesFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.TYPES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateTypeStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_TYPE_START;

  constructor(public payload: CompetitionType) {
  }
}

export class CreateTypeSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_TYPE_SUCCESS;

  constructor(public payload: CompetitionType) {
  }
}

export class CreateTypeError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_TYPE_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminExperiencesFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.EXPERIENCES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminExperiencesFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.EXPERIENCES_FETCH_SUCCESS;

  constructor(public payload: CompetitionExperience[]) {
  }
}

export class AdminExperiencesFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.EXPERIENCES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateExperienceStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_EXPERIENCE_START;

  constructor(public payload: CompetitionExperience) {
  }
}

export class CreateExperienceSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_EXPERIENCE_SUCCESS;

  constructor(public payload: CompetitionExperience) {
  }
}

export class CreateExperienceError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_EXPERIENCE_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminAgesFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.AGES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminAgesFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.AGES_FETCH_SUCCESS;

  constructor(public payload: CompetitionAge[]) {
  }
}

export class AdminAgesFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.AGES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateAgeStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_AGE_START;

  constructor(public payload: CompetitionAge) {
  }
}

export class CreateAgeSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_AGE_SUCCESS;

  constructor(public payload: CompetitionAge) {
  }
}

export class CreateAgeError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_AGE_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminInvoiceDataFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.ADMIN_INVOICE_DATA_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminInvoiceDataFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.ADMIN_INVOICE_DATA_FETCH_SUCCESS;

  constructor(public payload: InvoiceData[]) {
  }
}

export class AdminInvoiceDataFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.ADMIN_INVOICE_DATA_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateInvoiceStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_INVOICE_START;

  constructor(public payload: Invoice) {
  }
}

export class CreateInvoiceSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_INVOICE_SUCCESS;

  constructor(public payload: Invoice) {
  }
}

export class CreateInvoiceError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_INVOICE_ERROR;

  constructor(public payload: Error) {
  }
}

export class DeleteInvoiceStart implements Action {
  public type: string = AdminCompetitionActionTypes.DELETE_INVOICE_START;

  constructor(public payload: InvoiceData) {
  }
}

export class DeleteInvoiceSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.DELETE_INVOICE_SUCCESS;

  constructor(public payload: Invoice) {
  }
}

export class DeleteInvoiceError implements Action {
  public type: string = AdminCompetitionActionTypes.DELETE_INVOICE_ERROR;

  constructor(public payload: Error) {
  }
}

export class DownloadInvoiceStart implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_INVOICE_START;

  constructor(public payload: InvoiceData) {
  }
}

export class DownloadInvoiceSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_INVOICE_SUCCESS;

  constructor(public payload: {data: any, invoiceData: InvoiceData}) {
  }
}

export class DownloadInvoiceError implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_INVOICE_ERROR;

  constructor(public payload: Error) {
  }
}

export class AdminRulesFetchStart implements Action {
  public type: string = AdminCompetitionActionTypes.RULES_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminRulesFetchSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.RULES_FETCH_SUCCESS;

  constructor(public payload: CompetitionRule[]) {
  }
}

export class AdminRulesFetchError implements Action {
  public type: string = AdminCompetitionActionTypes.RULES_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export class CreateRuleStart implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_RULE_START;

  constructor(public payload: CompetitionRule) {
  }
}

export class CreateRuleSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_RULE_SUCCESS;

  constructor(public payload: CompetitionRule) {
  }
}

export class CreateRuleError implements Action {
  public type: string = AdminCompetitionActionTypes.CREATE_RULE_ERROR;

  constructor(public payload: Error) {
  }
}

export class UpdateRuleStart implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_RULE_START;

  constructor(public payload: any) {
  }
}

export class UpdateRuleSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_RULE_SUCCESS;

  constructor(public payload: CompetitionRule) {
  }
}

export class UpdateRuleError implements Action {
  public type: string = AdminCompetitionActionTypes.UPDATE_RULE_ERROR;

  constructor(public payload: Error) {
  }
}

export class RemoveRuleStart implements Action {
  public type: string = AdminCompetitionActionTypes.REMOVE_RULE_START;

  constructor(public payload?: any) {
  }
}

export class RemoveRuleSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.REMOVE_RULE_SUCCESS;

  constructor(public payload: CompetitionRule) {
  }
}

export class RemoveRuleError implements Action {
  public type: string = AdminCompetitionActionTypes.REMOVE_RULE_ERROR;

  constructor(public payload: Error) {
  }
}

export class GenerateRulesStart implements Action {
  public type: string = AdminCompetitionActionTypes.GENERATE_RULES_START;

  constructor(public payload?: any) {
  }
}

export class GenerateRulesSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.GENERATE_RULES_SUCCESS;

  constructor(public payload: CompetitionRule[]) {
  }
}

export class GenerateRulesError implements Action {
  public type: string = AdminCompetitionActionTypes.GENERATE_RULES_ERROR;

  constructor(public payload: Error) {
  }
}

export class ExpandRule implements Action {
  public type: string = AdminCompetitionActionTypes.EXPAND_RULE;

  constructor(public payload: CompetitionRule) {
  }
}

export class CollapseRule implements Action {
  public type: string = AdminCompetitionActionTypes.COLLAPSE_RULE;

  constructor(public payload: CompetitionRule) {
  }
}

export class SelectRule implements Action {
  public type: string = AdminCompetitionActionTypes.SELECT_RULE;

  constructor(public payload: CompetitionRule) {
  }
}

export class DeselectRule implements Action {
  public type: string = AdminCompetitionActionTypes.DESELECT_RULE;

  constructor(public payload: CompetitionRule) {
  }
}

export class DownloadAudioStart implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_AUDIO_START;

  constructor(public payload?: any) {
  }
}

export class DownloadAudioSuccess implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_AUDIO_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class DownloadAudioError implements Action {
  public type: string = AdminCompetitionActionTypes.DOWNLOAD_AUDIO_ERROR;

  constructor(public payload: Error) {
  }
}

export type AdminCompetitionActions =
    | AdminCompetitionsFetchStart
    | AdminCompetitionsFetchSuccess
    | AdminSelectCompetitionStart
    | AdminSelectCompetitionSuccess
    | AdminSelectCompetitionError
    | UpdateCompetitionStart
    | UpdateCompetitionSuccess
    | UpdateCompetitionError

    | AdminStylesFetchStart
    | AdminStylesFetchSuccess
    | AdminStylesFetchError
    | CreateStyleStart
    | CreateStyleSuccess
    | CreateStyleError

    | AdminCompetitionLicenceFetchStart
    | AdminCompetitionLicenceFetchSuccess
    | AdminCompetitionLicenceFetchError

    | AdminTypesFetchStart
    | AdminTypesFetchSuccess
    | AdminTypesFetchError
    | CreateTypeStart
    | CreateTypeSuccess
    | CreateTypeError

    | AdminExperiencesFetchStart
    | AdminExperiencesFetchSuccess
    | AdminExperiencesFetchError
    | CreateExperienceStart
    | CreateExperienceSuccess
    | CreateExperienceError

    | AdminAgesFetchStart
    | AdminAgesFetchSuccess
    | AdminAgesFetchError
    | CreateAgeStart
    | CreateAgeSuccess
    | CreateAgeError

    | AdminRulesFetchStart
    | AdminRulesFetchSuccess
    | AdminRulesFetchError
    | CreateRuleStart
    | CreateRuleSuccess
    | CreateRuleError
    | UpdateRuleStart
    | UpdateRuleSuccess
    | UpdateRuleError
    | GenerateRulesStart
    | GenerateRulesSuccess
    | GenerateRulesError
    | RemoveRuleStart
    | RemoveRuleSuccess
    | RemoveRuleError
    | ExpandRule
    | CollapseRule
    | SelectRule
    | DeselectRule
    | AdminInvoiceDataFetchStart
    | AdminInvoiceDataFetchSuccess
    | AdminInvoiceDataFetchError
    | CreateInvoiceStart
    | CreateInvoiceSuccess
    | CreateInvoiceError
    | DeleteInvoiceStart
    | DeleteInvoiceSuccess
    | DeleteInvoiceError
    | DownloadInvoiceStart
    | DownloadInvoiceSuccess
    | DownloadInvoiceError

    | DownloadAudioStart
    | DownloadAudioSuccess
    | DownloadAudioError
    ;