import { createFeatureSelector } from '@ngrx/store';
import { AdminModuleState } from '../model';
import * as fromAdminCompetition from './admin-competition.reducer';
import * as fromAdminParticipant from './admin-participant.reducer';

export const ADMIN_FEATURE = 'competitionAdmin';

export const initialState = {
  ...fromAdminCompetition.initialState,
  ...fromAdminParticipant.initialState
};

export const reducers = {
  competition: fromAdminCompetition.adminCompetition,
  participant: fromAdminParticipant.adminParticipant
};

export const getAdminState = createFeatureSelector<AdminModuleState>(ADMIN_FEATURE);