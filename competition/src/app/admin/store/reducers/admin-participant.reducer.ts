import {
  AdminParticipantActions,
  AdminParticipantActionTypes
} from '../actions';
import {
  AdminParticipantModuleState
} from '../model';

export const initialState: AdminParticipantModuleState = {
  list: [],
  selectedSid: undefined,
  participant: undefined,
  school: undefined,
  performances: [],
  dancers: [],
  payments: [],
  loading: {
    participants: false
  }
};

export function adminParticipant(
    state: AdminParticipantModuleState = initialState,
    action: AdminParticipantActions
): AdminParticipantModuleState {
  switch (action.type) {
    case AdminParticipantActionTypes.PARTICIPANTS_FETCH_START:
      return {
        ...state,
        loading: {
          ...state.loading,
          participants: true
        }
      };
    case AdminParticipantActionTypes.PARTICIPANTS_FETCH_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: {
          ...state.loading,
          participants: false
        }
      };
    case AdminParticipantActionTypes.SELECT_PARTICIPANT_START:
      return {
        ...state,
        selectedSid: action.payload,
        participant: state.list ?
            state.list.find(participant => participant.sid === action.payload) :
            undefined
      };
    case AdminParticipantActionTypes.SELECT_PARTICIPANT_SUCCESS:
      return {
        ...state,
        participant: action.payload
      };
    case AdminParticipantActionTypes.USER_SCHOOL_FETCH_SUCCESS:
      return {
        ...state,
        school: action.payload
      };
    case AdminParticipantActionTypes.USER_PERFORMANCES_FETCH_SUCCESS:
      return {
        ...state,
        performances: action.payload
      };
    case AdminParticipantActionTypes.USER_DANCERS_FETCH_SUCCESS:
      return {
        ...state,
        dancers: action.payload
      };
    case AdminParticipantActionTypes.USER_PAYMENTS_FETCH_SUCCESS:
      return {
        ...state,
        payments: action.payload
      };
    default:
      return state;
  }
}
