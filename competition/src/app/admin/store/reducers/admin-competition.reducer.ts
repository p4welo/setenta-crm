import { CompetitionRule, InvoiceData } from '@competition/model';
import { append } from 'ramda';

import { AdminCompetitionModuleState } from '../model';
import {
  AdminCompetitionActions,
  AdminCompetitionActionTypes,
} from '../actions';

export const initialState: AdminCompetitionModuleState = {
  list: [],
  selectedSid: undefined,
  competition: undefined,
  licence: {
    musicUpload: false,
    results: false
  },
  styles: [],
  types: [],
  experiences: [],
  ages: [],
  rules: [],
  invoiceData: [],
  downloadingAudio: false,
  loading: {
    rules: true
  }
};

export function adminCompetition(
    state: AdminCompetitionModuleState = initialState,
    action: AdminCompetitionActions
): AdminCompetitionModuleState {
  switch (action.type) {
    case AdminCompetitionActionTypes.COMPETITIONS_FETCH_SUCCESS:
      return {
        ...state,
        list: action.payload
      };
    case AdminCompetitionActionTypes.SELECT_COMPETITION_START:
      return {
        ...state,
        selectedSid: action.payload,
        competition: state.list ?
            state.list.find(competition => competition.sid === action.payload) :
            undefined
      };
    case AdminCompetitionActionTypes.SELECT_COMPETITION_SUCCESS:
      return {
        ...state,
        competition: action.payload
      };
    case AdminCompetitionActionTypes.COMPETITION_LICENCE_FETCH_SUCCESS:
      return {
        ...state,
        licence: action.payload
      };
    case AdminCompetitionActionTypes.STYLES_FETCH_SUCCESS:
      return {
        ...state,
        styles: action.payload
      };
    case AdminCompetitionActionTypes.CREATE_STYLE_SUCCESS:
      return {
        ...state,
        styles: append(action.payload, state.styles)
      };
    case AdminCompetitionActionTypes.TYPES_FETCH_SUCCESS:
      return {
        ...state,
        types: action.payload
      };
    case AdminCompetitionActionTypes.CREATE_TYPE_SUCCESS:
      return {
        ...state,
        types: append(action.payload, state.types)
      };
    case AdminCompetitionActionTypes.EXPERIENCES_FETCH_SUCCESS:
      return {
        ...state,
        experiences: action.payload
      };
    case AdminCompetitionActionTypes.CREATE_EXPERIENCE_SUCCESS:
      return {
        ...state,
        experiences: append(action.payload, state.experiences)
      };
    case AdminCompetitionActionTypes.AGES_FETCH_SUCCESS:
      return {
        ...state,
        ages: action.payload
      };
    case AdminCompetitionActionTypes.CREATE_AGE_SUCCESS:
      return {
        ...state,
        ages: append(action.payload, state.ages)
      };
    case AdminCompetitionActionTypes.RULES_FETCH_START:
      return {
        ...state,
        rules: [],
        loading: {
          ...state.loading,
          rules: true
        }
      };
    case AdminCompetitionActionTypes.ADMIN_INVOICE_DATA_FETCH_SUCCESS:
      return {
        ...state,
        invoiceData: action.payload
      };
    case AdminCompetitionActionTypes.CREATE_INVOICE_SUCCESS:
      return {
        ...state,
        invoiceData: state.invoiceData.map((data: InvoiceData) => {
          console.warn(action.payload.user);
          console.warn(data.sid);
          if (data.sid === action.payload.user.sid) {
            return {
              ...data,
              invoiceSid: action.payload.sid
            };
          } else {
            return data;
          }
        })
      };
    case AdminCompetitionActionTypes.RULES_FETCH_SUCCESS:
      return {
        ...state,
        rules: action.payload.map((rule: CompetitionRule) => ({
          ...rule,
          expanded: true
        })),
        loading: {
          ...state.loading,
          rules: false
        }
      };
    case AdminCompetitionActionTypes.GENERATE_RULES_START:
      return {
        ...state,
        loading: {
          ...state.loading,
          rules: true
        }
      };
    case AdminCompetitionActionTypes.CREATE_RULE_SUCCESS:
      return {
        ...state,
        rules: append(action.payload, state.rules)
      };
    case AdminCompetitionActionTypes.UPDATE_RULE_SUCCESS:
      return {
        ...state,
        rules: state.rules.map((rule: CompetitionRule) => {
          if (rule.sid === action.payload.sid) {
            return {
              ...rule,
              ...action.payload
            }
          } else {
            return rule;
          }
        })
      };
    case AdminCompetitionActionTypes.DELETE_INVOICE_SUCCESS:
      return {
        ...state,
        invoiceData: state.invoiceData.map((data: InvoiceData) => {
          if (data.sid === action.payload.sid) {
            return {
              ...data,
              invoiceSid: undefined
            }
          } else {
            return data;
          }
        })
      }
    case AdminCompetitionActionTypes.REMOVE_RULE_SUCCESS:
      return {
        ...state,
        rules: state.rules.filter((rule: CompetitionRule) => rule.sid !== action.payload.sid)
      };
    case AdminCompetitionActionTypes.EXPAND_RULE:
      return {
        ...state,
        rules: state.rules.map((rule: CompetitionRule) => {
          if (rule.sid === action.payload.sid) {
            return {
              ...rule,
              expanded: true
            }
          }
          return rule;
        })
      };
    case AdminCompetitionActionTypes.COLLAPSE_RULE:
      return {
        ...state,
        rules: state.rules.map((rule: CompetitionRule) => {
          if (rule.sid === action.payload.sid) {
            return {
              ...rule,
              expanded: false
            }
          }
          return rule;
        })
      };
    case AdminCompetitionActionTypes.SELECT_RULE:
      return {
        ...state,
        rules: state.rules.map((rule: CompetitionRule) => {
          return {
            ...rule,
            checked: rule.sid === action.payload.sid
          };
        })
      };
    case AdminCompetitionActionTypes.DESELECT_RULE:
      return {
        ...state,
        rules: state.rules.map((rule: CompetitionRule) => {
          if (rule.sid === action.payload.sid) {
            return {
              ...rule,
              checked: false
            }
          }
          return rule;
        })
      };
    case AdminCompetitionActionTypes.DOWNLOAD_INVOICE_START:
      return {
        ...state,
        downloadingInvoice: action.payload.sid
      };
    case AdminCompetitionActionTypes.DOWNLOAD_INVOICE_SUCCESS:
    case AdminCompetitionActionTypes.DOWNLOAD_INVOICE_ERROR:
      return {
        ...state,
        downloadingInvoice: undefined
      };
    case AdminCompetitionActionTypes.DOWNLOAD_AUDIO_START:
      return {
        ...state,
        downloadingAudio: true
      };
    case AdminCompetitionActionTypes.DOWNLOAD_AUDIO_SUCCESS:
    case AdminCompetitionActionTypes.DOWNLOAD_AUDIO_ERROR:
      return {
        ...state,
        downloadingAudio: false
      };
    default:
      return state;
  }
}