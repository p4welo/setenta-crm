import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NotificationService } from '@competition/core/services';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap, withLatestFrom, tap,
} from 'rxjs/operators';
import { saveAs } from "file-saver";

import {
  adminGetCompetitionAudio,
  adminGetCompetitionListUrl,
  getCompetitionAgesUrl,
  getCompetitionExperiencesUrl,
  getCompetitionInvoiceDatas,
  getCompetitionInvoicePdfUrl,
  getCompetitionInvoicesUrl,
  getCompetitionInvoiceUrl, getCompetitionLicenceUrl,
  getCompetitionRulesUrl,
  getCompetitionRuleUrl,
  getCompetitionStylesUrl,
  getCompetitionTypesUrl,
  getCompetitionUrl,
  getGenerateCompetitionRulesUrl
} from '@competition/core/utils';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence, CompetitionRule,
  CompetitionStyle,
  CompetitionType, Invoice, InvoiceData
} from '@competition/model';

import { AdminCompetitionService } from '../../services';
import {
  AdminCompetitionActions,
  AdminCompetitionActionTypes,
  AdminAgesFetchError,
  AdminAgesFetchSuccess,
  AdminCompetitionsFetchError,
  AdminCompetitionsFetchSuccess,
  CreateAgeError,
  CreateAgeSuccess,
  CreateExperienceError,
  CreateExperienceSuccess,
  CreateStyleError,
  CreateStyleSuccess,
  CreateTypeError,
  CreateTypeSuccess,
  DownloadAudioError,
  DownloadAudioSuccess,
  AdminExperiencesFetchError,
  AdminExperiencesFetchSuccess,
  AdminSelectCompetitionError,
  AdminSelectCompetitionSuccess,
  AdminStylesFetchError,
  AdminStylesFetchSuccess,
  AdminTypesFetchError,
  AdminTypesFetchSuccess,
  AdminRulesFetchSuccess,
  AdminRulesFetchError,
  GenerateRulesSuccess,
  GenerateRulesError,
  RemoveRuleSuccess,
  RemoveRuleError,
  AdminRulesFetchStart,
  UpdateRuleSuccess,
  UpdateRuleError,
  CreateRuleSuccess,
  CreateRuleError,
  AdminInvoiceDataFetchSuccess,
  AdminInvoiceDataFetchError,
  CreateInvoiceSuccess,
  CreateInvoiceError,
  DownloadInvoiceSuccess,
  DownloadInvoiceError,
  DeleteInvoiceSuccess,
  DeleteInvoiceError, AdminCompetitionLicenceFetchSuccess, AdminCompetitionLicenceFetchError
} from '../actions';

@Injectable()
export class AdminCompetitionEffects {
  public constructor(
      private actions$: Actions<{ type: any; payload: any }>,
      private http: HttpClient,
      private adminCompetitionService: AdminCompetitionService,
      private notificationService: NotificationService
  ) {
  }

  @Effect()
  public fetchList$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.COMPETITIONS_FETCH_START),
      switchMap(() => this.http.get<Competition[]>(adminGetCompetitionListUrl()).pipe(
          map((response) => new AdminCompetitionsFetchSuccess(response)),
          catchError((error) => of(new AdminCompetitionsFetchError(error)))
      ))
  );

  @Effect()
  public fetchSelectedCompetition$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.SELECT_COMPETITION_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<Competition>(getCompetitionUrl(competitionSid)).pipe(
              map((response) => new AdminSelectCompetitionSuccess(response)),
              catchError((error) => of(new AdminSelectCompetitionError(error)))
          )
      )
  );

  public fetchLicence$ = createEffect(() => this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.COMPETITION_LICENCE_FETCH_START),
      switchMap((action: AdminCompetitionActions) =>
          this.http.get<CompetitionLicence>(getCompetitionLicenceUrl(action.payload)).pipe(
              map((response) => new AdminCompetitionLicenceFetchSuccess(response)),
              catchError((error) => of(new AdminCompetitionLicenceFetchError(error)))
          )
      ),
  ));

  @Effect()
  public fetchStyles$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.STYLES_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<CompetitionStyle[]>(getCompetitionStylesUrl(competitionSid)).pipe(
              map((response) => new AdminStylesFetchSuccess(response)),
              catchError((error) => of(new AdminStylesFetchError(error)))
          )
      ),
  );

  @Effect()
  public fetchTypes$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.TYPES_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<CompetitionType[]>(getCompetitionTypesUrl(competitionSid)).pipe(
              map((response) => new AdminTypesFetchSuccess(response)),
              catchError((error) => of(new AdminTypesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchExperiences$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.EXPERIENCES_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<CompetitionExperience[]>(getCompetitionExperiencesUrl(competitionSid)).pipe(
              map((response) => new AdminExperiencesFetchSuccess(response)),
              catchError((error) => of(new AdminExperiencesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchAges$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.AGES_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<CompetitionAge[]>(getCompetitionAgesUrl(competitionSid)).pipe(
              map((response) => new AdminAgesFetchSuccess(response)),
              catchError((error) => of(new AdminAgesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchRules$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.RULES_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<CompetitionRule[]>(getCompetitionRulesUrl(competitionSid)).pipe(
              map((response) => new AdminRulesFetchSuccess(response)),
              catchError((error) => of(new AdminRulesFetchError(error)))
          )
      )
  );

  @Effect()
  public generateRules$: Observable<any> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.GENERATE_RULES_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<CompetitionRule[]>(getGenerateCompetitionRulesUrl(competitionSid), {}).pipe(
              switchMap((response) => [
                  new GenerateRulesSuccess(response),
                  new AdminRulesFetchStart()
              ]),
              catchError((error) => of(new GenerateRulesError(error)))
          )
      )
  );

  @Effect()
  public createRule$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_RULE_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.selectedRule$
      ),
      switchMap(([action, competitionSid, selectedRule]: [AdminCompetitionActions, string, CompetitionRule]) =>{
        const parentSid: string = selectedRule ? selectedRule.sid : '';
        return this.http.post<any>(getCompetitionRuleUrl(competitionSid, parentSid), action.payload).pipe(
            map((response) => new CreateRuleSuccess(response)),
            catchError((error) => of(new CreateRuleError(error)))
        );
      })
  );

  @Effect()
  public removeRule$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.REMOVE_RULE_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.selectedRule$
      ),
      switchMap(([a, competitionSid, selectedRule]: [AdminCompetitionActions, string, CompetitionRule]) =>
          this.http.delete(getCompetitionRuleUrl(competitionSid, selectedRule.sid)).pipe(
              map((response) => new RemoveRuleSuccess(selectedRule)),
              catchError((error) => of(new RemoveRuleError(error)))
          )
      )
  );

  @Effect()
  public updateRule$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.UPDATE_RULE_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.selectedRule$
      ),
      switchMap(([action, competitionSid, selectedRule]: [AdminCompetitionActions, string, CompetitionRule]) =>
          this.http.put<CompetitionRule>(getCompetitionRuleUrl(competitionSid, selectedRule.sid), action.payload).pipe(
              map((response) => new UpdateRuleSuccess(response)),
              catchError((error) => of(new UpdateRuleError(error)))
          )
      )
  );

  @Effect()
  public createStyle$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_STYLE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<CompetitionStyle>(getCompetitionStylesUrl(competitionSid), action.payload).pipe(
              map((response) => new CreateStyleSuccess(response)),
              catchError((error) => of(new CreateStyleError(error)))
          )
      )
  );

  @Effect()
  public createType$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_TYPE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<CompetitionType>(getCompetitionTypesUrl(competitionSid), action.payload).pipe(
              map((response) => new CreateTypeSuccess(response)),
              catchError((error) => of(new CreateTypeError(error)))
          )
      )
  );

  @Effect()
  public createExperience$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_EXPERIENCE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<CompetitionExperience>(getCompetitionExperiencesUrl(competitionSid), action.payload).pipe(
              map((response) => new CreateExperienceSuccess(response)),
              catchError((error) => of(new CreateExperienceError(error)))
          )
      )
  );

  @Effect()
  public createAge$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_AGE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<CompetitionAge>(getCompetitionAgesUrl(competitionSid), action.payload).pipe(
              map((response) => new CreateAgeSuccess(response)),
              catchError((error) => of(new CreateAgeError(error)))
          )
      )
  );

  @Effect({ dispatch: false })
  public createSuccess$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(
          AdminCompetitionActionTypes.CREATE_STYLE_SUCCESS,
          AdminCompetitionActionTypes.CREATE_TYPE_SUCCESS,
          AdminCompetitionActionTypes.CREATE_EXPERIENCE_SUCCESS,
          AdminCompetitionActionTypes.CREATE_AGE_SUCCESS,
          AdminCompetitionActionTypes.CREATE_RULE_SUCCESS,
          AdminCompetitionActionTypes.GENERATE_RULES_SUCCESS,
          AdminCompetitionActionTypes.UPDATE_RULE_SUCCESS,
          AdminCompetitionActionTypes.CREATE_INVOICE_SUCCESS
      ),
      tap(() => this.notificationService.success('Pomyślnie zapisano!'))
  );

  @Effect({ dispatch: false })
  public removeSuccess$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(
          AdminCompetitionActionTypes.REMOVE_RULE_SUCCESS,
          AdminCompetitionActionTypes.DELETE_INVOICE_SUCCESS
      ),
      tap(() => this.notificationService.success('Pomyślnie usunięto!'))
  );

  @Effect({ dispatch: false })
  public downloadInvoiceError$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_INVOICE_ERROR),
      tap(() => this.notificationService.error('Wystąpił problem z pobraniem faktury'))
  );

  @Effect()
  public downloadAudio$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_AUDIO_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get(adminGetCompetitionAudio(competitionSid), {
            responseType: "blob",
            headers: new HttpHeaders().append("Content-Type", "application/zip")
          }).pipe(
              map((response) => new DownloadAudioSuccess(response)),
              catchError((error) => of(new DownloadAudioError(error)))
          )
      )
  );

  @Effect({ dispatch: false })
  public saveAudio$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_AUDIO_SUCCESS),
      map((action: DownloadAudioSuccess) => action.payload),
      tap((data) => {
        saveAs(data, 'muzyka.zip');
      })
  );

  @Effect()
  public downloadInvoice$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_INVOICE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get(getCompetitionInvoicePdfUrl(competitionSid, action.payload.invoiceSid), {
            responseType: "blob",
            headers: new HttpHeaders().append("Content-Type", "application/pdf")
          }).pipe(
              map((response) => new DownloadInvoiceSuccess({
                data: response,
                invoiceData: action.payload
              })),
              catchError((error) => of(new DownloadInvoiceError(error)))
          )
      )
  );

  @Effect({ dispatch: false })
  public saveInvoice$: Observable<any> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_INVOICE_SUCCESS),
      map((action: DownloadInvoiceSuccess) => action.payload),
      tap(({data, invoiceData}) => {
        saveAs(data, `${invoiceData.name}-faktura.pdf`);
      })
  );

  @Effect({ dispatch: false })
  public downloadError$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DOWNLOAD_AUDIO_ERROR),
      tap(() => this.notificationService.error('Wystąpił błąd!'))
  );

  @Effect()
  public fetchInvoiceData$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.ADMIN_INVOICE_DATA_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.get<InvoiceData[]>(getCompetitionInvoiceDatas(competitionSid)).pipe(
              map((response) => new AdminInvoiceDataFetchSuccess(response)),
              catchError((error) => of(new AdminInvoiceDataFetchError(error)))
          )
      )
  );

  @Effect()
  public createInvoice$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.CREATE_INVOICE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.post<Invoice>(getCompetitionInvoicesUrl(competitionSid), action.payload).pipe(
              map((response) => new CreateInvoiceSuccess(response)),
              catchError((error) => of(new CreateInvoiceError(error)))
          )
      )
  );

  @Effect()
  public deleteInvoice$: Observable<AdminCompetitionActions> = this.actions$.pipe(
      ofType(AdminCompetitionActionTypes.DELETE_INVOICE_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminCompetitionActions, string]) =>
          this.http.delete(getCompetitionInvoiceUrl(competitionSid, action.payload.invoiceSid)).pipe(
              map((response) => new DeleteInvoiceSuccess(action.payload)),
              catchError((error) => of(new DeleteInvoiceError(error)))
          )
      )
  );

}