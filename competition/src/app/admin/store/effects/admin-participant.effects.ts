import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PageSize } from '@competition/model/print.model';
import * as jsPDF from 'jspdf';

import { AdminCompetitionService } from '@competition/admin/services';
import {
  AdminParticipantActions,
  AdminParticipantActionTypes,
  GenerateSchoolCardSuccess,
  GenerateSchoolNumbersSuccess,
  ParticipantsFetchError,
  ParticipantsFetchSuccess,
  UserDancersFetchError,
  UserDancersFetchSuccess,
  UserPaymentsFetchSuccess,
  UserPerformancesFetchError,
  UserPerformancesFetchSuccess,
  UserSchoolFetchError,
  UserSchoolFetchSuccess,
} from '../actions';
import {
  adminGetCompetitionUserDancers,
  adminGetCompetitionUserPerformances,
  adminGetCompetitionUsers,
  getUserPaymentsUrl,
  getUserSchoolUrl
} from '@competition/core/utils';
import {
  AdminCompetitionParticipant, CompetitionPayment,
  CompetitionType,
  Dancer_,
  Performance, School
} from '@competition/model';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class AdminParticipantEffects {
  public constructor(
      private actions$: Actions<{ type: any; payload: any }>,
      private http: HttpClient,
      private adminCompetitionService: AdminCompetitionService
  ) {
  }

  @Effect()
  public fetchList$: Observable<AdminParticipantActions> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.PARTICIPANTS_FETCH_START),
      withLatestFrom(this.adminCompetitionService.competitionSid$),
      switchMap(([action, competitionSid]: [AdminParticipantActions, string]) =>
          this.http.get<AdminCompetitionParticipant[]>(adminGetCompetitionUsers(competitionSid)).pipe(
              map((response) => new ParticipantsFetchSuccess(response)),
              catchError((error) => of(new ParticipantsFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchUserSchool$: Observable<AdminParticipantActions> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.USER_SCHOOL_FETCH_START),
      withLatestFrom(this.adminCompetitionService.participantSid$),
      switchMap(([action, participantSid]: [AdminParticipantActions, string]) =>
          this.http.get<School>(getUserSchoolUrl(participantSid)).pipe(
              map((response) => new UserSchoolFetchSuccess(response)),
              catchError((error) => of(new UserSchoolFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchUserPerformances$: Observable<AdminParticipantActions> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.USER_PERFORMANCES_FETCH_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.participantSid$
      ),
      switchMap(([a, competitionSid, participantSid]: [AdminParticipantActions, string, string]) =>
          this.http.get<Performance[]>(adminGetCompetitionUserPerformances(competitionSid, participantSid)).pipe(
              map((response) => new UserPerformancesFetchSuccess(response)),
              catchError((error) => of(new UserPerformancesFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchUserDancers$: Observable<AdminParticipantActions> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.USER_DANCERS_FETCH_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.participantSid$
      ),
      switchMap(([a, competitionSid, participantSid]: [AdminParticipantActions, string, string]) =>
          this.http.get<Dancer_[]>(adminGetCompetitionUserDancers(competitionSid, participantSid)).pipe(
              map((response) => new UserDancersFetchSuccess(response)),
              catchError((error) => of(new UserDancersFetchError(error)))
          )
      )
  );

  @Effect()
  public fetchUserPayments$: Observable<AdminParticipantActions> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.USER_PAYMENTS_FETCH_START),
      withLatestFrom(
          this.adminCompetitionService.competitionSid$,
          this.adminCompetitionService.participantSid$
      ),
      switchMap(([a, competitionSid, participantSid]: [AdminParticipantActions, string, string]) =>
          this.http.get<CompetitionPayment[]>(getUserPaymentsUrl(competitionSid, participantSid)).pipe(
              map((response) => new UserPaymentsFetchSuccess(response)),
              catchError((error) => of(new UserPerformancesFetchError(error)))
          )
      )
  );

  @Effect()
  public generateNumbers$: Observable<any> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.GENERATE_SCHOOL_NUMBERS_START),
      map((action: AdminParticipantActions) => action.payload),
      withLatestFrom(
          this.adminCompetitionService.userPerformances$,
          this.adminCompetitionService.userSchool$,
          this.adminCompetitionService.typesWithName$
      ),
      tap(([pageSize, performances, school, typesWithName]: [PageSize, Performance[], School, CompetitionType[]]) => {
        switch (pageSize) {
          case PageSize.A5:
            return this.generateNumbersPdfA5(performances, school, typesWithName);
          default:
            return this.generateNumbersPdfA4(performances, school, typesWithName);
        }
      }),
      map(() => new GenerateSchoolNumbersSuccess())
  );

  @Effect()
  public generateSchoolCard$: Observable<any> = this.actions$.pipe(
      ofType(AdminParticipantActionTypes.GENERATE_SCHOOL_CARD_START),
      withLatestFrom(
          this.adminCompetitionService.userPerformances$,
          this.adminCompetitionService.userSchool$,
          this.adminCompetitionService.userDancers$
      ),
      tap(([a, performances, school, dancers]: [AdminParticipantActions, Performance[], School, Dancer_[]]) =>
          this.generateSchoolCardPdf(performances, school, dancers)
      ),
      map(() => new GenerateSchoolCardSuccess())
  );

  private escapePolishChars(label: string): string {
    return label
        .split('ń').join('n')
        .split('ą').join('a')
        .split('ę').join('e')
        .split('ó').join('o')
        .split('ź').join('z')
        .split('ż').join('z')
        .split('ł').join('l')
        .split('Ń').join('N')
        .split('Ą').join('A')
        .split('Ę').join('E')
        .split('Ó').join('O')
        .split('Ź').join('Z')
        .split('Ż').join('Z')
        .split('Ł').join('L');
  }

  private generateSchoolCardPdf(performances: Performance[] = [], school: School, dancers: Dancer_[] = []): void {
    const pdf = new jsPDF();
    pdf.setFontSize(24).text(this.escapePolishChars(school.name), 20, 20);
    pdf.setFontSize(12).text(this.escapePolishChars(school.address.city), 20, 25);
    pdf.setFontSize(12).text(`Liczba tancerzy: ${dancers.length}`, 20, 40);
    pdf.setFontSize(12).text(`Liczba wystepów: ${performances.length}`, 20, 45);
    pdf.save(`${school.name.toLowerCase().split(' ').join('_')}-karta-szkoly.pdf`);
  }

  private generateNumbersPdfA5(performances: Performance[], school: School, typesWithName: CompetitionType[]): void {
    const pdf = new jsPDF();
    const length = performances.length;
    const canShowName = (performance: Performance, typesWithName: CompetitionType[]) =>
        typesWithName.filter((type) => type.name === performance.type).length > 0;

    for (let i = 0; i < length; i += 2) {
      let performance = performances[i];
      let showName = canShowName(performance, typesWithName);
      this.printPerformance(pdf, performance, showName, 100);

      if (i < length - 1) {
        performance = performances[i + 1];
        showName = canShowName(performance, typesWithName);
        this.printPerformance(pdf, performance, showName, 250);
      }

      if (i < length - 2) {
        pdf.addPage();
      }
    }
    pdf.save(`${school.name.toLowerCase().split(' ').join('_')}-numery-startowe.pdf`);
  }

  private generateNumbersPdfA4(performances: Performance[], school: School, typesWithName: CompetitionType[]): void {
    const pdf = new jsPDF();
    const length = performances.length;
    const canShowName = (performance: Performance, typesWithName: CompetitionType[]) =>
        typesWithName.filter((type) => type.name === performance.type).length > 0;

    for (let i = 0; i < length; i++) {
      let performance = performances[i];
      let showName = canShowName(performance, typesWithName);
      this.printPerformance(pdf, performance, showName, 150);

      if (i < length - 1) {
        pdf.addPage();
      }
    }
    pdf.save(`${school.name.toLowerCase().split(' ').join('_')}-numery-startowe.pdf`);
  }

  private printPerformance(pdf: jsPDF, performance: Performance, canShowName: boolean, y: number): void {
    // NUMBER
    // pdf.setFontSize(300);
    pdf.setFontSize(270);
    pdf.setFontStyle('bold');
    pdf.setTextColor(0);
    pdf.text(String(performance.number), 105, y, { align: 'center' });

    // LABEL
    pdf.setFontSize(10);
    pdf.setFontStyle('normal');
    // @ts-ignore
    pdf.setTextColor('#777777');
    pdf.text(this.getPerformancePrintLabel1(performance, canShowName), 105, y + 20, { align: 'center' });
    pdf.text(this.getPerformancePrintLabel2(performance, canShowName), 105, y + 25, { align: 'center' });
  }

  private getPerformancePrintLabel1(performance: Performance, canShowName: boolean): string {
    const namePart = canShowName ?
        performance.name :
        performance.dancers.reduce((accumulator, dancer: Dancer_) =>
            `${dancer.firstName} ${dancer.lastName} ${!!accumulator ? ('- ' + accumulator) : accumulator}`, '');
    // const getNotEmpty = (label) => label ? ` / ${label}` : '';
    //
    // const stylePart = getNotEmpty(performance.style);
    // const typePart = getNotEmpty(performance.type);
    // const experiencePart = getNotEmpty(performance.experience);
    // const agePart = getNotEmpty(performance.ageLevel);

    return this.escapePolishChars(`${namePart}`);
  }

  private getPerformancePrintLabel2(performance: Performance, canShowName: boolean): string {
    const getNotEmpty = (label) => label ? ` - ${label}` : '';

    const stylePart = performance.style;
    const typePart = getNotEmpty(performance.type);
    const experiencePart = getNotEmpty(performance.experience);
    const agePart = getNotEmpty(performance.ageLevel);

    return this.escapePolishChars(`${stylePart}${typePart}${experiencePart}${agePart}`);
  }


}