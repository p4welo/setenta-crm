import {
  AdminCompetitionParticipant,
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence, CompetitionPayment, CompetitionRule,
  CompetitionStyle,
  CompetitionType, Dancer_, InvoiceData, Performance, School
} from '@competition/model';

export interface AdminModuleState {
  competition: AdminCompetitionModuleState;
  participant: AdminParticipantModuleState;
}

export interface AdminCompetitionModuleState {
  list: Competition[];
  selectedSid: string | undefined;
  competition: Competition | undefined;
  licence: CompetitionLicence;
  styles: CompetitionStyle[];
  types: CompetitionType[];
  experiences: CompetitionExperience[];
  ages: CompetitionAge[];
  rules: CompetitionRule[];
  invoiceData: InvoiceData[];
  downloadingInvoice?: string;
  downloadingAudio: boolean;
  loading: {
    rules: boolean;
  };
}

export interface AdminParticipantModuleState {
  list: AdminCompetitionParticipant[];
  selectedSid: string | undefined;
  participant: AdminCompetitionParticipant | undefined;
  school: School | undefined;
  performances: Performance[] | undefined;
  dancers: Dancer_[] | undefined;
  payments: CompetitionPayment[] | undefined;
  loading: {
    participants: boolean;
  }
}