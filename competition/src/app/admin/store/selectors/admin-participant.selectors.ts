import { createSelector } from '@ngrx/store';
import { AdminModuleState } from '../model';
import * as fromFeature from '../reducers';

export const getAdminParticipant = createSelector(
    fromFeature.getAdminState,
    (state: AdminModuleState) => state.participant
);

export const getLoadingState = createSelector(
    getAdminParticipant,
    state => state.loading
);

export const getParticipantList = createSelector(
    getAdminParticipant,
    state => state.list
);

export const isLoadingParticipantList = createSelector(
    getLoadingState,
    state => state.participants
);

export const getParticipantSid = createSelector(
    getAdminParticipant,
    state => state.selectedSid
);

export const getUserDancers = createSelector(
    getAdminParticipant,
    state => state.dancers
);

export const getUserPayments = createSelector(
    getAdminParticipant,
    state => state.payments
);

export const getUserPerformances = createSelector(
    getAdminParticipant,
    state => state.performances
);

export const getUserSchool = createSelector(
    getAdminParticipant,
    state => state.school
);
