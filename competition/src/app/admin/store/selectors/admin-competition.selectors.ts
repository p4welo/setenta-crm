import { getSortedList } from '@competition/core/utils';
import { Competition, CompetitionType } from '@competition/model';
import { createSelector } from '@ngrx/store';
import * as dayjs from 'dayjs';

import { AdminModuleState } from '../model';
import * as fromFeature from '../reducers';

export const getAdminCompetition = createSelector(
    fromFeature.getAdminState,
    (state: AdminModuleState) => state.competition
);

export function getInitialState(): any {
  return fromFeature.initialState;
}

export const getList = createSelector(
    getAdminCompetition,
    state => state.list
);

export const getOrderedList = createSelector(
    getList,
    (competitions: Competition[]) => getSortedList(competitions, 'start')
);

const isOutdated = (competition: Competition) => {
  return dayjs().isAfter(dayjs(competition.end));
};

export const getUpcomingList = createSelector(
    getOrderedList,
    (competitions: Competition[]) => competitions.filter((competition: Competition) => !isOutdated(competition))
);

export const getOutdatedList = createSelector(
    getOrderedList,
    (competitions: Competition[]) => competitions.filter((competition: Competition) => isOutdated(competition))
);

export const getCompetitionSid = createSelector(
    getAdminCompetition,
    state => state.selectedSid
);

export const getCompetition = createSelector(
    getAdminCompetition,
    state => state.competition
);

export const getLicence = createSelector(
    getAdminCompetition,
    state => state.licence
);

export const getStyles = createSelector(
    getAdminCompetition,
    state => state.styles
);

export const getTypes = createSelector(
    getAdminCompetition,
    state => state.types
);

export const getTypesWithName = createSelector(
    getTypes,
    (types: CompetitionType[] = []) => types.filter((type) => type.showName)
);

export const getExperiences = createSelector(
    getAdminCompetition,
    state => state.experiences
);

export const getAges = createSelector(
    getAdminCompetition,
    state => state.ages
);

export const getInvoiceData = createSelector(
    getAdminCompetition,
    state => state.invoiceData
);

export const getDownloadingInvoiceSid = createSelector(
    getAdminCompetition,
    state => state.downloadingInvoice
);

export const getRules = createSelector(
    getAdminCompetition,
    state => state.rules
);

export const getSelectedRule = createSelector(
    getAdminCompetition,
    state => {
      const found = state.rules.filter((rule) => rule.checked);
      return found.length > 0 ? found[0] : null;
    }
);

export const getNewRules = createSelector(
    getAdminCompetition,
    state => nest(state.rules)
);

const nest = (items, id = 0, link = 'parentId') => items
    .filter(item => item[link] === id)
    .map(item => ({ ...item, rules: nest(items, item.id) }));

export const isDownloadingAudio = createSelector(
    getAdminCompetition,
    state => state.downloadingAudio
);

export const isRulesLoading = createSelector(
    getAdminCompetition,
    state => state.loading.rules
);
