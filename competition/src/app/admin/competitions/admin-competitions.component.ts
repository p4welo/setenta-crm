import { Component } from '@angular/core';
import { AdminCompetitionService } from '@competition/admin/services';
import { AdminCompetitionsFetchStart } from '@competition/admin/store';
import { Competition } from '@competition/model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './admin-competitions.component.html',
  styleUrls: ['./admin-competitions.component.scss']
})
export class AdminCompetitionsComponent {
  public upcomingList$: Observable<Competition[]>;
  public outdatedList$: Observable<Competition[]>;

  constructor(
      private competitionsService: AdminCompetitionService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.upcomingList$ = this.competitionsService.upcomingList$;
    this.outdatedList$ = this.competitionsService.outdatedList$;

    this.store$.dispatch(new AdminCompetitionsFetchStart());
  }
}