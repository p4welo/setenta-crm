import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParticipantsFetchStart } from '@competition/admin/store';

import { getSortingColumnClass } from '@competition/core/utils';
import { AdminCompetitionParticipant } from '@competition/model';
import { MatchMediaService } from '@competition/modules/match-media/match-media.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AdminCompetitionService } from '../services';

@Component({
  templateUrl: './admin-competition-participant-list.component.html',
  styleUrls: ['./admin-competition-participant-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCompetitionParticipantListComponent implements OnInit {

  public participants$: Observable<AdminCompetitionParticipant[]> = this.adminCompetitionService.participantList$;
  public loadingParticipants$: Observable<boolean> = this.adminCompetitionService.isLoadingParticipants$;
  public sortingProperty: string = 'fee';
  public sortingAscending: boolean = false;
  public media$: Observable<any>;

  constructor(
      private route: ActivatedRoute,
      private adminCompetitionService: AdminCompetitionService,
      private matchMediaService: MatchMediaService,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    this.media$ = this.matchMediaService.resize$;
    this.store$.dispatch(new ParticipantsFetchStart());
  }

  // SORTING
  public sortBy(property: string): void {
    if (this.sortingProperty === property) {
      this.sortingAscending = !this.sortingAscending;
    } else {
      this.sortingProperty = property;
      this.sortingAscending = true;
    }
  }

  public getSortingClass(property: string): string {
    return getSortingColumnClass(property, this.sortingProperty, this.sortingAscending);
  }

  public dancerSum(participants: AdminCompetitionParticipant[]): number {
    if (!participants) return 0;
    return participants.reduce((accumulator: number, current: AdminCompetitionParticipant) => accumulator + current.dancersAmount, 0);
  }

  public performanceSum(participants: AdminCompetitionParticipant[]): number {
    if (!participants) return 0;
    return participants.reduce((accumulator: number, current: AdminCompetitionParticipant) => accumulator + current.performancesAmount, 0);
  }

  public feeSum(participants: AdminCompetitionParticipant[]): number {
    if (!participants) return 0;
    return participants.reduce((accumulator: number, current: AdminCompetitionParticipant) => accumulator + current.fee, 0);
  }

  public availableSum(participants: AdminCompetitionParticipant[]): number {
    if (!participants) return 0;
    return participants.reduce((accumulator: number, current: AdminCompetitionParticipant) => accumulator + current.payments || 0, 0);
  }

  public isPaid(participant: AdminCompetitionParticipant): boolean {
    return participant.fee <= (participant.payments || 0);
  }

  public getBalance(participant: AdminCompetitionParticipant): number {
    return (participant.payments || 0) - participant.fee;
  }
}