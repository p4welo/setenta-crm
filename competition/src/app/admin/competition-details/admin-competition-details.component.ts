import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent } from '@angular/router';
import { AdminCompetitionService } from '../services';
import { AdminCompetitionLicenceFetchStart, AdminSelectCompetitionStart } from '../store';
import { Competition } from '@competition/model';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-details.component.html',
  styleUrls: ['./admin-competition-details.component.scss']
})
export class AdminCompetitionDetailsComponent implements OnInit, OnDestroy {

  public competition$: Observable<Competition> = this.adminCompetitionService.competition$;

  public mobileMenuVisible: boolean = false;
  public urlChangeSubscription: Subscription = new Subscription();

  constructor(
      private route: ActivatedRoute,
      private adminCompetitionService: AdminCompetitionService,
      private router: Router,
      private store$: Store<any>
  ) {
  }

  public ngOnInit(): void {
    const competitionSid = this.route.snapshot.paramMap.get('competitionSid');
    this.store$.dispatch(new AdminSelectCompetitionStart(competitionSid));
    this.store$.dispatch(new AdminCompetitionLicenceFetchStart(competitionSid));

    this.urlChangeSubscription.add(
        this.router.events
            .pipe(filter(e => e instanceof RouterEvent))
            .subscribe(() => this.mobileMenuVisible = false)
    );
  }

  public ngOnDestroy(): void {
    this.urlChangeSubscription.unsubscribe();
  }

  public toggleMobileMenu(): void {
    this.mobileMenuVisible = !this.mobileMenuVisible;
  }
}
