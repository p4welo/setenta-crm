import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DownloadAudioStart, AdminTypesFetchStart } from '@competition/admin/store';
import { PerformanceSongService } from '@competition/modules/performance-song/services';
import { Store } from '@ngrx/store';
import { AdminCompetitionService } from '@competition/admin/services';
import { CompetitionsService, NotificationService, PrintService } from '@competition/core/services';
import { competitionDayName } from '@competition/core/utils';
import {
  AdminCompetitionParticipant,
  Competition, CompetitionLicence,
  CompetitionStyle,
  CompetitionType,
  PerformanceBlock,
  PerformanceSection,
  PerformanceSectionWrapper
} from '@competition/model';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-starting-list.component.html',
  styleUrls: ['./admin-competition-starting-list.component.scss']
})
export class AdminCompetitionStartingListComponent implements OnInit {

  public participants$: Observable<AdminCompetitionParticipant[]>;
  public types$: Observable<CompetitionType[]> = this.adminCompetitionService.types$;
  public isDownloadingAudio: Observable<boolean> = this.adminCompetitionService.isDownloadingAudio$;
  public playingSongSid$: Observable<string> = this.performanceSongService.playingSongSid$;
  public licence$: Observable<CompetitionLicence> = this.adminCompetitionService.licence$;

  public list: PerformanceSectionWrapper;
  public competition: Competition;
  public dayNumbers: string[];
  public stylesWithTitle: CompetitionStyle[];
  public editMode: boolean = false;
  public filterMode: boolean = false;
  public resultsVisible: boolean = false;
  public selectedParticipant: AdminCompetitionParticipant;

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private notificationService: NotificationService,
      private adminCompetitionService: AdminCompetitionService,
      private competitionsService: CompetitionsService,
      private store$: Store<any>,
      private printService: PrintService,
      private performanceSongService: PerformanceSongService
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.loadCompetition();
    this.loadSections();
    this.store$.dispatch(new AdminTypesFetchStart());
    this.loadStyles();
    this.participants$ = this.adminCompetitionService.getCompetitionParticipants(this.competitionSid);
  }

  public getChapterNumbers(dayNumber: string): string[] {
    return this.list ? Object.keys(this.list[dayNumber]) : [];
  }

  public getSections(dayNumber: string, chapterNumber: string): PerformanceSection[] {
    return this.list[dayNumber][chapterNumber];
  }

  public getDayName(dayNumber: string): string {
    return competitionDayName(this.competition, +dayNumber);
  }

  public noPerformancesInside(sections: PerformanceSection[] = []): boolean {
    return sections.filter((section: PerformanceSection) => section.performances.length > 0).length === 0;
  }

  public print(): void {
    this.printService.print();
  }

  public toggleResults(): void {
    this.resultsVisible = !this.resultsVisible;
  }

  public downloadAudio(): void {
    this.store$.dispatch(new DownloadAudioStart());
  }

  public generate(): void {
    if (confirm('Czy na pewno chcesz wygenerować numery startowe OD NOWA?')) {
      this.competitionsService.generateCompetitionStartingList(this.competitionSid)
          .subscribe((value) => {
            this.notificationService.success('Pomyślnie wygenerowano');
            this.dayNumbers = Object.keys(value);
            this.list = value;
          });
    }
  }

  public get selectedParticipantName(): string {
    if (this.selectedParticipant) {
      return `${this.selectedParticipant.name} - ${this.selectedParticipant.city}`;
    }
    return 'Wybierz uczestnika';
  }

  public select(participant: AdminCompetitionParticipant): void {
    this.selectedParticipant = participant;
    this.loadSections(participant.sid);
  }

  public clear(): void {
    this.selectedParticipant = undefined;
    this.filterMode = false;
    this.loadSections();
  }

  public canShowTitle(section: PerformanceSection): boolean {
    if (this.stylesWithTitle && this.stylesWithTitle.length > 0) {
      let found = false;
      this.stylesWithTitle.forEach((styleWithTitle: CompetitionStyle) => {
        section.blocks.forEach((block: PerformanceBlock) => {
          if (block.style === styleWithTitle.name) {
            found = true;
            return;
          }
        });
      });
      return found;
    }
    return false;
  }

  private loadSections(userSid?: string): void {
    this.competitionsService.getCompetitionStartingList(this.competitionSid, userSid)
        .subscribe((value) => {
          this.dayNumbers = Object.keys(value);
          this.list = value;
        });
  }

  private loadStyles(): void {
    this.competitionsService.getCompetitionStyles(this.competitionSid)
        .subscribe((value: CompetitionStyle[]) => {
          this.stylesWithTitle = value.filter((style: CompetitionStyle) => style.showTitle);
        });
  }

  private loadCompetition(): void {
    this.adminCompetitionService.getCompetition(this.competitionSid)
        .subscribe((value) => {
          this.competition = value;
        });
  }
}