import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '../services';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-performance-list.component.html'
})
export class AdminCompetitionPerformanceListComponent implements OnInit{
  public grouppedPerformances;
  public categories: string[];

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private adminCompetitionService: AdminCompetitionService
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.adminCompetitionService.getPerformanceCategoriesUrl(this.competitionSid)
        .subscribe((groupped) => {
          this.grouppedPerformances = groupped;
          this.categories = Object.keys(groupped).sort();
        });
  }

  public splitted(category: string): string[] {
    return category.split('|');
  }

  public get canShowExperience(): boolean {
    if (!this.categories) {
      return false;
    }
    let found = false;
    this.categories.forEach((category) => {
      if (!!this.splitted(category)[2]) {
        found = true;
      }
    });
    return found;
  }
}