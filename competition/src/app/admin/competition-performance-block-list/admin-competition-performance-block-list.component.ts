import { HttpClient } from '@angular/common/http';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '@competition/admin/services';
import { CompetitionsService, NotificationService } from '@competition/core/services';
import { competitionDayName, getCompetitionPerformanceSectionUrl } from '@competition/core/utils';
import {
  Competition,
  CompetitionAge,
  CompetitionExperience,
  CompetitionStyle,
  CompetitionType,
  PerformanceSection,
  PerformanceSectionWrapper
} from '@competition/model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { pathOr } from 'ramda';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-performance-block-list.component.html'
})
export class AdminCompetitionPerformanceBlockListComponent implements OnInit {

  public sections: PerformanceSectionWrapper;
  public competition: Competition;
  public styles: CompetitionStyle[];
  public experiences: CompetitionExperience[];
  public types: CompetitionType[];
  public ages: CompetitionAge[];

  public dayNumbers: string[];
  public daysExpanded = {0: true};
  public addToBlockModalRef: BsModalRef;
  public editMode: boolean = false;

  private competitionSid: string;

  public selectedDayId: string;
  public selectedChapterId: string;
  public ngForm: FormGroup;

  constructor(
      private modalService: BsModalService,
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private adminCompetitionService: AdminCompetitionService,
      private notificationService: NotificationService,
      private competitionsService: CompetitionsService,
      private http: HttpClient
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.ngForm = this.formBuilder.group({
      style: ['', Validators.required],
      type: ['', Validators.required],
      experience: ['', Validators.required],
      age: ['', Validators.required]
    });

    this.loadCompetition();
    this.loadSections();
    this.loadStyles();
    this.loadExperiences();
    this.loadTypes();
    this.loadAges();
  }

  public addToBlock(dayNumber: string, chapterNumber: string, template: TemplateRef<any>): void {
    this.selectedDayId = dayNumber;
    this.selectedChapterId = chapterNumber;
    this.addToBlockModalRef = this.modalService.show(template);
  }

  public saveInChapter(block: any): void {
    const chapterSections = pathOr([], [this.selectedDayId, this.selectedChapterId], this.sections);
    const lengthInChapter = chapterSections.length;
    const lastPosition = lengthInChapter > 0 ? chapterSections[lengthInChapter-1].position : -1;

    this.competitionsService.createCompetitionPerformanceSection(this.competitionSid, {
      chapter: this.selectedChapterId,
      day: this.selectedDayId,
      position: lastPosition + 1,
      blocks: [block]
    })
        .pipe(first())
        .subscribe(() => {
          this.notificationService.success('Pomyślnie zapisano');
          this.loadSections();
        });
    this.addToBlockModalRef.hide();
  }

  public swapSections(section: PerformanceSection, destination: PerformanceSection): void {
    this.adminCompetitionService.swapSections(this.competitionSid, section.sid, destination.sid)
        .subscribe(() => {
          this.notificationService.success('Pomyślnie zamieniono');
          this.loadSections();
        });
  }

  public mergeSections(section: PerformanceSection, destination: PerformanceSection): void {
    if (confirm('Czy na pewno chcesz połączyć kategorie?')) {
      this.adminCompetitionService.mergeSections(this.competitionSid, section.sid, destination.sid)
          .subscribe(() => {
            this.notificationService.success('Pomyślnie połączono');
            this.loadSections();
          });
    }
  }

  public changeChapter(section: PerformanceSection, destinationChapter: number): void {
    console.warn('changeChapter', section, destinationChapter);
    const newPosition =
    this.http.put<PerformanceSection>(getCompetitionPerformanceSectionUrl(this.competitionSid, section.sid), {chapter: destinationChapter})
        .subscribe(() => {
          this.notificationService.success('Pomyślnie przeniesiono');
          this.loadSections();
        })
  }

  public removeSection(section: PerformanceSection): void {
    this.competitionsService.removeCompetitionPerformanceSection(this.competitionSid, section.sid)
        .subscribe(() => {
          this.notificationService.success('Pomyślnie usunięto');
          this.loadSections();
        })
  }

  public getDayName(dayNumber: string): string {
    return competitionDayName(this.competition, +dayNumber);
  }

  public getDaysAmount(): number {
    return this.dayNumbers.length;
  }

  public getChapterNumbers(dayNumber: string): string[] {

    return this.sections ? Object.keys(this.sections[dayNumber]): [];
  }

  public getChaptersAmount(dayNumber: string): number {
    return this.getChapterNumbers(dayNumber).length;
  }

  public getSections(dayNumber: string, chapterNumber: string): PerformanceSection[] {
    return this.sections[dayNumber][chapterNumber];
  }

  public getSectionsAmount(dayNumber: string, chapterNumber): number {
    return this.getSections(dayNumber, chapterNumber).length;
  }

  private loadCompetition(): void {
    this.adminCompetitionService.getCompetition(this.competitionSid)
        .pipe(first())
        .subscribe((value) => {
          this.competition = value;
        });
  }

  private loadSections(): void {
    this.competitionsService.getCompetitionPerformanceSections(this.competitionSid)
        .pipe(first())
        .subscribe((value) => {
          this.dayNumbers = Object.keys(value);
          this.sections = value;
        });
  }

  private loadStyles(): void {
    this.adminCompetitionService.getCompetitionStyles(this.competitionSid)
        .pipe(first())
        .subscribe((value) => {
          this.styles = value;
        });
  }

  private loadTypes(): void {
    this.competitionsService.getCompetitionTypes(this.competitionSid)
        .subscribe((value) => {
          this.types = value;
        });
  }

  private loadExperiences(): void {
    this.adminCompetitionService.getCompetitionExperiences(this.competitionSid)
        .pipe(first())
        .subscribe((value) => {
          this.experiences = value;
        });
  }

  private loadAges(): void {
    this.adminCompetitionService.getCompetitionAges(this.competitionSid)
        .pipe(first())
        .subscribe((value) => {
          this.ages = value;
        });
  }
}