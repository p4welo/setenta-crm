import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  getOutdatedList,
  getCompetitionSid,
  getUpcomingList,
  getStyles,
  getTypes,
  getExperiences,
  getAges,
  getRules,
  getCompetition,
  isDownloadingAudio,
  getParticipantList,
  getParticipantSid,
  getUserPerformances,
  getTypesWithName,
  getUserSchool,
  getUserDancers,
  getUserPayments,
  isLoadingParticipantList,
  getNewRules,
  getSelectedRule,
  isRulesLoading,
  getInvoiceData,
  getDownloadingInvoiceSid, getLicence
} from '../store/selectors';
import {
  adminGetCompetitionListUrl,
  adminGetCompetitionPerformanceCategories,
  adminGetCompetitionUserDancers,
  adminGetCompetitionUserPerformances,
  adminGetCompetitionUsers,
  adminGetPerformanceBlocks,
  getCompetitionAgesUrl,
  getCompetitionExperiencesUrl,
  getCompetitionInvoiceDatas,
  getCompetitionRuleUrl,
  getCompetitionStylesUrl,
  getCompetitionTypesUrl,
  getCompetitionUrl,
  getMergeCompetitionPerformanceSectionsUrl,
  getPaymentsUrl,
  getPerformanceUrl,
  getSwapCompetitionPerformanceSectionsUrl,
  getUserInvoiceDataUrl,
  getUserPaymentsUrl,
  getUserSchoolUrl
} from '@competition/core/utils';
import {
  AdminCompetitionParticipant,
  Competition,
  CompetitionAge,
  CompetitionExperience, CompetitionLicence,
  CompetitionPayment,
  CompetitionRule,
  CompetitionStyle,
  CompetitionType,
  Dancer_,
  InvoiceData,
  Performance,
  School,
} from '@competition/model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable()
export class AdminCompetitionService {

  public upcomingList$: Observable<Competition[]> = this.store$.select(getUpcomingList);
  public outdatedList$: Observable<Competition[]> = this.store$.select(getOutdatedList);
  public competitionSid$: Observable<string> = this.store$.select(getCompetitionSid);
  public competition$: Observable<Competition> = this.store$.select(getCompetition);
  public styles$: Observable<CompetitionStyle[]> = this.store$.select(getStyles);
  public types$: Observable<CompetitionType[]> = this.store$.select(getTypes);
  public typesWithName$: Observable<CompetitionType[]> = this.store$.select(getTypesWithName);
  public experiences$: Observable<CompetitionExperience[]> = this.store$.select(getExperiences);
  public ages$: Observable<CompetitionAge[]> = this.store$.select(getAges);
  public licence$: Observable<CompetitionLicence> = this.store$.select(getLicence);
  public rules$: Observable<CompetitionRule[]> = this.store$.select(getRules);
  public invoiceData$: Observable<InvoiceData[]> = this.store$.select(getInvoiceData);
  public downloadingInvoiceSid$: Observable<string> = this.store$.select(getDownloadingInvoiceSid);
  public newRules$: Observable<CompetitionRule[]> = this.store$.select(getNewRules);
  public selectedRule$: Observable<CompetitionRule> = this.store$.select(getSelectedRule);
  public isRulesLoading$: Observable<boolean> = this.store$.select(isRulesLoading);
  public isDownloadingAudio$: Observable<boolean> = this.store$.select(isDownloadingAudio);
  public isLoadingParticipants$: Observable<boolean> = this.store$.select(isLoadingParticipantList);

  public participantList$: Observable<AdminCompetitionParticipant[]> = this.store$.select(getParticipantList);
  public participantSid$: Observable<string> = this.store$.select(getParticipantSid);
  public userPerformances$: Observable<Performance[]> = this.store$.select(getUserPerformances);
  public userDancers$: Observable<Dancer_[]> = this.store$.select(getUserDancers);
  public userPayments$: Observable<CompetitionPayment[]> = this.store$.select(getUserPayments);
  public userSchool$: Observable<School> = this.store$.select(getUserSchool);

  constructor(
      private http: HttpClient,
      private store$: Store<any>
  ) {
  }

  public getMyList(): Observable<Competition[]> {
    return this.http.get<Competition[]>(adminGetCompetitionListUrl());
  }

  public getCompetition(competitionSid: string): Observable<Competition> {
    return this.http.get<Competition>(getCompetitionUrl(competitionSid));
  }

  public getCompetitionStyles(competitionSid: string): Observable<CompetitionStyle[]> {
    return this.http.get<CompetitionStyle[]>(getCompetitionStylesUrl(competitionSid));
  }

  public getCompetitionExperiences(competitionSid: string): Observable<CompetitionExperience[]> {
    return this.http.get<CompetitionExperience[]>(getCompetitionExperiencesUrl(competitionSid));
  }

  public getCompetitionAges(competitionSid: string): Observable<CompetitionAge[]> {
    return this.http.get<CompetitionAge[]>(getCompetitionAgesUrl(competitionSid));
  }

  public getCompetitionParticipants(competitionSid: string): Observable<AdminCompetitionParticipant[]> {
    return this.http.get<AdminCompetitionParticipant[]>(adminGetCompetitionUsers(competitionSid));
  }

  public getPerformanceCategoriesUrl(competitionSid: string): Observable<Performance[]> {
    return this.http.get<Performance[]>(adminGetCompetitionPerformanceCategories(competitionSid));
  }

  public getUserPerformances(competitionSid: string, userSid: string): Observable<Performance[]> {
    return this.http.get<Performance[]>(adminGetCompetitionUserPerformances(competitionSid, userSid));
  }

  public updatePerformance(competitionSid: string, performanceSid: string, properties: any): Observable<Performance> {
    return this.http.put<Performance>(getPerformanceUrl(competitionSid, performanceSid), properties);
  }

  public getUserDancers(competitionSid: string, userSid: string): Observable<any[]> {
    return this.http.get<any[]>(adminGetCompetitionUserDancers(competitionSid, userSid));
  }

  public getUserInvoiceData(userSid: string): Observable<InvoiceData> {
    return this.http.get<InvoiceData>(getUserInvoiceDataUrl(userSid));
  }

  public getUserSchool(userSid: string): Observable<School> {
    return this.http.get<School>(getUserSchoolUrl(userSid));
  }

  public getUserPayments(competitionSid: string, userSid: string): Observable<CompetitionPayment[]> {
    return this.http.get<CompetitionPayment[]>(getUserPaymentsUrl(competitionSid, userSid));
  }

  public createUserPayment(competitionSid: string, payment: CompetitionPayment): Observable<CompetitionPayment> {
    return this.http.post<CompetitionPayment>(getPaymentsUrl(competitionSid), payment);
  }

  public getPerformanceBlocks(competitionSid: string): Observable<any[]> {
    return this.http.get<any[]>(adminGetPerformanceBlocks(competitionSid));
  }

  public savePerformanceBlocks(competitionSid: string, performanceBlocks: any[]): Observable<any[]> {
    return this.http.post<any[]>(adminGetPerformanceBlocks(competitionSid), performanceBlocks);
  }

  public saveCompetitionRule(competitionSid: string, parentSid: string, rule: CompetitionRule): Observable<any> {
    return this.http.post<any>(getCompetitionRuleUrl(competitionSid, parentSid), rule);
  }

  public removeCompetitionRule(competitionSid: string, ruleSid: string): Observable<any> {
    return this.http.delete(getCompetitionRuleUrl(competitionSid, ruleSid));
  }

  public saveCompetitionStyle(competitionSid: string, style: CompetitionStyle): Observable<any> {
    return this.http.post<any>(getCompetitionStylesUrl(competitionSid), style);
  }

  public updateCompetition(competitionSid: string, properties: any): Observable<Competition> {
    return this.http.put<Competition>(getCompetitionUrl(competitionSid), properties);
  }

  public saveCompetitionType(competitionSid: string, type: CompetitionType): Observable<any> {
    return this.http.post<any>(getCompetitionTypesUrl(competitionSid), type);
  }

  public saveCompetitionExperience(competitionSid: string, experience: CompetitionExperience): Observable<any> {
    return this.http.post<any>(getCompetitionExperiencesUrl(competitionSid), experience);
  }

  public saveCompetitionAge(competitionSid: string, age: CompetitionAge): Observable<any> {
    return this.http.post<any>(getCompetitionAgesUrl(competitionSid), age);
  }

  public swapSections(competitionSid: string, originSid: string, destinationSid: string): Observable<any> {
    return this.http.put<any>(getSwapCompetitionPerformanceSectionsUrl(competitionSid, originSid), { destinationSid });
  }

  public mergeSections(competitionSid: string, originSid: string, destinationSid: string): Observable<any> {
    return this.http.put<any>(getMergeCompetitionPerformanceSectionsUrl(competitionSid, originSid), { destinationSid });
  }
}