import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '@competition/admin/services';
import {
  AdminInvoiceDataFetchStart,
  CreateInvoiceStart, DeleteInvoiceStart,
  DownloadInvoiceStart
} from '@competition/admin/store';
import { PrintService } from '@competition/core/services';
import { Competition, Invoice, InvoiceData } from '@competition/model';
import { Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-invoices.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCompetitionInvoicesComponent {

  public invoiceDatas$: Observable<InvoiceData[]> = this.adminCompetitionService.invoiceData$;
  public downloadingInvoiceSid$: Observable<string> = this.adminCompetitionService.downloadingInvoiceSid$;
  public competition$: Observable<Competition> = this.adminCompetitionService.competition$;
  public createInvoiceModalRef: BsModalRef;
  public ngForm: FormGroup;
  public selectedInvoiceData: InvoiceData;

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private modalService: BsModalService,
      private adminCompetitionService: AdminCompetitionService,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private store$: Store<any>,
      private printService: PrintService
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.store$.dispatch(new AdminInvoiceDataFetchStart());
    this.ngForm = this.formBuilder.group({
      nip: ['', Validators.required],
      name: ['', Validators.required],
      street: ['', Validators.required],
      zip: ['', Validators.required],
      city: ['', Validators.required],
      title: ['', Validators.required],
      netPrice: '',
      taxPrice: '',
      grossPrice: ''
    });
  }

  public createInvoice(competition: Competition, invoiceData: InvoiceData, template: TemplateRef<any>): void {
    this.createInvoiceModalRef = this.modalService.show(template);
    this.selectedInvoiceData = invoiceData;
    this.ngForm.setValue({
      nip: invoiceData.nip,
      name: invoiceData.name,
      street: invoiceData.street,
      zip: invoiceData.zip,
      city: invoiceData.city,
      title: `${competition.name} - opłata akredytacyjna`,
      netPrice: invoiceData.payments,
      taxPrice: 0,
      grossPrice: invoiceData.payments
    });
  }

  public downloadInvoice(invoiceData: InvoiceData): void {
    this.store$.dispatch(new DownloadInvoiceStart(invoiceData));
  }

  public print(): void {
    this.printService.print();
  }

  public saveInvoice({ nip, name, street, zip, city, title, netPrice, taxPrice }: Invoice): void {
    this.store$.dispatch(new CreateInvoiceStart({
      nip, name, street, zip, city, title, netPrice, taxPrice,
      userSid: this.selectedInvoiceData.sid
    }));
    this.createInvoiceModalRef.hide();
  }

  public deleteInvoice(invoiceData: InvoiceData): void {
    if (confirm('Czy na pewno chcesz usunąć fakturę?')) {
      this.store$.dispatch(new DeleteInvoiceStart(invoiceData));
    }
  }
}