import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdminCompetitionInvoicesComponent } from '@competition/admin/competition-invoices';
import { AdminCompetitionJudgeCardsComponent } from '@competition/admin/competition-judge-cards';
import { AdminUserPaymentsComponent } from '@competition/admin/competition-participant/components';
import { AdminPerformanceFormComponent } from '@competition/admin/competition-participant/components/performance-form';
import { AdminCompetitionPerformanceListComponent } from '@competition/admin/competition-performance-list';
import {
  AdminAddRuleComponent,
  AdminCompetitionRulesComponent,
  NewAdminCompetitionRulesComponent
} from '@competition/admin/competition-rules';
import { AdminRuleNodeComponent } from '@competition/admin/competition-rules/components/rule-node';
import { AdminCompetitionStartingListComponent } from '@competition/admin/competition-starting-list';
import { CoreModule } from '@competition/core';
import { MatchMediaModule } from '@competition/modules/match-media';
import { PerformanceSongModule } from '@competition/modules/performance-song';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule, BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { routes } from './admin.routes';
import { AdminCompetitionDetailsComponent } from './competition-details';
import { AdminCompetitionInfoComponent } from './competition-info'
import { AdminCompetitionInfoStaticComponent } from './competition-info/components';
import { AdminCompetitionParticipantComponent } from './competition-participant';
import { AdminCompetitionParticipantListComponent } from './competition-participant-list';
import { AdminCompetitionPerformanceBlockListComponent } from './competition-performance-block-list';
import { AdminCompetitionsComponent } from './competitions';
import { AdminCompetitionService } from './services';
import {
  getInitialState,
  AdminCompetitionEffects,
  reducers,
  ADMIN_FEATURE,
  AdminParticipantEffects
} from './store';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    DragDropModule,
    ScrollingModule,
    NgxMaskModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forChild(routes),
    StoreModule.forFeature(ADMIN_FEATURE, reducers, {
      initialState: getInitialState
    }),
    EffectsModule.forFeature([AdminCompetitionEffects, AdminParticipantEffects]),
    MatchMediaModule,
    PerformanceSongModule
  ],
  declarations: [
    AdminCompetitionsComponent,
    AdminCompetitionDetailsComponent,
    AdminCompetitionInfoComponent,
    AdminCompetitionParticipantComponent,
    AdminCompetitionParticipantListComponent,
    AdminCompetitionStartingListComponent,
    AdminCompetitionPerformanceBlockListComponent,
    AdminCompetitionPerformanceListComponent,
    AdminCompetitionJudgeCardsComponent,
    AdminCompetitionRulesComponent,
    NewAdminCompetitionRulesComponent,
    AdminCompetitionInfoStaticComponent,
    AdminCompetitionInvoicesComponent,
    AdminUserPaymentsComponent,
    AdminPerformanceFormComponent,
    AdminRuleNodeComponent,
    AdminAddRuleComponent
  ],
  providers: [
    AdminCompetitionService
  ]
})
export class AdminModule {

}