import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminCompetitionService } from '@competition/admin/services';
import { CompetitionsService, NotificationService, PrintService } from '@competition/core/services';
import { competitionDayName } from '@competition/core/utils';
import { Competition, Performance, PerformanceSection, PerformanceSectionWrapper } from '@competition/model';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './admin-competition-judge-cards.component.html',
  styleUrls: ['./admin-competition-judge-cards.component.scss']
})
export class AdminCompetitionJudgeCardsComponent implements OnInit {

  private readonly ELIMINATIONS_MAGIC_NUMBER = 8;

  public list: PerformanceSectionWrapper;
  public competition: Competition;
  public dayNumbers: string[];

  private competitionSid: string;

  constructor(
      private route: ActivatedRoute,
      private notificationService: NotificationService,
      private adminCompetitionService: AdminCompetitionService,
      private competitionService: CompetitionsService,
      private printService: PrintService
  ) {
    this.route.parent.paramMap
        .pipe(first())
        .subscribe((map => this.competitionSid = map.get('competitionSid')));
  }

  public ngOnInit(): void {
    this.loadCompetition();
    this.loadSections();
  }

  public getChapterNumbers(dayNumber: string): string[] {
    return this.list ? Object.keys(this.list[dayNumber]) : [];
  }

  public getSections(dayNumber: string, chapterNumber: string): PerformanceSection[] {
    return this.list[dayNumber][chapterNumber];
  }

  public getDayName(dayNumber: string): string {
    return competitionDayName(this.competition, +dayNumber);
  }

  public getRounds(section: PerformanceSection): string[] {
    const roundsAmount = section.eliminations ?
        Math.ceil(section.performances.length / this.ELIMINATIONS_MAGIC_NUMBER) :
        1;

    const result = [];
    for (let i = 0; i < roundsAmount; i++) {
      if (i === 0) {
        result.push('F');
      }
      else if (i === 1) {
        result.push('1/2F');
      }
      else if (i === roundsAmount - 1) {
        result.push('EL');
      }
      else {
        const value = Math.pow(2, i);
        result.push(`1/${value}F`)
      }
    }
    return result;
  }

  public canShowTitle(section: PerformanceSection): boolean {
    let result = false;
    if (section) {
      section.performances.forEach((performance: Performance) => {
        if (!!performance.title) {
          result = true;
        }
      });
    }
    return result;
  }

  public print(): void {
    this.printService.print();
  }

  private loadSections(userSid?: string): void {
    this.competitionService.getCompetitionStartingList(this.competitionSid, userSid)
        .subscribe((value) => {
          this.dayNumbers = Object.keys(value);
          this.list = value;
        });
  }

  private loadCompetition(): void {
    this.adminCompetitionService.getCompetition(this.competitionSid)
        .subscribe((value) => {
          this.competition = value;
        });
  }
}