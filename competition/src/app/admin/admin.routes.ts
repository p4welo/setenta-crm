import { Routes } from '@angular/router';
import { AdminCompetitionInvoicesComponent } from '@competition/admin/competition-invoices';
import { AdminCompetitionJudgeCardsComponent } from '@competition/admin/competition-judge-cards';
import { AdminCompetitionPerformanceListComponent } from '@competition/admin/competition-performance-list';
import {
  AdminCompetitionRulesComponent,
  NewAdminCompetitionRulesComponent
} from '@competition/admin/competition-rules';
import { AdminCompetitionDetailsComponent } from './competition-details';
import { AdminCompetitionInfoComponent } from './competition-info';
import { AdminCompetitionParticipantComponent } from './competition-participant';
import { AdminCompetitionParticipantListComponent } from './competition-participant-list';
import { AdminCompetitionPerformanceBlockListComponent } from './competition-performance-block-list';
import { AdminCompetitionStartingListComponent } from './competition-starting-list';
import { AdminCompetitionsComponent } from './competitions';

export const routes: Routes = [
  { path: 'competitions', component: AdminCompetitionsComponent, pathMatch: 'full' },
  {
    path: 'competitions/:competitionSid', component: AdminCompetitionDetailsComponent,
    children: [
      { path: '', redirectTo: 'info', pathMatch: 'full' },
      { path: 'info', component: AdminCompetitionInfoComponent },
      { path: 'rules', component: NewAdminCompetitionRulesComponent },
      { path: 'starting-list', component: AdminCompetitionStartingListComponent },
      { path: 'judge-cards', component: AdminCompetitionJudgeCardsComponent },
      { path: 'invoices', component: AdminCompetitionInvoicesComponent },
      { path: 'performance-list', component: AdminCompetitionPerformanceListComponent },
      { path: 'performance-block-list', component: AdminCompetitionPerformanceBlockListComponent },
      { path: 'participants', component: AdminCompetitionParticipantListComponent, pathMatch: 'full' },
      { path: 'participants/:participantSid', component: AdminCompetitionParticipantComponent }
    ]
  },
  {
    path: '',
    redirectTo: 'competitions',
    pathMatch: 'full'
  }
];