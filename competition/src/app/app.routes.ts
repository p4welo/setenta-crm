import { Routes } from '@angular/router';
import { AdminAuthGuard, AuthGuard } from '@competition/modules/auth/guards';

export const ROUTES: Routes = [{
  path: 'register',
  loadChildren: () => import('./authentication/register/').then(m => m.RegisterModule)
}, {
  path: 'activate',
  loadChildren: () => import('./authentication/activate/').then(m => m.ActivateModule)
}, {
  path: 'login',
  loadChildren: () => import('./authentication/login/').then(m => m.LoginModule)
}, {
  path: 'logout',
  loadChildren: () => import('./authentication/logout/').then(m => m.LogoutModule)
}, {
  path: 'password',
  loadChildren: () => import('./authentication/reset-password/').then(m => m.ResetPasswordModule)
}, {
  path: 'form',
  loadChildren: () => import('./competition-form/').then(m => m.CompetitionFormModule)
}, {
  path: 'home',
  canActivate: [AuthGuard],
  loadChildren: () => import('./home/').then(m => m.HomeModule)
}, {
  path: 'dancers',
  canActivate: [AuthGuard],
  loadChildren: () => import('./dancers/').then(m => m.DancersModule)
}, {
  path: 'competitions',
  canActivate: [AuthGuard],
  loadChildren: () => import('./competitions/').then(m => m.CompetitionsModule)
}, {
  path: 'profile',
  canActivate: [AuthGuard],
  loadChildren: () => import('./user/').then(m => m.UserModule)
}, {
  path: 'admin',
  canActivate: [AdminAuthGuard],
  loadChildren: () => import('./admin/').then(m => m.AdminModule)
}, {
  path: '',
  redirectTo: '/home',
  pathMatch: 'full'
}
];
