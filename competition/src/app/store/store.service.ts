import { ActionReducerMap } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

export interface State {
  router: RouterReducerState;
}

export const REDUCERS: ActionReducerMap<State> = {
  router: routerReducer
};

export const initialState = {
};

export function getInitialState(): any {
  return initialState;
}
