import { Action } from '@ngrx/store';
import { UserSession } from '@competition/model';
import { LoginForm } from '../model';

export const AuthActionTypes = {
  LOGIN_START: '[Auth] Login start',
  LOGIN_SUCCESS: '[Auth] Login success',
  LOGIN_ERROR: '[Auth] Login error',
  GET_CURRENT_USER_START: '[Auth] Get current user start',
  GET_CURRENT_USER_SUCCESS: '[Auth] Get current user success',
  GET_CURRENT_USER_ERROR: '[Auth] Get current user error',
  LOGOUT_START: '[Auth] Logout start',
  LOGOUT_SUCCESS: '[Auth] Logout success',
  LOGOUT_ERROR: '[Auth] Logout error'
};

export class AuthLoginStart implements Action {
  public type: string = AuthActionTypes.LOGIN_START;

  constructor(public payload: LoginForm) {
  }
}

export class AuthLoginSuccess implements Action {
  public type: string = AuthActionTypes.LOGIN_SUCCESS;

  constructor(public payload: UserSession) {
  }
}

export class AuthLoginError implements Action {
  public type: string = AuthActionTypes.LOGIN_ERROR;

  constructor(public payload: string) {
  }
}

export class AuthGetCurrentUserStart implements Action {
  public type: string = AuthActionTypes.GET_CURRENT_USER_START;

  constructor(public payload?: any) {
  }
}

export class AuthGetCurrentUserSuccess implements Action {
  public type: string = AuthActionTypes.GET_CURRENT_USER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class AuthGetCurrentUserError implements Action {
  public type: string = AuthActionTypes.GET_CURRENT_USER_ERROR;

  constructor(public payload: Error) {
  }
}

export class AuthLogoutStart implements Action {
  public type: string = AuthActionTypes.LOGOUT_START;

  constructor(public payload?: any) {
  }
}

export class AuthLogoutSuccess implements Action {
  public type: string = AuthActionTypes.LOGOUT_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class AuthLogoutError implements Action {
  public type: string = AuthActionTypes.LOGOUT_ERROR;

  constructor(public payload: string) {
  }
}

export type AuthActions =
    | AuthLoginStart
    | AuthLoginSuccess
    | AuthLoginError
    | AuthGetCurrentUserStart
    | AuthGetCurrentUserSuccess
    | AuthGetCurrentUserError
    | AuthLogoutStart
    | AuthLogoutSuccess
    | AuthLogoutError
    ;