import { User } from '@competition/model';

export interface AuthModuleState {
  initialized: boolean;
  loading: boolean;
  token?: string;
  user?: User;
  error?: string;
}

export interface LoginForm {
  login: string;
  password: string;
  type?: string;
}