import { Injectable } from '@angular/core';
import { User } from '@competition/model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import decode from 'jwt-decode';
import { path } from 'ramda';

import { SessionService } from '@competition/modules/storage/session.service';
import { getError, getUser, isLoading } from '../selectors';

@Injectable()
export class AuthService {

  private readonly AUTH_TOKEN = 'setenta.authToken';

  public currentUser$: Observable<User> = this.store$.select(getUser);
  public error$: Observable<string> = this.store$.select(getError);
  public isLoading$: Observable<boolean> = this.store$.select(isLoading);

  constructor(
      private sessionService: SessionService,
      private store$: Store<any>
  ){}

  public getAuthToken(): string {
    return this.sessionService.get(this.AUTH_TOKEN);
  }

  public setAuthToken(token: string): void {
    return this.sessionService.set(this.AUTH_TOKEN, token);
  }

  public clearAuthToken(): void {
    return this.sessionService.remove(this.AUTH_TOKEN);
  }

  public isAuthenticated(): boolean {
    const token = this.getAuthToken();
    try {
      const tokenPayload = decode(token);
      return !!tokenPayload.user;
    } catch (e) {
      this.clearAuthToken();
      return false;
    }
  }

  public isRole(role: string): boolean {
    const token = this.getAuthToken();
    try {
      const tokenPayload = decode(token);
      const type = path(['user', 'type'], tokenPayload);
      return role === type;
    } catch (e) {
      return false;
    }
  }

  public isAdminUser(): boolean {
    return this.isRole('CRM') || this.isRole('COMPETITION');
  }
}