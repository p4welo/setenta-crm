import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { NavigationService } from '@competition/core/services';
import { AuthService } from '../services';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private navigationService: NavigationService) {
  }

  public canActivate(): boolean {
    if (
        this.authService.isAuthenticated() &&
        (this.authService.isAdminUser())
    ) {
      return true;
    } else {
      this.navigationService.navigateToLoginPage();
      return false;
    }
  }
}