import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { NavigationService } from '@competition/core/services';
import { AuthService } from '@competition/modules/auth/services';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private navigationService: NavigationService) {
  }

  public canActivate(): boolean {
    if (this.authService.isAuthenticated()) {
      return true;
    }
    else {
      this.navigationService.navigateToLoginPage();
      return false;
    }
  }
}