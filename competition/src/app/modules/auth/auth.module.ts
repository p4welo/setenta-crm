import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CoreModule } from '@competition/core';
import { AdminAuthGuard, AuthGuard } from '@competition/modules/auth/guards';
import { AuthInterceptor } from './interceptors';
import { StorageModule } from '@competition/modules/storage';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AuthService } from './services';
import { AUTH_FEATURE } from './constants';
import { AuthEffects } from './effects';
import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CoreModule,
    StorageModule,
    StoreModule.forFeature(AUTH_FEATURE, reducers),
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: [],
  providers: [
    AuthService,
    AuthGuard,
    AdminAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  exports: []
})
export class AuthModule {

}