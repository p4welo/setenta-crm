import { AuthActions, AuthActionTypes } from '@competition/modules/auth/actions';
import { AuthModuleState } from '@competition/modules/auth/model';

export const initialState: AuthModuleState = {
  initialized: false,
  loading: false,
};

export function reducers(
    state: AuthModuleState = initialState,
    action: AuthActions
): AuthModuleState {
  switch (action.type) {
    case AuthActionTypes.LOGIN_START:
      return {
        ...state,
        loading: true
      };
    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        initialized: true,
        loading: false,
        token: action.payload.token,
        error: undefined
      };
    case AuthActionTypes.LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case AuthActionTypes.GET_CURRENT_USER_SUCCESS:
      return {
        ...state,
        user: action.payload
      };
    default:
      return state;
  }
}