import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavigationService } from '@competition/core/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { getCurrentUserUrl, getLoginUrl } from '@competition/core/utils';
import { User, UserSession } from '@competition/model';
import {
  AuthActions,
  AuthActionTypes,
  AuthGetCurrentUserError,
  AuthGetCurrentUserSuccess,
  AuthLoginError,
  AuthLoginStart,
  AuthLoginSuccess,
  AuthLogoutSuccess
} from '../actions';
import { AuthService } from '../services';

@Injectable()
export class AuthEffects {

  constructor(
      private actions$: Actions,
      private http: HttpClient,
      private authService: AuthService,
      private navigationService: NavigationService
  ) {
  }

  @Effect()
  public loginUser$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.LOGIN_START),
      switchMap((action: AuthLoginStart) => this.http.post(getLoginUrl(), action.payload).pipe(
          map((session: UserSession) => new AuthLoginSuccess(session)),
          catchError((e: Error) => of(new AuthLoginError('login.failed')))
      ))
  );

  @Effect({ dispatch: false })
  public storeToken$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.LOGIN_SUCCESS),
      tap((action: AuthLoginSuccess) => this.authService.setAuthToken(action.payload.token))
  );

  @Effect({ dispatch: false })
  public navigateOnLogin$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.LOGIN_SUCCESS),
      tap(() => this.navigationService.navigateToHomePage())
  );

  @Effect()
  public currentUser$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.GET_CURRENT_USER_START),
      switchMap((action: AuthLoginStart) => this.http.get<User>(getCurrentUserUrl()).pipe(
          map((user: User) => new AuthGetCurrentUserSuccess(user)),
          catchError((e: Error) => of(new AuthGetCurrentUserError(e)))
      ))
  );

  @Effect()
  public logout$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.LOGOUT_START),
      tap(() => this.authService.clearAuthToken()),
      map(() => new AuthLogoutSuccess())
  );

  @Effect({ dispatch: false })
  public navigateOnLogout$: Observable<AuthActions> = this.actions$.pipe(
      ofType(AuthActionTypes.LOGOUT_SUCCESS),
      tap(() => this.navigationService.navigateToLoginPage())
  );
}