import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private readonly AUTH_TOKEN: string = 'Authorization';
  private readonly TOKEN_PREFIX: string = 'JWT';

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getAuthToken();
    if (token) {
      const cloned = req.clone({
        headers: req.headers.set(this.AUTH_TOKEN, `${this.TOKEN_PREFIX} ${token}`)
      });

      return next.handle(cloned);
    }
    else {
      return next.handle(req);
    }
  }
}