import { AUTH_FEATURE } from '@competition/modules/auth/constants';
import { AuthModuleState } from '@competition/modules/auth/model';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getAuth = createFeatureSelector<AuthModuleState>(AUTH_FEATURE);

export const isLoading = createSelector(
    getAuth,
    state => state.loading
);

export const getError = createSelector(
    getAuth,
    state => state.error
);

export const getUser = createSelector(
    getAuth,
    state => state.user
);