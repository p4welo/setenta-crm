import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NotificationService } from '@competition/core/services';
import { getDancerListUrl, getDancerUrl } from '@competition/core/utils';
import { Dancer_ } from '@competition/model';
import {
  CreateDancerError,
  CreateDancerStart, CreateDancerSuccess,
  DancerActions,
  DancerActionTypes, DancersFetchError,
  DancersFetchStart, DancersFetchSuccess, DeleteDancerError, DeleteDancerStart, DeleteDancerSuccess
} from '@competition/modules/dancer-store/actions';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class DancerEffects {
  constructor(
      private actions$: Actions,
      private http: HttpClient,
      private notificationService: NotificationService
  ) {
  }

  @Effect()
  public fetchDancers$: Observable<DancerActions> = this.actions$.pipe(
      ofType(DancerActionTypes.DANCERS_FETCH_START),
      switchMap((a: DancersFetchStart) => this.http.get<Dancer_[]>(getDancerListUrl()).pipe(
          map((dancers: Dancer_[]) => new DancersFetchSuccess(dancers)),
          catchError((e: Error) => of(new DancersFetchError(e)))
      ))
  );

  @Effect()
  public createDancer$: Observable<DancerActions> = this.actions$.pipe(
      ofType(DancerActionTypes.CREATE_DANCER_START),
      map((action: CreateDancerStart) => action.payload),
      switchMap((dancer: Dancer_) => this.http.post(getDancerListUrl(), dancer).pipe(
          map((dancer: Dancer_) => new CreateDancerSuccess(dancer)),
          catchError((e: Error) => of(new CreateDancerError(e)))
      ))
  );

  @Effect()
  public deleteDancer$: Observable<DancerActions> = this.actions$.pipe(
      ofType(DancerActionTypes.DELETE_DANCER_START),
      map((action: DeleteDancerStart) => action.payload),
      switchMap((dancer: Dancer_) => this.http.delete(getDancerUrl(dancer.sid)).pipe(
          map(() => new DeleteDancerSuccess(dancer)),
          catchError((e: Error) => of(new DeleteDancerError(e)))
      ))
  );

  @Effect({ dispatch: false })
  public createSuccess$: Observable<any> = this.actions$.pipe(
      ofType(DancerActionTypes.CREATE_DANCER_SUCCESS),
      tap(() => this.notificationService.success("Pomyślnie dodano"))
  );

  @Effect({ dispatch: false })
  public deleteSuccess$: Observable<any> = this.actions$.pipe(
      ofType(DancerActionTypes.DELETE_DANCER_SUCCESS),
      tap(() => this.notificationService.success("Pomyślnie usunięto"))
  );
}