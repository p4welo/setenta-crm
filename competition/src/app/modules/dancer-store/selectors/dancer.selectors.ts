import { filterDancers, getSortedList } from '@competition/core/utils';
import { Dancer_ } from '@competition/model';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DANCER_FEATURE } from '../constants';
import { DancerModuleState } from '../model';

export const getDancersState = createFeatureSelector<DancerModuleState>(DANCER_FEATURE);

export const getList = createSelector(
    getDancersState,
    state => state.list
);

export const getFilteredList = createSelector(
    getDancersState,
    state => {
      const filtered: Dancer_[] = filterDancers(state.list, state.filter);
      return getSortedList(filtered, state.sortingProperty, state.sortingAscending);
    }
);

export const isInitialized = createSelector(
    getDancersState,
    state => state.initialized
);

export const isSubmitInProgress = createSelector(
    getDancersState,
    state => state.submitInProgress
);

export const getSortProperty = createSelector(
    getDancersState,
    state => state.sortingProperty
);

export const isSortAscending = createSelector(
    getDancersState,
    state => state.sortingAscending
);

export const getFilter = createSelector(
    getDancersState,
    state => state.filter
);
