import { Dancer_ } from '@competition/model';
import { DancerActions, DancerActionTypes } from '../actions';
import { DancerModuleState } from '../model';

export const initialState: DancerModuleState = {
  list: [],
  sortingAscending: true,
  sortingProperty: 'firstName',
  filter: undefined,
  initialized: false
};

export function reducers(
    state: DancerModuleState = initialState,
    action: DancerActions
): DancerModuleState {
  switch (action.type) {
    case DancerActionTypes.DANCERS_FETCH_SUCCESS:
      return {
        ...state,
        list: action.payload,
        initialized: true
      };
    case DancerActionTypes.SET_SORT_PROPERTY:
      return {
        ...state,
        sortingProperty: action.payload,
        sortingAscending: state.sortingProperty === action.payload ?
            !state.sortingAscending :
            true
      };
    case DancerActionTypes.SET_DANCER_FILTER:
      return {
        ...state,
        filter: action.payload
      };
    case DancerActionTypes.CREATE_DANCER_START:
      return {
        ...state,
        submitInProgress: true
      };
    case DancerActionTypes.CREATE_DANCER_SUCCESS:
      return {
        ...state,
        submitInProgress: false,
        list: [
          ...state.list,
          action.payload
        ]
      };
    case DancerActionTypes.DELETE_DANCER_SUCCESS:
      return {
        ...state,
        list: state.list.filter((dancer: Dancer_) => dancer.sid !== action.payload.sid)
      };
    default:
      return state;
  }
}