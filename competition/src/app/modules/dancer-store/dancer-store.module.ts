import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@competition/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { DANCER_FEATURE } from './constants';
import { DancerEffects } from './effects';
import { reducers } from './reducers';
import { DancerService } from './services';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    StoreModule.forFeature(DANCER_FEATURE, reducers),
    EffectsModule.forFeature([DancerEffects]),
  ],
  declarations: [],
  providers: [
    DancerService
  ],
  exports: []
})
export class DancerStoreModule {

}