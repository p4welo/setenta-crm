import { Injectable } from '@angular/core';
import { Dancer_ } from '@competition/model';
import {
  getFilteredList, getList,
  getSortProperty,
  isInitialized,
  isSortAscending, isSubmitInProgress
} from '@competition/modules/dancer-store/selectors';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable()
export class DancerService {

  public dancers$: Observable<Dancer_[]> = this.store$.select(getList);
  public filteredDancers$: Observable<Dancer_[]> = this.store$.select(getFilteredList);
  public initialized$: Observable<boolean> = this.store$.select(isInitialized);
  public submitInProgress$: Observable<boolean> = this.store$.select(isSubmitInProgress);
  public sortAscending$: Observable<boolean> = this.store$.select(isSortAscending);
  public sortProperty$: Observable<string> = this.store$.select(getSortProperty);

  constructor(
      private store$: Store<any>
  ) {
  }

}