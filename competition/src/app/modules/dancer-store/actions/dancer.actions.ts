import { Dancer_ } from '@competition/model';
import { Action } from '@ngrx/store';

export const DancerActionTypes = {
  DANCERS_FETCH_START: '[Dancer] Dancers fetch start',
  DANCERS_FETCH_SUCCESS: '[Dancer] Dancers fetch success',
  DANCERS_FETCH_ERROR: '[Dancer] Dancers fetch error',

  SET_SORT_PROPERTY: '[Dancer] Set sort property',
  SET_DANCER_FILTER: '[Dancer] Set dancer filter',

  CREATE_DANCER_START: '[Dancer] Create dancer start',
  CREATE_DANCER_SUCCESS: '[Dancer] Create dancer success',
  CREATE_DANCER_ERROR: '[Dancer] Create dancer error',

  DELETE_DANCER_START: '[Dancer] Delete dancer start',
  DELETE_DANCER_SUCCESS: '[Dancer] Delete dancer success',
  DELETE_DANCER_ERROR: '[Dancer] Delete dancer error'
};

export class DancersFetchStart implements Action {
  public type: string = DancerActionTypes.DANCERS_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class DancersFetchSuccess implements Action {
  public type: string = DancerActionTypes.DANCERS_FETCH_SUCCESS;

  constructor(public payload: Dancer_[]) {
  }
}

export class DancersFetchError implements Action {
  public type: string = DancerActionTypes.DANCERS_FETCH_ERROR;

  constructor(public payload?: any) {
  }
}

export class SetSortProperty implements Action {
  public type: string = DancerActionTypes.SET_SORT_PROPERTY;

  constructor(public payload: string) {
  }
}

export class SetDancerFilter implements Action {
  public type: string = DancerActionTypes.SET_DANCER_FILTER;

  constructor(public payload: string) {
  }
}

export class CreateDancerStart implements Action {
  public type: string = DancerActionTypes.CREATE_DANCER_START;

  constructor(public payload: Dancer_) {
  }
}

export class CreateDancerSuccess implements Action {
  public type: string = DancerActionTypes.CREATE_DANCER_SUCCESS;

  constructor(public payload: Dancer_) {
  }
}

export class CreateDancerError implements Action {
  public type: string = DancerActionTypes.CREATE_DANCER_ERROR;

  constructor(public payload: Error) {
  }
}

export class DeleteDancerStart implements Action {
  public type: string = DancerActionTypes.DELETE_DANCER_START;

  constructor(public payload: Dancer_) {
  }
}

export class DeleteDancerSuccess implements Action {
  public type: string = DancerActionTypes.DELETE_DANCER_SUCCESS;

  constructor(public payload: Dancer_) {
  }
}

export class DeleteDancerError implements Action {
  public type: string = DancerActionTypes.DELETE_DANCER_ERROR;

  constructor(public payload: Error) {
  }
}


export type DancerActions =
    | DancersFetchStart
    | DancersFetchSuccess
    | DancersFetchError
    | SetSortProperty
    | SetDancerFilter
    | CreateDancerStart
    | CreateDancerSuccess
    | CreateDancerError
    | DeleteDancerStart
    | DeleteDancerSuccess
    | DeleteDancerError
    ;