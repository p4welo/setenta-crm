import { Dancer_ } from '@competition/model';

export interface DancerModuleState {
  initialized: boolean;
  list: Dancer_[];
  sortingProperty?: string;
  sortingAscending?: boolean;
  filter?: string;
  submitInProgress?: boolean;
}
