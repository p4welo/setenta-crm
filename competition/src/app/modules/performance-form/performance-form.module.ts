import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@competition/core';
import { MatchMediaModule } from '@competition/modules/match-media';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule } from 'ngx-bootstrap';
import { PerformanceFormComponent } from './performance-form.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    MatchMediaModule,
    FormsModule,
    TranslateModule,
    BsDropdownModule.forRoot(),
  ],
  declarations: [
    PerformanceFormComponent
  ],
  exports: [
    PerformanceFormComponent
  ]
})
export class PerformanceFormModule {
}