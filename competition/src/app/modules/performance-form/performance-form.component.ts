import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { getPerformanceLeadingAge } from '@competition/competition-details/competition.utils';
import { ObjectStates } from '@competition/core/constants/object-state.const';
import { getSortedList, getSortingColumnClass } from '@competition/core/utils';
import { MatchMediaService } from '@competition/modules/match-media/match-media.service';
import { find, path, trim } from 'ramda';
import {
  Competition,
  CompetitionRule,
  Dancer_,
  Performance,
  RuleTypes
} from '@competition/model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-performance-form',
  templateUrl: './performance-form.component.html',
  styleUrls: ['./performance-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerformanceFormComponent implements OnChanges {
  @Input() public competition: Competition;
  @Input() public rules: CompetitionRule[] = [];
  @Input() public performance: Performance;
  @Input() public dancers: Dancer_[];

  @Output() public save: EventEmitter<Performance> = new EventEmitter();
  @Output() public cancel: EventEmitter<void> = new EventEmitter();

  public media$: Observable<any> = this.matchMediaService.resize$;

  public styles: CompetitionRule[] = [];
  public experiences: CompetitionRule[] = [];

  public selectedDancers: Dancer_[] = [];
  public selectedAge: CompetitionRule;
  public selectedType: CompetitionRule;
  public selectedStyle: CompetitionRule;
  public selectedExperience: CompetitionRule;
  public selectedName: string;
  public selectedTitle: string;

  public sortingProperty: string = 'firstName';
  public sortingAscending: boolean = true;

  constructor(
      private matchMediaService: MatchMediaService
  ) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.performance) {
      this.setInitialState();
    }
  }

  public get sortedDancers(): Dancer_[] {
    return getSortedList(this.dancers, this.sortingProperty, this.sortingAscending);
  }

  public toggleDancer(dancer: Dancer_): void {
    if (this.isDancerChecked(dancer)) {
      this.selectedDancers = this.selectedDancers.filter((d: Dancer_) => d.sid !== dancer.sid);
    } else {
      this.selectedDancers.push(dancer);
    }
    this.calculateAge();
  }

  public isDancerChecked(dancer: Dancer_): boolean {
    return this.selectedDancers.filter((d: Dancer_) => d.sid === dancer.sid).length > 0;
  }

  public getAge(dancer: Dancer_): number {
    return new Date().getFullYear() - +dancer.yearOfBirth;
  }

  public calculateAge(): void {
    if (this.rules) {
      const age: number = getPerformanceLeadingAge(this.selectedDancers, this.competition);
      const result = find(
          (rule: CompetitionRule) => this.isBetween(age, rule.age)
      )(this.rules.filter(rule => rule.ruleType === RuleTypes.AGE));
      this.selectedAge = result;
    } else {
      this.selectedAge = undefined;
    }
    this.calculateType();
  }

  public calculateType(): void {
    if (this.selectedAge) {
      this.selectedType = find(
          (rule: CompetitionRule) => this.isBetween(this.selectedDancers.length, rule.type)
      )(this.selectedAge.rules.filter(rule => rule.ruleType === RuleTypes.TYPE));
    } else {
      this.selectedType = undefined;
    }
    this.refreshStyles();
  }

  public refreshStyles(): void {
    if (this.selectedType) {
      this.styles = this.selectedType.rules.filter(rule => rule.ruleType === RuleTypes.STYLE);

      const selectedStyleName = path(['style', 'name'], this.selectedStyle);
      if (selectedStyleName) {
        const foundByName = this.styles.filter((rule: CompetitionRule) => rule.style.name === selectedStyleName);
        if (foundByName.length > 0) {
          this.setStyle(foundByName[0]);
        } else {
          this.setStyle(undefined);
        }
      }
    }
    this.refreshExperiences();
  }

  public setStyle(style: CompetitionRule): void {
    this.selectedStyle = style;
    this.refreshExperiences();
  }

  public get isStyleVisible(): boolean {
    return this.selectedType && this.selectedType.rules.filter(rule => rule.ruleType === RuleTypes.STYLE).length > 0
  }

  public refreshExperiences(): void {
    this.experiences = this.selectedStyle ? this.selectedStyle.rules.filter(rule => rule.ruleType === RuleTypes.EXPERIENCE) : [];

    const selectedExperienceName = path(['experience', 'name'], this.selectedExperience);
    if (selectedExperienceName && this.experiences.filter((rule: CompetitionRule) => rule.experience.name === selectedExperienceName).length < 1) {
      this.selectedExperience = undefined;
    }
  }

  public setExperience(experience: CompetitionRule): void {
    this.selectedExperience = experience;
  }

  public get isExperienceVisible(): boolean {
    return !!this.experiences && this.experiences.length > 0;
  }

  public get isNameVisible(): boolean {
    return (this.isExperienceVisible && !!this.selectedExperience ||
        !this.isExperienceVisible && !!this.selectedStyle) &&
        path(['type', 'showName'], this.selectedType);
  }

  public get isTitleVisible(): boolean {
    return path(['style', 'showTitle'], this.selectedStyle) &&
        (!this.isExperienceVisible || this.isExperienceVisible && !!this.selectedExperience) &&
        (!this.isNameVisible || this.isNameVisible && !!this.selectedName);
  }

  public get isSubmitVisible(): boolean {
    return !!this.selectedAge && !!this.selectedType && !!this.selectedStyle
        && (!this.isExperienceVisible || !!this.isExperienceVisible && !!this.selectedExperience)
        && (!this.isNameVisible || !!this.isNameVisible && !!this.selectedName)
        && (!this.isTitleVisible || !!this.isTitleVisible && !!this.selectedTitle);
  }

  public get isClosed(): boolean {
    return !!this.selectedAge && this.selectedAge.objectState === ObjectStates.INACTIVE;
  }

  public submit(): void {
    this.save.emit({
      sid: path(['sid'], this.performance),
      name: trim(this.selectedName || ''),
      title: trim(this.selectedTitle || ''),
      dancers: this.selectedDancers,
      style: path(['style', 'name'], this.selectedStyle),
      type: path(['type', 'name'], this.selectedType),
      experience: path(['experience', 'name'], this.selectedExperience),
      ageLevel: path(['age', 'name'], this.selectedAge)
    });
  }

  // SORTING
  public sortBy(property: string): void {
    if (this.sortingProperty === property) {
      this.sortingAscending = !this.sortingAscending;
    } else {
      this.sortingProperty = property;
      this.sortingAscending = true;
    }
  }

  public getSortingClass(property: string): string {
    return getSortingColumnClass(property, this.sortingProperty, this.sortingAscending);
  }

  private setInitialState(): void {
    if (this.performance) {
      this.selectedDancers = (this.performance.dancers || []).filter((dancer) =>
          this.dancers.filter((d) => d.sid === dancer.sid).length > 0
      );
      this.calculateAge();

      if (!!this.selectedType) {
        const filtered = this.selectedType.rules.filter(rule => rule.ruleType === RuleTypes.STYLE && rule.style.name === this.performance.style);
        this.setStyle(filtered.length > 0 ? filtered[0] : undefined);
      }

      if (!!this.selectedStyle) {
        const filtered = this.selectedStyle.rules.filter(rule => rule.ruleType === RuleTypes.EXPERIENCE && rule.experience.name === this.performance.experience)
        this.setExperience(filtered.length > 0 ? filtered[0] : undefined);
      }

      const name = this.performance.name;
      this.selectedName = name ? name : '';

      const title = this.performance.title;
      this.selectedTitle = title ? title : '';

    } else {
      this.selectedDancers = [];
    }
  }

  private isBetween(amount, object: { min: number, max: number }): boolean {
    return amount >= object.min && amount <= object.max;
  }
}