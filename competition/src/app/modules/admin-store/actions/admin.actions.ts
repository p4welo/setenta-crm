import { Competition } from '@competition/model';
import { Action } from '@ngrx/store';

export const AdminActionTypes = {
  COMPETITIONS_FETCH_START: '[Admin] Competitions fetch start',
  COMPETITIONS_FETCH_SUCCESS: '[Admin] Competitions fetch success',
  COMPETITIONS_FETCH_ERROR: '[Admin] Competitions fetch error'
};

export class AdminCompetitionsFetchStart implements Action {
  public type: string = AdminActionTypes.COMPETITIONS_FETCH_START;

  constructor(public payload?: any) {
  }
}

export class AdminCompetitionsFetchSuccess implements Action {
  public type: string = AdminActionTypes.COMPETITIONS_FETCH_SUCCESS;

  constructor(public payload: Competition[]) {
  }
}

export class AdminCompetitionsFetchError implements Action {
  public type: string = AdminActionTypes.COMPETITIONS_FETCH_ERROR;

  constructor(public payload: Error) {
  }
}

export type AdminActions =
    | AdminCompetitionsFetchStart
    | AdminCompetitionsFetchSuccess
    | AdminCompetitionsFetchError
    ;