import { AdminModuleState } from '../model';
import { AdminActions, AdminActionTypes } from '../actions';

export const initialState: AdminModuleState = {
  competitions: [],
  details: {}
};

export function reducers(
    state: AdminModuleState = initialState,
    action: AdminActions
): AdminModuleState {
  switch (action.type) {
    case AdminActionTypes.COMPETITIONS_FETCH_START:
      return {
        ...state
      };
    case AdminActionTypes.COMPETITIONS_FETCH_SUCCESS:
      return {
        ...state,
        competitions: action.payload
      };
    default:
      return state;
  }
}