import { getSortedList } from '@competition/core/utils';
import { Competition } from '@competition/model';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as dayjs from 'dayjs';

import { ADMIN_FEATURE } from '../constants';
import { AdminModuleState } from '../model';

export const getAdmin = createFeatureSelector<AdminModuleState>(ADMIN_FEATURE);

export const getCompetitions = createSelector(
    getAdmin,
    admin => admin.competitions
);

export const getOrderedList = createSelector(
    getCompetitions,
    (competitions: Competition[]) => getSortedList(competitions, 'start')
);

const isOutdated = (competition: Competition) => {
  return dayjs().isAfter(dayjs(competition.end));
};

export const getUpcomingList = createSelector(
    getOrderedList,
    (competitions: Competition[]) => competitions.filter((competition: Competition) => !isOutdated(competition))
);

export const getOutdatedList = createSelector(
    getOrderedList,
    (competitions: Competition[]) => competitions.filter((competition: Competition) => isOutdated(competition))
);