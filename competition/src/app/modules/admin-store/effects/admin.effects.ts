import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { adminGetCompetitionListUrl } from '@competition/core/utils';
import { Competition } from '@competition/model';
import {
  AdminActions,
  AdminActionTypes,
  AdminCompetitionsFetchError,
  AdminCompetitionsFetchStart,
  AdminCompetitionsFetchSuccess
} from '../actions';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class AdminEffects {
  public constructor(
      private actions$: Actions<{ type: any; payload: any }>,
      private http: HttpClient
  ) {
  }

  @Effect()
  public fetchCompetitions$: Observable<AdminActions> = this.actions$.pipe(
      ofType(AdminActionTypes.COMPETITIONS_FETCH_START),
      map((action: AdminCompetitionsFetchStart) => action.payload),
      switchMap(() => this.http.get<Competition[]>(adminGetCompetitionListUrl())),
      map((response: Competition[]) => new AdminCompetitionsFetchSuccess(response)),
      catchError((error) => of(new AdminCompetitionsFetchError(error)))
  );
}