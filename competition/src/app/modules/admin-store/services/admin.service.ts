import { Injectable } from '@angular/core';
import { AdminCompetitionsFetchStart } from '../actions';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Competition } from '@competition/model';
import { getCompetitions, getUpcomingList } from '../selectors';

@Injectable()
export class AdminService {
  public competitions$: Observable<Competition[]> = this.store$.select(getCompetitions);
  public upcomingCompetitions$: Observable<Competition[]> = this.store$.select(getUpcomingList);

  constructor(private store$: Store<any>) {
  }

  public dispatchCompetitionsFetchStart(): void {
    this.store$.dispatch(new AdminCompetitionsFetchStart())
  }
}