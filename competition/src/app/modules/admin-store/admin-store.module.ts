import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AdminEffects } from './effects';
import { AdminService } from './services';
import { reducers } from './reducers';
import { ADMIN_FEATURE } from './constants';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(ADMIN_FEATURE, reducers),
    EffectsModule.forFeature([AdminEffects]),
  ],
  declarations: [],
  providers: [
    AdminService
  ],
  exports: []
})
export class AdminStoreModule {
}