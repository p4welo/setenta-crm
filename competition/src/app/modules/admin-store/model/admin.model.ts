import { Competition } from '@competition/model';

export interface AdminModuleState {
  competitions: Competition[];
  details?: {
    [sid: string]: any;
  };
}