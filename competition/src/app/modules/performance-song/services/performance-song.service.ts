import { Injectable } from '@angular/core';
import { getSongSid, getSpeakerId } from '../selectors';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable()
export class PerformanceSongService {
  public playingSongSid$: Observable<string> = this.store$.select(getSongSid);
  public speakerId$: Observable<string> = this.store$.select(getSpeakerId);

  constructor(
      private store$: Store<any>
  ){}
}