import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PerformanceSongService } from '@competition/modules/performance-song/services';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { PERFORMANCE_SONG_FEATURE } from './constants';
import { PerformanceSongSpeakerComponent, PerformanceSongPlayerComponent } from './components';
import { PerformanceSoundEffects } from './effects';
import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PERFORMANCE_SONG_FEATURE, reducers),
    EffectsModule.forFeature([PerformanceSoundEffects])
  ],
  providers: [ PerformanceSongService ],
  declarations: [
      PerformanceSongSpeakerComponent,
      PerformanceSongPlayerComponent
  ],
  exports: [
    PerformanceSongPlayerComponent,
    PerformanceSongSpeakerComponent
  ]
})
export class PerformanceSongModule {

}