import { Component, OnInit } from '@angular/core';
import { InitSpeaker } from '@competition/modules/performance-song/actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-performance-song-speaker',
  templateUrl: './performance-song-speaker.component.html'
})
export class PerformanceSongSpeakerComponent implements OnInit {
  public readonly AUDIO_PLAYER_ID = `audio-player-${Date.now()}`;

  constructor(
      private store$: Store<any>
  ) {}

  public ngOnInit(): void {
    this.store$.dispatch(new InitSpeaker(this.AUDIO_PLAYER_ID))
  }
}