import {
  Component,
  Input
} from '@angular/core';
import { Performance } from '@competition/model';
import { PlaySongStart, StopSongStart } from '@competition/modules/performance-song/actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-performance-song-player',
  templateUrl: './performance-song-player.component.html',
  styleUrls: ['./performance-song-player.component.scss']
})
export class PerformanceSongPlayerComponent {

  @Input() public performance: Performance;
  @Input() public playing: boolean;

  constructor(
      private store$: Store<any>
  ) {
  }

  public toggle(): void {
    if (this.playing) {
      this.store$.dispatch(new StopSongStart())
    } else {
      this.store$.dispatch(new PlaySongStart(this.performance.audioSid));
    }
  }
}