import {
  PerformanceSongActions,
  PerformanceSongActionTypes
} from '../actions';
import { PerformanceSongModuleState } from '../model';

export const initialState: PerformanceSongModuleState = {
  speakerId: undefined,
  songSid: undefined
};

export function reducers(
    state: PerformanceSongModuleState = initialState,
    action: PerformanceSongActions
): PerformanceSongModuleState {
  switch (action.type) {
    case PerformanceSongActionTypes.INIT_SPEAKER:
      return {
        ...state,
        speakerId: action.payload
      };
    case PerformanceSongActionTypes.PLAY_SONG_SUCCESS:
      return {
        ...state,
        songSid: action.payload
      };
    case PerformanceSongActionTypes.STOP_SONG_SUCCESS:
      return {
        ...state,
        songSid: undefined
      };
    default:
      return state;
  }
}