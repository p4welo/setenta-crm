import { PERFORMANCE_SONG_FEATURE } from '@competition/modules/performance-song/constants';
import { PerformanceSongModuleState } from '@competition/modules/performance-song/model';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getPerformanceSong = createFeatureSelector<PerformanceSongModuleState>(PERFORMANCE_SONG_FEATURE);

export const getSongSid = createSelector(
    getPerformanceSong,
    state => state.songSid
);

export const getSpeakerId = createSelector(
    getPerformanceSong,
    state => state.speakerId
);