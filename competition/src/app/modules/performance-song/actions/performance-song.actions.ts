import { Action } from '@ngrx/store';

export const PerformanceSongActionTypes = {
  INIT_SPEAKER: '[Performance Song] Init speaker',
  PLAY_SONG_START: '[Performance Song] Play song start',
  PLAY_SONG_SUCCESS: '[Performance Song] Play song success',
  PLAY_SONG_ERROR: '[Performance Song] Play song error',
  STOP_SONG_START: '[Performance Song] Stop song start',
  STOP_SONG_SUCCESS: '[Performance Song] Stop song success',
  STOP_SONG_ERROR: '[Performance Song] Stop song error'
};

export class PlaySongStart implements Action {
  public type: string = PerformanceSongActionTypes.PLAY_SONG_START;

  constructor(public payload: string) {
  }
}

export class InitSpeaker implements Action {
  public type: string = PerformanceSongActionTypes.INIT_SPEAKER;

  constructor(public payload: string) {
  }
}

export class PlaySongSuccess implements Action {
  public type: string = PerformanceSongActionTypes.PLAY_SONG_SUCCESS;

  constructor(public payload: string) {
  }
}

export class PlaySongError implements Action {
  public type: string = PerformanceSongActionTypes.PLAY_SONG_ERROR;

  constructor(public payload: Error) {
  }
}

export class StopSongStart implements Action {
  public type: string = PerformanceSongActionTypes.STOP_SONG_START;

  constructor(public payload?: any) {
  }
}

export class StopSongSuccess implements Action {
  public type: string = PerformanceSongActionTypes.STOP_SONG_SUCCESS;

  constructor(public payload?: any) {
  }
}

export class StopSongError implements Action {
  public type: string = PerformanceSongActionTypes.STOP_SONG_ERROR;

  constructor(public payload: Error) {
  }
}

export type PerformanceSongActions =
    | InitSpeaker
    | PlaySongStart
    | PlaySongSuccess
    | PlaySongError
    | StopSongStart
    | StopSongSuccess
    | StopSongError
    ;