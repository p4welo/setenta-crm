export interface PerformanceSongModuleState {
  songSid: string | undefined;
  speakerId: string | undefined;
}