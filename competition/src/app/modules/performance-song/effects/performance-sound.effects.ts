import { Injectable } from '@angular/core';
import { getAudioUrl } from '@competition/core/utils';
import {
  PerformanceSongActions,
  PerformanceSongActionTypes, PlaySongError, PlaySongSuccess, StopSongError, StopSongSuccess
} from '@competition/modules/performance-song/actions';
import { PerformanceSongService } from '@competition/modules/performance-song/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class PerformanceSoundEffects {

  public constructor(
      private actions$: Actions<{ type: any; payload: any }>,
      private performanceSongService: PerformanceSongService
  ) {
  }

  @Effect()
  public playMusic$: Observable<PerformanceSongActions> = this.actions$.pipe(
      ofType(PerformanceSongActionTypes.PLAY_SONG_START),
      withLatestFrom(this.performanceSongService.speakerId$),
      map(([action, speakerId]: [PerformanceSongActions, string]) => {
        try {
          const speaker = this.getSpeakerInstance(speakerId);
          speaker.src = getAudioUrl(action.payload);
          speaker.play();
          return new PlaySongSuccess(action.payload)
        }
        catch (error) {
          return new PlaySongError(error);
        }
      })
  );

  @Effect()
  public stopMusic$: Observable<PerformanceSongActions> = this.actions$.pipe(
      ofType(PerformanceSongActionTypes.STOP_SONG_START),
      withLatestFrom(this.performanceSongService.speakerId$),
      map(([action, speakerId]: [PerformanceSongActions, string]) => {
        try {
          const speaker = this.getSpeakerInstance(speakerId);
          speaker.pause();
          speaker.currentTime = 0;
          return new StopSongSuccess(action.payload)
        }
        catch (error) {
          return new StopSongError(error);
        }
      })
  );

  private getSpeakerInstance(id: string): HTMLAudioElement {
    return <HTMLAudioElement>document.getElementById(id);
  }
}