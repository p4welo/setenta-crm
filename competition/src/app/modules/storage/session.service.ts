import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  private DEFAULT_COOKIE_EXPIRY: number = 600; // 10 mins

  public get(key: string): string {
    let value: string = null;
    const storage = this.getStorage();
    if (storage.getItem(key)) {
      value = storage.getItem(key);
    } else {
      document.cookie.split(';').some((item) => {
        const keyPosition = item.indexOf(key);
        if (item.indexOf(key) > -1) {
          // keyPosition can be 0 or 1, +1 for the '=' added after the key.
          value = decodeURIComponent(item.substring(keyPosition + key.length + 1, item.length));
          return true;
        }
        return false;
      });
    }
    return value;
  }

  public set(key: string, value: string, expiresIn?: number): void {
    try {
      this.getStorage().setItem(key, value);
    } catch (e) {
      const now = new Date();
      const expires: number = expiresIn || this.DEFAULT_COOKIE_EXPIRY;
      now.setTime(now.getTime() + (expires * 1000));
      document.cookie = `${key}=${encodeURIComponent(value)}; expires=${expires}; path=/`;
    }
  }

  public remove(key: string): void {
    this.getStorage().removeItem(key);
  }

  private getStorage() {
    return window.sessionStorage;
  }
}