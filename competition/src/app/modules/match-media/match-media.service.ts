import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { distinctUntilChanged, map, publishReplay, refCount, startWith } from 'rxjs/operators';

@Injectable()
export class MatchMediaService {

  private static rules: any = {
    SMALL_PHONE: '(max-width: 479px)',
    BIG_PHONE: '(min-width: 480px) and (max-width: 767px)',
    TABLET: '(min-width: 768px) and (max-width: 1023px)',
    LAPTOP: '(min-width: 1024px) and (max-width: 1199px)',
    DESKTOP: '(min-width: 1200px)'
  };

  public resize$: Observable<Event | string>;

  constructor() {
    this.resize$ = fromEvent(window, 'resize').pipe(
        startWith(this.currentBreakpoint()),
        map(this.currentBreakpoint),
        distinctUntilChanged(),
        publishReplay(1),
        refCount()
    );
  }

  public static is(mediaQuery: string): boolean {
    return window.matchMedia(mediaQuery).matches;
  }

  public static isSmallPhone(): boolean {
    return window.matchMedia(MatchMediaService.rules.SMALL_PHONE).matches;
  }

  public static isBigPhone(): boolean {
    return window.matchMedia(MatchMediaService.rules.BIG_PHONE).matches;
  }

  public static isMobile(): boolean {
    return MatchMediaService.isSmallPhone() || MatchMediaService.isBigPhone();
  }

  public static isTablet(): boolean {
    return window.matchMedia(MatchMediaService.rules.TABLET).matches;
  }

  public static isLaptop(): boolean {
    return window.matchMedia(MatchMediaService.rules.LAPTOP).matches;
  }

  public static isDesktop(): boolean {
    return window.matchMedia(MatchMediaService.rules.DESKTOP).matches;
  }

  private currentBreakpoint(): Event | string {
    const res = {
      SMALL_PHONE: MatchMediaService.isSmallPhone(),
      BIG_PHONE: MatchMediaService.isBigPhone(),
      TABLET: MatchMediaService.isTablet(),
      LAPTOP: MatchMediaService.isLaptop(),
      DESKTOP: MatchMediaService.isDesktop()
    };
    return Object.keys(res).filter(k => res[k])[0];
  }
}
