import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'device' })
export class DevicePipe implements PipeTransform {
  public transform(value: string, deviceType: string): boolean {
    switch (deviceType) {
      case 'MOBILE':
        return value === 'BIG_PHONE' || value === 'SMALL_PHONE';
      case 'BIG_PHONE':
        return value === 'BIG_PHONE';
      case 'SMALL_PHONE':
        return value === 'SMALL_PHONE';
      case 'MOBILE_TABLET':
        return value === 'BIG_PHONE' || value === 'SMALL_PHONE' || value === 'TABLET';
      case 'TABLET':
        return value === 'TABLET';
      case 'LAPTOP':
        return value === 'LAPTOP';
      case 'DESKTOP':
        return value === 'DESKTOP';
      default:
        return false;
    }
  }
}
