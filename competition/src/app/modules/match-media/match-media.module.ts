import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DevicePipe } from './device.pipe';
import { MatchMediaService } from './match-media.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DevicePipe
  ],
  exports: [
    DevicePipe
  ],
  providers: [
    MatchMediaService
  ]
})
export class MatchMediaModule {
}
