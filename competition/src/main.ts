import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { loadCustomScripts } from './initCustomScripts';

import { AppModule } from './app';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

loadCustomScripts(environment);
platformBrowserDynamic().bootstrapModule(AppModule);
