import { googleAnalyticsInit } from './google-analytics';
import { fullStoryInit } from './fullstory';
import { hotjarInit } from './hotjar';
import { initYandex } from './yandex';

export function loadCustomScripts(environment): void {
  if (environment.production) {
    hotjarInit(environment.HOTJAR_ID, environment.HOTJAR_SV);
    fullStoryInit(environment.FULLSTORY_ORG_ID);
    googleAnalyticsInit(environment.GOOGLE_ANALYTICS_TRACKING_ID);
    initYandex(environment.YANDEX_ID);
  }
}

