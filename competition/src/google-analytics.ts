export const googleAnalyticsInit = (gaTrackingId) => {
  const scriptTagName = 'script';
  const scriptElement = document.createElement(scriptTagName);
  scriptElement.async = true;
  scriptElement.src = `https://www.googletagmanager.com/gtag/js?id=${gaTrackingId}`;
  document.body.appendChild(scriptElement);

  window['dataLayer'] = window['dataLayer'] || [];

  function gtag(...args) {
    window['dataLayer'].push(args);
  }

  gtag('js', new Date());
  gtag('config', gaTrackingId);
};